#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "libgraph.h"

int main(int argc, char* argv[])
{
  if (argc != 3) {
    fprintf(stderr, "Usage: %s [meshA meshB]\n", argv[0]);
    return EXIT_FAILURE;
  }
  LibGraph_HybridMesh* hm;
  int nb_components = argc - 1;
  hm = LibGraph_allocHybridMesh(nb_components);
  LibGraph_loadHybridMeshFiles(hm, 1, nb_components, argv[1], argv[2]);
  LibGraph_Graph hg;
  LibGraph_hybridMesh2Graph(hm, &hg);
  LibGraph_checkGraph(&hg, 1);
  printf("Saving txt hybrid mesh in \"output.txt\"\n");
  LibGraph_saveHybridMesh("output.txt", hm);
  printf("Saving vtk hybrid mesh in \"output.vtk\"\n");
  LibGraph_saveVTKHybridMesh("output.vtk", hm);
  LibGraph_freeGraph(&hg);
  LibGraph_freeHybridMesh(hm);
  return EXIT_SUCCESS;
}
