#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "libgraph.h"

int main(int argc, char* argv[])
{
  if (argc != 3 && argc != 1) {
    fprintf(stderr, "Usage: %s [meshA meshB]\n", argv[0]);
    return EXIT_FAILURE;
  }

  LibGraph_Mesh meshA, meshB;

  if (argc == 3) {
    LibGraph_loadMesh(argv[1], &meshA, 0);
    LibGraph_loadMesh(argv[2], &meshB, 0);
  }

  const int size = 4;
  if (argc == 1) {
    LibGraph_generateGrid2D(size, size, 1, 1.0, 1.0, &meshA);
    LibGraph_generateGrid2D(size, size, 1, 1.0, 1.0, &meshB);
  }

  LibGraph_Graph gA, gB;
  LibGraph_mesh2Graph(&meshA, &gA);
  LibGraph_mesh2Graph(&meshB, &gB);

  if (argc == 1) {
    int shift = size * (size - 2);      /* merge 2*size vertices and size+2*(size-1) edges */
    LibGraph_addGraphVertices(&gB, shift, 0, 0); /* add it at start */
    LibGraph_checkGraph(&gB, true);
  }

  if (argc == 1) {
    printf("A: ");
    LibGraph_printGraph(&gA);
    printf("B: ");
    LibGraph_printGraph(&gB);
  }

  LibGraph_Graph new;
  LibGraph_mergeGraphs(&gA, &gB, 1, 1, 1, 1, &new);
  LibGraph_checkGraph(&new, true);

  if (argc == 1) {
    printf("AB: ");
    LibGraph_printGraph(&new);
    int nedges = 2 * size * (size - 1);
    assert((new.narcs / 2) == 2 * nedges - (size + 2 * (size - 1)));
  }

  LibGraph_freeGraph(&new);
  LibGraph_freeGraph(&gA);
  LibGraph_freeMesh(&meshA);
  LibGraph_freeGraph(&gB);
  LibGraph_freeMesh(&meshB);

  return EXIT_SUCCESS;
}
