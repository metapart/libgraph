#ifndef LIBGRAPH_MESH_H
#define LIBGRAPH_MESH_H

#include <stdbool.h>

//@{

/** Element types for meshes */
typedef enum _LibGraph_ElementType
{
  LIBGRAPH_POINT = 1,       /**< point (1 node) */
  LIBGRAPH_LINE = 2,        /**< line (2 nodes) */
  LIBGRAPH_TRIANGLE = 3,    /**< triangle (3 nodes, 1 face) */
  LIBGRAPH_QUADRANGLE = 4,  /**< quadrangle (4 nodes, 1 face) */
  LIBGRAPH_TETRAHEDRON = 5, /**< tetrahedron (4 nodes, 4 faces) */
  LIBGRAPH_HEXAHEDRON = 6,  /**< quadrilaterally-faced hexahedron (8 nodes, 6 faces) */
  LIBGRAPH_PRISM = 7,       /**< triangular prism (6 nodes, 5 faces) */
  LIBGRAPH_NB_ELEMENT_TYPES
} LibGraph_ElementType;

/** Node counts for each element type */
static const int LIBGRAPH_ELEMENT_SIZE[LIBGRAPH_NB_ELEMENT_TYPES] = {
    0, /* unused */
    1, /* point */
    2, /* line */
    3, /* triangle */
    4, /* quad */
    4, /* tetra */
    8, /* hexa */
    6  /* prism */
};

/** Mesh structure. */
typedef struct _LibGraph_Mesh
{
  LibGraph_ElementType elm_type; /**< element type */
  int nb_nodes;         /**< nb nodes */
  int nb_cells;         /**< nb cells */
  double* nodes;        /**< array of node coordinates (of size nb_nodes x 3D) */
  int* cells;           /**< array of node numbers (of size nb_cells x elm_size)  */
  // int nb_node_vars;     /**< nb of variables associated to nodes */
  // int nb_cell_vars;     /**< nb of variables associated to cells */
  // LibGraph_Variable* node_vars;  /**< array of point variable (of size nb_node_vars) */
  // LibGraph_Variable* cell_vars;  /**< array of cell variable (of size nb_cell_vars) */
} LibGraph_Mesh;


/** Allocate a new mesh */
LibGraph_Mesh* LibGraph_newMesh(void);

/** Duplicate a Mesh. */
void LibGraph_dupMesh(LibGraph_Mesh* oldm, LibGraph_Mesh* newm);

/** Compare two meshes, return 1 if equal */
bool LibGraph_compareMesh(LibGraph_Mesh *m1, LibGraph_Mesh *m2);

/** Free memory allocated within mesh structure. */
void LibGraph_freeMesh(LibGraph_Mesh* mesh);

/** Shift mesh from vector (vx,vy,vz). */
void LibGraph_shiftMesh(LibGraph_Mesh* mesh, double vx, double vy, double vz);

/** Scale mesh multiplying coordinates by (sx,sy,sz). */
void LibGraph_scaleMesh(LibGraph_Mesh* mesh, double sx, double sy, double sz);

/** Rotate mesh with (rx,ry,rz) angles (in degree) around the origin. */
void LibGraph_rotateMesh(LibGraph_Mesh* mesh, double rx, double ry, double rz);

/** Compute mesh restriction. */
void LibGraph_subMeshFromSet(const LibGraph_Mesh* m,     /* [in] graph */
                             int ncells,  /* [in] nb cells in set */
                             const int* cells,  /* [in] cell set */
                             LibGraph_Mesh* subm); /* [out] sub-mesh (restricted to the cell set) */

/** simplify a mesh by removing unused nodes */
void LibGraph_simplifyMesh(LibGraph_Mesh* meshin, LibGraph_Mesh* meshout);

/** remove mesh connectivity (point mesh) */
void LibGraph_removeMeshConnectivity(LibGraph_Mesh* oldm, LibGraph_Mesh* newm);

/** Print mesh struture. */
void LibGraph_printMesh(LibGraph_Mesh* mesh);

//@}

#endif
