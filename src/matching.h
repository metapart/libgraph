#ifndef LIBGRAPH_MATCHING_H
#define LIBGRAPH_MATCHING_H

#include "graph.h"

//@{

/** Compute a  Minimum- or Maximum- weight bipartite graph matching (exact solution) */
/** \return a permutation of size nparts */
int* LibGraph_bipartiteGraphMatching(LibGraph_Graph* g, int nparts, int maximum);

//@}

#endif
