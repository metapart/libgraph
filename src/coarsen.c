#include <assert.h>
#include <limits.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "coarsen.h"
#include "debug.h"
#include "graph.h"
#include "mesh.h"
#include "misc.h"
#include "tools.h"

#define UNMATCHED -1

/* *********************************************************** */

int LibGraph_match2cmap(int nvtxs, int* match, int* cmap)  // inout: match, outputs: cmap (ordered)
{
  for (int i = 0; i < nvtxs; i++) cmap[i] = -1;

  int ck = 0;
  for (int i = 0; i < nvtxs; i++) {
    int ii = match[i];
    if (i == ii || ii == UNMATCHED) { /* unmatched case */
      cmap[i] = ck++;
    }
    else if (i < ii) {
      cmap[i] = cmap[ii] = ck++;
    }
  }

  return ck;  // nb coarsened vertices
}

/* *********************************************************** */

static void createCoarseGraph(const LibGraph_Graph* g, int cnvtxs, int* cmap, int* match, LibGraph_Graph* cg)  // outputs: cg
{
  assert(g && cmap && match && cg);

  /* Warning: cmap must be ordered !!! */

  /* initialize coarser graph */
  cg->nvtxs = cnvtxs;
  int maxarcs = g->narcs;
  cg->narcs = 0;
  cg->xadj = malloc((cg->nvtxs + 1) * sizeof(int));
  cg->adjncy = malloc(maxarcs * sizeof(int)); /* max */
  cg->vwgts = malloc(cnvtxs * sizeof(int));
  cg->ewgts = malloc(maxarcs * sizeof(int)); /* max */

  PRINTADV("- cnvtxs: %d\n", cnvtxs);

  PRINTADV("- cmap: [");
  for (int i = 0; i < g->nvtxs; i++) PRINTADV("%d ", cmap[i]);
  PRINTADV("]\n");

  PRINTADV("- match: [");
  for (int i = 0; i < g->nvtxs; i++) PRINTADV("%d ", match[i]);
  PRINTADV("]\n");

  /* mached edge: (i, match[i]) */
  /* k = cmap[i] = cmap[match[i]] = super-vertex in coarser graph */
  int ck = 0;
  int z = 0;

  /* cedgemap: if vertex k is connected with kk in coarse graph, cg->adjncy[cedgemap[k]] = kk  */
  int* cedgemap = malloc(cnvtxs * sizeof(int));
  for (int x = 0; x < cnvtxs; x++) cedgemap[x] = -1;

  for (int i = 0; i < g->nvtxs; i++) {
    /* vertex ii matching with vertex jj */
    // int ii = srt[i];
    int ii = i;
    int jj = match[ii];
    assert(jj != UNMATCHED);

    /* coarse vertex k */
    int k = cmap[ii];
    int kk = cmap[jj];
    assert(k == kk);

    /* MATCHED VERTEX */
    if (ii <= jj) {
      assert(k == ck);  // => check if camp is ordered!!!
      ck++;

      /* new vertex k merging old vertices ii and jj */
      PRINTADV("coarse vtx %d: merging %d and %d\n", k, ii, jj);
      cg->xadj[k] = z;

      /* vertex weight */
      if (ii == jj)
        cg->vwgts[k] = g->vwgts ? g->vwgts[ii] : 1;
      else {
        if (g->vwgts)
          cg->vwgts[k] = g->vwgts[ii] + g->vwgts[jj];
        else
          cg->vwgts[k] = 2;
      }

      /* reset cedgemap */
      for (int x = g->xadj[ii]; x < g->xadj[ii + 1]; x++) {
        int iii = g->adjncy[x]; /* edge (ii, iii) */
        int ciii = cmap[iii];
        cedgemap[ciii] = -1;
      }
      for (int x = g->xadj[jj]; x < g->xadj[jj + 1]; x++) {
        int jjj = g->adjncy[x]; /* edge (jj, jjj) */
        int cjjj = cmap[jjj];
        cedgemap[cjjj] = -1;
      }

      /* add new edges that start from endpoint ii */
      for (int x = g->xadj[ii]; x < g->xadj[ii + 1]; x++) {
        int iii = g->adjncy[x];  /* edge (ii, iii) */
        if (iii == jj) continue; /* skip the matched edge */
        int ciii = cmap[iii];
        /* add new coarse edge */
        if (cedgemap[ciii] == -1) {
          cg->adjncy[z] = ciii;
          cg->ewgts[z] = g->ewgts ? g->ewgts[x] : 1;
          cedgemap[ciii] = z;
          z++;
        }
        /* merge with another coarse edge */
        else {
          int zz = cedgemap[ciii];
          assert(zz >= 0);
          cg->ewgts[zz] += g->ewgts ? g->ewgts[x] : 1;
        }
      }

      if (ii == jj) continue; /* UNMATCHED */

      /* add new edges that start from endpoint jj */
      for (int x = g->xadj[jj]; x < g->xadj[jj + 1]; x++) {
        int jjj = g->adjncy[x];  /* edge (jj, jjj) */
        if (jjj == ii) continue; /* skip the matched edge */
        int cjjj = cmap[jjj];
        /* add new coarse edge */
        if (cedgemap[cjjj] == -1) {
          cg->adjncy[z] = cjjj;
          cg->ewgts[z] = g->ewgts ? g->ewgts[x] : 1;
          cedgemap[cjjj] = z;
          z++;
        }
        /* merge with another coarse edge */
        else {
          int zz = cedgemap[cjjj];
          assert(zz >= 0);
          cg->ewgts[zz] += g->ewgts ? g->ewgts[x] : 1;
        }
      }

    } /* end of if(ii <= jj) */

  } /* end of for() */

  assert(ck == cnvtxs);
  cg->xadj[ck] = z;
  cg->narcs = z;

// check vertex and edge weights of the coarsest graph
#ifdef DEBUG
  int cvwgt = 0, cewgt = 0;
  for (int i = 0; i < cg->nvtxs; i++) {
    cvwgt += cg->vwgts ? cg->vwgts[i] : 1;
    for (int j = cg->xadj[i]; j < cg->xadj[i + 1]; j++) {
      // int ii = cg->adjncy[j]; /* edge (i,ii) */
      cewgt += cg->ewgts ? cg->ewgts[j] : 1;
    }
  }

  int vwgt = 0, ewgt = 0, mergedewgt = 0;
  for (int i = 0; i < g->nvtxs; i++) {
    vwgt += g->vwgts ? g->vwgts[i] : 1;
    for (int j = g->xadj[i]; j < g->xadj[i + 1]; j++) {
      int ii = g->adjncy[j]; /* edge (i,ii) */
      ewgt += g->ewgts ? g->ewgts[j] : 1;
      if (match[i] == ii) mergedewgt += g->ewgts ? g->ewgts[j] : 1;
    }
  }

  assert(cvwgt == vwgt);
  assert(cewgt == ewgt - mergedewgt);
  PRINT("coarsening: edge compression = %.2f%% (original ew = %d, coarsen ew = %d)\n", (ewgt - cewgt) * 100.0 / ewgt,
        ewgt, cewgt);
#endif

  /* free mem */
  cg->adjncy = realloc(cg->adjncy, cg->narcs * sizeof(int));
  cg->ewgts = realloc(cg->ewgts, cg->narcs * sizeof(int));
  free(cedgemap);
}

/* *********************************************************** */

static int matchEdges_RM(const LibGraph_Graph* g, int* match, const int* partfixed)
{
  // max coarser vertex weight
  int maxvwgt = 0;
  for (int i = 0; i < g->nvtxs; i++) maxvwgt += g->vwgts ? g->vwgts[i] : 1;
  maxvwgt = ceil(maxvwgt * 2.0 / g->nvtxs);

  for (int i = 0; i < g->nvtxs; i++) match[i] = UNMATCHED;
  int* perm = malloc(g->nvtxs * sizeof(int));
  LibGraph_randomPermute(g->nvtxs, perm);

  int cnvtxs = 0;

  /* find a heavy-edge matching, subject to different constraints */
  for (int ii = 0; ii < g->nvtxs; ii++) {
    int i = perm[ii];

    if (match[i] == UNMATCHED) {
      int thevtx = i; /* default, matching with itself */

      // RM
      for (int j = g->xadj[i]; j < g->xadj[i + 1]; j++) {
        int k = g->adjncy[j]; /* edge (i,k) */
        if (match[k] == UNMATCHED) {
          /* dont match fixed vertices assigned to different parts */
          if (partfixed && partfixed[i] >= 0 && partfixed[k] >= 0 && partfixed[i] != partfixed[k]) continue;
          thevtx = k;
          break;
        }
      }

      // cmap[i] = cmap[thevtx] = cnvtxs++;
      match[i] = thevtx;
      match[thevtx] = i;
      cnvtxs++;
    }
  }

  /* free */
  free(perm);

  return cnvtxs;
}

/* *********************************************************** */

static int matchEdges_RMVW(const LibGraph_Graph* g, int* match, const int* partfixed)
{
  // max coarser vertex weight
  int maxvwgt = 0;
  for (int i = 0; i < g->nvtxs; i++) maxvwgt += g->vwgts ? g->vwgts[i] : 1;
  maxvwgt = ceil(maxvwgt * 2.0 / g->nvtxs);  // check this rule... ????

  for (int i = 0; i < g->nvtxs; i++) match[i] = UNMATCHED;
  int* perm = malloc(g->nvtxs * sizeof(int));
  LibGraph_randomPermute(g->nvtxs, perm);

  int cnvtxs = 0;

  /* find a heavy-edge matching, subject to different constraints */
  for (int ii = 0; ii < g->nvtxs; ii++) {
    int i = perm[ii];

    if (match[i] == UNMATCHED) {
      int thevtx = i; /* default, matching with itself */

      // RMVW
      for (int j = g->xadj[i]; j < g->xadj[i + 1]; j++) {
        int k = g->adjncy[j]; /* edge (i,k) */
        int vw = g->vwgts ? (g->vwgts[i] + g->vwgts[k]) : 2;
        if (match[k] == UNMATCHED && vw <= maxvwgt) {
          /* dont match fixed vertices assigned to different parts */
          if (partfixed && partfixed[i] >= 0 && partfixed[k] >= 0 && partfixed[i] != partfixed[k]) continue;
          thevtx = k;
          break;
        }
      }

      // cmap[i] = cmap[thevtx] = cnvtxs++;
      match[i] = thevtx;
      match[thevtx] = i;
      cnvtxs++;
    }
  }

  /* free */
  free(perm);

  return cnvtxs;
}

/* *********************************************************** */

/* classic heavy edge matching heuristic */
static int matchEdges_HEM(const LibGraph_Graph* g, int* match, const int* partfixed)
{
  for (int i = 0; i < g->nvtxs; i++) match[i] = UNMATCHED;
  int* perm = malloc(g->nvtxs * sizeof(int));
  LibGraph_randomPermute(g->nvtxs, perm);

  int cnvtxs = 0;

  /* find a heavy-edge matching, subject to different constraints */
  for (int ii = 0; ii < g->nvtxs; ii++) {
    int i = perm[ii];

    if (match[i] == UNMATCHED) {
      int thevtx = i; /* default, matching with itself */

      // HEM
      int maxewgt = 0;
      for (int j = g->xadj[i]; j < g->xadj[i + 1]; j++) {
        int k = g->adjncy[j]; /* edge (i,k) */
        int ew = g->ewgts ? g->ewgts[j] : 1;
        if (match[k] == UNMATCHED && maxewgt < ew) {
          /* dont match fixed vertices assigned to different parts */
          if (partfixed && partfixed[i] > -1 && partfixed[k] > -1 && partfixed[i] != partfixed[k]) continue;
          maxewgt = ew;
          thevtx = k;
        }
      }

      // cmap[i] = cmap[thevtx] = cnvtxs++;
      match[i] = thevtx;
      match[thevtx] = i;
      cnvtxs++;
    }
  }

  /* free */
  free(perm);

  return cnvtxs;
}

/* *********************************************************** */

/* heavy edge matching heuristic, that enforces matching of islands (vtx of degree 1) */
static int matchEdges_HEM2(const LibGraph_Graph* g, int* match, const int* partfixed)
{
  for (int i = 0; i < g->nvtxs; i++) match[i] = UNMATCHED;
  int* perm = malloc(g->nvtxs * sizeof(int));
  LibGraph_randomPermute(g->nvtxs, perm);

  int cnvtxs = 0;

  // matching of islands (optimization)
  for (int k = 0; k < g->nvtxs; k++) {
    int i = perm[k];
    int degree = g->xadj[i + 1] - g->xadj[i];
    if (degree == 1) {
      int j = g->xadj[i];
      int ii = g->adjncy[j]; /* edge (i,ii) */
      if (match[i] == UNMATCHED && match[ii] == UNMATCHED) {
        if (!partfixed) {
          match[ii] = i;
          match[i] = ii;
          cnvtxs++;
        }
        else if ((partfixed[i] == -1) || (partfixed[i] == partfixed[ii])) {
          match[ii] = i;
          match[i] = ii;
          cnvtxs++;
        }
      }
    }
  }

  PRINT("=> remove %d islands in HEM matching\n", cnvtxs);

  /* find a heavy-edge matching, subject to different constraints */
  for (int ii = 0; ii < g->nvtxs; ii++) {
    int i = perm[ii];

    if (match[i] == UNMATCHED) {
      int thevtx = i; /* default, matching with itself */

      // HEM
      int maxewgt = 0;
      for (int j = g->xadj[i]; j < g->xadj[i + 1]; j++) {
        int k = g->adjncy[j]; /* edge (i,k) */
        int ew = g->ewgts ? g->ewgts[j] : 1;
        if (match[k] == UNMATCHED && maxewgt < ew) {
          /* dont match fixed vertices assigned to different parts */
          if (partfixed && partfixed[i] > -1 && partfixed[k] > -1 && partfixed[i] != partfixed[k]) continue;
          maxewgt = ew;
          thevtx = k;
        }
      }

      // cmap[i] = cmap[thevtx] = cnvtxs++;
      match[i] = thevtx;
      match[thevtx] = i;
      cnvtxs++;
    }
  }

  /* free */
  free(perm);

  return cnvtxs;
}

/* *********************************************************** */

/* alternate heavy/light edge matching heuristic */
static int matchEdges_AEM(const LibGraph_Graph* g, int* match, const int* partfixed)
{
  for (int i = 0; i < g->nvtxs; i++) match[i] = UNMATCHED;
  int* perm = malloc(g->nvtxs * sizeof(int));
  LibGraph_randomPermute(g->nvtxs, perm);

  int cnvtxs = 0;

  /* find a heavy-edge matching, subject to different constraints */
  for (int ii = 0; ii < g->nvtxs; ii++) {
    int i = perm[ii];

    if (match[i] == UNMATCHED) {
      int thevtx = i; /* default, matching with itself */

      // HEM
      int maxewgt = 0;
      int minewgt = 0;
      for (int j = g->xadj[i]; j < g->xadj[i + 1]; j++) {
        int k = g->adjncy[j]; /* edge (i,k) */
        /* select alternately light/heavy edge */
        int ew = g->ewgts ? g->ewgts[j] : 1;
        if (match[k] == UNMATCHED) {
          /* dont match fixed vertices assigned to different parts */
          if (partfixed && partfixed[i] > -1 && partfixed[k] > -1 && partfixed[i] != partfixed[k]) continue;
          /* heavy edge matching */
          if (cnvtxs % 2 == 0 && maxewgt < ew) {
            maxewgt = ew;
            thevtx = k;
          }
          /* light edge matching */
          else if (cnvtxs % 2 == 1 && minewgt > ew) {
            minewgt = ew;
            thevtx = k;
          }
        }
      }
      /* match this edge */
      match[i] = thevtx;
      match[thevtx] = i;
      cnvtxs++;
    }
  }

  /* free */
  free(perm);

  return cnvtxs;
}

/* *********************************************************** */

/* MHEM: Modified HEM according to "Multilevel k-way Partitioning Scheme for Iregular Graphs" by
 * Karypis and Kumar, JPDC'98. */
/* try to reduce degree of coarsen vtx during matching...*/

/*
 * match i and k
 *                                        *
 *    --- z                     z         *
 *   /    |                    /          *
 *  i ---k---z''    =>   (i-k) --- z''    *
 *   \    |                    \          *
 *    ----z'                    z'        *
 *                                        *
 *
 *  In HEM, ew = w(i,k)
 *  In MHEM, ew = w(i,k) and weight of merged edges W(i,k) = w(i,z) + w(k,z) + w(i,z') + w(k,z')
 */

static int matchEdges_MHEM(const LibGraph_Graph* g, int* match, const int* partfixed)
{
  int cnt = 0;
  int cmpmergedew = 0;

  for (int i = 0; i < g->nvtxs; i++) match[i] = UNMATCHED;
  int* perm = malloc(g->nvtxs * sizeof(int));
  LibGraph_randomPermute(g->nvtxs, perm);

  int cnvtxs = 0;

  /* find a heavy-edge matching, subject to different constraints */
  for (int ii = 0; ii < g->nvtxs; ii++) {
    int i = perm[ii];

    if (match[i] == UNMATCHED) {
      int thevtx = i; /* default, matching with itself */

      // HEM
      int maxew = 0;
      int maxmergedew = 0;
      for (int j = g->xadj[i]; j < g->xadj[i + 1]; j++) {
        int k = g->adjncy[j]; /* edge (i,k) */
        int ew = g->ewgts ? g->ewgts[j] : 1;

        if (match[k] == UNMATCHED && ew >= maxew) {
          /* dont match fixed vertices assigned to different parts */
          if (partfixed && partfixed[i] > -1 && partfixed[k] > -1 && partfixed[i] != partfixed[k]) continue;

          /* find weight of merged edges when matching (i,k) */
          int mergedew = 0;
          for (int jj = g->xadj[k]; jj < g->xadj[k + 1]; jj++) {
            int z = g->adjncy[jj]; /* edge (k,z) */
            if (z != i) {
              for (int jjj = g->xadj[i]; jjj < g->xadj[i + 1]; jjj++) {
                int zz = g->adjncy[jjj]; /* edge (i,zz) */
                if (z == zz) {           /* edges (k,z) and (i,z) will be merged  */
                  // weight of edge (k,z)
                  int ew1 = g->ewgts ? g->ewgts[jj] : 1;
                  // weight of edge (i,z) not used in original article, but I prefer to add it for
                  // symmetrical purpose...
                  int ew2 = g->ewgts ? g->ewgts[jjj] : 1;
                  mergedew += (ew1 + ew2);
                }
                cnt++;
              }
            }
          }

          /* find best matching... */
          if ((ew > maxew) || (ew == maxew && mergedew > maxmergedew)) {
            if (ew == maxew) cmpmergedew++;
            maxew = ew;
            thevtx = k;
            maxmergedew = mergedew;
          }
        }
      }

      // cmap[i] = cmap[thevtx] = cnvtxs++;
      match[i] = thevtx;
      match[thevtx] = i;
      cnvtxs++;
    }
  }

  // debug
  PRINT("MHEM complexity = %.2f of |E|=%d (useful %d times)\n", cnt * 1.0 / g->nedges, g->nedges, cmpmergedew);

  /* free */
  free(perm);

  return cnvtxs;
}

/* *********************************************************** */

/*
 * Metis 4.0 -- This macro converts a length array in a CSR one
 */

#define MAKECSR(i, n, a)                                                                                               \
  do {                                                                                                                 \
    for (i = 1; i < n; i++) a[i] += a[i - 1];                                                                          \
    for (i = n; i > 0; i--) a[i] = a[i - 1];                                                                           \
    a[0] = 0;                                                                                                          \
  } while (0)

/*
 * Metis 4.0 -- This function uses simple counting sort to return a
 * permutation array corresponding to the sorted order. The keys are
 * assumed to start from 0 and they are positive.  This sorting is
 * used during matching.
 */

static void Metis_BucketSortKeysInc(int n, int max, int* keys, int* tperm, int* perm)
{
  int i, ii;

  int* counts = calloc(max + 2, sizeof(int));
  for (i = 0; i < n; i++) counts[keys[i]]++;
  MAKECSR(i, max + 1, counts);

  for (ii = 0; ii < n; ii++) {
    i = tperm[ii];
    perm[counts[keys[i]]++] = i;
  }

  free(counts);
}

// expansion2 edge rating for edge (u,v)
#define EDGERATING_EXP2(ew, uw, vw) ((ew * ew) / (uw * vw))

// plain edge weight (worse edge rating function)
#define EDGERATING_PLAIN(ew, uw, vw) (ew)

/* *********************************************************** */

static int matchEdges_SHEM(const LibGraph_Graph* g, int* match, const int* partfixed)
{
  // max coarser vertex weight
  int maxvwgt = 0;
  for (int i = 0; i < g->nvtxs; i++) maxvwgt += g->vwgts ? g->vwgts[i] : 1;
  maxvwgt = ceil(maxvwgt * 2.0 / g->nvtxs);

  for (int i = 0; i < g->nvtxs; i++) match[i] = UNMATCHED;

  /* compute degrees and average degree */
  int* degrees = malloc(g->nvtxs * sizeof(int));
  int avgdegree = 0.7 * (g->xadj[g->nvtxs] / g->nvtxs);
  for (int i = 0; i < g->nvtxs; i++)
    degrees[i] = ((g->xadj[i + 1] - g->xadj[i] > avgdegree) ? avgdegree : g->xadj[i + 1] - g->xadj[i]);

  int* tperm = malloc(g->nvtxs * sizeof(int));
  LibGraph_randomPermute(g->nvtxs, tperm);

  /* nodes are sorted by increasing degree... */
  int* perm = malloc(g->nvtxs * sizeof(int));
  Metis_BucketSortKeysInc(g->nvtxs, avgdegree, degrees, tperm, perm);

  int cnvtxs = 0;

  /* find a heavy-edge matching, subject to different constraints */
  for (int ii = 0; ii < g->nvtxs; ii++) {
    int i = perm[ii];

    if (match[i] == UNMATCHED) {
      int thevtx = i; /* default, matching with itself */

      assert(degrees[i] > 0);

      /* Take care any islands (degrees[i] = 0). Islands should be matched with non-islands due to
       * coarsening! */
      /* if (degrees[i] == 0) { */
      /* 	for (int jj = g->nvtxs-1 ; jj > i; jj--) { */
      /* 	  int j = perm[jj]; */
      /* 	  if (match[k] == UNMATCHED && xadj[k] < xadj[k+1]) { */
      /* 	    maxidx = k; */
      /* 	    break; */
      /* 	  } */
      /* 	} */
      /* } */

      // HEM
      float maxewgt = -1.0f;
      for (int j = g->xadj[i]; j < g->xadj[i + 1]; j++) {
        int k = g->adjncy[j]; /* edge (i,k) */
        int ew = g->ewgts ? g->ewgts[j] : 1;
        int iw = g->vwgts ? g->vwgts[i] : 1;
        int kw = g->vwgts ? g->vwgts[k] : 1;
        assert(iw > 0 && kw > 0);
        int newvw = iw + kw;
        float ewrating = EDGERATING_EXP2(ew, iw, kw);
        // float ewrating = EDGERATING_PLAIN(ew, iw, kw);
        assert(ewrating >= 0.0f);

        if (match[k] == UNMATCHED && maxewgt < ewrating && newvw <= maxvwgt) {
          /* dont match fixed vertices assigned to different parts */
          if (partfixed && partfixed[i] >= 0 && partfixed[k] >= 0 && partfixed[i] != partfixed[k]) continue;
          maxewgt = ewrating;
          thevtx = k;
        }
      }

      // cmap[i] = cmap[thevtx] = cnvtxs++;
      match[i] = thevtx;
      match[thevtx] = i;
      cnvtxs++;
    }
  }

  /* free */
  free(perm);
  free(tperm);
  free(degrees);

  return cnvtxs;
}

/* *********************************************************** */

int LibGraph_matchEdges(const LibGraph_Graph* g, int ctype, int* match, const int* partfixed)
{
  switch (ctype) {
    // RM (random, no vertex weight)
    case LIBGRAPH_MATCH_RM: return matchEdges_RM(g, match, partfixed); break;

    // RMVW (random, vertex weight)
    case LIBGRAPH_MATCH_RMVW: return matchEdges_RMVW(g, match, partfixed); break;

    // HEM (classic Heavy Edge Matching)
    case LIBGRAPH_MATCH_HEM: return matchEdges_HEM(g, match, partfixed); break;

    // HEM2 (heavy edge matching, modified to match first vertices of degree 1)
    case LIBGRAPH_MATCH_HEM2: return matchEdges_HEM2(g, match, partfixed); break;

    // AEM (alternate heavy & light edge matching)
    case LIBGRAPH_MATCH_AEM: return matchEdges_AEM(g, match, partfixed); break;

    // MHEM (Modified Heavy Edge Matching)
    case LIBGRAPH_MATCH_MHEM: return matchEdges_MHEM(g, match, partfixed); break;

    // SHEM (Sorted Heavy Edge Matching)
    case LIBGRAPH_MATCH_SHEM: return matchEdges_SHEM(g, match, partfixed); break;

    default: assert(0);
  }

  return -1;
}

/* *********************************************************** */

void LibGraph_coarsenGraph(const LibGraph_Graph* g, int ctype, LibGraph_Graph* cg, int* cmap, const int* partfixed)
{
  assert(g && cg && cmap);

  int* match = malloc(g->nvtxs * sizeof(int));

  // perform edge-matching
  int cnvtxs = LibGraph_matchEdges(g, ctype, match, partfixed);  // outputs: match

  // create ordered mapping
  int cnvtxs2 = LibGraph_match2cmap(g->nvtxs, match, cmap);  // outputs: cmap
  assert(cnvtxs == cnvtxs2);

  PRINT("coarsening ratio %.2f (vtxs %d)\n", cnvtxs * 1.0 / g->nvtxs, cnvtxs);

  // create the coarse graph
  createCoarseGraph(g, cnvtxs, cmap, match, cg);

/* check vertex weight */
#ifdef DEBUG
  int vw = 0, cvw = 0;
  for (int i = 0; i < g->nvtxs; i++) vw += g->vwgts ? g->vwgts[i] : 1;
  for (int i = 0; i < cg->nvtxs; i++) cvw += cg->vwgts ? cg->vwgts[i] : 1;
  assert(vw == cvw);

  /* int ndeg1 = 0; */
  /* for(int i = 0 ; i < cg->nvtxs ; i++) { */
  /*   int degree = cg->xadj[i+1] - cg->xadj[i]; */
  /*   if(degree == 1) ndeg1++; */
  /* } */
  /* PRINT("nb vtxs of degree 1 in coarsest graph: %d\n", ndeg1); */

#endif

  // free mem
  free(match);
}

/* *********************************************************** */

int LibGraph_coarsenGraphMultilevel(LibGraph_Graph* g, int ctype, int maxcnvtxs, LibGraph_Graph* cg,
                                    int* cmap /* array of size g->nvtxs */
)
{
  assert(g && cg && cmap);

  int nblevels = 0;
  LibGraph_Graph current;
  LibGraph_dupGraph(g, &current);

  int* cmaptmp = malloc(g->nvtxs * sizeof(int));
  for (int i = 0; i < g->nvtxs; i++) cmap[i] = i;

  while (current.nvtxs > maxcnvtxs) {
    LibGraph_coarsenGraph(&current, ctype, cg, cmaptmp, NULL); /* cmaptmp of size current->nvtxs */
    for (int i = 0; i < g->nvtxs; i++) {
      assert(cmap[i] < current.nvtxs);
      cmap[i] = cmaptmp[cmap[i]];
      assert(cmap[i] < cg->nvtxs);
    }
    LibGraph_freeGraph(&current);
    LibGraph_dupGraph(cg, &current);
    nblevels++;
  }

  LibGraph_freeGraph(&current);

#ifdef DEBUG
  int nbislands = computeNumberOfIslands(g, cg->nvtxs, cmap);
  PRINT("nb islands in edge matching: %d\n", nbislands);
#endif

  return nblevels;
}

/* *********************************************************** */

int LibGraph_coarsenMeshMultilevel(LibGraph_Mesh* m, int ctype, int maxcnvtxs, LibGraph_Mesh* cm, LibGraph_Graph* cg,
                                   int* cmap /* array of size m->nb_cells */
)
{
  assert(m && cm && cmap);
  assert(maxcnvtxs > 0);
  assert(m->elm_type != LIBGRAPH_LINE);

  // 1) convert mesh to graph
  LibGraph_Graph g;
  LibGraph_mesh2Graph(m, &g);
  assert(m->nb_cells == g.nvtxs);
  assert(LibGraph_checkGraph(&g, 0));

  // printGraph(&g);

  // 2) coarsen graph
  int nblevels = LibGraph_coarsenGraphMultilevel(&g, ctype, maxcnvtxs, cg, cmap);

  PRINT("coarsen graph: type=%d, nblevels=%d, nvtxs=%d, cnvtxs=%d (max=%d)\n", ctype, nblevels, g.nvtxs, cg->nvtxs,
        maxcnvtxs);

  assert(LibGraph_checkGraph(&g, 0));
  assert(LibGraph_checkGraph(cg, 0));

  // printGraph(cg);

  // 3) compute coords of coarsen cells
  LibGraph_coarsenGraph2Mesh(m, cg, cmap, cm);

  LibGraph_freeGraph(&g);

  return nblevels;
}

/* *********************************************************** */

int LibGraph_coarsenLineMeshMultilevel(LibGraph_Mesh* m, int ctype, int maxcnvtxs, LibGraph_Mesh* cm,
                                       LibGraph_Graph* cg, int* cmap /* array of size m->nb_nodes */
)
{
  assert(m && cm && cmap);
  assert(maxcnvtxs > 0);
  assert(m->elm_type == LIBGRAPH_LINE);

  // 1) convert mesh to graph
  LibGraph_Graph g;
  LibGraph_lineMesh2Graph(m, &g);
  assert(m->nb_nodes == g.nvtxs);
  assert(LibGraph_checkGraph(&g, 0));

  // printGraph(&g);

  // 2) coarsen graph
  int nblevels = LibGraph_coarsenGraphMultilevel(&g, ctype, maxcnvtxs, cg, cmap);

  PRINT("coarsen graph: type=%d, nblevels=%d, nvtxs=%d, cnvtxs=%d (max=%d)\n", ctype, nblevels, g.nvtxs, cg->nvtxs,
        maxcnvtxs);

  assert(LibGraph_checkGraph(&g, 0));
  assert(LibGraph_checkGraph(cg, 0));

  // printGraph(cg);

  // 3) compute coords of coarsen cells
  LibGraph_coarsenGraph2Mesh(m, cg, cmap, cm);

  LibGraph_freeGraph(&g);

  return nblevels;
}

/* *********************************************************** */

void LibGraph_coarsenGraph2Mesh(
    const LibGraph_Mesh* m,   /**< [in] mesh */
    const LibGraph_Graph* cg, /**< [in] coarser graph */
    const int* cmap,          /**< [in] mapping from vertex to coarser vertex (array of size g->nvtxs) */
    LibGraph_Mesh* cm         /**< [out] coarser mesh */
)
{
  /* *********** regular mesh *********** */

  if (m->elm_type != LIBGRAPH_LINE) {
    int nvtxs = m->nb_cells;
    double* coords = malloc(nvtxs * 3 * sizeof(double));
    assert(coords);
    for (int i = 0; i < 3 * nvtxs; i++) coords[i] = 0.0;
    double* ccoords = malloc(cg->nvtxs * 3 * sizeof(double));
    assert(ccoords);
    for (int i = 0; i < 3 * cg->nvtxs; i++) ccoords[i] = 0.0;
    int* nccoords = malloc(cg->nvtxs * sizeof(int));
    assert(nccoords);
    for (int i = 0; i < cg->nvtxs; i++) nccoords[i] = 0;

    LibGraph_cellCenters(m, coords);

    for (int i = 0; i < nvtxs; i++) {
      int ci = cmap[i];
      assert(ci >= 0 && ci < cg->nvtxs);
      ccoords[ci * 3 + 0] += coords[i * 3 + 0];
      ccoords[ci * 3 + 1] += coords[i * 3 + 1];
      ccoords[ci * 3 + 2] += coords[i * 3 + 2];
      nccoords[ci]++;
      // printf("vtx %d: %.2f %.2f %.2f\n", i, coords[i*3+0], coords[i*3+1], coords[i*3+2]);
    }
    for (int ci = 0; ci < cg->nvtxs; ci++) {
      ccoords[ci * 3 + 0] /= nccoords[ci];
      ccoords[ci * 3 + 1] /= nccoords[ci];
      ccoords[ci * 3 + 2] /= nccoords[ci];
      // printf("cvtx %d: %.2f %.2f %.2f\n", ci, ccoords[ci*3+0], ccoords[ci*3+1], ccoords[ci*3+2]);
    }

    // 4) compute coarsen "line" mesh...
    LibGraph_graph2LineMesh(cg, ccoords, cm);

    // printMesh(cm); // debug

    free(coords);
    free(ccoords);
    free(nccoords);
  }

  /* *********** line mesh *********** */

  else {
    int nvtxs = m->nb_nodes;

    // compute coords of coarsen cells
    double* ccoords = malloc(cg->nvtxs * 3 * sizeof(double));
    assert(ccoords);
    for (int i = 0; i < 3 * cg->nvtxs; i++) ccoords[i] = 0.0;
    int* nccoords = malloc(cg->nvtxs * sizeof(int));
    assert(nccoords);
    for (int i = 0; i < cg->nvtxs; i++) nccoords[i] = 0;

    for (int i = 0; i < nvtxs; i++) {
      int ci = cmap[i];
      assert(ci >= 0 && ci < cg->nvtxs);
      ccoords[ci * 3 + 0] += m->nodes[i * 3 + 0];
      ccoords[ci * 3 + 1] += m->nodes[i * 3 + 1];
      ccoords[ci * 3 + 2] += m->nodes[i * 3 + 2];
      nccoords[ci]++;
    }
    for (int ci = 0; ci < cg->nvtxs; ci++) {
      ccoords[ci * 3 + 0] /= nccoords[ci];
      ccoords[ci * 3 + 1] /= nccoords[ci];
      ccoords[ci * 3 + 2] /= nccoords[ci];
    }

    // compute coarsen "line" mesh...
    LibGraph_graph2LineMesh(cg, ccoords, cm);

    // printMesh(cm); // debug

    free(ccoords);
    free(nccoords);
  }
}

/* *********************************************************** */
