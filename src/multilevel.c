#include <assert.h>
#include <limits.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

// #define DEBUG
// #define DEBUGADV

#include "bfs.h"
#include "coarsen.h"
#include "debug.h"
#include "diag.h"
#include "graph.h"
#include "multilevel.h"

#define EPSILON 1e-6

/* *********************************************************** */

struct ML
{
  int level;
  LibGraph_Graph* graph;
  int* cmap;      /* array of size graph->nvtxs */
  int* part;      /* array of size graph->nvtxs */
  int* partfixed; /* array of size graph->nvtxs (or NULL if useless) */
  int edgecut;
  double ub;
  struct ML* coarser;
  struct ML* finer;
};

/* *********************************************************** */

void LibGraph_partitionGraphMultiLevel(
    const LibGraph_Graph* g,            /**< [in] graph */
    int nparts,                         /**< [in] nb desired parts */
    int* part,                          /**< [in/out] array of pre-assigned partitions (of size g->nvtxs). */
    int ubfactor,                       /**< [in] unbalance factor */
    int usefixed,                       /**< [in] use fixed vertices */
    int ninitpass,                      /**< [in] nb of initial partitioning passes at coarsest level */
    int nrefpass,                       /**< [in] nb of refinement passes at each level  (-1 for infinite) */
    int cnvtxspart,                     /**< [in] max size of coarsest graph per part */
    LibGraph_CoarsenMethod coarsen,     /**< [in] graph coarsening routine */
    LibGraph_PartitionMethod partition, /**< [in] graph partitioning routine */
    LibGraph_RefineMethod refine,       /**< [in] graph refinement routine */
    const void* copt,                   /**< coarsening options */
    const void* popt,                   /**< initial partitioning options */
    const void* ropt                    /**< refinement options */
)

{
  // check
  assert(g && part);

  // init multilevel
  struct ML* mlroot = malloc(sizeof(struct ML));
  mlroot->level = 0; /* initial graph is level 0 */
  mlroot->graph = (LibGraph_Graph*)g;
  mlroot->cmap = NULL;
  mlroot->coarser = mlroot->finer = NULL;
  mlroot->part = malloc(mlroot->graph->nvtxs * sizeof(int));
  mlroot->edgecut = 0;
  mlroot->ub = 0.0;
  mlroot->partfixed = NULL;

  if (!usefixed)
    for (int i = 0; i < g->nvtxs; i++) part[i] = -1;

  /* fixed vertices or not ? */
  int nbfixed = 0;
  for (int i = 0; i < g->nvtxs; i++) {
    assert(part[i] >= -1 && part[i] < nparts);
    if (part[i] != -1) nbfixed++;
  }

  if (usefixed) {
    PRINT("INFO: %d fixed vertices\n", nbfixed);
    mlroot->partfixed = malloc(g->nvtxs * sizeof(int));
    memcpy(mlroot->partfixed, part, g->nvtxs * sizeof(int));
  }

  PRINT("=> multilevel parameters: ubfactor=%d%%, cnvtxspart=%d, ninitpass = %d, nrefpass = %d\n", ubfactor, cnvtxspart,
        ninitpass, nrefpass);

  /*  ==================== COARSENING PHASE  ==================== */

  PRINTADV("*************** MULTILEVEL COARSENING ***************\n");

  struct ML* ml = mlroot;
  int clevel = 1; /* initial graph is level 0 */
#ifdef DEBUGADV
  int toskip = 0;
  int vtegw = 0;  // vertex edge weight
  int vtw = 0;    // vertex weight
  char* degreefile = malloc(132 * sizeof(char));
  if (sprintf(degreefile, "degree-%d-%d", g->nvtxs, nparts) < 0) perror("Error: Buffer failure with sprintf\n");
  FILE* dfd = NULL;
  dfd = fopen(degreefile, "w");
  // level of coarsening , vertex degree(number of adjacent edges), vertex (adjecent) edges weight,
  // vertex weight
  fprintf(dfd, "#LEVEL #VTDEG #VTEGW #VTW\n");
#endif

  // START(0);

  do {
    ml->cmap = malloc(ml->graph->nvtxs * sizeof(int));

    /* initialize coarser graph */
    ml->coarser = malloc(sizeof(struct ML));
    ml->coarser->graph = malloc(sizeof(LibGraph_Graph));
    ml->coarser->level = ml->level + 1;
    ml->coarser->cmap = NULL;
    ml->coarser->part = NULL;
    ml->coarser->edgecut = 0;
    ml->coarser->ub = 0.0;
    ml->coarser->partfixed = NULL;
    ml->coarser->coarser = NULL;
    ml->coarser->finer = NULL;

#ifdef DEBUGADV
    printGraph(ml->graph);

    if (usefixed) {
      PRINTADV("- partfixed: [");
      for (int i = 0; i < ml->graph->nvtxs; i++) PRINTADV("%d ", ml->partfixed[i]);
      PRINTADV("]\n");
    }
    //#endif
    if (!toskip) {
      if (clevel > 30) {
        fprintf(dfd, "# Warning levels of coarsening phase greater that 30.\n");
        toskip = 1;
      }
      else {
        for (int i = 0; i < ml->graph->nvtxs; i++) {
          if (ml->graph->ewgts || ml->graph->vwgts) {
            vtegw = 0;
            for (int j = ml->graph->xadj[i]; j < ml->graph->xadj[i + 1]; j++) vtegw += ml->graph->ewgts[j];
            vtw = ml->graph->vwgts[i];
          }
          else {
            vtegw = 1;
            vtw = 1;
          }
          fprintf(dfd, "%d %d %d %d\n", clevel, ml->graph->xadj[i + 1] - ml->graph->xadj[i], vtegw, vtw);
        }
      }
    }
#endif

    /* compute coarser graph */
    coarsen(ml->graph, ml->coarser->graph, ml->cmap, ml->partfixed, copt);

#ifdef DEBUGADV
    PRINTADV("- mapping to coarser graph: [");
    for (int i = 0; i < ml->graph->nvtxs; i++) PRINTADV("%d ", ml->cmap[i]);
    PRINTADV("]\n");
#endif

    /* compute fixed vetices for coarser level */
    if (usefixed) {
      ml->coarser->partfixed = malloc(ml->coarser->graph->nvtxs * sizeof(int));
      for (int i = 0; i < ml->coarser->graph->nvtxs; i++) ml->coarser->partfixed[i] = -1;
      for (int i = 0; i < ml->graph->nvtxs; i++)
        if (ml->partfixed[i] != -1) {
          int cvtx = ml->cmap[i];
          assert(ml->coarser->partfixed[cvtx] == -1 ||
                 ml->coarser->partfixed[cvtx] == ml->partfixed[i]);  // check coarser fixed vertex part
          ml->coarser->partfixed[cvtx] = ml->partfixed[i];
        }
    }

    /* allocate partition for coarser graph */
    ml->coarser->part = malloc(ml->coarser->graph->nvtxs * sizeof(int));

    ml->coarser->finer = ml;
    ml = ml->coarser;
    clevel++;

  } while (ml->graph->nvtxs > (cnvtxspart * nparts));

  struct ML* mlcoarsest = ml;  // coarsest

// debug
#ifdef DEBUG
  int ndeg1 = 0;
  for (int i = 0; i < mlcoarsest->graph->nvtxs; i++) {
    int degree = mlcoarsest->graph->xadj[i + 1] - mlcoarsest->graph->xadj[i];
    if (degree == 1) ndeg1++;
  }
  PRINT("=> nb vtxs of degree 1 in coarsest graph: %d\n", ndeg1);
#endif

  // STOP(0);
  // double exectime0 = EXECTIME(0);
  // if(info) info->mlexectime[0] = exectime0;

  /* PRINT("=> multilevel coarsening in %.2f ms (%d levels, vtxs %d, compression %.2f %%, avg degree
   * %.2f)\n",  */
  /* 	exectime0, clevel, mlcoarsest->graph->nvtxs,  */
  /* 	100.0-mlcoarsest->graph->nvtxs*100.0/mlroot->graph->nvtxs,  */
  /* 	2.0*mlcoarsest->graph->nedges/mlcoarsest->graph->nvtxs);     */

  PRINT("=> multilevel coarsening done (%d levels, vtxs %d, compression %.2f %%, avg degree %.2f)\n", clevel,
        mlcoarsest->graph->nvtxs, 100.0 - mlcoarsest->graph->nvtxs * 100.0 / mlroot->graph->nvtxs,
        2.0 * mlcoarsest->graph->nedges / mlcoarsest->graph->nvtxs);

#ifdef DEBUGADV
  fclose(dfd);
#endif

#ifdef DEBUG
  // compute final matching
  int* matching = malloc(g->nvtxs * sizeof(int));
  for (int i = 0; i < g->nvtxs; i++) matching[i] = i;
  struct ML* mlc = mlroot;
  while (mlc->coarser) {
    for (int i = 0; i < g->nvtxs; i++) {
      int ci = matching[i];
      assert(mlc->cmap);
      matching[i] = mlc->cmap[ci];
    }
    mlc = mlc->coarser;
  }
  // if(info && info->mesh) addMeshVariable(info->mesh, "matching", CELL_INTEGER, 1, matching);
  free(matching);
#endif

#ifdef DEBUG
  // check vertex and edge weights of the coarsest graph
  int cvwgts = 0, cewgts = 0;
  for (int i = 0; i < mlcoarsest->graph->nvtxs; i++) {
    cvwgts += mlcoarsest->graph->vwgts ? mlcoarsest->graph->vwgts[i] : 1;
    for (int j = mlcoarsest->graph->xadj[i]; j < mlcoarsest->graph->xadj[i + 1]; j++) {
      // int ii = mlcoarsest->graph->adjncy[j]; /* edge (i,ii) */
      cewgts += mlcoarsest->graph->ewgts ? mlcoarsest->graph->ewgts[j] : 1;
    }
  }

  int vwgts = 0, ewgts = 0;
  for (int i = 0; i < g->nvtxs; i++) {
    vwgts += g->vwgts ? g->vwgts[i] : 1;
    for (int j = g->xadj[i]; j < g->xadj[i + 1]; j++) {
      // int ii = g->adjncy[j]; /* edge (i,ii) */
      ewgts += g->ewgts ? g->ewgts[j] : 1;
    }
  }

  assert(cvwgts == vwgts);
  PRINT("coarsening: edge compression = %.2f%% (original ew = %d, coarsen ew = %d)\n", (ewgts - cewgts) * 100.0 / ewgts,
        ewgts, cewgts);

#endif

  /* ==================== INITIAL PARTITION OF THE COARSEST GRAPH  ==================== */

  // START(1);

  PRINTADV("*************** PARTITIONING COARSEST GRAPH IN %d PARTS ***************\n", nparts);

// coarsest graph
#ifdef DEBUGADV
  LibGraph_printGraph(mlcoarsest->graph);
#endif

#ifdef DEBUGADV
  if (usefixed) {
    PRINTADV("- partfixed: [");
    for (int i = 0; i < mlcoarsest->graph->nvtxs; i++) PRINTADV("%d ", mlcoarsest->partfixed[i]);
    PRINTADV("]\n");
  }
#endif

  // add seeds & fixed vertex in debug mesh
  // if(info && info->mesh) addPartitionInMeshAtLevel(info->mesh, mlcoarsest, "coarser-fixed");

  /* compute locked vertices */
  // int * locked = NULL;
  // if (usefixed) {
  //   locked = malloc(mlcoarsest->graph->nvtxs * sizeof(int));
  //   memset(locked, 0, sizeof(int)*mlcoarsest->graph->nvtxs);
  //   for(int i = 0 ; i < mlcoarsest->graph->nvtxs ; i++) if(mlcoarsest->partfixed[i] != -1)
  //   locked[i] = 1;
  // }

  // perform several passes
  int* _part = malloc(mlcoarsest->graph->nvtxs * sizeof(int));
  mlcoarsest->edgecut = INT_MAX;
  int _edgecut;
  double _ub;

  for (int i = 0; i < ninitpass; i++) {
    // reset partition array
    for (int i = 0; i < mlcoarsest->graph->nvtxs; i++) _part[i] = -1; /* free vertices */
    if (usefixed) memcpy(_part, mlcoarsest->partfixed, mlcoarsest->graph->nvtxs * sizeof(int));

    // partition coarsest graph (with fixed vertices, if supported)
    // partition(mlcoarsest->graph, nparts, _part, locked, ubfactor, popt);
    partition(mlcoarsest->graph, nparts, _part, mlcoarsest->partfixed, ubfactor, popt);

    _ub = LibGraph_computeGraphMaxUnbalance(mlcoarsest->graph, nparts, _part);
    _edgecut = LibGraph_computeGraphEdgeCut(mlcoarsest->graph, nparts, _part);
    PRINT("INIT PARTITION: pass #%d -> edgecut = %d, ub = %.6f\n", i, _edgecut, _ub);

    // save part if better
    if ((_edgecut < mlcoarsest->edgecut && _ub * 100.0 <= (ubfactor + EPSILON)) ||
        (mlcoarsest->edgecut == INT_MAX && i == ninitpass - 1)) {
      mlcoarsest->ub = _ub;
      mlcoarsest->edgecut = _edgecut;
      memcpy(mlcoarsest->part, _part, mlcoarsest->graph->nvtxs * sizeof(int));
    }
  }

  // free memory
  free(_part);
  // if(usefixed) free(locked);

  PRINT("INIT PARTITION: best edgecut = %d\n", mlcoarsest->edgecut);

  // add seeds & fixed vertex in debug mesh
  // if(info && info->mesh) addPartitionInMeshAtLevel(info->mesh, mlcoarsest, "initial-part");

  PRINTADV("- level %d: edgecut %d and ub %d%% (initial partition)\n", mlcoarsest->level, mlcoarsest->edgecut,
           (int)round(mlcoarsest->ub * 100.0));

// check if fixed vertices dont move :-)
#ifdef DEBUG
  if (usefixed) {
    for (int i = 0; i < mlcoarsest->graph->nvtxs; i++)
      if (mlcoarsest->partfixed[i] != -1) assert(mlcoarsest->partfixed[i] == mlcoarsest->part[i]);
  }
#endif

#ifdef DEBUGADV
  PRINTADV("- partition at level level %d: [", mlcoarsest->level);
  for (int i = 0; i < mlcoarsest->graph->nvtxs; i++) PRINTADV("%d ", mlcoarsest->part[i]);
  PRINTADV("]\n");
#endif

  // STOP(1);
  // double exectime1 = EXECTIME(1);
  // if(info) info->mlexectime[1] = exectime1;

  /* PRINT("=> multilevel partitioning coarsest graph in %.2f ms (vtxs %d, cut %d)\n",  */
  /* 	exectime1, mlcoarsest->graph->nvtxs, mlcoarsest->edgecut); */

#ifdef DEBUGADV
  printGraphDiagnostic(mlcoarsest->graph, nparts, mlcoarsest->part);
#endif

// debug
#ifdef DEBUG
  int nbislands = computeNumberOfIslands(mlcoarsest->graph, nparts, mlcoarsest->part) - nparts;
  if (nbislands > 0) printf("WARNING: coarsest graph, nb islands = %d\n", nbislands);
#endif

  /*  ==================== REFINEMENT PHASE  ==================== */

  PRINTADV("*************** MULTILEVEL REFINEMENTS IN %d PARTS ***************\n", nparts);

  // START(2);

  // int kfmgaintot = 0;

  /* refine ml->graph, then compute partition for finer partition */
  while (ml) {
    assert(ml->part);

    if (refine) {
      /* compute locked vertices */
      int* locked = NULL;
      if (usefixed) {
        locked = malloc(ml->graph->nvtxs * sizeof(int));
        memset(locked, 0, sizeof(int) * ml->graph->nvtxs);
        for (int i = 0; i < ml->graph->nvtxs; i++)
          if (ml->partfixed[i] != -1) locked[i] = 1;
      }

      /* perform refinement */
      // int ubfactorlevel = ubfactor; /* adjust ubfactor for coarser graph */
      // #define UBFACTORMAX 50
      // if(clevel > 1) ubfactorlevel += ml->level*(UBFACTORMAX-ubfactor)/(clevel-1);

      ml->edgecut = 0;
      ml->ub = 0;

#ifdef DEBUG
      ml->edgecut = computeGraphEdgeCut(ml->graph, nparts, ml->part);
      ml->ub = computeGraphMaxUnbalance(ml->graph, nparts, ml->part);
#endif

      PRINTADV("- level %d: edgecut %d and ub %d%% (before refinement)\n", ml->level, ml->edgecut,
               (int)round(ml->ub * 100.0));
      // int kfmgain = refine(ml->graph, nparts, ubfactorlevel, ml->part, locked, nrefpass,
      // maxnegmove);
      refine(ml->graph, nparts, ml->part, locked, ubfactor, ropt);
      if (usefixed) free(locked);
      // PRINTADV("- KFM gain %d at level %d (ubfactor %d%%, nrefpass %d)\n", kfmgain, ml->level,
      // ubfactor, nrefpass);
      // kfmgaintot += kfmgain;
    }

#ifdef DEBUG
    /* update edgecut & ub */
    ml->edgecut = computeGraphEdgeCut(ml->graph, nparts, ml->part);
    ml->ub = computeGraphMaxUnbalance(ml->graph, nparts, ml->part);
#endif

// debug
#ifdef DEBUGADV
    PRINTADV("- level %d: edgecut %d and ub %d%% (after refinement)\n", ml->level, ml->edgecut,
             (int)round(ml->ub * 100.0));
    PRINTADV("- partition at level %d: [", ml->level);
    for (int i = 0; i < ml->graph->nvtxs; i++) PRINTADV("%d ", ml->part[i]);
    PRINTADV("]\n");
#endif

    // add partition in debug mesh
    /* if(info && info->mesh) {  */
    /*   char varname[255]; sprintf(varname,"level-%.2d", ml->level); */
    /*   addPartitionInMeshAtLevel(info->mesh, ml, varname); */
    /* } */

    /* projection: compute finer partition (used for next iteration) */
    if (ml->finer) {
      for (int i = 0; i < ml->finer->graph->nvtxs; i++) ml->finer->part[i] = ml->part[ml->finer->cmap[i]];
    }

    /* free memory from previous level */
    if (ml->coarser) {
      free(ml->coarser->part);
      free(ml->coarser->partfixed);
      free(ml->coarser->cmap);
      LibGraph_freeGraph(ml->coarser->graph);
      free(ml->coarser->graph);
      free(ml->coarser);
    }

    ml = ml->finer;  // next level
  }

  // STOP(2);
  // double exectime2 = EXECTIME(2);
  // if(info) info->mlexectime[2] = exectime2;
  /* PRINT("=> multilevel refinement in %.2f ms for gain %d (vtxs %d, cut %d)\n",  */
  /* 	exectime2, kfmgaintot, mlroot->graph->nvtxs, mlroot->edgecut);  */

  /*  ==================== END OF MULTILEVEL PROCESS  ==================== */

#ifdef DEBUG
  // check if fixed vertices dont move :-)
  if (usefixed) {
    for (int i = 0; i < g->nvtxs; i++)
      if (mlroot->partfixed[i] != -1) assert(mlroot->partfixed[i] == mlroot->part[i]);
  }
#endif

  /* output */
  // *edgecut =computeGraphEdgeCut(mlroot->graph, nparts, mlroot->part);
  memcpy(part, mlroot->part, mlroot->graph->nvtxs * sizeof(int));
// if(info) info->exectime = exectime0 + exectime1 + exectime2; // total

// debug
#ifdef DEBUG
  nbislands = computeNumberOfIslands(mlroot->graph, nparts, mlroot->part) - nparts;
  if (nbislands > 0) PRINT("WARNING: finest graph, nb islands = %d\n", nbislands);
#endif

  /* free root level (dont free mlroot->graph) */
  free(mlroot->part);
  free(mlroot->partfixed);
  free(mlroot->cmap);
  free(mlroot);
}

/* *********************************************************** */
