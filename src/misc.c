#include <assert.h>
#include <limits.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "misc.h"

/* *********************************************************** */

int LibGraph_gcd(int a, int b)
{
  while (b > 0) {
    int r = a % b;
    a = b;
    b = r;
  }
  return a;
}

/* *********************************************************** */

int LibGraph_lcm(int a, int b)
{
  return a * b / LibGraph_gcd(a, b);
}

/* *********************************************************** */

void LibGraph_randomPermute(int size, int* perm)
{
  // int * perm = malloc(size*sizeof(int));
  for (int i = 0; i < size; i++) perm[i] = i;
  for (int i = 0; i < size; i++) {
    int k = rand() * 1.0 * size / RAND_MAX;
    int tmp = perm[i];
    perm[i] = perm[k];
    perm[k] = tmp;
  }
}

/* *********************************************************** */

void LibGraph_generateRandomOrder(int* order, int size)
{
  order[0] = 0;
  for (int i = 1; i < size; i++) {
    int j = rand() % (i + 1);
    order[i] = order[j];
    order[j] = i;
  }
}

/* *********************************************************** */
