#ifndef LIBGRAPH_HYPERGRAPH_H
#define LIBGRAPH_HYPERGRAPH_H

//@{

/** Hypergraph structure. */
typedef struct _LibGraph_Hypergraph
{
  int nvtxs;   /**< nb vertices */
  int nhedges; /**< nb hyperedges */
  int* eptr;   /**< array of indirection on eind (of size nhedges+1) */
  int* eind;   /**< array with all consecutive hyperedges (as vertex set, of size eptr[nhedges]) */
  int* vwgts;  /**< array vertex weights (of size nvtxs) */
  int* hewgts; /**< array hyperedge weights (of size nhedges) */
} LibGraph_Hypergraph;

/** Allocate new hypergraph. */
LibGraph_Hypergraph* LibGraph_newHypergraph(void);

/** Duplicate an hypergraph */
void LibGraph_dupHypergraph(LibGraph_Hypergraph* hgold, LibGraph_Hypergraph* hgnew);

/** Free memory allocated within hypergraph structure. */
void LibGraph_freeHypergraph(LibGraph_Hypergraph* hg); /* [in] Hypergraph to free */

/** Compute dual hypergraph. */
void LibGraph_dualHypergraph(LibGraph_Hypergraph* hg,    /* [in] input hypergraph */
                             LibGraph_Hypergraph* dual); /* [out] dual hypergraph */

/** Compute subhypergraph. */
void LibGraph_subHypergraphFromRange(
    LibGraph_Hypergraph* hg,    /* [in] hypergraph */
    int vtxstart,      /* [in] first vertex in range */
    int vtxend,        /* [in] last vertex in range */
    LibGraph_Hypergraph* subhg, /* [out] sub-hypergraph (with only vertices in range [vtxstart,vtxend]) */
    int** vtxmap,      /* [out] vertex local-to-global mapping (allocated array of size subhg->nvtxs) */
    int** hedgemap);   /* [out] hyperedge local-to-global mapping (allocated array of size subhg->nhedges) */

/** Compute subhypergraph. */
void LibGraph_subHypergraphFromPart(
    LibGraph_Hypergraph* oldhg, /* [in] hypergraph */
    int* part,         /* [in] partition */
    int nparts,        /* [in] number of parts */
    int p,             /* [in] part to extract */
    LibGraph_Hypergraph* newhg, /* [out] subhypergraph */
    int** vtxs);       /* [out] allocated array of size subhg->nvtxs, oldindex = (*vtxs)[newindex] */

/** Print hypergraph structure. */
void LibGraph_printHypergraph(LibGraph_Hypergraph* hg); /* [in] input hypergraph to print */

/** Compute quotient hypergraph. */
void LibGraph_quotientHypergraph(const LibGraph_Hypergraph* input, /* [in] input hypergraph */
                                 int nparts,              /* [in] nb of partitions */
                                 const int* parts,        /* [in] array of computed partitions (of size input->nvtxs) */
                                 LibGraph_Hypergraph* output);     /* [out] quotient graph (with nparts vertices) */

//@}

#endif
