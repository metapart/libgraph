#ifndef LIBGRAPH_MISC_H
#define LIBGRAPH_MISC_H

//@{

/* random permute */
void LibGraph_randomPermute(int size, int* perm);

/** greatest common divisor */
int LibGraph_gcd(int a, int b);

/** least common multipe */
int LibGraph_lcm(int a, int b);

/** Generate a random order*/
void LibGraph_generateRandomOrder(int* order, /** [out] random order */
                                  int size    /** [in] Order size */
);

//@}

#endif
