#include <assert.h>
#include <limits.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// #define DEBUG
// #define DEBUGADV

#include "bfs.h"
#include "debug.h"
#include "graph.h"
#include "queue.h"

/* *********************************************************** */
/*                             BFS                             */
/* *********************************************************** */

/* BFS algorithm used to return a new seed from a set of starting vertices */
int LibGraph_bfs(const LibGraph_Graph* g, /* input graph */
                 int* startvtx,           /* set of starting vertices (set -1 to ignore a seed) */
                 int size,                /* size of startvtx */
                 int* d                   /* distance array from startvtx set (array of size g->nvtxs) */
)
{
  LibGraph_Queue q;
  LibGraph_queueInit(&q, g->nvtxs);

  for (int i = 0; i < g->nvtxs; i++) d[i] = -1;

  for (int i = 0; i < size; i++) {
    if (startvtx[i] != -1) { /* else ignore */
      LibGraph_queuePushBack(&q, startvtx[i]);
      d[startvtx[i]] = 0;
    }
  }

  int v = -1;
  while (!LibGraph_queueIsEmpty(&q)) {
    v = LibGraph_queuePopFront(&q);

    for (int i = g->xadj[v]; i < g->xadj[v + 1]; i++) {
      int u = g->adjncy[i];
      if (d[u] == -1) {
        d[u] = d[v] + 1;
        LibGraph_queuePushBack(&q, u);
      }
    }
  }

  LibGraph_queueFree(&q);

  return v;
}

/* *********************************************************** */
/*                             DFS                             */
/* *********************************************************** */

/* create the spanning-tree based on DFS algorithm */
int LibGraph_depthFirstSearch(const LibGraph_Graph* g, int root)
{
  /* flags, height  */
  int* vflags = malloc(g->nvtxs * sizeof(int));   /* vflags[v] is true if v has been visited */
  int* vheights = malloc(g->nvtxs * sizeof(int)); /* vheights[v] is the height of v in tree */
  memset(vflags, 0, g->nvtxs * sizeof(int));
  memset(vheights, 0, g->nvtxs * sizeof(int));

  /* stack(s) */
  const int stacksize = 2 * g->nvtxs;           /* which max ? */
  int* stack = malloc(stacksize * sizeof(int)); /* stack used for DFS */
  int* backstack = malloc(stacksize * sizeof(int));
  /* the vertex backstack[i] is the father (in the tree) of vertex stack[i] */
  int pstack = 0; /* pointer on top of stack (0 means empty stack) */

  /* push the root node onto the stack */
  stack[pstack] = backstack[pstack] = root;
  pstack++;
  vheights[root] = 0;
  printf("Depth-First Search (root %d):\n", root);
  /* while the stack is not empty, repeat... */
  while (pstack > 0) {
    /* POP: pop a node v from the stack */
    pstack--;
    int v = stack[pstack];
    int vv = backstack[pstack];
    if (vflags[v] != 0) continue; /* already explored, skip now ! */
    vflags[v] = 1;                /* label v as explored */
    if (v != vv) printf("tree edge (%d, %d)\n", vv, v);
    printf("height %d, vertex %d\n", vheights[v], v);

    /* PUSH: for all incident edges of v that have not yet been discovered onto the stack. */
    // for(int k = g->xadj[v] ; k < g->xadj[v+1] ; k++) { /* standard order */
    for (int k = g->xadj[v + 1] - 1; k >= g->xadj[v]; k--) { /* reverse order */
      int w = g->adjncy[k];                                  /* edge (v,w) */
      if (vflags[w] == 0) {                                  /* not visited */
        stack[pstack] = w;
        backstack[pstack] = v; /* from to w back to v... */
        pstack++;
        vheights[w] = vheights[v] + 1;
      }
      else { /* back edge */
        if (w != vv) printf("back edge (%d,%d)\n", v, w);
      }
      assert(pstack < stacksize);
    }
  }

  /* if the stack is empty, every node in the tree has been examined... */

  /* free memory */
  free(vflags);
  free(vheights);
  free(stack);
  free(backstack);

  return 0;
}

/* *********************************************************** */
/*                      test connectivity                      */
/* *********************************************************** */

/* test graph connectivity based on BFS algorithm  */
int LibGraph_isConnectedGraph(const LibGraph_Graph* g)
{
  assert(g);
  int* d = malloc(g->nvtxs * sizeof(int)); /* distance array of size g->nvtxs */
  for (int i = 0; i < g->nvtxs; i++) d[i] = -1;

  LibGraph_Queue q;
  LibGraph_queueInit(&q, g->nvtxs);

  int componentsize = 0;

  assert(g->nvtxs > 0);
  LibGraph_queuePushBack(&q, 0); /* initialize queue with vtx 0 */
  d[0] = 0;
  componentsize++;

  int v = -1;
  while (!LibGraph_queueIsEmpty(&q)) {
    v = LibGraph_queuePopFront(&q);

    for (int i = g->xadj[v]; i < g->xadj[v + 1]; i++) {
      int u = g->adjncy[i];
      if (d[u] == -1) {
        d[u] = d[v] + 1;
        LibGraph_queuePushBack(&q, u);
        componentsize++;
      }
    }
  }

  LibGraph_queueFree(&q);
  free(d);

  assert(componentsize <= g->nvtxs);

  // printf("WARNING: test if graph is connected: component size = %d / %d vtxs\n", componentsize,
  // g->nvtxs);
  return (componentsize == g->nvtxs);  // 1 single connected component
}

/* *********************************************************** */

/* test subgraph connectivity based on BFS algorithm  */
int LibGraph_isConnectedSubgraph(const LibGraph_Graph* g, int* used)
{
  assert(g);
  assert(used);
  assert(g->nvtxs > 0);

  int* d = malloc(g->nvtxs * sizeof(int)); /* distance array of size g->nvtxs */
  for (int i = 0; i < g->nvtxs; i++) d[i] = -1;

  int nvtxs = 0;
  for (int i = 0; i < g->nvtxs; i++)
    if (used[i]) nvtxs++;
  assert(nvtxs > 0 && nvtxs <= g->nvtxs);

  LibGraph_Queue q;
  LibGraph_queueInit(&q, g->nvtxs);

  int componentsize = 0;

  int startvtx;
  for (startvtx = 0; startvtx < g->nvtxs; startvtx++)
    if (used[startvtx]) break;
  assert(startvtx < g->nvtxs);

  LibGraph_queuePushBack(&q, startvtx); /* initialize queue with a start vtx  */
  d[startvtx] = 0;
  componentsize++;

  int v = -1;
  while (!LibGraph_queueIsEmpty(&q)) {
    v = LibGraph_queuePopFront(&q);
    assert(used[v]);

    for (int i = g->xadj[v]; i < g->xadj[v + 1]; i++) {
      int u = g->adjncy[i];
      if (used[u] && d[u] == -1) {
        d[u] = d[v] + 1;
        LibGraph_queuePushBack(&q, u);
        componentsize++;
      }
    }
  }

  LibGraph_queueFree(&q);
  free(d);

  assert(componentsize <= nvtxs);

  // printf("WARNING: test if graph is connected: component size = %d / %d vtxs\n", componentsize,
  // g->nvtxs);
  return (componentsize == nvtxs);  // 1 single connected component
}

/* *********************************************************** */

/* test if a graph partition is connected (using BFS algorithm)  */
/* return the nb of islands >= nparts */
int LibGraph_computeNumberOfIslands(const LibGraph_Graph* g, int nparts, const int* part)
{
  int nbislands = 0;

  int* visited = malloc(g->nvtxs * sizeof(int));      /* array of size g->nvtxs */
  for (int i = 0; i < g->nvtxs; i++) visited[i] = 0;  // not yet visited

  LibGraph_Queue q;
  LibGraph_queueInit(&q, g->nvtxs);

  assert(g->nvtxs > 0);

  int thevtx = 0;
  int islandsize = 0;

  while (1) {
    while (thevtx < g->nvtxs) {
      if (visited[thevtx] == 0) break; /* next vertex to visit found */
      thevtx++;
    }
    if (thevtx == g->nvtxs) break; /* all vertices yet visited */

    int thepart = part[thevtx];
    LibGraph_queuePushBack(&q, thevtx); /* initialize queue with thevtx */
    visited[thevtx] = 1;
    islandsize = 1;

    int v = -1;
    while (!LibGraph_queueIsEmpty(&q)) {
      v = LibGraph_queuePopFront(&q);
      for (int i = g->xadj[v]; i < g->xadj[v + 1]; i++) {
        int u = g->adjncy[i];                        /* for all edges (v,u) */
        if (visited[u] == 0 && part[u] == thepart) { /* vtx u not yet visited, inside the current part */
          visited[u] = 1;
          LibGraph_queuePushBack(&q, u);
          islandsize++;
        }
      }
    }

    PRINT("island #%d: size %d, part %d, first vtx %d\n", nbislands, islandsize, thepart, thevtx);

    nbislands++;
  }

  LibGraph_queueFree(&q);

  free(visited);

  return nbislands;
}

/* *********************************************************** */
/*                   Levelset Algorithm                        */
/* *********************************************************** */

/* warning: the output part can contain free vertices */
void LibGraph_levelset1(const LibGraph_Graph* g, /* input graph */
                        int size,          /* nb parts */
                        int maxpartsize,   /* max part size allowed (-1 if infinite) */
                        int* part          /* input/output partition (with seeds) */
)
{
  if (maxpartsize == -1) maxpartsize = INT_MAX; /* infinite part size (no constraint) */

  /* check input partition */
  for (int i = 0; i < g->nvtxs; i++) assert(part[i] >= -1 && part[i] < size);

  /* initialize queue */
  LibGraph_Queue q;
  LibGraph_queueInit(&q, g->nvtxs);

  int* ps = malloc(size * sizeof(int)); /* part size */
  for (int i = 0; i < size; i++) ps[i] = 0;

  for (int i = 0; i < g->nvtxs; i++) {
    if (part[i] != -1) {
      LibGraph_queuePushBack(&q, i);
      ps[part[i]] += g->vwgts ? g->vwgts[i] : 1;
      // ps[part[i]]++;
    }
  }

  /* check there is at least 1 seed per part */
  for (int i = 0; i < size; i++) assert(ps[i] > 0);

  int v = -1;
  while (!LibGraph_queueIsEmpty(&q)) {
    v = LibGraph_queuePopFront(&q);

    for (int i = g->xadj[v]; i < g->xadj[v + 1]; i++) {
      int u = g->adjncy[i];
      /* assign the free vertex u to the same bubble as v */
      if (part[u] == -1 && ps[part[v]] <= maxpartsize) {
        part[u] = part[v];
        ps[part[u]] += g->vwgts ? g->vwgts[u] : 1;  // ps[part[u]]++;
        LibGraph_queuePushBack(&q, u);
      }
    }
  }

  LibGraph_queueFree(&q);
  free(ps);
}

/* *********************************************************** */

void LibGraph_levelset2(const LibGraph_Graph* g, /* input graph */
                        int size,          /* nb parts */
                        int* maxpartsize,  /* array of max part size allowed for each part (NULL if infinite) */
                        int* part          /* input/output partition (with seeds) */
)
{
  /* check input partition */
  for (int i = 0; i < g->nvtxs; i++) assert(part[i] >= -1 && part[i] < size);

  /* initialize queue */
  LibGraph_Queue q;
  LibGraph_queueInit(&q, g->nvtxs);

  int* ps = malloc(size * sizeof(int)); /* part size */
  for (int i = 0; i < size; i++) ps[i] = 0;

  for (int i = 0; i < g->nvtxs; i++) {
    if (part[i] != -1) {
      LibGraph_queuePushBack(&q, i);
      ps[part[i]] += g->vwgts ? g->vwgts[i] : 1;
    }
  }

  /* check there is at least 1 seed per part */
  for (int i = 0; i < size; i++) assert(ps[i] > 0);

  int v = -1;
  while (!LibGraph_queueIsEmpty(&q)) {
    v = LibGraph_queuePopFront(&q);
    int p = part[v];
    int max = maxpartsize ? maxpartsize[p] : INT_MAX; /* infinite part size (no constraint) */

    for (int i = g->xadj[v]; i < g->xadj[v + 1]; i++) {
      int u = g->adjncy[i]; /* edge (v,u) */
      /* assign the free vertex u to the same bubble as v */
      if (part[u] == -1 && ps[p] <= max) {
        part[u] = part[v];
        ps[part[u]] += g->vwgts ? g->vwgts[u] : 1;
        LibGraph_queuePushBack(&q, u);
      }
    }
  }

  LibGraph_queueFree(&q);
  free(ps);
}

/* *********************************************************** */
