#ifndef LIBGRAPH_KFM_H
#define LIBGRAPH_KFM_H

#include "graph.h"

/** Refine graph with KFM method (implemented in kfm.c). */
/** \return edgecut gain after refinement */
int LibGraph_refineKFM(const LibGraph_Graph* g, /**< [in] graph */
                       int nparts,              /**< [in] nb of input parts */
                       int ubfactor,            /**< [in] unbalanced factor */
                       int* part,               /**< [in/out] array of input partition (of size g->nvtxs) */
                       int* locked,             /**< [in] boolean array of locked vertices in part (of size g->nvtxs) */
                       int npass,     /**< [in] nb of refinement passes (-1 for infinite passes until convergence) */
                       int maxnegmove /**< [in] max negative move allowed for each refinment pass (-1 for infinite) */
);

/** Refine graph with KFM method using a band graph. */
/** \return edgecut gain after refinement */
int LibGraph_refineBandKFM(
    const LibGraph_Graph* g, /**< [in] graph */
    int nparts,              /**< [in] nb of input parts */
    int ubfactor,            /**< [in] unbalanced factor */
    int* part,               /**< [in/out] array of input partition (of size g->nvtxs) */
    int* locked,             /**< [in] boolean array of locked vertices in part (of size g->nvtxs) */
    int npass,               /**< [in] nb of refinement passes (-1 for infinite passes until convergence) */
    int maxnegmove,          /**< [in] max negative move allowed for each refinment pass (-1 for infinite) */
    int width                /**< [in] band width */
);

#endif
