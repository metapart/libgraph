#include <assert.h>
#include <limits.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "bfs.h"
#include "debug.h"
#include "graph.h"
#include "queue.h"
#include "seeds.h"

/* *********************************************************** */
/*                            SEEDS                            */
/* *********************************************************** */

/* input partition with fixed/free vertices & output seeds */
int LibGraph_computeGraphSeeds(const LibGraph_Graph* g, int nparts, int optimize, int* part)
{
  // char buffer[256];
  int* d = malloc(g->nvtxs * sizeof(int)); /* distance */
  // int * fixed = malloc(g->nvtxs*sizeof (int));
  // memcpy(fixed, part, g->nvtxs*sizeof (int));
  int* ps = malloc(nparts * sizeof(int));    /* part size */
  int* seeds = malloc(nparts * sizeof(int)); /* seeds */
  /* starting vertices for BFS (first fixed vertices then nparts seeds (-1 if not used) */
  int* startvtx = malloc((g->nvtxs + nparts) * sizeof(int));
  int size = 0; /* size of startvtx array */
  int fixedvtxsize = 0;

  /* 0) initialize */
  for (int i = 0; i < g->nvtxs; i++) d[i] = -1;
  for (int i = 0; i < nparts; i++) ps[i] = 0;

  for (int i = 0; i < g->nvtxs; i++) {
    if (part[i] != -1) {
      ps[part[i]]++;
      startvtx[fixedvtxsize++] = i;
    }
  }

  // PRINT("initial fixed vertices (%d seeds) [", fixedvtxsize);
  // for (int i = 0; i < nparts; i++) PRINT("%d ", ps[i]);
  // PRINT("]\n");

  int nbseedsrequired = 0;
  for (int i = 0; i < nparts; i++)
    if (ps[i] == 0) nbseedsrequired++;

  if (nbseedsrequired == 0) goto bybye; /* no seeds required */

  /* 1) Create initial seed set */

  size = fixedvtxsize + nparts;

  for (int i = 0; i < nparts; i++) {
    seeds[i] = -1;
    startvtx[fixedvtxsize + i] = -1;
  }

  for (int i = 0; i < nparts; i++) {
    if (fixedvtxsize == 0 && i == 0) { /* no starting vertices for BFS */
      int v = rand() % g->nvtxs;
      int seed = LibGraph_bfs(g, &v, 1, d);  // first seed set {v} of size 1
      assert(part[seed] == -1);
      part[seed] = i;
      seeds[i] = seed;
      startvtx[fixedvtxsize + i] = seed;
      PRINT("-> add seed %d for part %d\n", seed, i);
    }
    else if (ps[i] == 0) { /* no fixed vertices, add a seed for part i */
      int seed = LibGraph_bfs(g, startvtx, size, d);
      assert(part[seed] == -1);
      part[seed] = i;
      seeds[i] = seed;
      startvtx[fixedvtxsize + i] = seed;
      PRINT("-> add seed %d for part %d\n", seed, i);
    }
    // snprintf (buffer, sizeof (buffer), "dist-%03d", i);
    // if(mesh) addMeshVariable (m, buffer, CELL_INTEGER, 1, d);
  }

  /* replace first seed (that was chosen randomly) if there is no fixed vertices */
  if (fixedvtxsize == 0) {
    int s = startvtx[fixedvtxsize];  // seed for part 0
    part[s] = -1;
    startvtx[fixedvtxsize] = -1;

    int seed = LibGraph_bfs(g, startvtx, size, d);
    assert(part[seed] == -1);
    part[seed] = 0;
    seeds[0] = seed;
    startvtx[fixedvtxsize] = seed;
    PRINT("-> replace first seed %d for part %d\n", seed, 0);
  }

  // if(mesh) addMeshVariable(mesh, "seeds-init", CELL_INTEGER, 1, part);

  if (optimize) {
    /* 2) Optimize seed set */
    int maxstep = 20;
    int step = 0;
    int changed = 0;

    do {
      changed = 0;
      for (int i = 0; i < nparts; i++) {
        int oldseed = startvtx[fixedvtxsize + i];  // i-th seed
        assert(seeds[i] == oldseed);
        if (oldseed == -1) continue;

        // bfs
        startvtx[fixedvtxsize + i] = -1;  // ignore i-th seed
        int newseed = LibGraph_bfs(g, startvtx, size, d);
        startvtx[fixedvtxsize + i] = oldseed;  // restore it

        if (d[newseed] > d[seeds[i]]) {  // if new seed is better...
          seeds[i] = newseed;
          startvtx[fixedvtxsize + i] = newseed;
          part[oldseed] = -1;
          part[newseed] = i;

          changed = 1;
          PRINT("-> choose new seed %d for part %d\n", newseed, i);
        }

        // snprintf (buffer, sizeof (buffer), "optdist-%03d-%03d", step, i);
        // if(mesh) addMeshVariable(m, buffer, CELL_INTEGER, 1, d);
      }

      step++;
    } while (changed && step < maxstep);

    // for (int i = 0; i < g->nvtxs; i++) d[i] = -1;
    // for (int i = 0; i < nparts; i++) d[seeds[i]] = i;
    // if(mesh) addMeshVariable(mesh, "seeds-opt", CELL_INTEGER, 1, part);

    PRINT("optimize seeds: %d steps\n", step);
  }

bybye:

  /* free */
  free(d);
  free(ps);
  free(startvtx);
  free(seeds);
  // free(fixed);

  return nbseedsrequired;
}

/* *********************************************************** */

int LibGraph_computeSeedDistance(const LibGraph_Graph* g, int nparts, int* part)
{
  int* dist = malloc(g->nvtxs * sizeof(int));
  int* seeds = malloc(nparts * sizeof(int));
  for (int i = 0; i < nparts; i++) seeds[i] = -1;

  /* find seeds */
  for (int i = 0; i < g->nvtxs; i++) {
    int p = part[i];
    if (p != -1) {
      assert(seeds[p] == -1); /* only support single seed per part */
      seeds[p] = i;
    }
  }

  /* start BFS for each seed */
  int mindist = INT_MAX;
  for (int k = 0; k < nparts; k++) {
    int startvtx = seeds[k];
    LibGraph_bfs(g, &startvtx, 1, dist);
    for (int i = 0; i < nparts; i++) {
      if (i != k) {
        int d = dist[seeds[i]]; /* distance between seeds k and i */
        if (d < mindist) mindist = d;
      }
    }
  }

  free(dist);
  free(seeds);

  return mindist;
}

/* *********************************************************** */

/* compute graph bubbles from input seeds */
void LibGraph_computeGraphBubbles(const LibGraph_Graph* g, /* input graph */
                                  int size,                /* nb parts */
                                  int maxpartsize,         /* max part size allowed (-1 if infinite) */
                                  int* part                /* input/output partition (with seeds) */
)
{
  if (maxpartsize == -1) maxpartsize = INT_MAX; /* infinite part size (no constraint) */

  /* check input partition */
  for (int i = 0; i < g->nvtxs; i++) assert(part[i] >= -1 && part[i] < size);

  /* initialize queue */
  LibGraph_Queue q;
  LibGraph_queueInit(&q, g->nvtxs);

  int* ps = malloc(size * sizeof(int)); /* part size */
  for (int i = 0; i < size; i++) ps[i] = 0;

  for (int i = 0; i < g->nvtxs; i++) {
    if (part[i] != -1) {
      LibGraph_queuePushBack(&q, i);
      ps[part[i]] += g->vwgts ? g->vwgts[i] : 1;
      // ps[part[i]]++;
    }
  }

  /* check there is at least 1 seed per part */
  for (int i = 0; i < size; i++) assert(ps[i] > 0);

  int v = -1;
  while (!LibGraph_queueIsEmpty(&q)) {
    v = LibGraph_queuePopFront(&q);

    for (int i = g->xadj[v]; i < g->xadj[v + 1]; i++) {
      int u = g->adjncy[i];
      /* assign the free vertex u to the same bubble as v */
      if (part[u] == -1 && ps[part[v]] <= maxpartsize) {
        part[u] = part[v];
        ps[part[u]] += g->vwgts ? g->vwgts[u] : 1;  // ps[part[u]]++;
        LibGraph_queuePushBack(&q, u);
      }
    }
  }

  LibGraph_queueFree(&q);
  free(ps);
}

/* *********************************************************** */

/* Given an input incomplete partition, compute the closest extra seed for a given part. */
int LibGraph_computeExtraSeed(const LibGraph_Graph* g, /* input graph */
                              int nparts,              /* nb of parts in partition */
                              int* part, /* input incomplete partition array of size g->nvtxs (-1 if free) */
                              int p      /* the part  */
)
{
  // check
  assert(p >= 0 && p < nparts);

  LibGraph_Queue q;
  LibGraph_queueInit(&q, g->nvtxs);

  int* d = malloc(g->nvtxs * sizeof(int));
  for (int i = 0; i < g->nvtxs; i++) d[i] = -1;

  for (int i = 0; i < g->nvtxs; i++) {
    if (part[i] == p) {
      LibGraph_queuePushBack(&q, i);
      d[i] = 0;
    }
  }

  int v = -1;
  while (!LibGraph_queueIsEmpty(&q)) {
    v = LibGraph_queuePopFront(&q);
    if (part[v] == -1) {
      free(d);
      return v;
    }  // we find the closest vertex...

    for (int i = g->xadj[v]; i < g->xadj[v + 1]; i++) {
      int u = g->adjncy[i];
      if (d[u] == -1) {
        d[u] = d[v] + 1;
        LibGraph_queuePushBack(&q, u);
      }
    }
  }

  LibGraph_queueFree(&q);

  free(d);
  return -1; /* fail to find a new seed */
}

/* *********************************************************** */
