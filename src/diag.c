#include <assert.h>
#include <limits.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "bfs.h"
#include "diag.h"
#include "graph.h"
#include "list.h"

#define MIN(a, b) (((a) <= (b)) ? (a) : (b))
#define MAX(a, b) (((a) >= (b)) ? (a) : (b))

/* *********************************************************** */

void LibGraph_printGraphDiagnostic(const LibGraph_Graph* g, int nparts, const int* part)
{
  /* weights in partition */

  double wparts[nparts];
  double wtot = 0.0;
  for (int k = 0; k < nparts; k++) wparts[k] = 0.0;
  for (int i = 0; i < g->nvtxs; i++) {
    int w = (g->vwgts != NULL) ? g->vwgts[i] : 1;
    if (part[i] > -1) {
      wparts[part[i]] += w;
      wtot += w;
    }
  }

  double wparts100[nparts];
  for (int k = 0; k < nparts; k++) wparts100[k] = wparts[k] * 100.0 / wtot;

  double maxub = 0.0;
  for (int k = 0; k < nparts; k++)
    if (wparts[k] > maxub) maxub = wparts[k];
  maxub /= (wtot / nparts);

  double _maxub = LibGraph_computeGraphMaxUnbalance(g, nparts, part);

  /* mean, standard deviation */

  double mean = 0.0, mean100 = 0.0, sd = 0.0, sd100 = 0.0;

  for (int k = 0; k < nparts; k++) mean += wparts[k];
  mean /= nparts;
  mean100 = mean * 100.0 / wtot;

  for (int k = 0; k < nparts; k++) sd += (mean - wparts[k]) * (mean - wparts[k]);
  sd = sqrt(sd / nparts);

  sd100 = sd * 100.0 / mean;

  /* edgecut */

  int edgecut = LibGraph_computeGraphEdgeCut(g, nparts, part);

  /* communication table */

  int* comm = malloc(nparts * nparts * sizeof(int));
  memset(comm, 0, nparts * nparts * sizeof(int));
  int* commsum = malloc(nparts * sizeof(int));
  memset(commsum, 0, nparts * sizeof(int));

  for (int i = 0; i < g->nvtxs; i++) {
    for (int j = g->xadj[i]; j < g->xadj[i + 1]; j++) {
      int ii = g->adjncy[j]; /* edge (i,ii) */
      if (part[i] > -1 && part[ii] > -1 && part[i] != part[ii]) {
        int ew = (g->ewgts == NULL) ? 1 : g->ewgts[j];
        comm[part[i] * nparts + part[ii]] += ew; /* (part[i] (row) -> part[ii] (col)) */
        commsum[part[i]] += ew;
      }
    }
  }

  // nb islands
  int nbislands = LibGraph_computeNumberOfIslands(g, nparts, part);

  // degrees
  double avgdegree;
  int mindegree, maxdegree;
  LibGraph_computeGraphMinMaxDegree(g, &avgdegree, &mindegree, &maxdegree);

  // vtx/edge weight
  double avgew, minew, maxew;
  LibGraph_computeGraphMinMaxEdgeWeight(g, &avgew, &minew, &maxew);
  double avgvw, minvw, maxvw;
  LibGraph_computeGraphMinMaxVertexWeight(g, &avgvw, &minvw, &maxvw);

  /* print status */
  printf("  - nb vertices = %d\n", g->nvtxs);
  printf("  - nb arcs = %d\n", g->narcs);
  printf("  - nb parts = %d\n", nparts);
  printf("  - nb disconnected islands = %d\n", MAX(nbislands - nparts, 0));
  printf("  - edgecut = %d\n", edgecut);
  printf("  - avg degree = %.2f, min degree = %d, max degree = %d\n", avgdegree, mindegree, maxdegree);
  printf("  - avg ew = %.2f, min ew = %.2f, max ew = %.2f\n", avgew, minew, maxew);
  printf("  - avg vw = %.2f, min vw = %.2f, max vw = %.2f\n", avgvw, minvw, maxvw);
  printf("  - part weights = [");
  for (int k = 0; k < nparts; k++) printf("%d ", (int)wparts[k]);
  printf("]\n");
  printf("  - part weights in %% = [");
  for (int k = 0; k < nparts; k++) printf("%.1f ", wparts100[k]);
  printf("]\n");
  printf("  - total weight = %d\n", (int)round(wtot));
  printf("  - mean weight = %d (%.1f %%)\n", (int)round(mean), mean100);
  printf("  - max unbalance = %.2f %% (%.2f %%)\n", (maxub - 1.0) * 100, _maxub * 100.0);
  printf("  - standard deviation for weights = %d (%.1f %%)\n", (int)round(sd), sd100);
  printf("  - edgecut / part = [");
  for (int k = 0; k < nparts; k++) printf("%d ", commsum[k]);
  printf("]\n");

  /* free */
  free(comm);
  free(commsum);
}

/* *********************************************************** */

static void computeIntracomm(const LibGraph_Hypergraph* hg, int nparts, const int* part, int* intracomm)
{
  for (int snd = 0; snd < nparts; snd++) {
    for (int rcv = 0; rcv < nparts; rcv++) intracomm[snd * nparts + rcv] = 0;
  }

  /* for all hyperedges */
  for (int i = 0; i < hg->nhedges; i++) {
    int procs[nparts]; /* > 0 if k-th proc shares this hyperedge */
    for (int k = 0; k < nparts; k++) procs[k] = 0;
    for (int j = hg->eptr[i]; j < hg->eptr[i + 1]; j++) {
      int vtx = hg->eind[j];
      int proc = part[vtx];
      procs[proc]++;
    }
    int nbprocs = 0; /* nb procs that shares this hyperedge */
    for (int k = 0; k < nparts; k++) {
      if (procs[k] > 0) nbprocs++;
    }
    int wgts = (hg->hewgts == NULL) ? 1 : hg->hewgts[i];

    /* Part external degree */
    int l = part[hg->eind[hg->eptr[i]]];
    for (int k = 0; k < nparts; k++) {
      if (procs[k] > 0) {
        if (k != l) { /* k does not own the center of the hyperedge */
          intracomm[k * nparts + l] += wgts;
          intracomm[l * nparts + k] += wgts;
        }
      }
    }
  }
}

/* *********************************************************** */

void LibGraph_printHypergraphDiagnostic(const LibGraph_Hypergraph* hg, int nparts, const int* part)
{
  /* part weights */

  double wparts[nparts];
  double wtot = 0.0;
  for (int k = 0; k < nparts; k++) wparts[k] = 0.0;
  for (int i = 0; i < hg->nvtxs; i++) {
    int w = (hg->vwgts != NULL) ? hg->vwgts[i] : 1;
    wparts[part[i]] += w;
    wtot += w;
  }

  /* mean, standard deviation */

  double mean = 0.0, sd = 0.0;
  for (int k = 0; k < nparts; k++) mean += wparts[k];
  mean /= nparts;

  for (int k = 0; k < nparts; k++) sd += (mean - wparts[k]) * (mean - wparts[k]);
  sd = sqrt(sd);

  /* communication volume */

  int comm[nparts][nparts]; /* comm[snd][rcv] = cost */
  int commvol = 0;
  int commtab[nparts];
  int commcnt[nparts];
  for (int snd = 0; snd < nparts; snd++) {
    commtab[snd] = 0;
    for (int rcv = 0; rcv < nparts; rcv++) comm[snd][rcv] = 0;
  }

  /* for all hyperedges */
  for (int i = 0; i < hg->nhedges; i++) {
    int procs[nparts]; /* > 0 if k-th proc shares this hyperedge */
    for (int k = 0; k < nparts; k++) procs[k] = 0;
    for (int j = hg->eptr[i]; j < hg->eptr[i + 1]; j++) {
      int vtx = hg->eind[j];
      int proc = part[vtx];
      procs[proc]++;
    }
    int nbprocs = 0; /* nb procs that shares this hyperedge */
    for (int k = 0; k < nparts; k++) {
      if (procs[k] > 0) nbprocs++;
    }
    int wgts = (hg->hewgts == NULL) ? 1 : hg->hewgts[i];

    /* lambda-1 connectivity */
    commvol += (nbprocs - 1) * wgts;

    /* Part external degree */
    int l = part[hg->eind[hg->eptr[i]]];
    for (int k = 0; k < nparts; k++) {
      if (procs[k] > 0) {
        if (k == l) /* k owns the center of the hyperedge */
          commtab[k] += (nbprocs - 1) * wgts;
        else {
          commtab[k] += wgts;
          comm[k][l] += wgts;
          comm[l][k] += wgts;
        }
      }
    }

    /*
      for(int k = 0 ; k < nparts ; k++) {
      for(int l = 0 ; l < nparts ; l++) {
      if (k != l && procs[k] > 0 && procs[l] > 0)
      comm[k][l] += (hg->hewgts == NULL)?1:hg->hewgts[i];
      }
      }*/
  }

  /* communication count */
  for (int k = 0; k < nparts; k++) { /* sender */
    commcnt[k] = 0;
    for (int l = 0; l < nparts; l++) { /* receiver */
      if (comm[k][l] > 0) commcnt[k]++;
    }
  }

  /* mean, standard deviation */

  double commmean = 0.0, commsd = 0.0;
  for (int k = 0; k < nparts; k++) commmean += commtab[k];
  commmean /= nparts;

  for (int k = 0; k < nparts; k++) commsd += (commmean - commtab[k]) * (commmean - commtab[k]);
  commsd = sqrt(commsd);

  /* lambda-1 edgecut */
  int edgecut = commvol;

  /* print status */
  printf("  - nb vertices = %d\n", hg->nvtxs);
  printf("  - nb hyperedges = %d\n", hg->nhedges);
  printf("  - nb parts = %d\n", nparts);
  printf("  - total weight = %d\n", (int)round(wtot));
  printf("  - mean part weight = %d (%.1f %%)\n", (int)round(mean), (mean * 1.0 / wtot) * 100.0);
  printf("  - sd part weight = %d (%.1f %%)\n", (int)round(sd), (sd * 1.0 / wtot) * 100.0);
  printf("  - load balance in %% = [");
  for (int k = 0; k < nparts; k++) printf("%.1f ", (wparts[k] * 1.0 / wtot) * 100.0);
  printf("]\n");
  printf("  - edge cut (l-1) = %d\n", edgecut);
  printf("  - total comm volume = %d\n", commvol);
  printf("  - mean comm = %d (%.1f %%)\n", (int)round(commmean), (commmean * 1.0 / commvol) * 100.0);
  printf("  - sd comm = %d (%.1f %%)\n", (int)round(commsd), (commsd * 1.0 / commvol) * 100.0);
  printf("  - nb comms = [");
  for (int k = 0; k < nparts; k++) printf("%d ", commcnt[k]);
  printf("]\n");
  printf("  - comm balance in %% = [");
  for (int k = 0; k < nparts; k++) printf("%.1f (%d) ", (commtab[k] * 1.0 / commvol) * 100.0, commtab[k]);
  printf("]\n");
  printf("  - comm scheme = [");
  for (int k = 0; k < nparts; k++)
    for (int l = 0; l < nparts; l++)
      if (comm[k][l] > 0) printf("%d-%d (%d) ", k, l, comm[k][l]);
  printf("]\n");

  // communication matrix
  int intracomms[nparts][nparts];
  computeIntracomm(hg, nparts, part, (int*)intracomms);
  printf("  - comm matrix =\n");
  for (int i = 0; i < nparts; i++) {
    if (i == 0)
      printf("    [ ");
    else
      printf("      ");
    for (int j = 0; j < nparts; j++) printf("%d ", intracomms[i][j]);
    if (i == nparts - 1)
      printf("]\n");
    else
      printf("\n");
  }
}

/* *********************************************************** */

int LibGraph_computeHypergraphEdgeCut(const LibGraph_Hypergraph* hg, int nparts, const int* part)
{
  int cut = 0;
  /* for all hyperedges */
  for (int i = 0; i < hg->nhedges; i++) {
    int procs[nparts]; /* > 0 if k-th proc shares this hyperedge */
    for (int k = 0; k < nparts; k++) procs[k] = 0;
    for (int j = hg->eptr[i]; j < hg->eptr[i + 1]; j++) {
      int vtx = hg->eind[j];
      int proc = part[vtx];
      procs[proc]++;
    }
    int nbprocs = 0; /* nb procs that shares this hyperedge */
    for (int k = 0; k < nparts; k++) {
      if (procs[k] > 0) nbprocs++;
    }
    int wgts = (hg->hewgts == NULL) ? 1 : hg->hewgts[i];

    /* lambda-1 connectivity */
    cut += (nbprocs - 1) * wgts;
  }

  return cut;
}

/* *********************************************************** */

int LibGraph_computeGraphEdgeCut(const LibGraph_Graph* g, int nparts, const int* part)
{
  int edgecut = 0;
  /* for all edges e=(i,ii) */
  for (int i = 0; i < g->nvtxs; i++) {
    for (int j = g->xadj[i]; j < g->xadj[i + 1]; j++) {
      int ii = g->adjncy[j];
      if (part[i] != part[ii]) edgecut += (g->ewgts == NULL) ? 1 : g->ewgts[j];
    }
  }
  return edgecut / 2;
}

/* *********************************************************** */

double LibGraph_computeGraphMaxUnbalance(const LibGraph_Graph* g, int nparts, const int* part)
{
  // compute the real maxnparts
  int maxpartnum = 0;
  for (int i = 0; i < g->nvtxs; i++)
    if (part[i] > maxpartnum) maxpartnum = part[i];
  int maxnparts = maxpartnum + 1;
  // assert(nparts <= maxnparts);

  // compute real part weights
  int wparts[maxnparts];
  int wtot = 0;
  for (int k = 0; k < maxnparts; k++) wparts[k] = 0;
  for (int i = 0; i < g->nvtxs; i++) {
    if (part[i] != -1) {  // ignore free vertices for incomplete partition
      int w = g->vwgts ? g->vwgts[i] : 1;
      wparts[part[i]] += w;
      wtot += w;
    }
  }

  // compute the real nb parts
  int nrealparts = 0;
  for (int k = 0; k < maxnparts; k++)
    if (wparts[k] > 0) nrealparts++;

  // assert(nparts == nrealparts); // check
  assert(nrealparts <= nparts);  // for naive cpl phase

  int maxpart = 0;
  for (int k = 0; k < maxnparts; k++)
    if (wparts[k] > maxpart) maxpart = wparts[k];
  double maxub = maxpart / (wtot * 1.0 / nrealparts);  // nrealparts or nparts ???
  maxub -= 1.0;

  // printf("nparts = %d, nrealparts = %d, maxpartnum = %d, maxub = %.2f\n", nparts, nrealparts,
  // maxpartnum, maxub);

  if (maxub < 0.0) return 0.0;
  // if(!isnormal(maxub)) return 1.0;
  // if(maxnparts > nparts) return  1.0;
  // if(nrealparts != nparts) return 1.0;

  return maxub;
}

/* *********************************************************** */

void LibGraph_computeGraphMinMaxUnbalance(const LibGraph_Graph* g, int nparts, const int* part, double* minub,
                                          double* maxub)
{
  // check part is regular
  for (int i = 0; i < g->nvtxs; i++) assert(part[i] >= 0 && part[i] < nparts);

  // compute part weights
  int wparts[nparts];
  int wtot = 0;
  for (int k = 0; k < nparts; k++) wparts[k] = 0;
  for (int i = 0; i < g->nvtxs; i++) {
    int w = g->vwgts ? g->vwgts[i] : 1;
    wparts[part[i]] += w;
    wtot += w;
  }

  int maxpart = 0, minpart = INT_MAX;
  for (int k = 0; k < nparts; k++) {
    if (wparts[k] > maxpart) maxpart = wparts[k];
    if (wparts[k] < minpart) minpart = wparts[k];
  }

  if (maxub) {
    *maxub = (maxpart / (wtot * 1.0 / nparts)) - 1.0;
    if (*maxub < 0.0) *maxub = 0.0;
  }
  if (minub) {
    *minub = 1.0 - (minpart / (wtot * 1.0 / nparts));
    if (*minub < 0.0) *minub = 0.0;
  }
}

/* *********************************************************** */

void LibGraph_computeGraphMinMaxDegree(const LibGraph_Graph* g, double* avgdegree, int* mindegree, int* maxdegree)
{
  *avgdegree = g->narcs / g->nvtxs;
  *maxdegree = INT_MIN;
  *mindegree = INT_MAX;

  /* for all edges e=(i,ii) */
  for (int i = 0; i < g->nvtxs; i++) {
    int degree = g->xadj[i + 1] - g->xadj[i];
    assert(degree >= 0);
    if (degree > *maxdegree) *maxdegree = degree;
    if (degree < *mindegree) *mindegree = degree;
  }
}

/* *********************************************************** */

void LibGraph_computeGraphMinMaxEdgeWeight(const LibGraph_Graph* g, double* avgew, double* minew, double* maxew)
{
  *avgew = 0.0;
  *maxew = INT_MIN;
  *minew = INT_MAX;

  /* for all edges e=(i,ii) */
  for (int i = 0; i < g->nvtxs; i++)
    for (int j = g->xadj[i]; j < g->xadj[i + 1]; j++) {
      // int ii =  g->adjncy[j]; /* edge (i,ii) */
      int ew = g->ewgts ? g->ewgts[j] : 1;
      assert(ew >= 0);
      if (ew > *maxew) *maxew = ew;
      if (ew < *minew) *minew = ew;
      *avgew += ew;
    }

  *avgew = *avgew / g->narcs;
}

/* *********************************************************** */

void LibGraph_computeGraphMinMaxVertexWeight(const LibGraph_Graph* g, double* avgvw, double* minvw, double* maxvw)
{
  *avgvw = 0.0;
  *maxvw = INT_MIN;
  *minvw = INT_MAX;

  /* for all edges e=(i,ii) */
  for (int i = 0; i < g->nvtxs; i++) {
    int vw = g->vwgts ? g->vwgts[i] : 1;
    assert(vw >= 0);
    if (vw > *maxvw) *maxvw = vw;
    if (vw < *minvw) *minvw = vw;
    *avgvw += vw;
  }

  *avgvw /= g->nvtxs;
}

/* *********************************************************** */

int LibGraph_computeFillIn(const LibGraph_Graph* g, /**< [in] graph struct */
                           const int* perm,         /**< [in] permutation array (or NULL) */
                           LibGraph_Graph* gfill    /**< [inout] fill-in graph struct (or NULL)*/
)
{
  assert(g);
  // it is not required for g to be sorted...

  int nvtxs = g->nvtxs;
  int fillin = 0;

  // init adjacency lists
  LibGraph_List** adjlist = malloc(nvtxs * sizeof(LibGraph_List*));
  int maxdegree = 0;
  int maxlistsize = 0;

  for (int i = 0; i < nvtxs; i++) {
    int ii = perm ? perm[i] : i;
    // printf("column %d -> perm column %d\n", i, ii);
    adjlist[ii] = malloc(sizeof(LibGraph_List));
    int degree = g->xadj[i + 1] - g->xadj[i];
    if (degree > maxdegree) maxdegree = degree;
    LibGraph_initList(adjlist[ii], degree);

    for (int k = g->xadj[i]; k < g->xadj[i + 1]; k++) {
      int j = g->adjncy[k]; /* edge (i,j) */
      int jj = perm ? perm[j] : j;
      // printf(" * edge (%d,%d) -> perm edge (%d,%d)\n", i, j, ii, jj);
      if (ii < jj) LibGraph_insertList(adjlist[ii], jj);
    }
    if (adjlist[ii]->size > maxlistsize) maxlistsize = adjlist[ii]->size;
    LibGraph_sortList(adjlist[ii]);  // n.log(n) complexity (required only if g not sorted)
  }

  // debug
  /* for(int i = 0 ; i < nvtxs ; i++) { */
  /*   printf("adjlist[%d]: ", i);      */
  /*   printList(adjlist[i]); */
  /* } */

  // printf("max degree = %d, max list size = %d\n", maxdegree, maxlistsize);

  // compute fill-in
  LibGraph_List* tmplist = malloc(sizeof(LibGraph_List));
  LibGraph_initList(tmplist, maxlistsize);
  for (int i = 0; i < nvtxs; i++) {      /* for all column */
    if (adjlist[i]->size < 2) continue;  // no fill-in
    int j0 = adjlist[i]->array[0];       // first non-zero element
    // int maxfillin =  adjlist[i]->size - 1;
    for (int k = 1; k < adjlist[i]->size; k++) {
      int j = adjlist[i]->array[k]; /* edge (i,j) */
      /* add possible fill-in for edge (j0,j) in tmplist */
      LibGraph_insertList(tmplist, j);  // already sorted !
    }

    LibGraph_List* newlist = malloc(sizeof(LibGraph_List));
    LibGraph_mergeSortedList(adjlist[j0], tmplist, newlist, 0);  // linear complexity
    fillin += (newlist->size - adjlist[j0]->size);
    LibGraph_freeList(adjlist[j0]);
    free(adjlist[j0]);
    LibGraph_resetList(tmplist);
    adjlist[j0] = newlist;
  }

  LibGraph_freeList(tmplist);
  free(tmplist);

  // generate fill-in graph
  if (gfill) {
    // compute symmetric fill-in in lower triangular matrix
    LibGraph_List** invadjlist = malloc(nvtxs * sizeof(LibGraph_List*));
    for (int i = 0; i < nvtxs; i++) {
      invadjlist[i] = malloc(sizeof(LibGraph_List));
      LibGraph_initList(invadjlist[i], maxlistsize);  // really maxlistsize ?
    }

    for (int i = 0; i < nvtxs; i++) {
      for (int k = 0; k < adjlist[i]->size; k++) {
        int j = adjlist[i]->array[k];          /* edge (i,j) */
        LibGraph_insertList(invadjlist[j], i); /* already sorted */
      }
    }

    // merge edges of both lower and upper triangular matrices
    for (int i = 0; i < nvtxs; i++) {
      LibGraph_List* newlist = malloc(sizeof(LibGraph_List));
      LibGraph_mergeSortedList(adjlist[i], invadjlist[i], newlist, 0);  // linear complexity
      LibGraph_freeList(adjlist[i]);
      free(adjlist[i]);
      LibGraph_freeList(invadjlist[i]);
      free(invadjlist[i]);
      adjlist[i] = newlist;
    }

    free(invadjlist);

    // debug
    /* for(int i = 0 ; i < nvtxs ; i++) { */
    /*   printf("adjlist[%d]: ", i);      */
    /*   printList(adjlist[i]); */
    /* } */

    // generate the fill-in graph
    gfill->nvtxs = nvtxs;
    gfill->narcs = 0;
    for (int i = 0; i < nvtxs; i++) gfill->narcs += adjlist[i]->size;
    gfill->xadj = malloc((gfill->nvtxs + 1) * sizeof(int));
    gfill->adjncy = malloc(gfill->narcs * sizeof(int));
    assert(gfill->xadj && gfill->adjncy);
    gfill->vwgts = NULL;
    gfill->ewgts = NULL;

    int kk = 0;
    for (int i = 0; i < nvtxs; i++) {
      gfill->xadj[i] = kk;
      for (int k = 0; k < adjlist[i]->size; k++) {
        int j = adjlist[i]->array[k]; /* edge (i,j) */
        gfill->adjncy[kk] = j;
        kk++;
      }
    }
    gfill->xadj[nvtxs] = kk; /* end */
  }

  // free mem
  for (int i = 0; i < nvtxs; i++) {
    LibGraph_freeList(adjlist[i]);
    free(adjlist[i]);
  }
  free(adjlist);

  return fillin;
}

/* *********************************************************** */
