#ifndef LIBGRAPH_GRID_H
#define LIBGRAPH_GRID_H

#include "mesh.h"

void LibGraph_generateGrid2D(int width, int height, double z0, double cellsizeX, double cellsizeY,
                             LibGraph_Mesh* grid); /* [out] */

void LibGraph_generateGrid3D(int dimX, int dimY, int dimZ, double cellsizeX, double cellsizeY, double cellsizeZ,
                             LibGraph_Mesh* grid); /* [out] */

#endif
