#ifndef LIBGRAPH_IO_H
#define LIBGRAPH_IO_H

#include "array.h"
#include "distgraph.h"
#include "graph.h"
#include "hybridmesh.h"
#include "hypergraph.h"
#include "mesh.h"

//@{

/** Load graph and/or mesh in the desired format ("metis", "scotch", "mm", "rb", "mesh") */
int LibGraph_loadGraphMesh(const char* filename, const char* coordfile, const char* format, LibGraph_Graph** pg,
                           LibGraph_Mesh** pm);

/* ******* GRAPH ******* */
int LibGraph_loadParallelGraph(const char* filename,    /**< [in] input filename */
                               const char* coordfile,   /**< [in] coord filename */
                               const char* format,      /**< [in] format of input file */
                               int prank,               /**< [in] process rank */
                               int psize,               /**< [in] number of processes */
                               LibGraph_DistGraph** pg, /**< [out] dist graph */
                               double** pcoords         /**< [out] pointer to table of coords */
);

/** Load graph (Metis format). */
int LibGraph_loadMetisGraph(const char* input, /**< [in] input filename */
                            LibGraph_Graph* g  /**< [out] Graph struct */
);

/** Load graph in parallel (Metis format). */
int LibGraph_loadParallelMetisGraph(const char* input, int prank, int psize, LibGraph_DistGraph* g);

/** Load graph (Rutherford-Boeing Sparse Matrix format). */
int LibGraph_loadRBGraph(const char* input, /**< [in] input filename */
                         LibGraph_Graph* g  /**< [out] Graph struct */
);

/** Load graph (Scotch format, .grf or .src). */
int LibGraph_loadScotchGraph(const char* input, /**< [in] input filename */
                             LibGraph_Graph* g  /**< [out] Graph struct */
);

/** Load Scotch coordinates (Scotch .xyz format). */
int LibGraph_loadScotchCoordinates(const char* infile, /**< [in] input filename */
                                   int n,              /**< [in] partition length */
                                   double* coords      /**< [in/out] coordinate array to fill */
);

/** Save graph (Metis format). */
int LibGraph_saveGraph(const char* output,     /**< [in] output filename (Metis format) */
                       const LibGraph_Graph* g /**< [in] Graph struct */
);

/** Load graph (Scotch format, .grf .xyz and .map). */
int LibGraph_saveScotchGraph(const char* basename,    /**< [in] output file basename */
                             const LibGraph_Graph* g, /**< [in] Graph struct */
                             int nparts,              /**< [in] nb of parts  */
                             const int* part          /**< [in] partition array  (of size g->nvtxs)  */
);

/** Save graph in Tulip format (.tlp) */
int LibGraph_saveTulipGraph(const char* output,      /**< [in] output filename (Tulip format) */
                            const LibGraph_Graph* g, /**< [in] Graph struct */
                            char labels[][255],      /**< [in] Vertex labels (of size g->nvtxs) */
                            float coords[][3]        /**< [in] Vertex coords (of size g->nvtxs) */
);

/** Save bipartite graph in Tulip format with our own layout (.tlp) */
int LibGraph_saveTulipBigraph(const char* output,     /**< [in] output filename (Tulip format) */
                              int nvtxsA,             /**< [in] nb vertex in part A */
                              int nvtxsB,             /**< [in] nb vertex in part B */
                              const LibGraph_Graph* g /**< [in] Graph struct */
);

/* ******* HYPERGRAPH ******* */

/** Load Hypergraph (custom format) */
int LibGraph_loadHypergraph(const char* file,       /**< [in] input file name */
                            LibGraph_Hypergraph* hg /**< [out] Hypergraph struct */
);

/** Load Hypergraph (ISPD98 Circuit Benchmark Suite) */
int LibGraph_loadISPDHypergraph(const char* netfile, const char* wfile, LibGraph_Hypergraph* hg);

/** Load hypergraph (Rutherford-Boeing Sparse Matrix format). */
int LibGraph_loadRBHypergraph(const char* input,      /**< [in] input filename */
                              LibGraph_Hypergraph* hg /**< [out] Hypergraph struct */
);

/** Save Hypergraph (custom format) */
void LibGraph_saveHypergraph(const char* file,             /**< [in] output file name */
                             const LibGraph_Hypergraph* hg /**< [in] Hypergraph struct */
);

/** Save hypergraph (HMetis format). */
void LibGraph_saveHMetisHypergraph(const char* output,           /**< [in] output filename */
                                   const LibGraph_Hypergraph* hg /**< [in] Hypergraph struct */
);

/* ******* MESH ******* */

/** Load mesh (internal text format). */
int LibGraph_loadMesh(const char* input,   /**< [in] input filename (simple text format) */
                      LibGraph_Mesh* mesh, /**< [out] Mesh struct */
                      bool no_nodes        /**< [in] set if nodes should NOT be allocated. 0 by default */
);

/** Save mesh (internal text format). */
int LibGraph_saveMesh(const char* output,       /**< [in] output filename */
                      const LibGraph_Mesh* mesh /**< [in] Mesh struct */
);

/** Load mesh (VTK format v2.0, line, triangle or quadrangle). */
int LibGraph_loadVTKMesh(const char* input,  /**< [in] input filename */
                         LibGraph_Mesh* mesh /**< [out] Mesh struct */
);

/** Load mesh (MSH format v2.0, triangle only) */
int LibGraph_loadMSHMesh(const char* input,       /**< [in] input filename  */
                         const char* material_id, /**< [in] variable ID for mesh material    */
                         LibGraph_Mesh* mesh      /**< [out] Mesh struct */
);

/** Save mesh in VTK format v2.0 */
int LibGraph_saveVTKMesh(const char* output,        /**< [in] output filename */
                         const LibGraph_Mesh* mesh, /**< [in] Mesh struct */
                         const LibGraph_Variables* vars);

/** Save graph in VTK format */
void LibGraph_saveVTKGraph(const char* vtkfile, const LibGraph_Graph* graph, const double* coords);

/** Save graph in SVG format */
void LibGraph_saveSVGGraph(const char* svgfile, const LibGraph_Graph* graph, const double* coords, int vtag, int etag);

/** Save graph partition in SVG format */
void LibGraph_saveSVGGraphPart(const char* svgfile, const LibGraph_Graph* graph, const double* coords, int nparts,
                               const int* part, int vtag, int etag);

/* ******* MISC ******* */

#ifdef LIBGRAPH_USE_PNG

/** Save sparse matrix (graph) as PNG image */
int LibGraph_saveMatrixImage(const char* output,          /**< [in] output filename (PNG format) */
                             int size,                    /**< [in] square image size (or 0 if size = black->nvtxs) */
                             const LibGraph_Graph* red,   /**< [in] red graph (or NULL)  */
                             const LibGraph_Graph* green, /**< [in] green graph (or NULL)  */
                             const LibGraph_Graph* blue   /**< [in] blue graph (or NULL)  */
);

#endif

/** Save partition as text file. */
void LibGraph_savePartition(int n,              /**< [in] partition length */
                            const int* parts,   /**< [in] partition array (of size n) */
                            const char* outfile /**< [in] output filename */
);

/** Load partition as text file. */
void LibGraph_loadPartition(const char* infile, /**< [in] input filename */
                            int n,              /**< [in] partition length */
                            int* parts          /**< [in/out] partition array to fill */
);

/** Load 3D double-precision coordinates as text file. */
int LibGraph_loadCoordinates(const char* infile, /**< [in] input filename */
                             int n,              /**< [in] nb of coordinates */
                             double* coords      /**< [in] array of 3d coordinates */
);

/** Save 3D double-precision coordinates as text file. */
int LibGraph_saveCoordinates(const char* outfile, /**< [in] input filename */
                             int n,               /**< [in] nb of coordinates */
                             const double* coords /**< [in] array of 3d coordinates */
);

/** Load 3D double-precision coordinates as text file. */
int LibGraph_loadParallelCoordinates(const char* infile,              /**< [in] input filename */
                                     const LibGraph_DistGraph* graph, /**< [in] distgraph */
                                     double* coords                   /**< [in] array of 3d coordinates */
);

/** Load hybridmesh from different files */
int LibGraph_loadHybridMeshFiles(LibGraph_HybridMesh* hm, bool shared_nodes, int nb_components, ...);

/** Load hybridmesh from one file (that contain the info for hybrid mesh) */
int LibGraph_loadHybridMesh(LibGraph_HybridMesh** phm, bool shared_nodes, const char* infile);

/** Save hybridmesh (internal text format). */
int LibGraph_saveHybridMesh(const char* output,           /**< [in] output filename */
                            const LibGraph_HybridMesh* hm /**< [in] HybridMesh struct */
);

/** Save hybridmesh in VTK format v2.0 */
int LibGraph_saveVTKHybridMesh(const char* output,           /**< [in] output filename */
                               const LibGraph_HybridMesh* hm /**< [in] HybridMesh struct */
);

//@}

#endif
