#include <assert.h>
#include <stdlib.h>

#include "grid.h"

/* *********************************************************** */

#define INDEX2D(I, J, DX, DY) ((J) * (DX) + (I))
#define INDEX3D(I, J, K, DX, DY, DZ) ((K) * (DX) * (DY) + (J) * (DX) + (I))

/* *********************************************************** */

void LibGraph_generateGrid2D(int width, int height, double z0, double cellsizeX, double cellsizeY, LibGraph_Mesh* grid) /* [out] */
{
  int elmsize = LIBGRAPH_ELEMENT_SIZE[LIBGRAPH_QUADRANGLE];

  /* initialization */
  grid->elm_type = LIBGRAPH_QUADRANGLE;
  grid->nb_nodes = (width + 1) * (height + 1);
  grid->nb_cells = width * height;
  grid->cells = malloc(grid->nb_cells * elmsize * sizeof(int));
  grid->nodes = malloc(grid->nb_nodes * 3 * sizeof(double));
  // grid->nb_node_vars = 0;
  // grid->nb_cell_vars = 0;
  // grid->node_vars = NULL;
  // grid->cell_vars = NULL;

  /*
    The k-th cell, that connects nodes {a,b,c,d}

    a---b
    | k |
    d---c

  */

  int idx = 0;

  /* for all cells */
  for (int j = 0; j < height; j++)
    for (int i = 0; i < width; i++) {
      /* cell index */
      int k = INDEX2D(i, j, width, height);
      assert(k == idx);
      /* node indices */
      int a = INDEX2D(i, j, width + 1, height + 1);
      int b = INDEX2D(i + 1, j, width + 1, height + 1);
      int c = INDEX2D(i + 1, j + 1, width + 1, height + 1);
      int d = INDEX2D(i, j + 1, width + 1, height + 1);
      grid->cells[elmsize * k + 0] = a;
      grid->cells[elmsize * k + 1] = b;
      grid->cells[elmsize * k + 2] = c;
      grid->cells[elmsize * k + 3] = d;
      idx++;
    }

  /* for all nodes of code  */
  for (int j = 0; j < height + 1; j++)
    for (int i = 0; i < width + 1; i++) {
      int k = j * (width + 1) + i;
      grid->nodes[3 * k + 0] = i * cellsizeX; /* x */
      grid->nodes[3 * k + 1] = j * cellsizeY; /* y */
      grid->nodes[3 * k + 2] = z0;            /* z */
    }
}

/* *********************************************************** */

void LibGraph_generateGrid3D(int dimX, int dimY, int dimZ, double cellsizeX, double cellsizeY, double cellsizeZ, LibGraph_Mesh* grid)
{
  int elmsize = LIBGRAPH_ELEMENT_SIZE[LIBGRAPH_HEXAHEDRON];

  /* initialization */
  grid->elm_type = LIBGRAPH_HEXAHEDRON;
  grid->nb_nodes = (dimX + 1) * (dimY + 1) * (dimZ + 1);
  grid->nb_cells = dimX * dimY * dimZ;
  grid->cells = malloc(grid->nb_cells * elmsize * sizeof(int));
  grid->nodes = malloc(grid->nb_nodes * 3 * sizeof(double));
  // grid->nb_node_vars = 0;
  // grid->nb_cell_vars = 0;
  // grid->node_vars = NULL;
  // grid->cell_vars = NULL;

  /*
   * The k-th cell, that connects nodes {a,b,c,d,e,f,g,h}
   *
   *    e---f
   *  a---b
   *  | h | g
   *  d---c
   *
   *  lower corner : a
   *  upper corner : g
   *
   *    -y
   *    ^
   *    |
   *    ---> +x
   *   /
   *  -z
   */

  int idx = 0;

  /* for all cells (x,y,z) */
  for (int z = 0; z < dimZ; z++)
    for (int y = 0; y < dimY; y++)
      for (int x = 0; x < dimX; x++) {
        /* cell index */
        int k = INDEX3D(x, y, z, dimX, dimY, dimZ);
        assert(k == idx);
        /* node indices */
        int a = INDEX3D(x, y, z, dimX + 1, dimY + 1, dimZ + 1);
        int b = INDEX3D(x + 1, y, z, dimX + 1, dimY + 1, dimZ + 1);
        int c = INDEX3D(x + 1, y + 1, z, dimX + 1, dimY + 1, dimZ + 1);
        int d = INDEX3D(x, y + 1, z, dimX + 1, dimY + 1, dimZ + 1);
        int e = INDEX3D(x, y, z + 1, dimX + 1, dimY + 1, dimZ + 1);
        int f = INDEX3D(x + 1, y, z + 1, dimX + 1, dimY + 1, dimZ + 1);
        int g = INDEX3D(x + 1, y + 1, z + 1, dimX + 1, dimY + 1, dimZ + 1);
        int h = INDEX3D(x, y + 1, z + 1, dimX + 1, dimY + 1, dimZ + 1);
        grid->cells[elmsize * k + 0] = a;
        grid->cells[elmsize * k + 1] = b;
        grid->cells[elmsize * k + 2] = c;
        grid->cells[elmsize * k + 3] = d;
        grid->cells[elmsize * k + 4] = e;
        grid->cells[elmsize * k + 5] = f;
        grid->cells[elmsize * k + 6] = g;
        grid->cells[elmsize * k + 7] = h;
        idx++;
      }

  /* for all nodes of code  */
  for (int z = 0; z < dimZ + 1; z++)
    for (int y = 0; y < dimY + 1; y++)
      for (int x = 0; x < dimX + 1; x++) {
        int k = INDEX3D(x, y, z, dimX + 1, dimY + 1, dimZ + 1);
        grid->nodes[3 * k + 0] = x * cellsizeX; /* x */
        grid->nodes[3 * k + 1] = y * cellsizeY; /* y */
        grid->nodes[3 * k + 2] = z * cellsizeZ; /* z */
      }
}

/* *********************************************************** */
