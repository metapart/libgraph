#ifndef LIBGRAPH_TOOLS_H
#define LIBGRAPH_TOOLS_H

#include "graph.h"
#include "hybridmesh.h"
#include "hypergraph.h"
#include "list.h"
#include "mesh.h"

//@{

/** Graph-to-line-mesh conversion (graph vertices are mapped to mesh nodes). */
void LibGraph_graph2LineMesh(const LibGraph_Graph* graph, /**< [in] input graph */
                             const double* coords,        /**< [in] input coordinate array (of size graph->nvtxs*3) */
                             LibGraph_Mesh* mesh          /**< [out] output mesh (with line cells) */
);

/* Build a mesh dual structure (dual[k] is the list of all cells sharing by the k-th node in mesh).
 */
void LibGraph_mesh2DualList(LibGraph_Mesh* mesh, /**< input mesh */
                            LibGraph_List* dual  /**< array of mesh->nb_nodes lists */
);

/** Line-Mesh-to-graph conversion (mesh nodes are mapped to graph vertices). */
void LibGraph_lineMesh2Graph(LibGraph_Mesh* mesh,  /**< [in] input line mesh */
                             LibGraph_Graph* graph /**< [out] graph */
);

/** Mesh-to-graph conversion (mesh cells are mapped to graph vertices). */
void LibGraph_mesh2Graph(LibGraph_Mesh* mesh,  /**< [in] input mesh */
                         LibGraph_Graph* graph /**< [out] dual graph */
);

/** HybridMesh-to-graph conversion ( hybridmesh cells are mapped to graph vertices). */
void LibGraph_hybridMesh2Graph(LibGraph_HybridMesh* hm, LibGraph_Graph* graph);

/** Convert a graph to binary dense matrix. */
void LibGraph_graph2Mat(LibGraph_Graph* g, int* mat);

/** Convert a edge-weighted graph to dense matrix (such as mat(i,j) = weight of edge (i,j)). */
void LibGraph_graph2MatW(LibGraph_Graph* g, int* mat);

/** Convert binary dense matrix to a graph. */
void LibGraph_mat2Graph(int nvtxs, int* mat, LibGraph_Graph* g);

/** Compute the laplacian matrix for an input graph. */
void LibGraph_laplacian(LibGraph_Graph* g, /**< [in] graph */
                        int* matrix        /**< [out] laplacian matrix of size |V|.|V| */
);

/** Mesh-to-hypergraph conversion. */
void LibGraph_mesh2Hypergraph(LibGraph_Mesh* mesh,      /* [in] input mesh */
                              LibGraph_Hypergraph* hg); /* [out] hypergraph */

/**
    Edge-oriented Graph-to-hypergraph conversion: one hyperedge per
    graph edge.
*/
void LibGraph_graph2HypergraphE(LibGraph_Graph* g,        /* [in]  graph */
                                LibGraph_Hypergraph* hg); /* [out] hypergraph */

/**
    Vertex-oriented Graph-to-hypergraph conversion: one hyperedge per
    graph vertex and its neighbors.
*/
void LibGraph_graph2HypergraphV(LibGraph_Graph* g,        /* [in]  graph */
                                LibGraph_Hypergraph* hg); /* [out] hypergraph */

/** Build a graph from an hypergraph by replacing hyperedges by cliques. */
void LibGraph_hypergraph2Graph(LibGraph_Hypergraph* hg, LibGraph_Graph* g);

/** Convert a graph to a list of matrix coordinates for non-zero elements. */
void LibGraph_graph2Coords(LibGraph_Graph* g, /**< [in] graph */
                           int* coords, /**< [out] array of edges (v,v') of size 2.|E| (sorted in increasing order) */
                           int* ewgts,  /**< [out] array of edge weights of size 2.|E| sorted in same order as coords
                                           (NULL  if not used) */
                           int* vwgts   /**< [out] array of vertex weights of size |V| (NULL if not used) */
);

/** Convert a list of matrix coordinates for non-zero elements to a graph. */
void LibGraph_coords2Graph(int nvtxs,        /**< [in] number of vertices |V| */
                           int nedges,       /**< [in] number of edges |E| */
                           int* coords,      /**< [in] array of edges (v,v') of size 2.|E| (sorted in increasing
                                                order) */
                           int* ewgts,       /**< [in] array of edge weights of size 2.|E| in same order than
                                                coords (NULL if not used) */
                           int* vwgts,       /**< [in] array of vertex weights of size |V| (NULL if not used) */
                           LibGraph_Graph* g /**< [out] graph */
);
/** remove duplicated coordinates and return the reallocated array adress */
void LibGraph_stripCoords(int* ncoords, /**< [inout] number of coords */
                          int** coords, /**< [inout] array of coords of size 2.ncoords (sorted in increasing order) */
                          int** ewgts   /**< [inout] array of edge weights of size 2.|E| in same order than coords (NULL
                                           if   not used) */
);

/** Add new vertices and migration edges according to the communication model
 * given by an hypergraph */
int LibGraph_addMigrationEdges(LibGraph_Graph* oldg,       /**< [in] old graph */
                               int* oldpart,               /**< [in] old partition */
                               LibGraph_Hypergraph* comms, /**< [in] communication hypergraph. Vertices are new
                                                     parts, hyperedges are old parts. */
                               int repartmult,             /**< [in] repartition multiplier */
                               int migcost,                /**< [in] new edges weight */
                               LibGraph_Graph* newg        /**< [out] new graph */
);

/* ********************** GEOM ********************** */

void LibGraph_cellCenter(const LibGraph_Mesh* msh, /* [in] input mesh */
                         int i,                    /* [in] cell index */
                         double G[3]);             /* [out] gravity center */

void LibGraph_cellCenters(const LibGraph_Mesh* msh, /* [in] input mesh */
                          double* centers);         /* [out] gravity centers (array of size msh->nb_cells*3) */

//@}

#endif
