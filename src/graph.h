#ifndef LIBGRAPH_GRAPH_H
#define LIBGRAPH_GRAPH_H

#include <stdbool.h>

//@{

/** Graph CSR structure. */
typedef struct _LibGraph_Graph
{
  int nvtxs;   /**< nb vertices */
  int narcs;   /**< nb arcs (= 2* nb egdges) */
  int* xadj;   /**< array of indirection on adjncy (of size nvtxs+1) */
  int* adjncy; /**< adjacency array of the CSR structure (of size 2*nedges) */
  int* vwgts;  /**< array vertex weights (of size nvtxs) */
  int* ewgts;  /**< array vertex edges (of size 2*nedges) */
} LibGraph_Graph;

/** Allocate new graph structure */
LibGraph_Graph* LibGraph_newGraph(void);

/** wrap graph structure */
void LibGraph_wrapGraph(LibGraph_Graph* g, /**< [in] input graph */
                        int nvtxs,         /**< [in] nb vertices */
                        int narcs,         /**< [in] nb arcs */
                        int* xadj,         /**< [in] array of indirection on adjncy (of size nvtxs+1) */
                        int* adjncy,       /**< [in] adjacency array of the CSR structure (of size narcs) */
                        int* vwgts,        /**< [in] array vertex weights (of size nvtxs) */
                        int* ewgts         /**< [in] array vertex edges (of size narcs) */
);

/** Free memory allocated within graph structure. */
void LibGraph_freeGraph(LibGraph_Graph* g);

/** Duplicate a graph. */
void LibGraph_dupGraph(LibGraph_Graph* oldg, LibGraph_Graph* newg);

/** Check graph and return if graph is valid. */
int LibGraph_checkGraph(LibGraph_Graph* graph, /**< [in] input graph */
                        int exitonerror        /**< [in] boolean to exit on error  */
);

/** Compare two graphs, return 1 if equal */
bool LibGraph_compareGraph(LibGraph_Graph* g1, LibGraph_Graph* g2);

/** Compute subgraph. */
void LibGraph_subGraphFromRange(LibGraph_Graph* g,     /* [in] graph */
                                int vtxstart,          /* [in] first vertex in range */
                                int vtxend,            /* [in] last vertex in range */
                                LibGraph_Graph* subg); /* [out] sub-graph (with only vertices in
                                                 range [vtxstart,vtxend]) */

/** Compute subgraph. */
void LibGraph_subGraphFromSet(const LibGraph_Graph* g, /* [in] graph */
                              int nvtxs,               /* [in] nb vertices in set */
                              const int* vtxs,         /* [in] vertex set */
                              LibGraph_Graph* subg);   /* [out] sub-graph (with only vertices in the vtxs set) */

/** Compute subgraph. */
void LibGraph_subGraphFromArray(const LibGraph_Graph* g,   /**< [in] graph */
                                const int* array,          /**< [in] boolean vertex array (of size g->nvtxs) */
                                LibGraph_Graph* subg /**< [out] sub-graph (with only vertices in the vtxs set) */
);

/** Given a bipartition, split a graph in two subgraphs. */
void LibGraph_splitGraph(const LibGraph_Graph* g, /**< [in] graph */
                         const int* part,         /**< [in] array of bi-partition (of size g->nvtxs) */
                         LibGraph_Graph* gLeft,   /**< [out] left sub-graph relative to part 0 */
                         LibGraph_Graph* gRight,  /**< [out] right sub-graph relative to part 1 */
                         int* map                 /**< [out] array of size g->nvtxs, mapping from global
                                                     to local vertex index */
);

/** Print graph structure. */
void LibGraph_printGraph(LibGraph_Graph* g /**< [in] input  graph to print */
);

/** Compute quotient graph. */
void LibGraph_quotientGraph(const LibGraph_Graph* input, /**< [in] input graph */
                            int nparts,                  /**< [in] nb of partitions */
                            const int* parts,      /**< [in] array of computed partitions (of size input->nvtxs) */
                            LibGraph_Graph* output /**< [out] quotient graph (with nparts vertices) */
);

/** create bipartite graph */
void LibGraph_createBigraph(int nvtxsA,       /**< [in] nb vertex in part A */
                            int nvtxsB,       /**< [in] nb vertex in part B */
                            int* edges,       /**< [in] matrix of edges of dimensions nvtxsA x nvtxsB */
                            LibGraph_Graph* g /**< [out] bipartite graph */
);

/** make graph out of all edges (iarray[k],jarray[k]) */
void LibGraph_makeGraph(int nvtxs, int nedges, int* iarray, /* array of vertices i (sorted in increasing order) */
                        int* jarray,                        /* array of vertices j */
                        int* ewgts, int* vwgts, LibGraph_Graph* g);

/** permute a graph. */
void LibGraph_permGraph(LibGraph_Graph* oldg, int* perm, LibGraph_Graph* newg);

/** Sort graph adjacency array. */
void LibGraph_sortGraph(LibGraph_Graph* g);

/** merge two graphs that share some vertices and edges (sum vertex/edge
 * weights) */
void LibGraph_mergeGraphs(LibGraph_Graph* gA,  /**< [in] input graph A */
                          LibGraph_Graph* gB,  /**< [in] input graph B */
                          int vwfA,            /**< [in] vertex weight factor for A */
                          int ewfA,            /**< [in] edge weight factor for A */
                          int vwfB,            /**< [in] vertex weight factor for B */
                          int ewfB,            /**< [in] edge weight factor for B */
                          LibGraph_Graph* newg /**< [out] output graph */
);

/* void mergeGraphs2(LibGraph_Graph* gA,               /\**< [in] input graph A *\/ */
/* 		  LibGraph_Graph* gB,               /\**< [in] input graph B *\/ */
/* 		  LibGraph_Graph* new               /\**< [out] output graph *\/ */
/* 		  ); */

/** Add some new vertices in a graph (without edges). */
void LibGraph_addGraphVertices(LibGraph_Graph* g, /**< [in] input graph */
                               int nnewvtxs,      /**< [in] number of new vertices to be added */
                               int w,             /**< [in] weight of new vertices to be added */
                               int endpos         /**< [in] if true (endpos=1), add new vertices at end, else at
                                                     start */
);

/** Shuffle vertex numbering of a graph. */
// void shuffleGraph(Graph * gold, Graph * gnew);

//@}

#endif
