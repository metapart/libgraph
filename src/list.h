#ifndef LIBGRAPH_LIST_H
#define LIBGRAPH_LIST_H

//@{

/* *********************************************************** */
/*                           LIST                              */
/* *********************************************************** */

/** List structure. */
typedef struct _LibGraph_List
{
  int size;   /**< nb elements */
  int max;    /**< max elements */
  int* array; /**< array of elements (of size max) */
} LibGraph_List;

/** Init list. */
void LibGraph_initList(LibGraph_List* l, int max);

/** Free list. */
void LibGraph_freeList(LibGraph_List* l);

/** Print list. */
void LibGraph_printList(LibGraph_List* l);

/** Reset list (without freeing memory). */
void LibGraph_resetList(LibGraph_List* l);

/** Insert an element in an integer list (realloc memory if required). */
void LibGraph_insertList(LibGraph_List* l, int element);

/** Quicksort routine for integer list. */
void LibGraph_sortList(LibGraph_List* l);

/** Find an element in a sorted integer list. Log complexity. Return -1 if not found. */
int LibGraph_findSortedList(LibGraph_List* l, int element);

/** Insert an element in a sorted integer list (realloc memory if required). Linear complexity. */
/* Return false if it tries to insert a duplicated element in unique list. */
int LibGraph_insertSortedList(LibGraph_List* l, int element, int duplicate);

/** Merge two sorted lists. Linear complexity. */
void LibGraph_mergeSortedList(LibGraph_List* la, LibGraph_List* lb, LibGraph_List* lc, int duplicate);

//@}

#endif
