#include <assert.h>
#include <errno.h>
#include <malloc.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>

#ifdef LIBGRAPH_USE_PNG
#include <png.h>
#endif

#include "debug.h"
#include "distgraph.h"
#include "graph.h"
#include "hybridmesh.h"
#include "io.h"
#include "mesh.h"
#include "mmio.h"
#include "svg.h"
#include "tools.h"

#define MAX_LINE_LENGTH 64 * 1024  // 64 Ko
#define MIN(a, b) (((a) <= (b)) ? (a) : (b))
#define MAX(a, b) (((a) >= (b)) ? (a) : (b))

#define TRUE 1
#define FALSE 0

/* *********************************************************** */

// enum InputFormat {MESH, METIS, SCOTCH, MM, RB };

int LibGraph_loadGraphMesh(const char* filename, const char* coordfile, const char* format, LibGraph_Graph** pg,
                           LibGraph_Mesh** pm)
{
  LibGraph_Graph* g = NULL;
  LibGraph_Mesh* m = NULL;
  if (!strcmp(format, "metis")) {
    g = LibGraph_newGraph();
    int r = LibGraph_loadMetisGraph(filename, g);
    if (!r) {
      fprintf(stderr, "ERROR: fail to load graph \"%s\" in \"%s\" format!\n", filename, format);
      free(g);
      return 0;
    }
    if (coordfile) {
      double* coords = malloc(3 * g->nvtxs * sizeof(double));
      assert(coords);
      r = LibGraph_loadCoordinates(coordfile, g->nvtxs, coords);
      if (!r) {
        fprintf(stderr, "ERROR: fail to load coordinates \"%s\" in \"%s\" format!\n", coordfile, format);
        free(m);
        return 0;
      }
      m = LibGraph_newMesh();
      LibGraph_graph2LineMesh(g, coords, m);
      free(coords);
    }
  }
  else if (!strcmp(format, "scotch")) {
    g = LibGraph_newGraph();
    int r = LibGraph_loadScotchGraph(filename, g);
    if (!r) {
      fprintf(stderr, "ERROR: fail to load graph \"%s\" in \"%s\" format!\n", filename, format);
      free(g);
      return 0;
    }
    if (coordfile) {
      double* coords = malloc(3 * g->nvtxs * sizeof(double));
      assert(coords);
      r = LibGraph_loadScotchCoordinates(coordfile, g->nvtxs, coords);
      if (!r) {
        fprintf(stderr, "ERROR: fail to load coordinates \"%s\" in \"%s\" format!\n", coordfile, format);
        free(m);
        return 0;
      }
      m = LibGraph_newMesh();
      LibGraph_graph2LineMesh(g, coords, m);
      free(coords);
    }
  }
  else if (!strcmp(format, "mm")) {
    g = LibGraph_newGraph();
    int r = LibGraph_loadMMGraph(filename, g);
    if (!r) {
      fprintf(stderr, "ERROR: fail to load graph \"%s\" in \"%s\" format!\n", filename, format);
      free(g);
      return 0;
    }
    if (coordfile) {
      double* coords = malloc(3 * g->nvtxs * sizeof(double));
      assert(coords);
      r = LibGraph_loadMMCoordinates(coordfile, g->nvtxs, coords);
      if (!r) {
        fprintf(stderr, "ERROR: fail to load coordinates \"%s\" in \"%s\" format!\n", coordfile, format);
        free(m);
        return 0;
      }
      m = LibGraph_newMesh();
      LibGraph_graph2LineMesh(g, coords, m);
      free(coords);
    }
  }
  else if (!strcmp(format, "rb")) {
    g = LibGraph_newGraph();
    int r = LibGraph_loadRBGraph(filename, g);
    if (!r) {
      fprintf(stderr, "fail to load graph \"%s\" in \"%s\" format!\n", filename, format);
      free(g);
      return 0;
    }
    if (coordfile) { /* load coordfile in MM format (?) */
      double* coords = malloc(3 * g->nvtxs * sizeof(double));
      assert(coords);
      r = LibGraph_loadMMCoordinates(coordfile, g->nvtxs, coords);
      if (!r) {
        fprintf(stderr, "ERROR: fail to load coordinates \"%s\" in \"mm\" format!\n", coordfile);
        free(m);
        return 0;
      }
      m = LibGraph_newMesh();
      LibGraph_graph2LineMesh(g, coords, m);
      free(coords);
    }
  }
  else if (!strcmp(format, "mesh")) {
    m = LibGraph_newMesh();
    g = LibGraph_newGraph();
    int r = LibGraph_loadMesh(filename, m, 0);
    if (!r) {
      fprintf(stderr, "ERROR: fail to load mesh \"%s\" in \"%s\" format!\n", filename, format);
      free(m);
      return 0;
    }
    LibGraph_mesh2Graph(m, g);
  }
  else {
    fprintf(stderr, "ERROR: unknown format \"%s\"!\n", format);
    return 0;
  }

  /* output */
  if (pg) *pg = g;
  if (pm) *pm = m;
  if (!pg && g) {
    LibGraph_freeGraph(g);
    free(g);
  }
  if (!pm && m) {
    LibGraph_freeMesh(m);
    free(m);
  }

  return 1;
}

int LibGraph_loadParallelGraph(const char* filename, const char* coordfile, const char* format, int prank, int psize,
                               LibGraph_DistGraph** pg, double** pcoords)
{
  LibGraph_DistGraph* g = NULL;
  if (!strcmp(format, "metis")) {
    g = LibGraph_newDistGraph();
    int r = LibGraph_loadParallelMetisGraph(filename, prank, psize, g);
    if (!r) {
      fprintf(stderr, "ERROR: fail to load graph \"%s\" in \"%s\" format!\n", filename, format);
      free(g);
      return 0;
    }
    if (coordfile) {
      double* coords = malloc(3 * g->nvtxs * sizeof(double));
      assert(coords);
      r = LibGraph_loadParallelCoordinates(coordfile, g, coords);
      if (!r) {
        fprintf(stderr, "ERROR: fail to load coordinates \"%s\" in \"%s\" format!\n", coordfile, format);
        free(coords);
        return 0;
      }
      *pcoords = coords;
    }
  }
  else {  // TODO implement other formats
    assert(0);
  }

  /* output */
  if (pg) *pg = g;
  if (!pg && g) {
    LibGraph_freeDistGraph(g);
    free(g);
  }

  return 1;
}
/* *********************************************************** */

int LibGraph_loadVTKMesh(const char* input, LibGraph_Mesh* mesh)
{
  mesh->elm_type = LIBGRAPH_TRIANGLE; /* triangle only */
  // mesh->nb_node_vars = 0;
  // mesh->nb_cell_vars = 0;
  // mesh->node_vars = NULL;
  // mesh->cell_vars = NULL;

  FILE* file;

  /* ouverture du fichier d'entree */
  if (NULL == (file = fopen(input, "r"))) {
    fprintf(stderr, "Opening file failed.\n");
    return 0;
  }

  /* considered section in the vtk file */
  enum Section_
  {
    MESH_FORMAT = 0,
    POINTS,
    CELLS,
    CELL_TYPES,
    NB_DE_SECTIONS
  };

  char* labels[NB_DE_SECTIONS];
  labels[MESH_FORMAT] = "DATASET";
  labels[POINTS] = "POINTS";
  labels[CELLS] = "CELLS";
  labels[CELL_TYPES] = "CELL_TYPES";

  char* ligne;
  char* temp;

  if (NULL == (ligne = malloc(MAX_LINE_LENGTH))) {
    fprintf(stderr, "Memory Allocation Failed for one ligne.\n");
    return 0;
  }

  int cas = -1, i;
  int nodeRank = 0;
  int cellRank = 0;
  int cpt_ligne = 1;
  int nodesAllocated = FALSE;
  int cellsAllocated = FALSE;

  /* Read the file */
  while (NULL != fgets(ligne, MAX_LINE_LENGTH, file)) {
    /* skip the ligne if /n */
    if (0 == strncmp("\n", ligne, 1)) {
      cpt_ligne++;
      continue;
    }

    /* find out in which section of the file we are reading */
    for (i = 0; i < NB_DE_SECTIONS; i++) {
      if (0 == strncmp(labels[i], ligne, strlen(labels[i]))) {
        cas = i;
        // printf("section %s detected\n", labels[i]);
        break;
      }
    }

    /* swith section do ... */
    switch (cas) {
      case POINTS:

        if (ligne != NULL) {
          // si on est en debut de section on recupere le nombre de noeuds sinon les coordonnees
          // noeuds
          int nb_nodes;
          if (0 == strncmp(labels[cas], ligne, strlen(labels[cas]))) {
            nb_nodes = strtol(ligne + strlen(labels[cas]) + 1, &temp, 0);
            // printf("nb_nodes= %d\n",nb_nodes);
            mesh->nb_nodes = nb_nodes;
          }
          else {
            // allocation of mesh->nodes only one time
            if (!nodesAllocated) {
              if (NULL == (mesh->nodes = malloc(mesh->nb_nodes * 3 * sizeof(double)))) {
                fprintf(stderr, "Memory Allocation Failed for nodes\n");
              }
              // printf("nodes allocated\n");
              nodesAllocated = TRUE;
            }
            // remplissage de mesh->nodes
            double x, y, z;
            sscanf(ligne, "%lf %lf %lf", &x, &y, &z);
            // printf("ligne%d x = %.9lf y = %.9lf z = %.9lf\n", cpt_ligne, x,y,z);
            mesh->nodes[3 * nodeRank] = x;
            mesh->nodes[3 * nodeRank + 1] = y;
            mesh->nodes[3 * nodeRank + 2] = z;
            nodeRank++;
          }
        }

        break;

      case CELLS:

        if (ligne != NULL) {
          // si on est en debut de section on recupere le nombre d'elements sinon les elements
          int nb_cells;
          if (0 == strncmp(labels[cas], ligne, strlen(labels[cas]))) {
            nb_cells = strtol(ligne + strlen(labels[cas]) + 1, &temp, 0);
            // printf("nb_cells= %d\n",nb_cells);
            mesh->nb_cells = nb_cells;
          }
          else {
            // allocation of mesh->cells only one time
            if (!cellsAllocated) {
              if (NULL == (mesh->cells = malloc(mesh->nb_cells * 3 * sizeof(int)))) {
                fprintf(stderr, "Memory Allocation Failed for cells\n");
              }
              cellsAllocated = TRUE;
            }
            // remplissage de mesh->cells
            int nbPts, nd1, nd2, nd3;
            sscanf(ligne, "%d %d %d %d", &nbPts, &nd1, &nd2, &nd3);
            if (nbPts == 3) {
              mesh->cells[3 * cellRank] = nd1;
              mesh->cells[3 * cellRank + 1] = nd2;
              mesh->cells[3 * cellRank + 2] = nd3;
              cellRank++;
            }
            else
              printf("WARNING : ELEMENT IS NOT A TRIANGLE !");
          }
        }

        break;
    }

    cpt_ligne++;
  }

  free(ligne);
  fclose(file);
  return 1;
}

/* *********************************************************** */

int LibGraph_loadMSHMesh(const char* input, const char* material_id, LibGraph_Mesh* mesh)
{
  mesh->elm_type = LIBGRAPH_TRIANGLE; /* triangle only */
  // mesh->nb_node_vars = 0;
  // mesh->nb_cell_vars = 0;
  // mesh->node_vars = NULL;
  // mesh->cell_vars = NULL;

  FILE* file;

  /* ouverture du fichier d'entree */
  if (NULL == (file = fopen(input, "r"))) {
    fprintf(stderr, "Opening file failed.\n");
    return 0;
  }

  /* considered section in the msh file */
  enum Section_
  {
    MESH_FORMAT = 0,
    END_MESH_FORMAT,
    NODES,
    END_NODES,
    ELEMENTS,
    END_ELEMENTS,
    NB_DE_SECTIONS
  };

  char* labels[NB_DE_SECTIONS];
  labels[MESH_FORMAT] = "$MeshFormat";
  labels[END_MESH_FORMAT] = "$EndMeshFormat";
  labels[NODES] = "$Nodes";
  labels[END_NODES] = "$EndNodes";
  labels[ELEMENTS] = "$Elements";
  labels[END_ELEMENTS] = "$EndElements";

  char* ligne;

  if (NULL == (ligne = malloc(MAX_LINE_LENGTH))) {
    fprintf(stderr, "Memory Allocation Failed for one ligne.\n");
    return 0;
  }

  int cas = -1, i;
  int nodeRank = 0;
  int cellRank = 0;
  int cpt_ligne = 1;
  int nodesAllocated = FALSE;
  int cellsAllocated = FALSE;
  int EndOfSection = 0;
  int sectionLigneNumber = 1;
  double* cellMaterial = NULL;

  /* Read the file */
  while (NULL != fgets(ligne, MAX_LINE_LENGTH, file)) {
    /* skip the ligne if /n */
    if (0 == strncmp("\n", ligne, 1)) {
      cpt_ligne++;
      continue;
    }

    /* find out in which section of the file we are reading */
    for (i = 0; i < NB_DE_SECTIONS; i++) {
      if (0 == strncmp(labels[i], ligne, strlen(labels[i]))) {
        cas = i;
        break;
      }
    }

    /* swith section do ... */
    switch (cas) {
      case NODES:
        sectionLigneNumber = 1;
        while ((NULL != fgets(ligne, MAX_LINE_LENGTH, file)) && (!EndOfSection)) {
          if (0 == strncmp(labels[cas + 1], ligne, strlen(labels[cas + 1]))) break;

          if (ligne != NULL) {
            /* if we are at the first ligne of the section we get the number of nodes */
            if (sectionLigneNumber == 1) {
              int nb_nodes;
              sscanf(ligne, "%d", &nb_nodes);
              mesh->nb_nodes = nb_nodes;
            }
            else { /* else we get nodes' x,y and z */
              if (!nodesAllocated) {
                if (NULL == (mesh->nodes = malloc(mesh->nb_nodes * 3 * sizeof(double)))) {
                  fprintf(stderr, "Memory Allocation Failed for nodes\n");
                }
                nodesAllocated = TRUE; /* allocation of mesh->nodes only one time */
              }
              /* filling in mesh->nodes */
              double tmp, x, y, z;
              sscanf(ligne, "%lf %lf %lf %lf", &tmp, &x, &y, &z);
              mesh->nodes[3 * nodeRank] = x;
              mesh->nodes[3 * nodeRank + 1] = y;
              mesh->nodes[3 * nodeRank + 2] = z;
              nodeRank++;
            }
          }
          sectionLigneNumber++;
        }

        break;

      case ELEMENTS:
        sectionLigneNumber = 1;
        while ((NULL != fgets(ligne, MAX_LINE_LENGTH, file)) && (!EndOfSection)) {
          if (0 == strncmp(labels[cas + 1], ligne, strlen(labels[cas + 1]))) break;

          if (ligne != NULL) {
            /* if we are at the first ligne of the section we get the number of elements */
            if (sectionLigneNumber == 1) {
              int nb_cells;
              sscanf(ligne, "%d", &nb_cells);
              mesh->nb_cells = nb_cells;
            }
            else {
              if (!cellsAllocated) {
                mesh->cells = malloc(mesh->nb_cells * 3 * sizeof(int));
                cellMaterial = malloc(mesh->nb_cells * sizeof(double));
                assert(mesh->cells != NULL && cellMaterial != NULL);
                cellsAllocated = TRUE; /* allocation of mesh->cells only one time */
              }

              /* filling in mesh->cells */
              int nodNumber, elmType, numberOfTags, physicalSurface, PlaneSurface, tag3, nd1, nd2, nd3;
              sscanf(ligne, "%d %d %d %d %d %d %d %d %d", &nodNumber, &elmType, &numberOfTags, &physicalSurface,
                     &PlaneSurface, &tag3, &nd1, &nd2, &nd3);
              if ((elmType == 2) &&
                  (numberOfTags == 3)) { /* we assume that the number of tags is 3 in case of triangle */
                mesh->cells[3 * cellRank] = nd1 - 1;
                mesh->cells[3 * cellRank + 1] = nd2 - 1;
                mesh->cells[3 * cellRank + 2] = nd3 - 1;
                cellMaterial[cellRank] = physicalSurface;

                /* add a variable for material in the mesh */
                if (material_id != NULL && sectionLigneNumber == mesh->nb_cells + 1) {
                  // LibGraph_addMeshVariable(mesh, material_id, LIBGRAPH_CELL_DOUBLE, 1, cellMaterial);
                  fprintf(stderr, "variables not yet supported!\n");  // TODO: update this
                }

                cellRank++;
              }
              else
                printf("WARNING : ELEMENT IS NOT A TRIANGLE !");
            }
            sectionLigneNumber++;
          }
        }
        break;
    }

    cpt_ligne++;
  }

  free(ligne);
  fclose(file);

  return 1;
}

/* *********************************************************** */

int LibGraph_saveVTKMesh(const char* output, const LibGraph_Mesh* mesh, const LibGraph_Variables* vars)
{
  /* only line, triangle or quadrangle */
  assert(mesh->elm_type == LIBGRAPH_POINT || mesh->elm_type == LIBGRAPH_LINE || mesh->elm_type == LIBGRAPH_TRIANGLE ||
         mesh->elm_type == LIBGRAPH_QUADRANGLE || mesh->elm_type == LIBGRAPH_TETRAHEDRON ||
         mesh->elm_type == LIBGRAPH_HEXAHEDRON || mesh->elm_type == LIBGRAPH_PRISM);
  int elm_size = LIBGRAPH_ELEMENT_SIZE[mesh->elm_type];

  FILE* file;

  /* open output file */
  if (NULL == (file = fopen(output, "w"))) {
    fprintf(stderr, "Opening file %s failed.\n", output);
    return 0;
  }

  /* save header */
  fprintf(file, "# vtk DataFile Version 2.0\n");
  fprintf(file, "Saved %s\n", output);
  fprintf(file, "ASCII\n");
  fprintf(file, "DATASET UNSTRUCTURED_GRID\n"); /* UNSTRUCTURED GRID, POLYDATA ??? */

  /* save section POINTS (3D coordinates) */
  fprintf(file, "POINTS %d double\n", mesh->nb_nodes);
  if (mesh->nodes) {
    for (int i = 0; i < mesh->nb_nodes; i++) {
      /* fprintf(file, "%.16lf %.16lf %.16lf\n", mesh->nodes[3*i], mesh->nodes[3*i+1],
       * mesh->nodes[3*i+2]); */
      fprintf(file, "%f %f %f\n", mesh->nodes[3 * i], mesh->nodes[3 * i + 1], mesh->nodes[3 * i + 2]);
    }
    fprintf(file, "\n");
  }
  /* save section CELLS*/
  fprintf(file, "CELLS %d %d\n", mesh->nb_cells, (elm_size + 1) * mesh->nb_cells);
  /* the size of data is (first argument of next ligne + 1) * mesh->nb_cells (in case of triangle 2D
   * = 4) */
  for (int i = 0; i < mesh->nb_cells; i++) {
    // fprintf(file, "%d %d %d %d\n", mesh->elm_size, mesh->cells[3*i], mesh->cells[3*i+1],
    // mesh->cells[3*i+2]);
    fprintf(file, "%d ", elm_size);      /* element size */
    for (int j = 0; j < elm_size; j++) { /* cell connectivity */
      fprintf(file, "%d ", mesh->cells[elm_size * i + j]);
    }
    fprintf(file, "\n");
  }

  fprintf(file, "\n");

  /* save section CELL_TYPES*/
  fprintf(file, "CELL_TYPES %d\n", mesh->nb_cells);
  if (mesh->elm_type == LIBGRAPH_POINT) { /* point */
    for (int i = 0; i < mesh->nb_cells; i++) { fprintf(file, "1\n"); /* 1 refers to VTK_VERTEX type  */ }
  }
  else if (mesh->elm_type == LIBGRAPH_LINE) { /* line */
    for (int i = 0; i < mesh->nb_cells; i++) { fprintf(file, "3\n"); /* 3 refers to VTK_LINE type  */ }
  }
  else if (mesh->elm_type == LIBGRAPH_TRIANGLE) { /* triangle */
    for (int i = 0; i < mesh->nb_cells; i++) { fprintf(file, "5\n"); /* 5 refers to VTK_TRIANGLE type  */ }
  }
  else if (mesh->elm_type == LIBGRAPH_QUADRANGLE) { /* quadrangle */
    for (int i = 0; i < mesh->nb_cells; i++) { fprintf(file, "9\n"); /* 9 refers to VTK_QUAD type  */ }
  }
  else if (mesh->elm_type == LIBGRAPH_TETRAHEDRON) { /* tetrahedron */
    for (int i = 0; i < mesh->nb_cells; i++) { fprintf(file, "10\n"); /* 10 refers to VTK_TETRA type  */ }
  }
  else if (mesh->elm_type == LIBGRAPH_HEXAHEDRON) { /* hexahedron */
    for (int i = 0; i < mesh->nb_cells; i++) { fprintf(file, "12\n"); /* 12 refers to VTK_HEXAHEDRON type  */ }
  }
  else if (mesh->elm_type == LIBGRAPH_PRISM) { /* prism */
    for (int i = 0; i < mesh->nb_cells; i++) { fprintf(file, "13\n"); /* 13 refers to VTK_WEDGE type  */ }
  }

  /* save section POINT_DATA */
  // printf("mesh->nb_node_vars = %d \n", mesh->nb_node_vars);
  LibGraph_Variables* node_vars = LibGraph_filterVariables(vars, LIBGRAPH_MESH_NODE_VAR);

  if (node_vars && node_vars->nb_vars > 0) {
    fprintf(file, "\n");
    fprintf(file, "POINT_DATA %d \n", mesh->nb_nodes);
    fprintf(file, "FIELD FieldData %d \n", node_vars->nb_vars);
    for (int i = 0; i < node_vars->nb_vars; i++) {
      char* id = LibGraph_getVariableID(node_vars, i);
      LibGraph_Array* array = LibGraph_getVariableArray(node_vars, i);
      const char* type;
      if (array->typecode == LIBGRAPH_INT)
        type = "int";
      else if (array->typecode == LIBGRAPH_DOUBLE)
        type = "double";
      else
        assert(0 && "Unknown variable type.");
      fprintf(file, "%s %d %d %s\n", id, array->nb_components, array->nb_tuples, type);
      if (array->data) {
        int data_size = array->nb_components * array->nb_tuples;
        for (int j = 0; j < data_size; j++) {
          if (array->typecode == LIBGRAPH_INT)
            fprintf(file, "%d ", ((int*)array->data)[j]);
          else if (array->typecode == LIBGRAPH_DOUBLE)
            fprintf(file, "%lf ", ((double*)array->data)[j]);
          if ((i + 1) % (3 * array->nb_components) == 0) /* le modulo est juste  un choix pour l'affichage */
            fprintf(file, "\n");
        }
      }
      fprintf(file, "\n");
    }
  }
  LibGraph_freeVariables(node_vars, false);
  free(node_vars);

  /* save section CELL_DATA*/
  // printf("mesh->nb_cell_vars = %d \n", mesh->nb_cell_vars);
  LibGraph_Variables* cell_vars = LibGraph_filterVariables(vars, LIBGRAPH_MESH_CELL_VAR);

  if (cell_vars && cell_vars->nb_vars > 0) {
    fprintf(file, "\n");
    fprintf(file, "CELL_DATA %d \n", mesh->nb_cells);
    fprintf(file, "FIELD FieldData %d \n", cell_vars->nb_vars);
    for (int i = 0; i < cell_vars->nb_vars; i++) {
      char* id = LibGraph_getVariableID(cell_vars, i);
      LibGraph_Array* array = LibGraph_getVariableArray(cell_vars, i);
      const char* type;
      if (array->typecode == LIBGRAPH_INT)
        type = "int";
      else if (array->typecode == LIBGRAPH_DOUBLE)
        type = "double";
      else
        assert(0 && "Unknown variable type.");
      fprintf(file, "%s %d %d %s\n", id, array->nb_components, array->nb_tuples, type);
      if (array->data) {
        int data_size = array->nb_components * array->nb_tuples;
        for (int j = 0; j < data_size; j++) {
          if (array->typecode == LIBGRAPH_INT)
            fprintf(file, "%d ", ((int*)array->data)[j]);
          else if (array->typecode == LIBGRAPH_DOUBLE)
            fprintf(file, "%lf ", ((double*)array->data)[j]);
          if ((i + 1) % (3 * array->nb_components) == 0) /* le modulo est juste  un choix pour l'affichage */
            fprintf(file, "\n");
        }
      }
      fprintf(file, "\n");
    }
  }
  LibGraph_freeVariables(cell_vars, false);
  free(cell_vars);

  fclose(file);
  return 1;
}

/* *********************************************************** */

void LibGraph_saveVTKGraph(const char* vtkfile, const LibGraph_Graph* graph, const double* coords)
{
  LibGraph_Mesh mesh;
  LibGraph_graph2LineMesh(graph, coords, &mesh);
  LibGraph_saveVTKMesh(vtkfile, &mesh, NULL);
  LibGraph_freeMesh(&mesh);
}

/* *********************************************************** */

/* custom format */
void LibGraph_saveHypergraph(const char* filename, const LibGraph_Hypergraph* hg)
{
  FILE* file = fopen(filename, "w");

  /* First line: number of vertices, hyperedges, pins, vwgts?, hewgts? */
  fprintf(file, "%d %d %d %d %d\n", hg->nvtxs, hg->nhedges, hg->eptr[hg->nhedges], hg->vwgts ? 1 : 0,
          hg->hewgts ? 1 : 0);
  for (int e = 0; e < hg->nhedges; e++) {
    fprintf(file, "%d", hg->eptr[e + 1] - hg->eptr[e]);
    for (int i = hg->eptr[e]; i < hg->eptr[e + 1]; i++) fprintf(file, " %d", hg->eind[i]);
    if (hg->hewgts) fprintf(file, " %d", hg->hewgts[e]); /* hyperedge weights */
    fprintf(file, "\n");
  }

  if (hg->vwgts) {
    for (int v = 0; v < hg->nvtxs; v++) fprintf(file, "%d ", hg->vwgts[v]); /* vertex weights */
    fprintf(file, "\n");
  }

  fclose(file);
}

/* *********************************************************** */

void LibGraph_saveHMetisHypergraph(const char* output, const LibGraph_Hypergraph* hg)
{
  /* open file */
  FILE* file = fopen(output, "w");

  /* header */
  /* fprintf(file, "# header (nb hyperedges, nb vertices, code)\n"); */
  fprintf(file, "%d %d 11\n", hg->nhedges, hg->nvtxs);

  /* write hyperedges */
  /* fprintf(file, "# add hyperedges (cost vertices...)\n"); */
  for (int i = 0; i < hg->nhedges; i++) {
    fprintf(file, "%d", (hg->hewgts == NULL) ? 1 : hg->hewgts[i]); /* hyperedge cost */
    // int start = hg->eptr[i];
    // int end = hg->eptr[i+1];
    for (int j = hg->eptr[i]; j < hg->eptr[i + 1]; j++) /* vertices of i-th hyperegde */
      fprintf(file, " %d", hg->eind[j] + 1);            /* HMetis Format */
    fprintf(file, "\n");
  }

  /* vertex weights of code A */
  /*fprintf(file, "# vertex weights\n");*/
  for (int i = 0; i < hg->nvtxs; i++) { fprintf(file, "%d\n", (hg->vwgts == NULL) ? 1 : hg->vwgts[i]); }

  fclose(file);
}

/* *********************************************************** */

void LibGraph_savePartitionAsHypergraph(const char* output, int nvtxs, int nparts, int* part)
{
  int nvtxs2 = nvtxs;
  int nhedges2 = nparts;
  int eptr2[nhedges2 + 1];
  int eind2[nvtxs2];

  eptr2[0] = 0;
  for (int k = 0; k < nparts; k++) {
    eptr2[k + 1] = eptr2[k];
    for (int i = 0, j = eptr2[k]; i < nvtxs2; i++) {
      if (part[i] == k) {
        eind2[j++] = i;
        eptr2[k + 1]++;
        /* printf("part %d, vertex %d\n", k, i); */
      }
    }
  }

  /*
    for(int i = 0; i < nvtxs2 ; i++)
    printf("eind[%d] %d\n", i, eind2[i]);

    for(int i = 0; i < nhedges2+1 ; i++)
    printf("eptr2[%d] %d\n", i, eptr2[i]);
  */

  char output2[255];
  sprintf(output2, "%s.part.%d", output, nparts);

  LibGraph_Hypergraph hg;

  hg.nvtxs = nvtxs2;
  hg.nhedges = nhedges2;
  hg.vwgts = NULL;
  hg.eptr = eptr2;
  hg.eind = eind2;
  hg.hewgts = NULL;

  LibGraph_saveHypergraph(output2, &hg);
}

/* *********************************************************** */

int LibGraph_saveGraph(const char* output, const LibGraph_Graph* g)
{
  /* open output file */
  FILE* file;
  if (NULL == (file = fopen(output, "w"))) {
    fprintf(stderr, "ERROR: opening file %s failed.\n", output);
    return 0;
  }

  fprintf(file, "# Metis Graph Header (nvtxs, nedges, flags)\n");

  int flags = -1;

  if ((g->vwgts == NULL) && ((g->ewgts == NULL))) flags = 0;
  if ((g->vwgts == NULL) && ((g->ewgts != NULL))) flags = 1;
  if ((g->vwgts != NULL) && ((g->ewgts == NULL))) flags = 10;
  if ((g->vwgts != NULL) && ((g->ewgts != NULL))) flags = 11;

  assert(flags != -1);
  fprintf(file, "%d %d %d\n", g->nvtxs, g->narcs / 2, flags);
  printf("For output=%s, flags is %d\n", output, flags);
  for (int i = 0; i < g->nvtxs; i++) {
    if (flags == 10 || flags == 11) fprintf(file, "%d ", g->vwgts[i]);
    for (int j = g->xadj[i]; j < g->xadj[i + 1]; j++) {
      fprintf(file, "%d ", g->adjncy[j]);  // flags 0
      if (flags == 1 || flags == 11) fprintf(file, "%d ", g->ewgts[j]);
    }

    fprintf(file, "\n");
  }
  fclose(file);
  return 1;
}

/* *********************************************************** */

int LibGraph_loadMetisGraph(const char* input, LibGraph_Graph* g)
{
  int numbering_shift = -1;

  /* open input file */
  FILE* file;
  if (NULL == (file = fopen(input, "r"))) {
    fprintf(stderr, "ERROR: opening file %s failed.\n", input);
    return 0;
  }

  int nvtxs = -1;
  int nedges = -1;
  int flags = -1;
  int ncon = -1;

  int k = 0; /* k-th vertex */
  int idx = 0;

  char* line = malloc(MAX_LINE_LENGTH * sizeof(char));
  assert(line);

  while (NULL != fgets(line, MAX_LINE_LENGTH, file)) {
    // printf("[debug] %s", line);

    if (strlen(line) >= MAX_LINE_LENGTH - 1) {
      fprintf(stderr, "ERROR: line truncated when reading (too long)!\n");  // useless ???
      exit(EXIT_FAILURE);
    }

    /* skip commentary or void line */
    if ((0 == strncmp("\n", line, 1)) || (0 == strncmp("#", line, 1)) || (0 == strncmp("%", line, 1)))
      continue; /* skip */

    /* read graph header */
    if (flags == -1) {
      sscanf(line, "%d %d %d %d\n", &nvtxs, &nedges, &flags, &ncon);
      /* check */
      assert(nvtxs != -1 && nedges != -1);
      if (flags == -1) flags = 0;  // assume no edge/vertex weight.
      if (ncon == -1) ncon = 1;    // assume one weight per vertex.

      // debug
      PRINT("load graph %s: nvtxs = %d, nedges = %d, flags = %d, ncon = %d\n", input, nvtxs, nedges, flags, ncon);

      /* initialize graph */
      g->nvtxs = nvtxs;
      g->narcs = 2 * nedges;
      g->xadj = malloc((nvtxs + 1) * sizeof(int));
      g->adjncy = malloc(2 * nedges * sizeof(int));
      g->ewgts = g->vwgts = NULL;
      if ((flags == 10) || (flags == 11)) g->vwgts = malloc(ncon * nvtxs * sizeof(int));
      if ((flags == 1) || (flags == 11)) g->ewgts = malloc(2 * nedges * sizeof(int));
      continue;
    }

    /* for each vertex, parse 1 line */
    g->xadj[k] = idx;

    char* endptr;
    char* str = line;

    if (g->vwgts) { /* vertex weight */
      for (int i = 0; i < ncon; i++) {
        int w = strtol(str, &endptr, 10);
        assert(errno != ERANGE);
        if (endptr == str) break;
        g->vwgts[k * ncon + i] = w;
        str = endptr;
      }
      str = endptr;
    }

    /* adjacency list */
    while (1) {
      int kk = strtol(str, &endptr, 10); /* edge (k,kk) */
      assert(errno != ERANGE);
      if (endptr == str) break;
      assert(kk >= 0 && kk <= nvtxs);
      if (numbering_shift == -1 && kk == 0) numbering_shift = 0;
      if (numbering_shift == -1 && kk == nvtxs) numbering_shift = 1;

      /* printf("vtx %d => %d\n",k,kk); */
      g->adjncy[idx] = kk; /* edge (k,kk) */
      str = endptr;
      /* edge weight */
      if (g->ewgts) {
        int w = strtol(str, &endptr, 10);
        assert(errno != ERANGE);
        if (endptr == str) break;
        g->ewgts[idx] = w;
        str = endptr;
      }
      idx++;
    }
    k++;  // increment vertex index

    if (k == nvtxs) break;  // end of vertex parsing
  }
  g->xadj[k] = idx;
  // printf("[debug] k = %d, nvtxs = %d, idx = %d, nedges = %d\n", k, nvtxs, idx, nedges);
  assert(idx == nedges * 2);

  fclose(file);

  // change Fortran numbering convention to C
  assert(numbering_shift != -1);
  if (numbering_shift) {
    PRINT("WARNING: loading Metis graph file with Fortran numbering standard!\n");
    for (int i = 0; i < g->narcs; i++) {
      g->adjncy[i] -= 1;
      assert(g->adjncy[i] >= 0 && g->adjncy[i] < nvtxs);
    }
  }

  free(line);

  return 1;
}

int LibGraph_loadParallelMetisGraph(const char* input, int prank, int psize, LibGraph_DistGraph* g)
{
  int numbering_shift = -1;
  /* open input file */
  FILE* file;
  if (NULL == (file = fopen(input, "r"))) {
    fprintf(stderr, "ERROR: opening file %s failed.\n", input);
    return 0;
  }

  int nvtxs = -1;
  int nedges = -1;
  int flags = -1;
  int ncon = -1;

  int k = 0;       /* k-th vertex */
  int local_k = 0; /* local k-th vertex */
  int idx = 0;

  char* line = malloc(MAX_LINE_LENGTH * sizeof(char));
  assert(line);

  int* adjncy = NULL;
  int adjncy_allocated = 0;
  int* ewgts = NULL;
  while (NULL != fgets(line, MAX_LINE_LENGTH, file)) {
    // printf("[debug] %s", line);

    if (strlen(line) >= MAX_LINE_LENGTH - 1) {
      fprintf(stderr, "ERROR: line truncated when reading (too long)!\n");  // useless ???
      exit(EXIT_FAILURE);
    }

    /* skip commentary or void line */
    if ((0 == strncmp("\n", line, 1)) || (0 == strncmp("#", line, 1)) || (0 == strncmp("%", line, 1)))
      continue; /* skip */

    /* read graph header */
    if (flags == -1) {
      sscanf(line, "%d %d %d %d\n", &nvtxs, &nedges, &flags, &ncon);
      /* check */
      assert(nvtxs != -1 && nedges != -1);
      if (flags == -1) flags = 0;  // assume no edge/vertex weight.
      if (ncon == -1) ncon = 1;    // assume one weight per vertex.

      // debug
      PRINT("load graph %s: nvtxs = %d, nedges = %d, flags = %d, ncon = %d\n", input, nvtxs, nedges, flags, ncon);

      /* Compute vertex distribution*/
      int* vtxdist = (int*)malloc(sizeof(int[psize + 1]));
      int nvtxs_by_rank = nvtxs / psize;
      int nvtxs_remained = nvtxs % psize;
      vtxdist[0] = 0;
      for (int p = 0; p < psize; p++) {
        int add = 0;
        if (p < nvtxs_remained) add = 1;
        vtxdist[p + 1] = vtxdist[p] + nvtxs_by_rank + add;
      }

      int local_nvtxs = vtxdist[prank + 1] - vtxdist[prank];

      /* initialize graph */
      g->nvtxs = local_nvtxs;
      g->narcs = -1;
      g->xadj = malloc(sizeof(int[local_nvtxs + 1]));
      g->adjncy = NULL;
      g->ewgts = g->vwgts = NULL;
      g->vtxdist = vtxdist;
      if ((flags == 10) || (flags == 11)) g->vwgts = malloc(ncon * nvtxs * sizeof(int));
      continue;
    }

    /* If not my vertex */
    if (k < g->vtxdist[prank]) {
      k++;

      /* we need to parse non used lines to guess numbering shift */
      char* endptr;
      char* str = line;

      if (g->vwgts) { /* vertex weight */
        for (int i = 0; i < ncon; i++) {
          strtol(str, &endptr, 10);
          assert(errno != ERANGE);
          if (endptr == str) break;
          str = endptr;
        }
        str = endptr;
      }

      while (1) {
        int kk = strtol(str, &endptr, 10); /* edge (k,kk) */
        assert(errno != ERANGE);
        if (endptr == str) break;
        assert(kk >= 0 && kk <= nvtxs);
        if (numbering_shift == -1 && kk == 0) numbering_shift = 0;
        if (numbering_shift == -1 && kk == nvtxs) numbering_shift = 1;

        str = endptr;
        /* edge weight */
        if ((flags == 1) || (flags == 11)) {
          strtol(str, &endptr, 10);
          assert(errno != ERANGE);
          if (endptr == str) break;
          str = endptr;
        }
      }

      continue;
    }

    /* for each vertex, parse 1 line */
    g->xadj[local_k] = idx;

    char* endptr;
    char* str = line;

    if (g->vwgts) { /* vertex weight */
      for (int i = 0; i < ncon; i++) {
        int w = strtol(str, &endptr, 10);
        assert(errno != ERANGE);
        if (endptr == str) break;
        g->vwgts[local_k * ncon + i] = w;
        str = endptr;
      }
      str = endptr;
    }

    /* adjacency list */
    while (1) {
      int kk = strtol(str, &endptr, 10); /* edge (k,kk) */
      assert(errno != ERANGE);
      if (endptr == str) break;
      assert(kk >= 0 && kk <= nvtxs);
      if (numbering_shift == -1 && kk == 0) numbering_shift = 0;
      if (numbering_shift == -1 && kk == nvtxs) numbering_shift = 1;

      /* printf("vtx %d => %d\n",k,kk); */
      if (idx >= adjncy_allocated) {
        adjncy_allocated = adjncy_allocated ? adjncy_allocated * 2 : 2 * g->nvtxs;
        adjncy = realloc(adjncy, sizeof(int[adjncy_allocated]));

        if ((flags == 1) || (flags == 11)) { ewgts = realloc(ewgts, sizeof(int[adjncy_allocated])); }
      }
      adjncy[idx] = kk; /* edge (k,kk) */
      str = endptr;
      /* edge weight */
      if ((flags == 1) || (flags == 11)) {
        int w = strtol(str, &endptr, 10);
        assert(errno != ERANGE);
        if (endptr == str) break;
        ewgts[idx] = w;
        str = endptr;
      }
      idx++;
    }
    k++;        // increment vertex index
    local_k++;  // increment local vertex index

    if (k == g->vtxdist[prank + 1]) break;  // end of vertex parsing
  }
  g->xadj[local_k] = idx;
  g->narcs = idx;
  g->adjncy = realloc(adjncy, sizeof(int[idx]));
  if (ewgts) g->ewgts = realloc(ewgts, sizeof(int[idx]));

  fclose(file);

  // change Fortran numbering convention to C
  assert(numbering_shift != -1);
  if (numbering_shift) {
    PRINT("WARNING: loading Metis graph file with Fortran numbering standard!\n");
    for (int i = 0; i < g->narcs; i++) {
      g->adjncy[i] -= 1;
      assert(g->adjncy[i] >= 0 && g->adjncy[i] < nvtxs);
    }
  }

  free(line);

  return 1;
}

/* *********************************************************** */

/** Load graph (Scotch format, .grf or .src). */
int LibGraph_loadScotchGraph(const char* input, /**< [in] input filename */
                             LibGraph_Graph* g  /**< [out] Graph struct */
)
{
  /* open input file */
  FILE* file;
  if (NULL == (file = fopen(input, "r"))) {
    fprintf(stderr, "ERROR: opening file %s failed.\n", input);
    return 0;
  }

  assert(0);  // TODO

  /* /\* load graph scotch *\/ */
  /* SCOTCH_Graph sg; */
  /* int success = SCOTCH_graphLoad(&sg,  */
  /* 				 file,  */
  /* 				 0,  /\* C style numbering *\/ */
  /* 				 0); /\* keep edge/vertex weigths if they exist *\/ */

  /* if(success != 0) { */
  /*   fprintf(stderr, "ERROR: loading file %s in Scotch failed.\n",input); */
  /*   return 0; */
  /* } */

  /* SCOTCH_Num nvtxs, narcs, base; */
  /* SCOTCH_Num * xadj; */
  /* SCOTCH_Num * adjncy; */
  /* SCOTCH_Num * vwgts = NULL; */
  /* SCOTCH_Num * ewgts = NULL; */

  /* assert(sizeof(SCOTCH_Num) == sizeof(int)); */

  /* SCOTCH_graphData(&sg, */
  /* 		   &base,    /\*  numbering style *\/ */
  /* 		   &nvtxs,   /\* nb of vertices *\/ */
  /* 		   &xadj,    /\* adjacency index array *\/ */
  /* 		   NULL, */
  /* 		   &vwgts,   /\* vertex weights *\/ */
  /* 		   NULL,     /\* vertex label array *\/ */
  /* 		   &narcs,   /\* nb arcs *\/ */
  /* 		   &adjncy,  /\* adjacency array *\/ */
  /* 		   &ewgts    /\* edge weights *\/ */
  /* 		   ); */

  /* assert(base == 0); /\* C style *\/ */

  /* /\* fill graph structure *\/ */
  /* g->nvtxs = nvtxs; */
  /* g->nedges = narcs / 2; */
  /* g->xadj = malloc((g->nvtxs+1) * sizeof(int)); */
  /* assert(g->xadj); */
  /* memcpy(g->xadj, xadj, (g->nvtxs+1) * sizeof(int)); */
  /* g->adjncy = malloc((g->nedges*2) * sizeof(int)); */
  /* assert(g->adjncy); */
  /* memcpy(g->adjncy, adjncy, (g->nedges*2) * sizeof(int)); */

  /* /\* fill vertex weights *\/ */
  /* if(vwgts != NULL) { */
  /*   g->vwgts = malloc(g->nvtxs * sizeof(int)); */
  /*   memcpy(g->vwgts, vwgts, g->nvtxs * sizeof(int)); */
  /* } */
  /* else  */
  /*   g->vwgts = NULL; */

  /* /\* fill edge weights *\/ */
  /* if(ewgts != NULL) { */
  /*   g->ewgts = malloc(2*g->nedges * sizeof(int)); */
  /*   memcpy(g->ewgts, ewgts, 2*g->nedges * sizeof(int)); */
  /* } */
  /* else  */
  /*   g->ewgts = NULL; */

  /* /\* free memory *\/ */
  /* SCOTCH_graphExit(&sg); */

  return 1;
}

/* *********************************************************** */

/** Load graph (Rutherford-Boeing Sparse Matrix format). */
int LibGraph_loadRBGraph(const char* input, LibGraph_Graph* g)
{
  /* open input file */
  FILE* file;
  if (NULL == (file = fopen(input, "r"))) {
    fprintf(stderr, "ERROR: opening file %s failed.\n", input);
    return 0;
  }

  fscanf(file, "%*80c");           /* First line: Title */
  fscanf(file, "%*d %*d %*d %*d"); /* Second line: line counts */

  char options[3];
  int rows, cols, nonzeros;
  if (4 != fscanf(file, " %3c %d %d %d %*d", options, &rows, &cols, &nonzeros)) {
    fprintf(stderr, "ERROR: cannot parse line 3\n");
    return 0;
  }
  if (rows != cols) {
    fprintf(stderr, "ERROR: not a square matrix (%dx%d)\n", rows, cols);
    return 0;
  }
  /*if (options[0] == 'c') {
    fprintf (stderr, "ERROR: complex weights not supported\n");
    return 0;
    }
    if (options[0] == 'r') {
    fprintf (stderr, "ERROR: real weights not supported\n");
    return 0;
    }
    int weighted = (options[0] == 'i');*/
  if (options[0] != 'p') fprintf(stderr, "WARNING: matrix is not pattern only, values are ignored\n");
  int weighted = 0;
  if (options[1] != 's') {
    fprintf(stderr, "ERROR: matrix is not symmetric\n");
    return 0;
  }
  if (options[2] != 'a') {
    fprintf(stderr, "ERROR: matrix is not in compressed column form\n");
    return 0;
  }

  fscanf(file, " %*52c"); /* Fourth line: FORTRAN formats */

  int* xadj = malloc((rows + 1) * sizeof(int));
  for (int i = 0; i < rows + 1; i++) {
    fscanf(file, "%d", &xadj[i]);
    xadj[i]--;
  }
  int* adjncy = malloc(nonzeros * sizeof(int));
  for (int i = 0; i < nonzeros; i++) {
    fscanf(file, "%d", &adjncy[i]);
    adjncy[i]--;
  }
  int* ewgts;
  if (weighted) {
    ewgts = malloc(nonzeros * sizeof(int));
    for (int i = 0; i < nonzeros; i++) fscanf(file, "%d", &ewgts[i]);
  }

  int* col_size = malloc(cols * sizeof(int));
  for (int i = 0; i < cols; i++) col_size[i] = xadj[i + 1] - xadj[i];
  /* Add symmetric */
  for (int i = 0; i < cols; i++)
    for (int j = xadj[i]; j < xadj[i + 1]; j++)
      if (i != adjncy[j]) col_size[adjncy[j]]++;

  g->nvtxs = cols;
  g->narcs = 2 * nonzeros;  // TODO: check if nonzero = nedges or narcs?
  g->xadj = malloc((g->nvtxs + 1) * sizeof(int));
  g->xadj[0] = 0;
  for (int i = 0; i < rows; i++) g->xadj[i + 1] = g->xadj[i] + col_size[i];

  g->adjncy = malloc(g->narcs * sizeof(int));
  g->vwgts = NULL;
  if (weighted)
    g->ewgts = malloc(g->narcs * sizeof(int));
  else
    g->ewgts = NULL;
  int* p = calloc(cols, sizeof(int));
  for (int i = 0; i < cols; i++) {
    for (int j = xadj[i]; j < xadj[i + 1]; j++) {
      /* Add edge */
      g->adjncy[g->xadj[i] + p[i]] = adjncy[j];
      if (weighted) g->ewgts[g->xadj[i] + p[i]] = ewgts[j];
      p[i]++;
      /* Add symmetric edge */
      if (i != adjncy[j]) {
        g->adjncy[g->xadj[adjncy[j]] + p[adjncy[j]]] = i;
        if (weighted) g->ewgts[g->xadj[adjncy[j]] + p[adjncy[j]]] = ewgts[j];
        p[adjncy[j]]++;
      }
    }
    assert(p[i] == col_size[i]);
  }

  if (weighted) free(ewgts);
  free(p);
  free(col_size);
  free(adjncy);
  free(xadj);

  fclose(file);
  return 1;
}

/* *********************************************************** */

/* Load a Hypergraph of ISPD98 Circuit Benchmark Suite downloaded form
 * http://vlsicad.ucsd.edu/UCLAWeb/cheese/ispd98.html#Download */

/* Function to load hypergraphs from files nfile.net and wfile.are. Note that cells (free vertices)
   are indexed in the hypergraph representation as given in the file ( from 0 - pad offset ]
   while for pads (fixed vertices) the indexing starts from pad_offset until nb_modules-1 where
nb_modules = nb_of_free_and_fixed_vertices.*/

int LibGraph_loadISPDHypergraph(const char* netfilename, const char* wfilename, LibGraph_Hypergraph* hg)
{
  FILE* netfile = NULL;
  FILE* wfile = NULL;
  if (NULL == (netfile = fopen(netfilename, "r"))) {
    fprintf(stderr, "ERROR: opening file %s failed.\n", netfilename);
    exit(EXIT_FAILURE);
  }
  int zerovar, nbpins, pad_offset;
  for (int i = 0; i < 5; i++) {
    switch (i) {
      case 0:
        if (fscanf(netfile, "%d", &zerovar) != 1)
          return FALSE;
        else if (zerovar)
          fprintf(stderr, "WARNING: First input of the netfile should be 0!\n");
        break;
      case 1:
        if (fscanf(netfile, "%d", &nbpins) != 1) return FALSE;
        break;
      case 2:
        if (fscanf(netfile, "%d", &hg->nhedges) != 1) return FALSE;
        break;
      case 3:
        if (fscanf(netfile, "%d", &hg->nvtxs) != 1) return FALSE;
        break;
      case 4:
        if (fscanf(netfile, "%d", &pad_offset) != 1) return FALSE;
        break;
      default: break;
    }
  }
  // printf("INFO: %d PINS:%d NBEDGES:%d NVTXS:%d PADOFFSET:%d\n",zerovar, nbpins,
  // hg->nhedges,hg->nvtxs, pad_offset);
  /* Initializations for Hypergraph */
  hg->eptr = malloc((hg->nhedges + 1) * sizeof(int));
  hg->eind = malloc(nbpins * sizeof(int));

  /* We suppose that there are always weights attribute to vertices found in .are
   * but no weights for the hedges. */
  hg->vwgts = malloc(hg->nvtxs * sizeof(int));
  hg->hewgts = NULL;

  hg->eptr[0] = 0;

  char line[MAX_LINE_LENGTH];
  int pin_idx = 0;
  int idx = 0;
  int vrt_idx = 0;
  // printf("Reading the net file %s ...\n", netfilename);
  while (NULL != fgets(line, MAX_LINE_LENGTH, netfile)) {
    if (strlen(line) >= MAX_LINE_LENGTH - 1)
      fprintf(stderr, "WARNING: line truncated when reading file %s (too long)!\n", netfilename);

    /* skip commentary or void line */
    if ((0 == strncmp("\n", line, 1)) || (0 == strncmp("#", line, 1)) || (0 == strncmp("%", line, 1)))
      continue; /* skip */

    /* read pins */
    if (pin_idx < nbpins) {
      // char * str = line;
      // char * endptr;

      char* splits;  // 3 actually
      splits = strtok(line, " ");
      while (splits != NULL) {
        if (splits[0] == 'p') {
          // fixed vertex
          splits[0] = '0';
          // fixed are numbered from 1-#modules-pad_offset=1 so to renumber should add offset
          vrt_idx = atoi(splits) + pad_offset;
          hg->eind[pin_idx] = vrt_idx;
        }
        else if (splits[0] == 'a') {
          splits[0] = '0';
          vrt_idx = atoi(splits);
          hg->eind[pin_idx] = vrt_idx;
        }
        else if (splits[0] == 's') {  // new pin starts
          hg->eptr[idx++] = pin_idx;
        }
        splits = strtok(NULL, " ");
      }
    }
    pin_idx++;
  }  // while close
  // last index for the eptr of size nhedges + 1
  hg->eptr[idx] = nbpins;
  fclose(netfile);

  if (NULL == (wfile = fopen(wfilename, "r"))) {
    fprintf(stderr, "ERROR: opening file %s failed.\n", wfilename);
    exit(EXIT_FAILURE);
  }
  char vtx[MAX_LINE_LENGTH];
  int weight;

  // printf("Reading the weight file %s ...\n", wfilename);
  for (int i = 0; i < hg->nvtxs; i++) {
    if (fscanf(wfile, "%s %d", vtx, &weight) != 2) {
      free(hg->vwgts);
      return FALSE;
    }
    if (vtx[0] == 'p') {
      // fixed vertex
      vtx[0] = '0';
      // fixed are numbered from 1-#modules-pad_offset=1 so to renumber should add padd_offset
      vrt_idx = atoi(vtx) + pad_offset;
      hg->vwgts[vrt_idx] = weight;
    }
    else if (vtx[0] == 'a') {
      vtx[0] = '0';
      vrt_idx = atoi(vtx);
      hg->vwgts[vrt_idx] = weight;
    }
  }
  fclose(wfile);

  return EXIT_SUCCESS;
}

/* *********************************************************** */

int LibGraph_loadHypergraph(const char* filename, LibGraph_Hypergraph* hg) /* custom format */
{
  FILE* file = fopen(filename, "r");
  int hewgts, vwgts;
  int nnz;
  if (fscanf(file, "%d %d %d %d %d", &hg->nvtxs, &hg->nhedges, &nnz, &vwgts, &hewgts) != 5) return FALSE;

  hg->eptr = malloc((hg->nhedges + 1) * sizeof(int));
  hg->eind = malloc(nnz * sizeof(int));

  hg->vwgts = hg->hewgts = NULL;
  if (vwgts) hg->vwgts = malloc(hg->nvtxs * sizeof(int));
  if (hewgts) hg->hewgts = malloc(hg->nhedges * sizeof(int));

  hg->eptr[0] = 0;
  for (int e = 0; e < hg->nhedges; e++) {
    int netsize;
    if (fscanf(file, "%d", &netsize) != 1) {
      free(hg->eptr);
      free(hg->eind);
      if (hg->vwgts) free(hg->vwgts);
      if (hg->hewgts) free(hg->hewgts);
      return FALSE;
    }
    hg->eptr[e + 1] = hg->eptr[e] + netsize;
    for (int i = hg->eptr[e]; i < hg->eptr[e + 1]; i++)
      if (fscanf(file, "%d", &hg->eind[i]) != 1) {
        free(hg->eptr);
        free(hg->eind);
        if (hg->vwgts) free(hg->vwgts);
        if (hg->hewgts) free(hg->hewgts);
        return FALSE;
      }

    if (hg->hewgts)
      if (fscanf(file, "%d", &hg->hewgts[e]) != 1) return FALSE; /* hyperedge weight */
  }

  /* vertex weight */
  if (hg->vwgts) {
    for (int v = 0; v < hg->nvtxs; v++) {
      if (fscanf(file, "%d", &hg->vwgts[v]) != 1) return FALSE;
    }
  }

  fclose(file);
  return TRUE;
}

/* *********************************************************** */

/** Load hypergraph (Rutherford-Boeing Sparse Matrix format). */
int LibGraph_loadRBHypergraph(const char* input,      /**< [in] input filename */
                              LibGraph_Hypergraph* hg /**< [out] Hypergraph struct */
)
{
  /* open input file */
  FILE* file;
  if (NULL == (file = fopen(input, "r"))) {
    fprintf(stderr, "ERROR: opening file %s failed.\n", input);
    return 0;
  }

  fscanf(file, "%*80c");           /* First line: Title */
  fscanf(file, "%*d %*d %*d %*d"); /* Second line: line counts */

  char options[3];
  int rows, cols, nonzeros;
  if (4 != fscanf(file, " %3c %d %d %d %*d", options, &rows, &cols, &nonzeros)) {
    fprintf(stderr, "ERROR: cannot parse line 3\n");
    return 0;
  }
  if (options[0] != 'p') {
    fprintf(stderr, "ERROR: the matrix must be a pattern\n");
    return 0;
  }
  if (options[2] != 'a') {
    fprintf(stderr, "ERROR: matrix is not in compressed form\n");
    return 0;
  }

  fscanf(file, " %*52c"); /* Fourth line: FORTRAN formats */

  int* xadj = malloc((cols + 1) * sizeof(int));
  for (int i = 0; i < cols + 1; i++) {
    fscanf(file, "%d", &xadj[i]);
    xadj[i]--;
  }
  int* adjncy = malloc(nonzeros * sizeof(int));
  for (int i = 0; i < nonzeros; i++) {
    fscanf(file, "%d", &adjncy[i]);
    adjncy[i]--;
  }

  hg->nvtxs = rows;
  hg->nhedges = cols;
  hg->vwgts = NULL;
  hg->hewgts = NULL;
  if (options[1] == 's') { /* TOCHECK: Can a pattern matrix be skew symmetric ('z')? */
    int* col_size = malloc(cols * sizeof(int));
    for (int i = 0; i < cols; i++) col_size[i] = xadj[i + 1] - xadj[i];
    /* Add symmetric */
    for (int i = 0; i < cols; i++)
      for (int j = xadj[i]; j < xadj[i + 1]; j++)
        if (i != adjncy[j]) col_size[adjncy[j]]++;

    hg->eptr = malloc((hg->nhedges + 1) * sizeof(int));
    hg->eptr[0] = 0;
    for (int i = 0; i < hg->nhedges; i++) hg->eptr[i + 1] = hg->eptr[i] + col_size[i];

    hg->eind = malloc(hg->eptr[hg->nhedges] * sizeof(int));
    int* p = calloc(hg->nhedges, sizeof(int));
    for (int i = 0; i < hg->nhedges; i++) {
      for (int j = xadj[i]; j < xadj[i + 1]; j++) {
        /* Add edge */
        hg->eind[hg->eptr[i] + p[i]] = adjncy[j];
        p[i]++;
        /* Add symmetric edge */
        if (i != adjncy[j]) {
          hg->eind[hg->eptr[adjncy[j]] + p[adjncy[j]]] = i;
          p[adjncy[j]]++;
        }
      }
      assert(p[i] == col_size[i]);
    }
    free(p);
    free(col_size);
  }
  else {
    hg->eptr = malloc((hg->nhedges + 1) * sizeof(int));
    memcpy(hg->eptr, xadj, (hg->nhedges + 1) * sizeof(int));
    hg->eind = malloc(hg->eptr[hg->nhedges] * sizeof(int));
    memcpy(hg->eind, adjncy, hg->eptr[hg->nhedges] * sizeof(int));
  }

  free(adjncy);
  free(xadj);

  fclose(file);
  return 1;
}

/* *********************************************************** */

/* Simple Text Format

   # Grid2d Sample with Quad Elements
   # Header: elm_type, nb_nodes, nb_cells, nb_node_vars, nb_cells_vars
   4 1681 1600 0 0
   # 1600 cells of size 4
   0 1 42 41
   1 2 43 42
   2 3 44 43
   3 4 45 44
   4 5 46 45
   5 6 47 46
   6 7 48 47
   7 8 49 48
   8 9 50 49
   9 10 51 50
   ...
   # 1681 nodes (3d coordinates)
   0.000000 0.000000 0.000000
   1.000000 0.000000 0.000000
   2.000000 0.000000 0.000000
   3.000000 0.000000 0.000000
   ...

*/

int LibGraph_loadMesh(const char* input, LibGraph_Mesh* mesh, bool no_nodes)  // if set no double * nodes
{
  /* open input file */
  FILE* file;
  if (NULL == (file = fopen(input, "r"))) {
    fprintf(stderr, "ERROR: opening file %s failed.\n", input);
    return 0;
  }

  int elm_type = -1;
  int nb_nodes = -1;
  int nb_cells = -1;
  int nb_node_vars = -1;
  int nb_cell_vars = -1;

  char line[MAX_LINE_LENGTH];

  int node_idx = 0;
  int cell_idx = 0;

  while (NULL != fgets(line, MAX_LINE_LENGTH, file)) {
    if (strlen(line) >= MAX_LINE_LENGTH - 1) fprintf(stderr, "WARNING: line truncated when reading (too long)!\n");

    /* skip commentary or void line */
    if ((0 == strncmp("\n", line, 1)) || (0 == strncmp("#", line, 1)) || (0 == strncmp("%", line, 1)))
      continue; /* skip */

    /* read mesh header */
    if (elm_type == -1) {
      sscanf(line, "%d %d %d %d %d\n", &elm_type, &nb_nodes, &nb_cells, &nb_node_vars, &nb_cell_vars);
      /* check */
      assert(nb_nodes != -1 && nb_cells != -1);
      assert(elm_type == LIBGRAPH_LINE || elm_type == LIBGRAPH_TRIANGLE || elm_type == LIBGRAPH_QUADRANGLE ||
             elm_type == LIBGRAPH_TETRAHEDRON || elm_type == LIBGRAPH_HEXAHEDRON || elm_type == LIBGRAPH_PRISM);

      /* initialize mesh */
      mesh->elm_type = elm_type;
      mesh->nb_cells = nb_cells;
      if (no_nodes) {
        mesh->nb_nodes = 0;
        mesh->nodes = NULL;
      }
      else {
        mesh->nb_nodes = nb_nodes;
        mesh->nodes = malloc(nb_nodes * 3 * sizeof(double));
      }
      mesh->cells = malloc(nb_cells * LIBGRAPH_ELEMENT_SIZE[elm_type] * sizeof(int));

      /* not yet supported */
      // mesh->nb_node_vars = 0;
      // mesh->nb_cell_vars = 0;
      // mesh->node_vars = NULL;
      // mesh->cell_vars = NULL;

      // if (nb_node_vars > 0 || nb_cell_vars > 0) { fprintf(stderr, "WARNING: Variables are ignored while loading
      // %s!\n", input); }

      continue;
    }

    /* cells */
    if (cell_idx < nb_cells) {
      char* str = line;
      char* endptr;
      int elm_size = LIBGRAPH_ELEMENT_SIZE[elm_type];
      for (int k = 0; k < elm_size; k++) {
        int kk = strtol(str, &endptr, 10);
        assert(kk >= 0);
        if (errno == ERANGE) {
          perror("loadMesh:");
          return 0;
        }
        mesh->cells[cell_idx * elm_size + k] = kk;
        str = endptr;
      }
      cell_idx++;
      continue;
    }

    /* nodes */
    if (node_idx < nb_nodes) {
      if (no_nodes)
        continue;
      else {
        char* str = line;
        char* endptr;
        for (int k = 0; k < 3; k++) {
          double kk = strtod(str, &endptr);
          assert(errno != ERANGE);
          mesh->nodes[node_idx * 3 + k] = kk;
          assert(str != endptr);
          str = endptr;
        }
        node_idx++;
        continue;
      }
    }
  }
  assert(cell_idx == nb_cells);
  if (!no_nodes) assert(node_idx == nb_nodes);

  fclose(file);
  return 1;
}

/* *********************************************************** */

int LibGraph_saveMesh(const char* output, const LibGraph_Mesh* mesh)
{
  /* open output file */
  FILE* file;
  if (NULL == (file = fopen(output, "w"))) {
    fprintf(stderr, "ERROR: opening file %s failed.\n", output);
    return 0;
  }

  fprintf(file, "# Mesh Header (elm_type, nb_nodes, nb_cells, nb_node_vars, nb_cells_vars)\n");
  fprintf(file, "%d %d %d %d %d\n", mesh->elm_type, mesh->nb_nodes, mesh->nb_cells, 0, 0);

  // if (mesh->nb_node_vars > 0 || mesh->nb_cell_vars > 0)
  //   fprintf(stderr, "WARNING: Mesh variables are not saved in %s!\n", output);

  /* cells */
  int elm_size = LIBGRAPH_ELEMENT_SIZE[mesh->elm_type];
  fprintf(file, "# %d cells of size %d\n", mesh->nb_cells, elm_size);
  for (int i = 0; i < mesh->nb_cells; i++) {
    for (int k = 0; k < elm_size; k++) { fprintf(file, "%d ", mesh->cells[i * elm_size + k]); }
    fprintf(file, "\n");
  }

  /* nodes */
  fprintf(file, "# %d nodes (3d coordinates)\n", mesh->nb_nodes);
  if (mesh->nodes) {
    for (int i = 0; i < mesh->nb_nodes; i++) {
      for (int k = 0; k < 3; k++) { fprintf(file, "%f ", mesh->nodes[i * 3 + k]); }
      fprintf(file, "\n");
    }
  }

  /* TODO: variables are not saved! */

  fclose(file);
  return 1;
}

/* *********************************************************** */

int LibGraph_loadHybridMeshFiles(LibGraph_HybridMesh* hm, bool shared_nodes, int nb_components, ...)
{
  /* variables for the variadic form of the function*/
  assert(hm);
  va_list list_files;
  hm->nb_components = nb_components;
  hm->shared_nodes = shared_nodes;
  for (int i = 0; i < hm->nb_components; i++) assert(&(hm->components[i]));

  va_start(list_files, nb_components);
  const char* current_file = NULL;
  LibGraph_Mesh* cm = NULL;
  for (int j = 0; j < hm->nb_components; j++) {
    cm = LibGraph_getHybridMeshComponent(hm, j);
    current_file = va_arg(list_files, char*);
    if (!LibGraph_loadMesh(current_file, cm, shared_nodes)) exit(EXIT_FAILURE);
  }
  va_end(list_files);

  assert(current_file);  // current file points to the last file
  if (shared_nodes) {
    /* open input file */
    FILE* file = NULL;
    int elm_type = -1;
    int nb_nodes = -1;
    int nb_cells = -1;
    int cell_idx = 0;
    int node_idx = 0;
    int nb_node_vars = -1;
    int nb_cell_vars = -1;
    char line[MAX_LINE_LENGTH];
    if (NULL == (file = fopen(current_file, "r"))) {
      fprintf(stderr, "ERROR: opening file %s failed.\n", current_file);
      return 0;
    }
    while (NULL != fgets(line, MAX_LINE_LENGTH, file)) {
      if (strlen(line) >= MAX_LINE_LENGTH - 1) fprintf(stderr, "WARNING: line truncated when reading (too long)!\n");
      /* skip commentary or void line */
      if ((0 == strncmp("\n", line, 1)) || (0 == strncmp("#", line, 1)) || (0 == strncmp("%", line, 1)))
        continue; /* skip */
      /* read mesh header */
      if (elm_type == -1) {
        sscanf(line, "%d %d %d %d %d\n", &elm_type, &nb_nodes, &nb_cells, &nb_node_vars, &nb_cell_vars);
        /* check */
        assert(nb_nodes != -1 && nb_cells != -1);
        assert(elm_type == LIBGRAPH_LINE || elm_type == LIBGRAPH_TRIANGLE || elm_type == LIBGRAPH_QUADRANGLE ||
               elm_type == LIBGRAPH_TETRAHEDRON || elm_type == LIBGRAPH_HEXAHEDRON || elm_type == LIBGRAPH_PRISM);
        /* initialize shared nodes of hybridmesh - one time */
        if (!hm->nodes) hm->nodes = malloc(nb_nodes * 3 * sizeof(double));
        /* not supported */
        // hm->node_vars = NULL;  // TODO: When should I set it ??
        // hm->nb_node_vars = 0;
        // hm->cell_vars = NULL;
        // hm->nb_cell_vars = 0;
        // hm->nb_nodes = nb_nodes;
        continue;
      }
      /* skip cells */
      if (cell_idx < nb_cells) {
        cell_idx++;
        continue;
      }
      /* nodes */
      if (node_idx < nb_nodes) {
        char* str = line;
        char* endptr;
        for (int k = 0; k < 3; k++) {
          double kk = strtod(str, &endptr);
          assert(errno != ERANGE);
          hm->nodes[node_idx * 3 + k] = kk;
          assert(str != endptr);
          str = endptr;
        }
        node_idx++;
        continue;
      }
    }  // while
    assert(node_idx == nb_nodes);
    fclose(file);
    assert(hm->nb_nodes == nb_nodes);
  }  // shared

  if (shared_nodes)
    assert(hm->nodes);
  else {
    assert(!hm->nodes);
    // assert(!hm->node_vars);
  }
  return EXIT_SUCCESS;
}

/* *********************************************************** */

int LibGraph_loadHybridMesh(LibGraph_HybridMesh** phm, bool shared_nodes, const char* infile)
{
  LibGraph_HybridMesh* hm = NULL;
  assert(infile);
  LibGraph_Mesh* cm = NULL;
  FILE* file = NULL;
  int nb_components = -1;
  int* elm_type = NULL;
  int* nb_cells_file = NULL;
  int nb_nodes = -1;
  int nb_cells = -1;
  int cell_idx = 0;
  int node_idx = 0;
  // int nb_node_vars = -1;
  // int nb_cell_vars = -1;
  int* cell_idx_per_cmp = NULL;
  char line[MAX_LINE_LENGTH];
  if (NULL == (file = fopen(infile, "r"))) {
    fprintf(stderr, "ERROR: opening file %s failed.\n", infile);
    return 0;
  }
  while (NULL != fgets(line, MAX_LINE_LENGTH, file)) {
    if (strlen(line) >= MAX_LINE_LENGTH - 1) fprintf(stderr, "WARNING: line truncated when reading (too long)!\n");
    /* skip commentary or void line */
    if ((0 == strncmp("\n", line, 1)) || (0 == strncmp("#", line, 1)) || (0 == strncmp("%", line, 1)))
      continue; /* skip */
    /* read mesh header */
    if (nb_components == -1) {
      char* str = line;
      char* endptr;
      nb_components = strtol(str, &endptr, 10);
      assert(nb_components > 0);
      str = endptr;
      int line_size = 2 * nb_components + 4;
      int* kk = malloc(line_size * sizeof(int));
      for (int k = 0; k < line_size; k++) {
        kk[k] = strtol(str, &endptr, 10);
        assert(kk >= 0);
        if (errno == ERANGE) {
          perror("loadMesh:");
          return 0;
        }
        str = endptr;
      }
      elm_type = malloc(nb_components * sizeof(int));
      nb_cells_file = malloc(nb_components * sizeof(int));
      assert(nb_cells_file && elm_type);
      for (int i = 0; i < nb_components; i++) elm_type[i] = kk[i];
      nb_nodes = kk[nb_components + 0];
      nb_cells = kk[nb_components + 1];
      for (int i = 0; i < nb_components; i++) nb_cells_file[i] = kk[nb_components + 2 + i];
      // nb_node_vars = kk[2 * nb_components + 2];
      // nb_cell_vars = kk[2 * nb_components + 3];
      /* check */
      assert(nb_nodes != -1 && nb_cells != -1);

      for (int i = 0; i < nb_components; i++) {
        assert(elm_type[i] == LIBGRAPH_LINE || elm_type[i] == LIBGRAPH_TRIANGLE || elm_type[i] == LIBGRAPH_QUADRANGLE ||
               elm_type[i] == LIBGRAPH_TETRAHEDRON || elm_type[i] == LIBGRAPH_HEXAHEDRON ||
               elm_type[i] == LIBGRAPH_PRISM);
        assert(nb_cells_file[i] <= nb_cells);
      }

      hm = LibGraph_newHybridMesh(nb_components);
      assert(hm);
      /* initialize component meshes - one time */
      hm->nb_components = nb_components;
      hm->shared_nodes = shared_nodes;

      for (int i = 0; i < hm->nb_components; i++) {
        cm = LibGraph_getHybridMeshComponent(hm, i);
        assert(cm);
        assert(!cm->nodes);
        cm->elm_type = elm_type[i];
        cm->nb_cells = nb_cells_file[i];
        cm->nb_nodes = 0;
        cm->nodes = NULL;
        cm->cells = malloc(cm->nb_cells * LIBGRAPH_ELEMENT_SIZE[cm->elm_type] * sizeof(int));
      }
      /* initialize shared nodes of hybridmesh - one time */
      if (hm->shared_nodes && !hm->nodes)
        hm->nodes = malloc(nb_nodes * 3 * sizeof(double));
      else {
        for (int i = 0; i < hm->nb_components; i++) {
          cm = LibGraph_getHybridMeshComponent(hm, i);
          cm->nb_nodes = nb_nodes;
          cm->nodes = malloc(nb_nodes * 3 * sizeof(double));
          // cm->nb_node_vars = nb_node_vars;
          // cm->node_vars = NULL;
        }
      }
      // hm->node_vars = NULL;  // TODO: When should I set it ??
      // hm->nb_node_vars = nb_node_vars;
      // hm->cell_vars = NULL;  //
      // hm->nb_cell_vars = nb_cell_vars;
      hm->nb_nodes = nb_nodes;
      cell_idx_per_cmp = calloc((hm->nb_components), sizeof(int));

      continue;
    }

    // should put the code to read cells and nodes
    if (cell_idx < nb_cells) {
      int i = LibGraph_locateMeshComponent(hm, cell_idx);
      cm = LibGraph_getHybridMeshComponent(hm, i);
      char* str = line;
      char* endptr;
      int elm_size = LIBGRAPH_ELEMENT_SIZE[cm->elm_type];
      for (int k = 0; k < elm_size; k++) {
        int kk = strtol(str, &endptr, 10);
        assert(kk >= 0);
        if (errno == ERANGE) {
          perror("loadMesh:");
          return 0;
        }
        cm->cells[cell_idx_per_cmp[i] * elm_size + k] = kk;
        str = endptr;
      }
      cell_idx_per_cmp[i]++;
      cell_idx++;
      continue;
    }

    /* nodes */
    if (node_idx < nb_nodes) {
      char* str = line;
      char* endptr;
      for (int k = 0; k < 3; k++) {
        double kk = strtod(str, &endptr);
        assert(errno != ERANGE);
        if (hm->shared_nodes && hm->nodes) { hm->nodes[node_idx * 3 + k] = kk; }
        else {
          for (int i = 0; i < hm->nb_components; i++) {
            cm = LibGraph_getHybridMeshComponent(hm, i);
            cm->nodes[node_idx * 3 + k] = kk;
          }
        }
        assert(str != endptr);
        str = endptr;
      }
      node_idx++;
      continue;
    }
  }  // while

  /* output */
  if (phm) *phm = hm;
  if (!phm && hm) {
    LibGraph_freeHybridMesh(hm);
    free(hm);
  }

  return EXIT_SUCCESS;
}

/* *********************************************************** */

int LibGraph_saveHybridMesh(const char* output, const LibGraph_HybridMesh* hm)
{
  assert(hm);
  FILE* file;
  LibGraph_Mesh* cm = NULL;
  int nb_cells = 0;
  int elm_size = 0;
  if (NULL == (file = fopen(output, "w"))) {
    fprintf(stderr, "Opening file %s failed.\n", output);
    return EXIT_FAILURE;
  }
  /* save header */
  fprintf(file,
          "# Mesh Header (nb_components, elm_types (* nb_components), nb_nodes, nb_cells, "
          "nb_cells_per_component (* nb_components) nb_node_vars, nb_cell_vars)\n");
  fprintf(file, "%d ", hm->nb_components);
  for (int i = 0; i < hm->nb_components; i++) {
    cm = LibGraph_getHybridMeshComponent(hm, i);
    assert(cm);
    fprintf(file, "%d ", cm->elm_type);
    nb_cells = nb_cells + cm->nb_cells;
  }
  if (hm->shared_nodes)
    fprintf(file, "%d ", hm->nb_nodes);
  else {
    cm = LibGraph_getHybridMeshComponent(hm, 0);
    fprintf(file, "%d ", cm->nb_nodes);
  }
  fprintf(file, "%d ", nb_cells);
  for (int i = 0; i < hm->nb_components; i++) {
    cm = LibGraph_getHybridMeshComponent(hm, i);
    fprintf(file, "%d ", cm->nb_cells);
  }
  if (hm->shared_nodes)
    fprintf(file, "%d ", -1);  // fprintf(file, "%d ", hm->nb_node_vars);
  else {
    cm = LibGraph_getHybridMeshComponent(hm, 0);
    fprintf(file, "%d ", -1);  // fprintf(file, "%d ", cm->nb_node_vars);
  }
  fprintf(file, "%d ", -1);  // fprintf(file, "%d ", hm->nb_cell_vars);
  fprintf(file, "\n");

  for (int k = 0; k < hm->nb_components; k++) {
    cm = LibGraph_getHybridMeshComponent(hm, k);
    assert(cm);
    fprintf(file, "# Component %d Cells %d\n", k, cm->elm_type);
    elm_size = LIBGRAPH_ELEMENT_SIZE[cm->elm_type];
    for (int i = 0; i < cm->nb_cells; i++) {
      for (int j = 0; j < elm_size; j++) { fprintf(file, "%d ", cm->cells[i * elm_size + j]); }
      fprintf(file, "\n");
    }
  }

  fprintf(file, "#Nodes\n");
  if (hm->shared_nodes && hm->nodes) {
    for (int i = 0; i < hm->nb_nodes; i++) {
      for (int j = 0; j < 3; j++) { fprintf(file, "%.10lf ", hm->nodes[i * 3 + j]); }
      fprintf(file, "\n");
    }
  }
  else {
    // take first component
    cm = LibGraph_getHybridMeshComponent(hm, 0);
    assert(cm);
    for (int i = 0; i < cm->nb_nodes; i++) {
      for (int j = 0; j < 3; j++) { fprintf(file, "%.10lf ", cm->nodes[i * 3 + j]); }
      fprintf(file, "\n");
    }
  }

  fclose(file);
  return EXIT_SUCCESS;
}

/* *********************************************************** */

int LibGraph_saveVTKHybridMesh(const char* output, const LibGraph_HybridMesh* hm)
{
  /* only line, triangle or quadrangle */
  assert(hm);
  int nb_cells = 0;
  LibGraph_Mesh* cm = NULL;
  int nb_nodes_cell = 0;
  int elm_size = 0;
  for (int i = 0; i < hm->nb_components; i++) {
    cm = LibGraph_getHybridMeshComponent(hm, i);
    assert(cm);
    assert(cm->elm_type == LIBGRAPH_LINE || cm->elm_type == LIBGRAPH_TRIANGLE || cm->elm_type == LIBGRAPH_QUADRANGLE ||
           cm->elm_type == LIBGRAPH_TETRAHEDRON || cm->elm_type == LIBGRAPH_HEXAHEDRON ||
           cm->elm_type == LIBGRAPH_PRISM);

    nb_cells = nb_cells + cm->nb_cells;
    nb_nodes_cell = nb_nodes_cell + ((LIBGRAPH_ELEMENT_SIZE[cm->elm_type] + 1) * cm->nb_cells);
    // nb_nodes = nb_nodes + cm -> nb_nodes;
  }
  FILE* file;
  /* open output file */
  if (NULL == (file = fopen(output, "w"))) {
    fprintf(stderr, "Opening file %s failed.\n", output);
    return EXIT_FAILURE;
  }

  /* save header */
  fprintf(file, "# vtk DataFile Version 2.0\n");
  fprintf(file, "Saved %s\n", output);
  fprintf(file, "ASCII\n");
  fprintf(file, "DATASET UNSTRUCTURED_GRID\n"); /* UNSTRUCTURED GRID, POLYDATA ??? */

  /* save section POINTS (3D coordinates) */
  if (hm->shared_nodes && hm->nodes) {
    fprintf(file, "POINTS %d double\n", hm->nb_nodes);
    for (int i = 0; i < hm->nb_nodes; i++)
      fprintf(file, "%f %f %f\n", hm->nodes[3 * i], hm->nodes[3 * i + 1], hm->nodes[3 * i + 2]);
    fprintf(file, "\n");
  }
  else {
    // take first component
    cm = LibGraph_getHybridMeshComponent(hm, 0);
    assert(cm);
    fprintf(file, "POINTS %d double\n", cm->nb_nodes);
    for (int i = 0; i < cm->nb_nodes; i++)
      fprintf(file, "%f %f %f\n", cm->nodes[3 * i], cm->nodes[3 * i + 1], cm->nodes[3 * i + 2]);
    fprintf(file, "\n");
  }

  /* save section CELLS*/
  fprintf(file, "CELLS %d %d\n", nb_cells, nb_nodes_cell);
  for (int k = 0; k < hm->nb_components; k++) {
    cm = LibGraph_getHybridMeshComponent(hm, k);
    assert(cm);
    elm_size = LIBGRAPH_ELEMENT_SIZE[cm->elm_type];
    for (int i = 0; i < cm->nb_cells; i++) {
      fprintf(file, "%d ", elm_size);
      for (int j = 0; j < elm_size; j++) {
        /* cell connectivity */
        fprintf(file, "%d ", cm->cells[elm_size * i + j]);
      }
      fprintf(file, "\n");
    }
  }
  fprintf(file, "\n");

  /* save section CELL_TYPES*/
  fprintf(file, "CELL_TYPES %d\n", nb_cells);
  for (int k = 0; k < hm->nb_components; k++) {
    cm = LibGraph_getHybridMeshComponent(hm, k);
    if (cm->elm_type == LIBGRAPH_POINT) { /* point */
      for (int i = 0; i < cm->nb_cells; i++) { fprintf(file, "1\n"); /* 1 refers to VTK_VERTEX type  */ }
    }
    else if (cm->elm_type == LIBGRAPH_LINE) { /* line */
      for (int i = 0; i < cm->nb_cells; i++) { fprintf(file, "3\n"); /* 3 refers to VTK_LINE type  */ }
    }
    else if (cm->elm_type == LIBGRAPH_TRIANGLE) { /* triangle */
      for (int i = 0; i < cm->nb_cells; i++) { fprintf(file, "5\n"); /* 5 refers to VTK_TRIANGLE type  */ }
    }
    else if (cm->elm_type == LIBGRAPH_QUADRANGLE) { /* quadrangle */
      for (int i = 0; i < cm->nb_cells; i++) { fprintf(file, "9\n"); /* 9 refers to VTK_QUAD type  */ }
    }
    else if (cm->elm_type == LIBGRAPH_TETRAHEDRON) { /* tetrahedron */
      for (int i = 0; i < cm->nb_cells; i++) { fprintf(file, "10\n"); /* 10 refers to VTK_TETRA type  */ }
    }
    else if (cm->elm_type == LIBGRAPH_HEXAHEDRON) { /* hexahedron */
      for (int i = 0; i < cm->nb_cells; i++) { fprintf(file, "12\n"); /* 12 refers to VTK_HEXAHEDRON type  */ }
    }
    else if (cm->elm_type == LIBGRAPH_PRISM) { /* prism */
      for (int i = 0; i < cm->nb_cells; i++) { fprintf(file, "13\n"); /* 13 refers to VTK_WEDGE type  */ }
    }
  }

#ifdef TO_UPDATE
  /* save section POINT_DATA */
  if (hm->shared_nodes && hm->nodes) {
    if (0 != hm->nb_node_vars) {
      fprintf(file, "\n");
      fprintf(file, "POINT_DATA %d \n", hm->nb_nodes);
      fprintf(file, "FIELD FieldData %d \n", hm->nb_node_vars);
      for (int i = 0; i < hm->nb_node_vars; i++) {
        const char* type;
        if (hm->node_vars[i].type == LIBGRAPH_VAR_INT)
          type = "int";
        else if (hm->node_vars[i].type == LIBGRAPH_VAR_DOUBLE)
          type = "double";
        else
          assert(0 && "Unknown variable type.");
        fprintf(file, "%s %d %d %s\n", hm->node_vars[i].id, hm->node_vars->nb_components, hm->node_vars->nb_tuples,
                type);
        if (NULL != hm->node_vars[i].data) {
          int data_size = hm->node_vars->nb_components * hm->node_vars->nb_tuples;
          for (int j = 0; j < data_size; j++) {
            if (hm->node_vars[i].type == LIBGRAPH_VAR_INT)
              fprintf(file, "%d ", ((int*)hm->node_vars[i].data)[j]);
            else if (hm->node_vars[i].type == LIBGRAPH_VAR_DOUBLE)
              fprintf(file, "%lf ", ((double*)hm->node_vars[i].data)[j]);
            if ((i + 1) % (3 * hm->node_vars->nb_components) == 0) fprintf(file, "\n");
          }
        }
        fprintf(file, "\n");
      }
    }
  }       // end if
  else {  // same thing but with first component
    cm = LibGraph_getHybridMeshComponent(hm, 0);
    if (0 != cm->nb_node_vars) {
      fprintf(file, "\n");
      fprintf(file, "POINT_DATA %d \n", cm->nb_nodes);
      fprintf(file, "FIELD FieldData %d \n", cm->nb_node_vars);
      for (int i = 0; i < cm->nb_node_vars; i++) {
        const char* type;
        if (cm->node_vars[i].type == LIBGRAPH_VAR_INT)
          type = "int";
        else if (cm->node_vars[i].type == LIBGRAPH_VAR_DOUBLE)
          type = "double";
        else
          assert(0 && "Unknown variable type.");
        fprintf(file, "%s %d %d %s\n", cm->node_vars[i].id, cm->node_vars->nb_components, cm->node_vars->nb_tuples,
                type);
        if (NULL != cm->node_vars[i].data) {
          int data_size = cm->node_vars->nb_components * cm->node_vars->nb_tuples;
          for (int j = 0; j < data_size; j++) {
            if (cm->node_vars[i].type == LIBGRAPH_VAR_INT)
              fprintf(file, "%d ", ((int*)cm->node_vars[i].data)[j]);
            else if (cm->node_vars[i].type == LIBGRAPH_VAR_DOUBLE)
              fprintf(file, "%lf ", ((double*)cm->node_vars[i].data)[j]);
            if ((i + 1) % (3 * cm->node_vars->nb_components) == 0) fprintf(file, "\n");
          }
        }
        fprintf(file, "\n");
      }
    }
  }  // end else
  /* save section CELL_DATA */

  if (0 != hm->nb_cell_vars) {
    fprintf(file, "\n");
    fprintf(file, "CELL_DATA %d \n", LibGraph_getHybridMeshNbCells(hm));
    fprintf(file, "FIELD FieldData %d \n", hm->nb_cell_vars);
    for (int i = 0; i < hm->nb_cell_vars; i++) {
      const char* type;
      if (hm->cell_vars[i].type == LIBGRAPH_VAR_INT)
        type = "int";
      else if (hm->cell_vars[i].type == LIBGRAPH_VAR_DOUBLE)
        type = "double";
      else
        assert(0 && "Unknown variable type.");
      fprintf(file, "%s %d %d %s\n", hm->cell_vars[i].id, hm->cell_vars->nb_components, hm->cell_vars->nb_tuples, type);
      if (NULL != hm->cell_vars[i].data) {
        int data_size = hm->cell_vars->nb_components * hm->cell_vars->nb_tuples;
        for (int j = 0; j < data_size; j++) {
          if (hm->cell_vars[i].type == LIBGRAPH_VAR_INT)
            fprintf(file, "%d ", ((int*)hm->cell_vars[i].data)[j]);
          else if (hm->cell_vars[i].type == LIBGRAPH_VAR_DOUBLE)
            fprintf(file, "%lf ", ((double*)hm->cell_vars[i].data)[j]);
          if ((i + 1) % (3 * hm->cell_vars->nb_components) == 0) /* le modulo est juste  un choix pour l'affichage */
            fprintf(file, "\n");
        }
      }
      fprintf(file, "\n");
    }
  }

#endif

  /* IF CELL_VAR IS CODED INTO EACH COMPONENT MESH */
  /*   /\* TODO : for all components ??*\/ */
  /*   for (int k = 0; k < hm->nb_components; k++) { */
  /*     cm = getHybridMeshComponent(hm,k);   */
  /*     if(0 != cm->nb_cell_vars) { */
  /* 	fprintf(file, "\n") ; */
  /* 	fprintf(file, "CELL_DATA %d \n", cm->nb_cells); */
  /* 	fprintf(file, "FIELD FieldData %d \n", cm->nb_cell_vars); */
  /* 	for ( int i = 0 ; i < cm->nb_cell_vars ; i++) { */
  /* 	  const char *type; */
  /* 	  if (cm->cell_vars[i].type == VAR_INT) */
  /* 	    type = "int"; */
  /* 	  else if (cm->cell_vars[i].type == VAR_DOUBLE) */
  /* 	    type = "double"; */
  /* 	  else */
  /* 	    assert (0 && "Unknown variable type."); */
  /* 	  fprintf(file, "%s %d %d %s\n", cm->cell_vars[i].id, cm->cell_vars->nb_components,  */
  /* 		  cm->cell_vars->nb_tuples, type);	 */
  /* 	  if(NULL != cm->cell_vars[i].data){ */
  /* 	    int data_size = cm->cell_vars->nb_components * cm->cell_vars->nb_tuples ; */
  /* 	    for ( int j = 0 ; j < data_size ; j++) { */
  /* 	      if (cm->cell_vars[i].type == VAR_INT) */
  /* 		fprintf(file, "%d ", ((int *)cm->cell_vars[i].data)[j]); */
  /* 	      else if (cm->cell_vars[i].type == VAR_DOUBLE) */
  /* 		fprintf(file, "%lf ", ((double *)cm->cell_vars[i].data)[j]); */
  /* 	      if ((i+1)%(3 * cm->cell_vars->nb_components)==0) /\* le modulo est juste  un choix
   * pour l'affichage *\/ */
  /* 		fprintf(file, "\n") ; */
  /* 	    } */
  /* 	  } */
  /* 	  fprintf(file, "\n") ; */
  /* 	} */
  /*     } */
  /*   }// for all components */

  fclose(file);
  return 1;
}

/* *********************************************************** */

void LibGraph_savePartition(int n, const int* parts, const char* outfile)
{
  FILE* file = fopen(outfile, "w");
  assert(file != NULL);
  for (int i = 0; i < n; i++) fprintf(file, "%d\n", parts[i]);
  fclose(file);
}

/* *********************************************************** */

void LibGraph_loadPartition(const char* infile, int n, int* parts)
{
  FILE* file = fopen(infile, "r");
  assert(file != NULL);
  for (int i = 0; i < n; i++) {
    fscanf(file, "%d\n", &parts[i]);
    assert(parts[i] >= 0);
  }
  fclose(file);
}

/* *********************************************************** */

int LibGraph_loadCoordinates(const char* infile, int n, double* coords)
{
  FILE* file = fopen(infile, "r");
  assert(file);
  if (!file) return 0;
  double x, y, z;
  for (int i = 0; i < n; i++) {
    if (fscanf(file, "%lf %lf %lf\n", &x, &y, &z) == 3) {
      coords[3 * i + 0] = x;
      coords[3 * i + 1] = y;
      coords[3 * i + 2] = z;
    }
    else
      return 0;  // fail to read coordinates
  }
  fclose(file);
  return 1;
}

int LibGraph_loadParallelCoordinates(const char* infile, const LibGraph_DistGraph* g, double* coords)
{
  FILE* file = fopen(infile, "r");
  assert(file);
  if (!file) return 0;

  int total_vtxs = g->vtxdist[g->psize];
  int start = g->vtxdist[g->prank];
  int end = g->vtxdist[g->prank + 1];

  double x, y, z;
  for (int i = 0; i < total_vtxs; i++) {
    if (fscanf(file, "%lf %lf %lf\n", &x, &y, &z) == 3) {
      if (i < start)
        continue;  // not yet our turn
      else if (i >= end)
        break;  // finished
      coords[3 * i + 0] = x;
      coords[3 * i + 1] = y;
      coords[3 * i + 2] = z;
    }
    else
      return 0;  // fail to read coordinates
  }
  fclose(file);
  return 1;
}
/* *********************************************************** */

int LibGraph_saveCoordinates(const char* outfile, int n, const double* coords)
{
  FILE* file = fopen(outfile, "w");
  assert(file);
  if (!file) return 0;
  double x, y, z;
  for (int i = 0; i < n; i++) {
    x = coords[3 * i + 0];
    y = coords[3 * i + 1];
    z = coords[3 * i + 2];
    fprintf(file, "%lf %lf %lf\n", x, y, z);
  }
  fclose(file);
  return 1;
}

/* *********************************************************** */

int LibGraph_loadScotchCoordinates(const char* infile, int n, double* coords)
{
  FILE* file = fopen(infile, "r");
  if (file == NULL) return EXIT_FAILURE;

  /* header */
  int n0 = 0, ncoords = 0;
  assert(fscanf(file, "%d %d", &ncoords, &n0) == 2);
  assert(n0 == n && ncoords == 3);

  for (int i = 0; i < n; i++) {
    assert(fscanf(file, "%*d %lf %lf %lf", &coords[3 * i + 0], &coords[3 * i + 1], &coords[3 * i + 2]) == 3);
  }
  fclose(file);

  return 1;
}

/* *********************************************************** */

/*
   Tulip Syntax
   (tlp "2.0"
   ; commentary
   (author "toto")
   ; (nodes id_node1 id_node2 ...)
   (nodes 1 2 3 4 5 )
   ; (edge id id_source id_target)
   (edge 2 2 1)
   ; properties
   ; ...
   )
*/

int LibGraph_saveTulipGraph(const char* output, const LibGraph_Graph* g, char labels[][255],
                            float coords[][3])

{
  // TODO: convert 1d array into 2d array
  // given int* coords, use _coords = float (*)[3] (coords)

  /* open output file */
  FILE* file;
  if (NULL == (file = fopen(output, "w"))) {
    fprintf(stderr, "ERROR: opening file %s failed.\n", output);
    return 0;
  }

  /* header */
  fprintf(file, "(tlp \"2.0\"\n");
  fprintf(file, "(author \"lbc2\")\n\n");

  /* nodes */
  fprintf(file, "(nodes ");
  for (int i = 0; i < g->nvtxs; i++) fprintf(file, "%d ", i);
  fprintf(file, ")\n\n");

  /* edges */
  int k = 0;
  for (int i = 0; i < g->nvtxs; i++)
    for (int j = g->xadj[i]; j < g->xadj[i + 1]; j++) {
      if (i <= g->adjncy[j]) {
        fprintf(file, "(edge %d %d %d)\n", k, i, g->adjncy[j]);
        k++;
      }
    }

  /* labels */
  fprintf(file, "\n(property  0 string \"viewLabel\"\n");
  fprintf(file, " (default \"\" \"\" )\n");
  if (labels) {
    for (int i = 0; i < g->nvtxs; i++) fprintf(file, " (node %d \"%s\")\n", i, labels[i]);
  }
  else {
    for (int i = 0; i < g->nvtxs; i++) fprintf(file, " (node %d \"%d\")\n", i, i);  // default
  }
  fprintf(file, ")\n");

  /* coords */
  if (coords) {
    fprintf(file, "\n(property  0 layout \"viewLayout\"\n");
    fprintf(file, " (default \"(0,0,0)\" \"()\" )\n");
    for (int i = 0; i < g->nvtxs; i++)
      fprintf(file, " (node %d \" (%.2f,%.2f,%.2f) \")\n", i, coords[i][0], coords[i][1], coords[i][2]);
    fprintf(file, ")\n");
  }

  /* eof */

  fprintf(file, "\n)\n");
  fclose(file);
  return 1;
}

/* *********************************************************** */

int LibGraph_saveTulipBigraph(const char* output,     /**< [in] output filename (Tulip format) */
                              int nvtxsA,             /**< [in] nb vertex in part A */
                              int nvtxsB,             /**< [in] nb vertex in part B */
                              const LibGraph_Graph* g /**< [in] Graph struct */
)
{
  /* labels */
  char labels[nvtxsA + nvtxsB][255];
  for (int i = 0; i < nvtxsA; i++) sprintf(labels[i], "A%d", i);
  for (int j = 0; j < nvtxsB; j++) sprintf(labels[nvtxsA + j], "B%d", j);
  /* layout */
  float coords[nvtxsA + nvtxsB][3];
  for (int i = 0; i < nvtxsA; i++) {
    coords[i][0] = 0;
    coords[i][1] = i * 100 * nvtxsB;
    coords[i][2] = 0;
  }
  for (int j = 0; j < nvtxsB; j++) {
    coords[nvtxsA + j][0] = nvtxsA * nvtxsB * 50;
    coords[nvtxsA + j][1] = j * 100 * nvtxsA;
    coords[nvtxsA + j][2] = 0;
  }
  /* save */
  return LibGraph_saveTulipGraph(output, g, labels, coords);
}

/* *********************************************************** */

int LibGraph_saveScotchGraph(const char* basename, const LibGraph_Graph* g, int nparts, const int* part)
{
  assert(0);  // TODO

  /* char outputgrf[256]; */
  /* snprintf(outputgrf, sizeof(outputgrf), "%s.grf", basename); */
  /* char outputmap[256]; */
  /* snprintf(outputmap, sizeof(outputmap), "%s.map", basename); */

  /* /\* open output files *\/ */
  /* FILE * filegrf; */
  /* FILE * filemap; */

  /* if (NULL == (filegrf = fopen(outputgrf, "w"))) { */
  /*   fprintf(stderr, "Opening file %s failed.\n", outputgrf); */
  /*   return 0; */
  /* } */

  /* if (part) { */
  /*   if (NULL == (filemap = fopen(outputmap, "w"))) { */
  /*     fprintf(stderr, "Opening file %s failed.\n", outputmap); */
  /*     return 0; */
  /*   } */
  /* } */

  /* SCOTCH_Graph sg; */
  /* SCOTCH_graphInit(&sg); */
  /* SCOTCH_graphBuild(&sg, 0, g->nvtxs, g->xadj, g->xadj+1, g->vwgts, NULL, 2*g->nedges, g->adjncy,
   * g->ewgts); */
  /* SCOTCH_graphCheck(&sg); */
  /* SCOTCH_graphSave(&sg, filegrf); */
  /* fclose(filegrf); */

  /* if(part) { */
  /*   SCOTCH_Mapping map; */
  /*   SCOTCH_Arch arch; */
  /*   SCOTCH_archInit(&arch); */
  /*   SCOTCH_archCmplt(&arch, nparts); */
  /*   SCOTCH_graphMapInit(&sg, &map, &arch, part); */
  /*   SCOTCH_graphMapSave(&sg, &map, filemap); */
  /*   SCOTCH_graphMapExit(&sg, &map); */
  /*   SCOTCH_archExit(&arch); */
  /*   fclose(filemap); */
  /* } */

  /* SCOTCH_graphExit(&sg); */

  return 1;
}

/* *********************************************************** */

/* Write "bitmap" to a PNG file specified by "path"; returns 0 on
   success, non-zero on error.
   -> http://www.libpng.org/pub/png/libpng-1.2.5-manual.html
   -> http://www.lemoda.net/c/write-png/
*/

#ifdef LIBGRAPH_USE_PNG

static int LibGraph_savePNG(size_t width, size_t height,
                            uint8_t* rgb, /* array of pixels (red, green, blue) of size width*height*3 bytes */
                            const char* path)
{
  FILE* fp;
  png_structp png_ptr = NULL;
  png_infop info_ptr = NULL;
  png_byte** row_pointers = NULL;

  assert(sizeof(png_byte) == sizeof(uint8_t));

  int status = -1;
  int pixel_size = 3;
  int depth = 8;

  fp = fopen(path, "wb");
  if (!fp) goto fopen_failed;

  png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
  if (png_ptr == NULL) goto png_create_write_struct_failed;

  info_ptr = png_create_info_struct(png_ptr);
  if (info_ptr == NULL) goto png_create_info_struct_failed;

  /* Set up error handling. */
  if (setjmp(png_jmpbuf(png_ptr))) goto png_failure;

  /* Set image attributes. */
  png_set_IHDR(png_ptr, info_ptr, width, height, depth, PNG_COLOR_TYPE_RGB, PNG_INTERLACE_NONE,
               PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);

  /* Initialize rows of PNG. */
  row_pointers = png_malloc(png_ptr, height * sizeof(png_byte*));
  for (size_t y = 0; y < height; ++y) row_pointers[y] = rgb + y * width * pixel_size;

  /* Write the image data to "fp". */

  png_init_io(png_ptr, fp);
  png_set_rows(png_ptr, info_ptr, row_pointers);
  png_write_png(png_ptr, info_ptr, PNG_TRANSFORM_IDENTITY, NULL);

  /* Return */

  status = 0;
  png_free(png_ptr, row_pointers);

png_failure:
png_create_info_struct_failed:
  png_destroy_write_struct(&png_ptr, &info_ptr);
png_create_write_struct_failed:
  fclose(fp);
fopen_failed:

  return status;
}

#endif

/* *********************************************************** */

#ifdef LIBGRAPH_USE_PNG

int LibGraph_saveMatrixImage(const char* output,    /**< [in] output filename (PNG format) */
                             int size,              /**< [in] square image size (or 0 if size = black->nvtxs) */
                             LibGraph_Graph* red,   /**< [in] red graph (or NULL)  */
                             LibGraph_Graph* green, /**< [in] green graph (or NULL)  */
                             LibGraph_Graph* blue   /**< [in] blue graph (or NULL)  */
)
{
  int nvtxs = 0;
  assert(red || green || blue);
  if (red)
    nvtxs = red->nvtxs;
  else if (green)
    nvtxs = green->nvtxs;
  else
    nvtxs = blue->nvtxs;
  if (size <= 0 || size > nvtxs) size = nvtxs;  // use 1 pixel per matrix element
  if (blue) assert(blue->nvtxs == nvtxs);
  if (green) assert(green->nvtxs == nvtxs);
  if (red) assert(red->nvtxs == nvtxs);

  uint8_t* rgb = calloc(size * size * 3, sizeof(uint8_t));
  assert(rgb);
  memset(rgb, 0x00, size * size * 3 * sizeof(uint8_t));  // black image
  // memset(rgb, 0xFF, size*size*3*sizeof(uint8_t)); // white image

  int q = nvtxs / size;
  int r = nvtxs % size;
  assert(q * size + r == nvtxs);
  int off = r * (q + 1);

  // build image
  for (int i = 0; i < nvtxs; i++) {
    int ii = 0;
    if (i < off)
      ii = i / (q + 1);
    else
      ii = r + (i - off) / q;
    assert(ii >= 0 && ii < size);

    // red graph
    if (red) {
      for (int k = red->xadj[i]; k < red->xadj[i + 1]; k++) {
        int j = red->adjncy[k]; /* edge (i,j) */
        int jj = 0;
        if (j < off)
          jj = j / (q + 1);
        else
          jj = r + (j - off) / q;
        assert(jj >= 0 && jj < size);
        rgb[ii * size * 3 + jj * 3 + 0] |= 0x00; /* red */
        rgb[ii * size * 3 + jj * 3 + 1] |= 0xFF; /* green */
        rgb[ii * size * 3 + jj * 3 + 2] |= 0xFF; /* blue */
      }
    }

    // green graph
    if (green) {
      for (int k = green->xadj[i]; k < green->xadj[i + 1]; k++) {
        int j = green->adjncy[k]; /* edge (i,j) */
        int jj = 0;
        if (j < off)
          jj = j / (q + 1);
        else
          jj = r + (j - off) / q;
        assert(jj >= 0 && jj < size);
        rgb[ii * size * 3 + jj * 3 + 0] |= 0xFF; /* red */
        rgb[ii * size * 3 + jj * 3 + 1] |= 0x00; /* green */
        rgb[ii * size * 3 + jj * 3 + 2] |= 0xFF; /* blue */
      }
    }

    // blue graph
    if (blue) {
      for (int k = blue->xadj[i]; k < blue->xadj[i + 1]; k++) {
        int j = blue->adjncy[k]; /* edge (i,j) */
        int jj = 0;
        if (j < off)
          jj = j / (q + 1);
        else
          jj = r + (j - off) / q;
        assert(jj >= 0 && jj < size);
        rgb[ii * size * 3 + jj * 3 + 0] |= 0xFF; /* red */
        rgb[ii * size * 3 + jj * 3 + 1] |= 0xFF; /* green */
        rgb[ii * size * 3 + jj * 3 + 2] |= 0x00; /* blue */
      }
    }
  }

  // inverse image
  for (int i = 0; i < size * size * 3; i++) rgb[i] = ~rgb[i];

  // debug
  /* for(int i = 0 ; i < size ; i++) { */
  /*   for(int j = 0 ; j < size ; j++)  */
  /*     if(rgb[i*size*3+j*3] > 0) printf("x"); else printf(" "); */
  /*   printf("\n");     */
  /* } */

  // save image
  int status = LibGraph_savePNG(size, size, rgb, output);
  assert(status == 0);

  return status;
}

#endif

/* *********************************************************** */

void LibGraph_saveSVGGraph(const char* svgfile, const LibGraph_Graph* graph, const double* coords, int vtag, int etag)
{
  double _coords[graph->nvtxs][2];
  for (int i = 0; i < graph->nvtxs; i++) _coords[i][0] = coords[i * 3], _coords[i][1] = coords[i * 3 + 1];
  FILE* svg;
  svg = fopen(svgfile, "w+");
  LibGraph_svgBegin(svg);
  LibGraph_svgDrawGraph(svg, graph, _coords, vtag, etag, 0);  // vtag, etag, oriented
  LibGraph_svgEnd(svg);
  fclose(svg);
}

/* *********************************************************** */

void LibGraph_saveSVGGraphPart(const char* svgfile, const LibGraph_Graph* graph, const double* coords, int nparts, const int* part, int vtag,
                               int etag)
{
  double _coords[graph->nvtxs][2];
  for (int i = 0; i < graph->nvtxs; i++) _coords[i][0] = coords[i * 3], _coords[i][1] = coords[i * 3 + 1];
  FILE* svg;
  svg = fopen(svgfile, "w+");
  LibGraph_svgBegin(svg);
  LibGraph_svgDrawGraphPart(svg, graph, _coords, vtag, etag, nparts, part);  // vtag, etag
  LibGraph_svgEnd(svg);
  fclose(svg);
}

/* *********************************************************** */
