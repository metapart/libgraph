#ifndef LIBGRAPH_QUEUE_H
#define LIBGRAPH_QUEUE_H

//@{

/* *********************************************************** */
/*                          QUEUE                              */
/* *********************************************************** */

/** Queue structure. */
typedef struct _LibGraph_Queue
{
  int* tab;
  int size;
  int start;
  int end;
} LibGraph_Queue;

void LibGraph_queueInit(LibGraph_Queue* self, int size);
void LibGraph_queuePushBack(LibGraph_Queue* self, int v);
int LibGraph_queuePopFront(LibGraph_Queue* self);
int LibGraph_queueIsEmpty(const LibGraph_Queue* self);
void LibGraph_queueFree(LibGraph_Queue* self);

//@}

#endif
