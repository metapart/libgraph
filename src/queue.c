#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "queue.h"

/* *********************************************************** */

void LibGraph_queueInit(LibGraph_Queue* self, int size)
{
  self->size = size + 1;
  self->tab = malloc(self->size * sizeof(int));
  self->start = 0;
  self->end = 0;
}

/* *********************************************************** */

void LibGraph_queuePushBack(LibGraph_Queue* self, int v)
{
  self->tab[self->end++] = v;
  if (self->end >= self->size) self->end -= self->size;
}

/* *********************************************************** */

int LibGraph_queuePopFront(LibGraph_Queue* self)
{
  int v = self->tab[self->start++];
  if (self->start >= self->size) self->start -= self->size;
  return v;
}

/* *********************************************************** */

int LibGraph_queueIsEmpty(const LibGraph_Queue* self)
{
  return self->start == self->end;
}

/* *********************************************************** */

void LibGraph_queueFree(LibGraph_Queue* self)
{
  free(self->tab);
}

/* *********************************************************** */
