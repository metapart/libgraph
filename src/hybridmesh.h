#ifndef LIBGRAPH_HYBRIDMESH_H
#define LIBGRAPH_HYBRIDMESH_H

#include <stdbool.h>
#include "mesh.h"

//@{

/* *********************************************************** */
/*                  HYBRIDMESH STRUCT                          */
/* *********************************************************** */

/** HybridMesh structure. */
typedef struct _LibGraph_HybridMesh
{
  int nb_components;          // number of mesh components
  LibGraph_Mesh* components;  // array of mesh components
  bool shared_nodes;          // boolean flag to indicate if nodes are stored here (shared) or are distributed to the components
  int nb_nodes;
  double* nodes;  // NULL
  // int nb_node_vars;
  // LibGraph_Variable* node_vars;  // NULL
  // int nb_cell_vars;
  // LibGraph_Variable* cell_vars;  // NULL
} LibGraph_HybridMesh;

/** Allocate a HybridMesh with a certain number of components. */
LibGraph_HybridMesh* LibGraph_newHybridMesh(int nb_components);

/** Duplicate a HybridMesh. */
void LibGraph_dupHybridMesh(LibGraph_HybridMesh* oldg, LibGraph_HybridMesh* newg);

/** Free memory allocated within HybridMesh structure. */
void LibGraph_freeHybridMesh(LibGraph_HybridMesh* hm);

/** Print hybridMesh */
void LibGraph_printHybridMesh(LibGraph_HybridMesh* hm);
/* Return the component i */
LibGraph_Mesh* LibGraph_getHybridMeshComponent(const LibGraph_HybridMesh* hm, int i);

/* Return the number of cells in the hybridmesh */
int LibGraph_getHybridMeshNbCells(LibGraph_HybridMesh* hm);
/* Return the number of nodes in the hybridmesh */
int LibGraph_getHybridMeshNbNodes(LibGraph_HybridMesh* hm);

/* Locate the mesh component that corresponds
to an hybridmesh cell_idx */
int LibGraph_locateMeshComponent(LibGraph_HybridMesh* hm, int cell_idx);

//@}

#endif
