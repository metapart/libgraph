#ifndef LIBGRAPH_REFINE_H
#define LIBGRAPH_REFINE_H

#include "graph.h"

//@{

/* *********************************************************** */
/*                     REFINEMENT ROUTINE                      */
/* *********************************************************** */

/** Compute band graph.
 * \return the number of vertices inside band.
 */
int LibGraph_createBandGraph(const LibGraph_Graph* g, /**< [in] graph */
                             const int nparts,        /**< [in] nb of partitions */
                             const int* part,         /**< [in] array of partitions */
                             const int* locked, /**< [in] boolean array of locked vertices in part (of size g->nvtxs) */
                             LibGraph_Graph* band, /**< [out] band graph */
                             int width,            /**< [in] band width */
                             int* frontier /**< [out] frontier array (of size g->nvtxs, non-null element if inside the
                                              band graph) */
);

/** Refine graph with Greedy strategy based on "Multilevel k-way
    Partitioning Scheme for Iregular Graphs" by Karypis and Kumar,
    JPDC'98. */
/** try to optimize balance and cut alternately */
/** \return edgecut gain after refinement */
int LibGraph_refineGreedy(
    LibGraph_Graph* g, /**< [in] graph */
    int nparts,        /**< [in] nb of input parts */
    int ubfactor,      /**< [in] unbalanced factor */
    int* part,         /**< [in/out] array of input partition (of size g->nvtxs) */
    int* locked,       /**< [in] boolean array of locked vertices in part (of size g->nvtxs) */
    int npass,         /**< [in] nb of refinement passes (-1 for infinite passes until convergence) */
    int maxnegmove     /**< [in] max negative move allowed for each refinment pass (-1 for infinite) */
);

/** Same as refineGreedy(), but try to optimize both balance and cut
   simultaneously. */
int LibGraph_refineGreedy2(
    LibGraph_Graph* g, /**< [in] graph */
    int nparts,        /**< [in] nb of input parts */
    int ubfactor,      /**< [in] unbalanced factor */
    int* part,         /**< [in/out] array of input partition (of size g->nvtxs) */
    int* locked,       /**< [in] boolean array of locked vertices in part (of size g->nvtxs) */
    int npass,         /**< [in] nb of refinement passes (-1 for infinite passes until convergence) */
    int maxnegmove     /**< [in] max negative move allowed for each refinment pass (-1 for infinite) */
);

/* *********************************************************** */

//@}

#endif
