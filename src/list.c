#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "list.h"
#include "sort.h"

/* *********************************************************** */

void LibGraph_initList(LibGraph_List* l, int max)
{
  assert(l);
  assert(max >= 0);
  l->size = 0;
  l->max = max;
  l->array = NULL;
  if (max > 0) {
    l->array = malloc(max * sizeof(int));
    assert(l->array);
  }
}

/* *********************************************************** */

void LibGraph_freeList(LibGraph_List* l)
{
  assert(l);
  if (l->size > 0 && l->array) free(l->array);
  l->array = NULL;
}

/* *********************************************************** */

void LibGraph_resetList(LibGraph_List* l)
{
  assert(l);
  l->size = 0;
}

/* *********************************************************** */

void LibGraph_insertList(LibGraph_List* l, int element)
{
  assert(l);
  // assert(l->array);
  // assert(l->max > 0);

  /* if not enough memory before insertion */
  if (l->size == l->max) {
    l->max = 2 * (l->max + 1);
    l->array = realloc(l->array, l->max * sizeof(int));
    assert(l->array);
  }

  /* insert element at the end */
  l->array[l->size] = element;
  l->size++;
}

/* *********************************************************** */

void LibGraph_sortList(LibGraph_List* l)
{
  assert(l);
  if (l->size == 0) return;
  int* perm = malloc(l->size * sizeof(int));
  int* oldarray = malloc(l->size * sizeof(int));
  LibGraph_sortI(l->size, l->array, perm);
  memcpy(oldarray, l->array, l->size * sizeof(int));
  for (int i = 0; i < l->size; i++) l->array[i] = oldarray[perm[i]];
  free(perm);
  free(oldarray);
}

/* *********************************************************** */

int LibGraph_findSortedList(LibGraph_List* l, int element)
{
  assert(l);
  int lb = 0;
  int ub = l->size - 1;

  while (lb < ub) {
    /* divide in two intervals: [lb,k] and [k+1,ub] */
    int length = ub - lb + 1;    /* length >= 2 */
    int k = lb + length / 2 - 1; /* k >= lb */
    assert(lb <= k && k + 1 <= ub);
    if (element <= l->array[k])
      ub = k;
    else
      lb = k + 1;
  }

  if (lb == ub && l->array[lb] == element) return lb;

  return -1; /* not found */
}

/* *********************************************************** */

int LibGraph_insertSortedList(LibGraph_List* l, int element, int duplicate)
{
  assert(l);
  assert(l->array);
  assert(l->max > 0);

  /* if not enough memory before insertion */
  if (l->size == l->max) {
    l->max *= 2;
    l->array = realloc(l->array, l->max * sizeof(int));
    assert(l->array);
  }

  /* look for the element position k */
  int k = 0;
  while (k < l->size) {
    if (element < l->array[k]) break;
    k++;
  }

  if (!duplicate && element == l->array[k]) return 0;  // duplicated element not allowed

  /* shift elements [k, l->size-1] */
  for (int i = l->size; i > k; i--) l->array[i] = l->array[i - 1];

  /* insert the element in position k */
  l->array[k] = element;
  l->size++;

  return 1;
}

/* *********************************************************** */

void LibGraph_mergeSortedList(LibGraph_List* la, LibGraph_List* lb, LibGraph_List* lc, int duplicate)
{
  assert(la && lb && lc);
  LibGraph_initList(lc, la->size + lb->size);

  int ka = 0;
  int kb = 0;

  while (ka < la->size && kb < lb->size) {
    int va = la->array[ka];
    int vb = lb->array[kb];
    if (va < vb) {
      LibGraph_insertList(lc, va);
      ka++;
    }
    else if (vb < va) {
      LibGraph_insertList(lc, vb);
      kb++;
    }
    else {
      LibGraph_insertList(lc, va);
      if (duplicate) LibGraph_insertList(lc, vb);
      ka++;
      kb++;
    }
  }
  for (int k = ka; k < la->size; k++) LibGraph_insertList(lc, la->array[k]);
  for (int k = kb; k < lb->size; k++) LibGraph_insertList(lc, lb->array[k]);

  assert(lc->size >= la->size && lc->size >= lb->size);
  assert(lc->size <= la->size + lb->size);
}

/* *********************************************************** */

void LibGraph_printList(LibGraph_List* l)
{
  assert(l);
  printf("{");
  for (int i = 0; i < l->size; i++) printf("%d ", l->array[i]);
  printf("}\n");
}

/* *********************************************************** */
