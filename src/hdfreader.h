#ifndef LIBGRAPH_HDFREADER_H
#define LIBGRAPH_HDFREADER_H

#ifdef LIBGRAPH_USE_HDF5

#include <hdf5.h>

herr_t LibGraph_hdfPrintGroups(hid_t file_id, char** grouplist, int groupsize, int hybrid, char* outfile);
herr_t LibGraph_hdfCheckGroups(hid_t file_id, char** grouplist, int groupsize);
herr_t LibGraph_hdfOpenFile(const char* filename, hid_t* fileid);

#endif

#endif
