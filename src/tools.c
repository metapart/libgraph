#include <assert.h>
#include <limits.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "debug.h"
#include "graph.h"
#include "hypergraph.h"
#include "sort.h"
#include "tools.h"

/* *********************************************************** */

#define MIN(a, b) (((a) <= (b)) ? (a) : (b))
#define MAX(a, b) (((a) >= (b)) ? (a) : (b))

#define MAXNBCELLSPERNODE 60 /* max nb of cells shared by a node */

/* *********************************************************** */

/* Build a mesh dual structure (dual[k] is the list of all cells sharing by the k-th node in mesh).
 */
void LibGraph_mesh2DualList(LibGraph_Mesh* mesh, /** input mesh */
                            LibGraph_List* dual  /** array of mesh->nb_nodes lists */
)
{
  assert(mesh && dual);
  assert(mesh->nodes);
  int elm_size = LIBGRAPH_ELEMENT_SIZE[mesh->elm_type];

  /* initialize List */
  for (int i = 0; i < mesh->nb_nodes; i++) {
    dual[i].size = 0;
    dual[i].max = MAXNBCELLSPERNODE;
    dual[i].array = malloc(dual[i].max * sizeof(int));
  }

  /* for all nodes of each cell */
  for (int i = 0; i < mesh->nb_cells; i++) {
    for (int j = 0; j < elm_size; j++) {
      int node = mesh->cells[i * elm_size + j];
      assert(node < mesh->nb_nodes);
      if (dual[node].size + 1 > dual[node].max) {
        dual[node].max *= 2;
        dual[node].array = realloc(dual[node].array, dual[node].max * sizeof(int));
        assert(dual[node].array != NULL);
      }
      dual[node].array[dual[node].size++] = i;
    }
  }

  /*   printf("DUAL\n"); */
  /*   for(int i = 0 ; i < mesh->nb_nodes ; i++) { */
  /*     printf("  %d -> [ ", i); */
  /*     for(int j = 0 ; j < dual[i].size ; j++) { */
  /*       printf("%d ", dual[i].array[j]); */
  /*     } */
  /*     printf("]\n"); */
  /*   } */
}

/* *********************************************************** */

void LibGraph_graph2LineMesh(const LibGraph_Graph* graph, const double* coords, LibGraph_Mesh* mesh)
{
  mesh->elm_type = LIBGRAPH_LINE; /* lines */
  mesh->nb_nodes = graph->nvtxs;
  mesh->nb_cells = graph->narcs / 2;
  mesh->nodes = malloc(3 * mesh->nb_nodes * sizeof(double));
  mesh->cells = malloc(2 * mesh->nb_cells * sizeof(int));
  // mesh->nb_node_vars = 0;
  // mesh->nb_cell_vars = 0;
  // mesh->node_vars = NULL;
  // mesh->cell_vars = NULL;

  int k = 0;
  for (int i = 0; i < graph->nvtxs; i++) {
    for (int j = graph->xadj[i]; j < graph->xadj[i + 1]; j++) {
      int ii = graph->adjncy[j]; /* edge k = (i,ii) */
      if (i <= ii) {
        mesh->cells[k * 2] = i;
        mesh->cells[k * 2 + 1] = ii;
        k++;
      }
    }
  }
  assert(k == mesh->nb_cells);

  /* coords */
  memcpy(mesh->nodes, coords, 3 * mesh->nb_nodes * sizeof(double));
}

/* *********************************************************** */

void LibGraph_lineMesh2Graph(LibGraph_Mesh* mesh, LibGraph_Graph* graph)
{
  assert(mesh->elm_type == LIBGRAPH_LINE); /* lines */
  assert(mesh->nodes);
  graph->nvtxs = mesh->nb_nodes;
  graph->narcs = mesh->nb_cells * 2;
  graph->xadj = malloc((graph->nvtxs + 1) * sizeof(int));
  assert(graph->xadj);
  graph->adjncy = malloc(graph->narcs * sizeof(int));
  assert(graph->adjncy);
  graph->vwgts = NULL; /* not used */
  graph->ewgts = NULL; /* not used */

  /* create the dual structure */
  LibGraph_List* dual; /* array of nb_nodes List */
  dual = malloc(mesh->nb_nodes * sizeof(LibGraph_List));
  LibGraph_mesh2DualList(mesh, dual);

  int k = 0;
  for (int i = 0; i < mesh->nb_nodes; i++) {
    /* i-th node/vertex */
    graph->xadj[i] = k;
    for (int j = 0; j < dual[i].size; j++) {
      int c = dual[i].array[j];  // in mesh, node i is connected to cell c
      int v0 = mesh->cells[c * 2];
      int v1 = mesh->cells[c * 2 + 1];
      assert(i == v0 || i == v1);
      int ii = (v0 == i) ? v1 : v0;
      graph->adjncy[k++] = ii; /* k-th edge (i,ii) */
    }
  }
  graph->xadj[mesh->nb_nodes] = k; /* end */

  assert(k == graph->narcs);

  /* free dual structure */
  for (int i = 0; i < mesh->nb_nodes; i++) free(dual[i].array);
  free(dual);
}

/* *********************************************************** */

/* connectivity parameters for polyhedron only */
#define EDGE_CONNECTIVITY 0
#define FACE_CONNECTIVITY 1

void LibGraph_mesh2Graph(LibGraph_Mesh* mesh, LibGraph_Graph* graph)
{
  assert(mesh);
  assert(mesh->nodes);
  /*
     The graph vertices are represented by mesh elements.

     If the mesh uses surfacic elements (triangle or quad), there is one
     edge in the graph if two elements are connected by a mesh edge.

     If the mesh uses volumic elements (tetrahedron, hexahedron or
     prism), there is one edge in the graph if two elements are
     connected by a mesh face (FACE_CONNECTIVITY) and/or by a mesh
     edge (EDGE_CONNECTIVITY).
  */

  assert(mesh->elm_type == LIBGRAPH_LINE || mesh->elm_type == LIBGRAPH_TRIANGLE ||
         mesh->elm_type == LIBGRAPH_QUADRANGLE || mesh->elm_type == LIBGRAPH_TETRAHEDRON ||
         mesh->elm_type == LIBGRAPH_HEXAHEDRON || mesh->elm_type == LIBGRAPH_PRISM);

  int elm_size = LIBGRAPH_ELEMENT_SIZE[mesh->elm_type];

  /* 1) create the dual structure */

  LibGraph_List* dual; /* array of nb_nodes List */
  dual = malloc(mesh->nb_nodes * sizeof(LibGraph_List));
  LibGraph_mesh2DualList(mesh, dual);

  /* sort all lists */
  for (int i = 0; i < mesh->nb_nodes; i++) {
    assert(dual[i].size >= 0);
    LibGraph_sortList(&dual[i]);
  }

  /* List of found elements, connected to a given cell */
  /* useful to remove duplicated edges */
  LibGraph_List found;
  found.size = 0;
  found.max = MAXNBCELLSPERNODE;
  found.array = malloc(found.max * sizeof(int));

  /* 2) create the graph */
  graph->nvtxs = mesh->nb_cells;
  int maxarcs = 2 * (mesh->nb_cells * elm_size);
  graph->narcs = 0;
  graph->xadj = malloc((graph->nvtxs + 1) * sizeof(int));
  assert(graph->xadj);
  graph->adjncy = malloc(maxarcs * sizeof(int)); /* max */
  assert(graph->adjncy);
  graph->vwgts = NULL; /* not used */
  graph->ewgts = NULL; /* not used */

  int adjncyIndex = 0;

  /* ****** LINE (node connectivity) ****** */
  if (mesh->elm_type == LIBGRAPH_LINE) {
    /* for all cells */
    for (int i = 0; i < mesh->nb_cells; i++) {
      graph->xadj[i] = adjncyIndex;
      int n1 = mesh->cells[i * 2];
      int n2 = mesh->cells[i * 2 + 1];
      /* test if another line j element is connected to the i-th line element */
      for (int k1 = 0; k1 < dual[n1].size; k1++) {
        int j = dual[n1].array[k1];
        if (j != i) {
          graph->adjncy[adjncyIndex++] = j;
          assert(adjncyIndex <= maxarcs);
        }
      }
      for (int k2 = 0; k2 < dual[n2].size; k2++) {
        int j = dual[n2].array[k2];
        if (j != i) {
          graph->adjncy[adjncyIndex++] = j;
          assert(adjncyIndex <= maxarcs);
        }
      }

      /* check memory */
      if ((adjncyIndex + 2) >= maxarcs) {
        maxarcs *= 2;
        graph->adjncy = realloc(graph->adjncy, maxarcs * sizeof(int));
        assert(graph->adjncy);
      }
    }
  }

  /* ****** TRIANGLE & QUANDRANGLE (edge connectivity) ****** */
  else if (mesh->elm_type == LIBGRAPH_TRIANGLE || mesh->elm_type == LIBGRAPH_QUADRANGLE) {
    /* for all cells */
    for (int i = 0; i < mesh->nb_cells; i++) {
      graph->xadj[i] = adjncyIndex;
      /* for all edges of the i-th cell */
      for (int j = 0; j < elm_size; j++) {
        int n1 = mesh->cells[i * elm_size + (j % elm_size)];
        int n2 = mesh->cells[i * elm_size + ((j + 1) % elm_size)];
        /* test if another cell is connected to the i-th cell */
        int k;
        for (k = 0; k < dual[n1].size; k++) {
          int kk = dual[n1].array[k];
          if (kk != i) {
            if (LibGraph_findSortedList(&dual[n2], kk) != -1) {
              graph->adjncy[adjncyIndex++] = kk;
              assert(adjncyIndex <= maxarcs);
            }
          }
        }
      }

      /* check memory */
      if ((adjncyIndex + elm_size) >= maxarcs) {
        maxarcs *= 2;
        graph->adjncy = realloc(graph->adjncy, maxarcs * sizeof(int));
        assert(graph->adjncy);
      }
    }
  }

  /* ****** TETRAHEDRON (face/edge connectivity) ****** */
  else if (mesh->elm_type == LIBGRAPH_TETRAHEDRON) {
#if (FACE_CONNECTIVITY == 1)
    const int nb_faces = 4;
    int faces[nb_faces][3];
    faces[0][0] = 0;
    faces[0][1] = 1;
    faces[0][2] = 2;
    faces[1][0] = 0;
    faces[1][1] = 1;
    faces[1][2] = 3;
    faces[2][0] = 1;
    faces[2][1] = 3;
    faces[2][2] = 2;
    faces[3][0] = 0;
    faces[3][1] = 2;
    faces[3][2] = 3;
#endif

#if (EDGE_CONNECTIVITY == 1)
    const int nb_edges = 6;
    int edges[nb_edges][2];
    edges[0][0] = 0;
    edges[0][1] = 1;
    edges[1][0] = 1;
    edges[1][1] = 2;
    edges[2][0] = 2;
    edges[2][1] = 0;
    edges[3][0] = 0;
    edges[3][1] = 3;
    edges[4][0] = 1;
    edges[4][1] = 3;
    edges[5][0] = 2;
    edges[5][1] = 3;
#endif

    /* for all cells */
    for (int i = 0; i < mesh->nb_cells; i++) {
      graph->xadj[i] = adjncyIndex;
      found.size = 0;  // reset

#if (FACE_CONNECTIVITY == 1)

      /* for all faces of the i-th cell */
      for (int f = 0; f < nb_faces; f++) {
        /* consider face f with 3 nodes {n1,n2,n3} */
        int v1 = faces[f][0];
        int v2 = faces[f][1];
        int v3 = faces[f][2];
        /* consider triangle face with nodes {n1,n2,n3} */
        int n1 = mesh->cells[i * elm_size + v1];
        int n2 = mesh->cells[i * elm_size + v2];
        int n3 = mesh->cells[i * elm_size + v3];
        /* search a cell connected to i by face {n1,n2,n3} */
        for (int k = 0; k < dual[n1].size; k++) {
          /* check if cell kk is connected to cell i */
          int kk = dual[n1].array[k];
          if (kk != i) {
            if (LibGraph_findSortedList(&dual[n2], kk) != -1 && LibGraph_findSortedList(&dual[n3], kk) != -1)
              LibGraph_insertList(&found, kk);
          }
        }
      }

#endif

#if (EDGE_CONNECTIVITY == 1)

      /* for all edges of the i-th cell */
      for (int e = 0; e < nb_edges; e++) {
        /* consider edge e with 2 nodes {n1,n2} */
        int v1 = edges[e][0];
        int v2 = edges[e][1];
        int n1 = mesh->cells[i * elm_size + v1];
        int n2 = mesh->cells[i * elm_size + v2];
        /* search a cell connected to i by edge {n1,n2} */
        for (int k = 0; k < dual[n1].size; k++) {
          /* check if cell kk is connected to cell i */
          int kk = dual[n1].array[k];
          if (kk != i) {
            if (findSortedList(&dual[n2], kk) != -1) insertList(&found, kk);
          }
        }
      }

#endif

      /* insert graph edges */
      LibGraph_sortList(&found);
      for (int x = 0; x < found.size; x++) {
        if (x > 0 && found.array[x] == found.array[x - 1]) continue;  // skip duplicated edges
        graph->adjncy[adjncyIndex++] = found.array[x];
        if (adjncyIndex >= maxarcs) {
          maxarcs *= 2;
          graph->adjncy = realloc(graph->adjncy, maxarcs * sizeof(int));
          assert(graph->adjncy);
        }
      }
    }
  }

  /* ****** HEXAHEDRON (face/edge connectivity) ****** */
  else if (mesh->elm_type == LIBGRAPH_HEXAHEDRON) {
#if (FACE_CONNECTIVITY == 1)
    const int nb_faces = 6;
    int faces[nb_faces][4];
    faces[0][0] = 0;
    faces[0][1] = 1;
    faces[0][2] = 2;
    faces[0][3] = 3;
    faces[1][0] = 4;
    faces[1][1] = 5;
    faces[1][2] = 6;
    faces[1][3] = 7;
    faces[2][0] = 0;
    faces[2][1] = 1;
    faces[2][2] = 5;
    faces[2][3] = 4;
    faces[3][0] = 2;
    faces[3][1] = 6;
    faces[3][2] = 7;
    faces[3][3] = 3;
    faces[4][0] = 1;
    faces[4][1] = 2;
    faces[4][2] = 6;
    faces[4][3] = 5;
    faces[5][0] = 0;
    faces[5][1] = 3;
    faces[5][2] = 7;
    faces[5][3] = 4;
#endif

#if (EDGE_CONNECTIVITY == 1)
    const int nb_edges = 12;
    int edges[nb_edges][2];
    edges[0][0] = 0;
    edges[0][1] = 1;
    edges[1][0] = 1;
    edges[1][1] = 2;
    edges[2][0] = 2;
    edges[2][1] = 3;
    edges[3][0] = 3;
    edges[3][1] = 0;
    edges[4][0] = 4;
    edges[4][1] = 5;
    edges[5][0] = 5;
    edges[5][1] = 6;
    edges[6][0] = 6;
    edges[6][1] = 7;
    edges[7][0] = 7;
    edges[7][1] = 4;
    edges[8][0] = 0;
    edges[8][1] = 4;
    edges[9][0] = 1;
    edges[9][1] = 5;
    edges[10][0] = 2;
    edges[10][1] = 6;
    edges[11][0] = 3;
    edges[11][1] = 7;
#endif

    /* for all cells */
    for (int i = 0; i < mesh->nb_cells; i++) {
      graph->xadj[i] = adjncyIndex;
      found.size = 0;  // reset

#if (FACE_CONNECTIVITY == 1)

      /* for all faces of the i-th cell */
      for (int f = 0; f < nb_faces; f++) {
        /* consider face f with 4 nodes {n1,n2,n3,n4} */
        int v1 = faces[f][0];
        int v2 = faces[f][1];
        int v3 = faces[f][2];
        int v4 = faces[f][3];
        int n1 = mesh->cells[i * elm_size + v1];
        int n2 = mesh->cells[i * elm_size + v2];
        int n3 = mesh->cells[i * elm_size + v3];
        int n4 = mesh->cells[i * elm_size + v4];

        /* search a cell connected to i by face {n1,n2,n3,n4} */
        for (int k = 0; k < dual[n1].size; k++) {
          /* check if cell kk is connected to cell i */
          int kk = dual[n1].array[k];
          if (kk != i) {
            if (LibGraph_findSortedList(&dual[n2], kk) != -1 && LibGraph_findSortedList(&dual[n3], kk) != -1 &&
                LibGraph_findSortedList(&dual[n4], kk) != -1)
              LibGraph_insertList(&found, kk);
          }
        }
      }

#endif

#if (EDGE_CONNECTIVITY == 1)
      /* for all edges of the i-th cell */
      for (int e = 0; e < nb_edges; e++) {
        /* consider edge e with 2 nodes {n1,n2} */
        int v1 = edges[e][0];
        int v2 = edges[e][1];
        int n1 = mesh->cells[i * elm_size + v1];
        int n2 = mesh->cells[i * elm_size + v2];
        /* search a cell connected to i by edge {n1,n2} */
        for (int k = 0; k < dual[n1].size; k++) {
          /* check if cell kk is connected to cell i */
          int kk = dual[n1].array[k];
          if (kk != i) {
            if (findSortedList(&dual[n2], kk) != -1) insertList(&found, kk);
          }
        }
      }

#endif

      /* insert graph edges */
      LibGraph_sortList(&found);
      for (int x = 0; x < found.size; x++) {
        if (x > 0 && found.array[x] == found.array[x - 1]) continue;  // skip duplicated edges
        graph->adjncy[adjncyIndex++] = found.array[x];
        if (adjncyIndex >= maxarcs) {
          maxarcs *= 2;
          graph->adjncy = realloc(graph->adjncy, maxarcs * sizeof(int));
          assert(graph->adjncy);
        }
      }
    }
  }

  /* ****** PRISM (face/edge connectivity) ****** */
  else if (mesh->elm_type == LIBGRAPH_PRISM) {
#if (FACE_CONNECTIVITY == 1)
    const int nb_faces = 5;
    int faces[nb_faces][4];
    faces[0][0] = 0;
    faces[0][1] = 1;
    faces[0][2] = 2;
    faces[0][3] = -1; /* triangle */
    faces[1][0] = 3;
    faces[1][1] = 4;
    faces[1][2] = 5;
    faces[1][3] = -1; /* triangle */
    faces[2][0] = 0;
    faces[2][1] = 2;
    faces[2][2] = 5;
    faces[2][3] = 3;
    faces[3][0] = 2;
    faces[3][1] = 1;
    faces[3][2] = 4;
    faces[3][3] = 5;
    faces[4][0] = 0;
    faces[4][1] = 1;
    faces[4][2] = 4;
    faces[4][3] = 3;
#endif

#if (EDGE_CONNECTIVITY == 1)
    const int nb_edges = 9;
    int edges[nb_edges][2];
    edges[0][0] = 0;
    edges[0][1] = 1;
    edges[1][0] = 1;
    edges[1][1] = 2;
    edges[2][0] = 2;
    edges[2][1] = 0;
    edges[3][0] = 3;
    edges[3][1] = 4;
    edges[4][0] = 4;
    edges[4][1] = 5;
    edges[5][0] = 5;
    edges[5][1] = 3;
    edges[6][0] = 0;
    edges[6][1] = 3;
    edges[7][0] = 1;
    edges[7][1] = 4;
    edges[8][0] = 2;
    edges[8][1] = 5;
#endif

    /* for all cells */
    for (int i = 0; i < mesh->nb_cells; i++) {
      graph->xadj[i] = adjncyIndex;
      found.size = 0;  // reset

#if (FACE_CONNECTIVITY == 1)

      /* for all faces of the i-th cell */
      for (int f = 0; f < nb_faces; f++) {
        /* consider face f with 3 or 4 nodes {n1,n2,n3,n4} */
        int v1 = faces[f][0];
        int v2 = faces[f][1];
        int v3 = faces[f][2];
        int v4 = faces[f][3];
        int n1 = mesh->cells[i * elm_size + v1];
        int n2 = mesh->cells[i * elm_size + v2];
        int n3 = mesh->cells[i * elm_size + v3];
        int n4 = (v4 != -1) ? mesh->cells[i * elm_size + v4] : -1; /* not always used */

        /* search a cell connected to i by face {n1,n2,n3} or {n1,n2,n3,n4} */
        for (int k = 0; k < dual[n1].size; k++) {
          /* check if cell kk is connected to cell i */
          int kk = dual[n1].array[k];
          if (kk != i) {
            if (LibGraph_findSortedList(&dual[n2], kk) != -1 && LibGraph_findSortedList(&dual[n3], kk) != -1) {
              if (n4 != -1 && LibGraph_findSortedList(&dual[n4], kk) != -1) /* quad face */
                LibGraph_insertList(&found, kk);
              else if (n4 == -1) /* triangular face */
                LibGraph_insertList(&found, kk);
            }
          }
        }
      }

#endif

#if (EDGE_CONNECTIVITY == 1)
      /* for all edges of the i-th cell */
      for (int e = 0; e < nb_edges; e++) {
        /* consider edge e with 2 nodes {n1,n2} */
        int v1 = edges[e][0];
        int v2 = edges[e][1];
        int n1 = mesh->cells[i * elm_size + v1];
        int n2 = mesh->cells[i * elm_size + v2];
        /* search a cell connected to i by edge {n1,n2} */
        for (int k = 0; k < dual[n1].size; k++) {
          /* check if cell kk is connected to cell i */
          int kk = dual[n1].array[k];
          if (kk != i) {
            if (findSortedList(&dual[n2], kk) != -1) insertList(&found, kk);
          }
        }
      }

#endif

      /* insert graph edges */
      LibGraph_sortList(&found);
      for (int x = 0; x < found.size; x++) {
        if (x > 0 && found.array[x] == found.array[x - 1]) continue;  // skip duplicated edges
        graph->adjncy[adjncyIndex++] = found.array[x];
        if (adjncyIndex >= maxarcs) {
          maxarcs *= 2;
          graph->adjncy = realloc(graph->adjncy, maxarcs * sizeof(int));
          assert(graph->adjncy);
        }
      }
    }
  }

  /* end */

  graph->xadj[graph->nvtxs] = adjncyIndex;
  graph->narcs = adjncyIndex;

  /* free unused memory */
  graph->adjncy = realloc(graph->adjncy, graph->narcs * sizeof(int));
  free(found.array);

  /* free dual structure */
  for (int i = 0; i < mesh->nb_nodes; i++) free(dual[i].array);
  free(dual);
}

/* *********************************************************** */

/* Edge version: one hyperedge (of size 2) per graph edge. */
void LibGraph_graph2HypergraphE(LibGraph_Graph* g, LibGraph_Hypergraph* hg)
{
#ifdef DEBUG
  assert(LibGraph_checkGraph(g));
#endif

  hg->nvtxs = g->nvtxs;
  hg->nhedges = g->narcs / 2;
  hg->vwgts = (g->vwgts == NULL) ? NULL : malloc(hg->nvtxs * sizeof(int));
  hg->hewgts = (g->ewgts == NULL) ? NULL : malloc(hg->nhedges * sizeof(int));
  hg->eptr = malloc((hg->nhedges + 1) * sizeof(int));
  assert(hg->eptr);
  hg->eind = malloc(hg->nhedges * 2 * sizeof(int));
  assert(hg->eind);

  long int ptr = 0;
  long int k = 0;

  for (int i = 0; i < g->nvtxs; i++) {
    if (g->vwgts != NULL) hg->vwgts[i] = g->vwgts[i]; /* vertex weight */
    assert(g->xadj[i] < g->xadj[i + 1]);              // check connectivity!!!
    for (int j = g->xadj[i]; j < g->xadj[i + 1]; j++) {
      int ii = g->adjncy[j];
      // for each edge (i, ii), add an hyperedge...
      if (i < ii) {
        hg->eptr[ptr] = k;
        /* printf("[debug] add hyperedge (%d,%d)\n", i, ii); */
        hg->eind[k++] = i;
        hg->eind[k++] = ii;
        if (g->ewgts != NULL) hg->hewgts[ptr] = g->ewgts[j]; /* hyperedge weight */
        ptr++;                                               /* next hyperedge */
        assert(ptr < INT_MAX);
        assert(k < INT_MAX);
      }
    }
  }
  hg->eptr[ptr] = k; /* end */

  /* check */
  assert(ptr == g->narcs / 2);
  assert(k == g->narcs);
}

/* *********************************************************** */

/* Vertex version: one hyperedge per graph vertex and its neighbor. */
void LibGraph_graph2HypergraphV(LibGraph_Graph* g, LibGraph_Hypergraph* hg)
{
  hg->nvtxs = g->nvtxs;
  hg->nhedges = g->nvtxs;
  hg->eptr = malloc((hg->nhedges + 1) * sizeof(int));
  int eind_size = g->nvtxs * 5;
  hg->eind = malloc(eind_size * sizeof(int));
  hg->vwgts = (g->vwgts == NULL) ? NULL : malloc(hg->nvtxs * sizeof(int));
  hg->hewgts = NULL; /* ? */

  int k = 0;
  for (int i = 0; i < g->nvtxs; i++) {
    /* add vertex i in current hyperedge */
    hg->eptr[i] = k;
    if (k >= eind_size) {
      eind_size *= 2;
      hg->eind = realloc(hg->eind, eind_size * sizeof(int));
    }
    hg->eind[k] = i;
    k++;
    /* add neighbors of vertex i in current hyperedge */
    for (int j = g->xadj[i]; j < g->xadj[i + 1]; j++) {
      if (k >= eind_size) {
        eind_size *= 2;
        hg->eind = realloc(hg->eind, eind_size * sizeof(int));
      }
      hg->eind[k] = g->adjncy[j];
      k++;
    }
  }
  hg->eptr[hg->nvtxs] = k; /* end */

  if (g->vwgts != NULL) memcpy(hg->vwgts, g->vwgts, hg->nvtxs * sizeof(int));

  /* free unused memory */
  hg->eind = realloc(hg->eind, k * sizeof(int));
}

/* *********************************************************** */

void LibGraph_hypergraph2Graph(LibGraph_Hypergraph* hg, LibGraph_Graph* g)
{
  g->nvtxs = hg->nvtxs;
  g->xadj = malloc((g->nvtxs + 1) * sizeof(int));
  g->adjncy = malloc(hg->eptr[hg->nhedges] * sizeof(int));
  g->ewgts = malloc(hg->eptr[hg->nhedges] * sizeof(int));

  if (hg->vwgts) {
    g->vwgts = malloc(g->nvtxs * sizeof(int));
    memcpy(g->vwgts, hg->vwgts, g->nvtxs * sizeof(int));
  }
  else
    g->vwgts = NULL;

  g->xadj[0] = 0;
  for (int v = 0; v < g->nvtxs; v++) {
    g->xadj[v + 1] = g->xadj[v];
    for (int h = 0; h < hg->nhedges; h++) {
      /* look for v in h */
      int i;
      for (i = hg->eptr[h]; i < hg->eptr[h + 1]; i++) {
        if (hg->eind[i] == v) break;
      }
      if (i == hg->eptr[h + 1]) /* v is not in h */
        continue;

      for (i = hg->eptr[h]; i < hg->eptr[h + 1]; i++) {
        if (hg->eind[i] != v) {
          int j;
          for (j = g->xadj[v]; j < g->xadj[v + 1]; j++) {
            if (g->adjncy[j] == hg->eind[i]) {
              g->ewgts[j] += (hg->hewgts ? hg->hewgts[h] : 1);
              break;
            }
          }
          if (j == g->xadj[v + 1]) { /* hg->eind[i] is not a neighbor of v, add it */
            g->adjncy[g->xadj[v + 1]] = hg->eind[i];
            g->ewgts[g->xadj[v + 1]] = (hg->hewgts ? hg->hewgts[h] : 1);
            g->xadj[v + 1]++;
          }
        }
      }
    }
  }

  g->adjncy = realloc(g->adjncy, g->xadj[g->nvtxs] * sizeof(int));
  g->ewgts = realloc(g->ewgts, g->xadj[g->nvtxs] * sizeof(int));
}

/* *********************************************************** */

void LibGraph_mesh2Hypergraph(LibGraph_Mesh* mesh, LibGraph_Hypergraph* hg)
{
  LibGraph_Graph g;
  LibGraph_mesh2Graph(mesh, &g);

  int elm_size = LIBGRAPH_ELEMENT_SIZE[mesh->elm_type];

  /* create hypergraph */

  hg->nvtxs = mesh->nb_cells;
  hg->nhedges = mesh->nb_cells;
  hg->eptr = malloc((hg->nhedges + 1) * sizeof(int));
  /* Correct for triangle, quad and terahedron but not in general*/
  int maxeind = hg->nhedges * (elm_size + 1);
  hg->eind = malloc(maxeind * sizeof(int));
  hg->vwgts = NULL;
  hg->hewgts = NULL;

  int k = 0;
  for (int i = 0; i < hg->nvtxs; i++) {
    hg->eptr[i] = k;
    hg->eind[k++] = i;
    for (int j = g.xadj[i]; j < g.xadj[i + 1]; j++) { hg->eind[k++] = g.adjncy[j]; }
  }
  hg->eptr[hg->nvtxs] = k; /* end */

  /* free unused memory */
  hg->eind = realloc(hg->eind, k * sizeof(int));

  LibGraph_freeGraph(&g);
}

/* *********************************************************** */

void LibGraph_laplacian(LibGraph_Graph* g, int* matrix)
{
  memset(matrix, 0, g->nvtxs * g->nvtxs * sizeof(int));
  for (int i = 0; i < g->nvtxs; i++) {
    int d = g->xadj[i + 1] - g->xadj[i];
    assert(d >= 0);
    matrix[i * g->nvtxs + i] = d;
    for (int j = g->xadj[i]; j < g->xadj[i + 1]; j++) {
      int k = g->adjncy[j]; /* edge (i,k) */
      assert(k != i);
      matrix[i * g->nvtxs + k] = -1;
      matrix[k * g->nvtxs + i] = -1;
    }
  }
}

/* *********************************************************** */

void LibGraph_graph2Mat(LibGraph_Graph* g, int* mat)
{
  assert(g && mat);
  memset(mat, 0, g->nvtxs * g->nvtxs * sizeof(int));
  for (int i = 0; i < g->nvtxs; i++) {
    for (int j = g->xadj[i]; j < g->xadj[i + 1]; j++) {
      int k = g->adjncy[j]; /* edge (i,k) */
      mat[i * g->nvtxs + k] = 1;
      mat[k * g->nvtxs + i] = 1;
    }
  }
}

/* *********************************************************** */

void LibGraph_graph2MatW(LibGraph_Graph* g, int* mat)
{
  assert(g && mat);
  assert(g->ewgts);
  memset(mat, 0, g->nvtxs * g->nvtxs * sizeof(int));
  for (int i = 0; i < g->nvtxs; i++) {
    for (int j = g->xadj[i]; j < g->xadj[i + 1]; j++) {
      int k = g->adjncy[j]; /* edge (i,k) */
      mat[i * g->nvtxs + k] = g->ewgts[j];
      mat[k * g->nvtxs + i] = g->ewgts[j];
    }
  }
}

/* *********************************************************** */

void LibGraph_mat2Graph(int nvtxs, int* mat, LibGraph_Graph* g)
{
  assert(g && mat);
  g->nvtxs = nvtxs;
  g->narcs = 0;
  g->xadj = malloc((g->nvtxs + 1) * sizeof(int));
  g->vwgts = g->ewgts = NULL;

  /* compute nb edges */
  for (int i = 0; i < nvtxs; i++)
    for (int j = 0; j < nvtxs; j++)
      if (i < j && mat[i * nvtxs + j] > 0) g->narcs++;
  g->narcs *= 2;

  g->adjncy = malloc(g->narcs * sizeof(int));

  /* add edges... */
  int k = 0;
  for (int i = 0; i < nvtxs; i++) {
    g->xadj[i] = k;
    for (int j = 0; j < nvtxs; j++) {
      if (i != j && mat[i * nvtxs + j] > 0) {
        g->adjncy[k] = j;
        k++;
      }
    }
  }
  g->xadj[nvtxs] = k; /* end */
}

/* *********************************************************** */

void LibGraph_graph2Coords(LibGraph_Graph* g, /**< [in] graph */
                           int* coords,       /**< [out] array of edges (v,v') of size 2.|E| */
                           int* ewgts, /**< [out] array of edge weights of size 2.|E| sorted in same order as coords
                                          (NULL if not used) */
                           int* vwgts  /**< [out] array of vertex weights of size |V| (NULL if not used) */
)
{
  assert(g && coords);

  int k = 0;
  for (int i = 0; i < g->nvtxs; i++) {
    if (vwgts) vwgts[i] = g->vwgts ? g->vwgts[i] : 1;
    for (int j = g->xadj[i]; j < g->xadj[i + 1]; j++) {
      int ii = g->adjncy[j]; /* edge (i,ii) */
      assert(j == k);
      coords[2 * k] = i;
      coords[2 * k + 1] = ii;
      if (ewgts) ewgts[k] = g->ewgts ? g->ewgts[j] : 1;
      k++;
    }
  }
  assert(k == g->narcs);
}

/* *********************************************************** */

void LibGraph_coords2Graph(int nvtxs,        /**< [in] number of vertices |V| */
                           int nedges,       /**< [in] number of edges |E| */
                           int* coords,      /**< [in] array of edges (v,v') of size 2.|E| (with v sorted in
                                                increasing order) */
                           int* ewgts,       /**< [in] array of edge weights of size 2.|E| in same order than
                                                coords (NULL if not used) */
                           int* vwgts,       /**< [in] array of vertex weights of size |V| (NULL if not used) */
                           LibGraph_Graph* g /**< [out] graph */
)
{
  assert(g && coords);
  g->nvtxs = nvtxs;
  g->narcs = nedges * 2;
  g->xadj = malloc((g->nvtxs + 1) * sizeof(int));
  assert(g->xadj);
  g->adjncy = malloc(g->narcs * sizeof(int));
  assert(g->adjncy);
  g->vwgts = NULL;
  if (vwgts) {
    g->vwgts = calloc(g->nvtxs, sizeof(int));
    assert(g->vwgts);
  }
  g->ewgts = NULL;
  if (ewgts) {
    g->ewgts = calloc(g->narcs, sizeof(int));
    assert(g->ewgts);
  }

  int k = 0;  /* k-th coord */
  int i = -1; /* i-th vtx */
  int j = 0;  /* j-th edge */
  int v = -1, vv = -1, oldv = -1, oldvv = -1;

  while (k < 2 * nedges) {
    v = coords[2 * k];
    vv = coords[2 * k + 1];

    assert(v >= 0 && v < nvtxs);
    assert(vv >= 0 && vv < nvtxs);
    assert(oldv < v || (oldv == v && oldvv <= vv));
    assert(!(v == oldv && vv == oldvv)); /* duplicated edges */

    /* start edge with new vtx endpoint */
    if (oldv == -1) { g->xadj[++i] = 0; }
    else if (oldv != v) {
      g->xadj[++i] = j;
    }

    /* skip single vertices */
    if (i < v) continue;

    /* add j-th edge (v,vv) */
    g->adjncy[j] = vv;
    oldv = v;
    oldvv = vv;

    j++;
    k++;  // next edge & coord
  }

  /* end */
  for (int k = i + 1; k <= nvtxs; k++) g->xadj[k] = j;
  assert(j == 2 * nedges);

  /* weighted graph */
  if (vwgts) memcpy(g->vwgts, vwgts, g->nvtxs * sizeof(int));
  if (ewgts) memcpy(g->ewgts, ewgts, g->narcs * sizeof(int));
}

/* *********************************************************** */

void LibGraph_stripCoords(int* ncoords, int** coords, int** ewgts)
{
  int* _coords = *coords;
  assert(_coords);
  int _ncoords = *ncoords;
  assert(_ncoords > 0);
  int* _ewgts = ewgts ? *ewgts : NULL;
  int totalew = 0;
  if (ewgts && *ewgts == NULL) {
    _ewgts = malloc(_ncoords * sizeof(int));
    for (int i = 0; i < _ncoords; i++) _ewgts[i] = 1;
  }
  if (_ewgts)
    for (int i = 0; i < _ncoords; i++) totalew += _ewgts[i];

  int k = 1;  /* src */
  int kk = 1; /* dst */
  while (k < _ncoords) {
    assert(kk <= k);
    int v = _coords[k * 2];
    int vv = _coords[k * 2 + 1];
    int ew = _ewgts ? _ewgts[k] : 1;  // current edge in src
    int oldv = _coords[(kk - 1) * 2];
    int oldvv = _coords[(kk - 1) * 2 + 1];
    assert(oldv <= v);
    if (oldv == v) assert(oldvv <= vv);  // check increasing order
    if (oldv == v && oldvv == vv) {      // duplicated edge found
      if (_ewgts) _ewgts[kk - 1] += ew;
      k++;
    }
    else {  // regular edge
      _coords[kk * 2] = v;
      _coords[kk * 2 + 1] = vv;
      if (_ewgts) _ewgts[kk] = ew;
      k++;
      kk++;
    }
  }

  // printf("remove %d duplicated interedges! \n", _ncoords - kk/2);
  _ncoords = kk;
  _coords = realloc(_coords, _ncoords * 2 * sizeof(int));
  if (_ewgts) _ewgts = realloc(_ewgts, _ncoords * sizeof(int));
  assert(_coords);

  // check
  assert(kk % 2 == 0 && kk <= _ncoords);
  int checktotalew = 0;
  if (_ewgts)
    for (int i = 0; i < _ncoords; i++) checktotalew += _ewgts[i];
  assert(totalew == checktotalew);

  // return
  *coords = _coords;
  *ncoords = _ncoords;
  if (_ewgts) *ewgts = _ewgts;
}

/* *********************************************************** */

static void polygonCenter(int n,             /* [in] nb of vertices of polygon */
                          double* coords[3], /* [in] array of size n of pointer on 3D coords */
                          double G[3])       /* [out] gravity center */
{
  /* gravity center of polygon */
  for (int k = 0; k < 3; k++) {
    G[k] = 0.0;
    for (int i = 0; i < n; i++) G[k] += coords[i][k];
    G[k] /= n;
  }
}

/* *********************************************************** */

void LibGraph_cellCenter(const LibGraph_Mesh* msh, /* [in] input mesh */
                         int i,                    /* [in] cell index */
                         double G[3])              /* [out] gravity center */
{
  assert(msh->nodes);
  int cellsize = LIBGRAPH_ELEMENT_SIZE[msh->elm_type];
  int* cell = &msh->cells[i * cellsize];

  double* coords[cellsize];
  for (int k = 0; k < cellsize; k++) coords[k] = &msh->nodes[cell[k] * 3];

  polygonCenter(cellsize, coords, G);
}

/* *********************************************************** */

void LibGraph_cellCenters(const LibGraph_Mesh* msh, double* centers)
{
  for (int i = 0; i < msh->nb_cells; i++) LibGraph_cellCenter(msh, i, centers + 3 * i);
}

/* *********************************************************** */
/*
  The graph vertices are represented by mesh elements.
  If the mesh uses surfacic elements (triangle or quad), there is one
  edge in the graph if two elements are connected by a mesh edge.
  If the mesh uses volumic elements (tetrahedron, hexahedron or
  prism), there is one edge in the graph if two elements are
  connected by a mesh face (FACE_CONNECTIVITY) and/or by a mesh
  edge (EDGE_CONNECTIVITY).
*/
void LibGraph_hybridMesh2Graph(LibGraph_HybridMesh* hm, LibGraph_Graph* graph)
{
  assert(hm);
  int nb_cells = 0;
  LibGraph_Mesh* cm = NULL;
  int elm_size = 0;
  // int elm_size = hm->max_elm_size;
  for (int i = 0; i < hm->nb_components; i++) {
    cm = LibGraph_getHybridMeshComponent(hm, i);
    assert(cm);
    assert(cm->elm_type == LIBGRAPH_LINE || cm->elm_type == LIBGRAPH_TRIANGLE || cm->elm_type == LIBGRAPH_QUADRANGLE ||
           cm->elm_type == LIBGRAPH_TETRAHEDRON || cm->elm_type == LIBGRAPH_HEXAHEDRON ||
           cm->elm_type == LIBGRAPH_PRISM);

    nb_cells = nb_cells + cm->nb_cells;
  }

  /* 1) create the dual structure */
  /*
    For the k-th node, dual[k] is the list of all cells sharing this
    node.
  */

  const int MAX = 60;  /* max nb of cells shared by a node */
  LibGraph_List* dual; /* array of nb_nodes List */
  int nb_nodes = 0;
  if (hm->shared_nodes && hm->nodes)
    nb_nodes = hm->nb_nodes;
  else {
    cm = LibGraph_getHybridMeshComponent(hm, 0);
    nb_nodes = cm->nb_nodes;
  }
  dual = malloc(nb_nodes * sizeof(LibGraph_List));
  for (int i = 0; i < nb_nodes; i++) {
    dual[i].size = 0;
    dual[i].max = MAX;
    dual[i].array = malloc(dual[i].max * sizeof(int));
  }

  /* for all nodes of each cell */
  int cell_idx = 0;
  for (int k = 0; k < hm->nb_components; k++) {
    cm = LibGraph_getHybridMeshComponent(hm, k);
    assert(cm);
    elm_size = LIBGRAPH_ELEMENT_SIZE[cm->elm_type];
    for (int i = 0; i < cm->nb_cells; i++) {
      for (int j = 0; j < elm_size; j++) {
        int node = cm->cells[i * elm_size + j];
        assert(node < nb_nodes);
        if (dual[node].size + 1 > dual[node].max) {
          dual[node].max *= 2;
          dual[node].array = realloc(dual[node].array, dual[node].max * sizeof(int));
          assert(dual[node].array != NULL);
        }
        dual[node].array[dual[node].size++] = cell_idx;
      }
      cell_idx++;
    }
  }
  assert(cell_idx == nb_cells);

  /* sort all lists */
  for (int i = 0; i < nb_nodes; i++) {
    assert(dual[i].size >= 0);
    LibGraph_sortList(&dual[i]);
  }

  /* printf("DUAL\n"); */
  /* for(int i = 0 ; i < nb_nodes ; i++) { */
  /*   printf("  %d -> [ ", i); */
  /*   for(int j = 0 ; j < dual[i].size ; j++) { */
  /*     printf("%d ", dual[i].array[j]); */
  /*   } */
  /*   printf("]\n"); */
  /* } */
  // TODO: Until here all good!

  /* List of found elements, connected to a given cell */
  /* useful to remove duplicated edges */
  LibGraph_List found;
  found.size = 0;
  found.max = MAX;
  found.array = malloc(found.max * sizeof(int));

  /* 2) create the graph */
  graph->nvtxs = nb_cells;
  int maxedges = 0;
  for (int k = 0; k < hm->nb_components; k++) {
    cm = LibGraph_getHybridMeshComponent(hm, k);
    assert(cm);
    elm_size = LIBGRAPH_ELEMENT_SIZE[cm->elm_type];
    for (int i = 0; i < cm->nb_cells; i++) maxedges = maxedges + elm_size;
  }
  graph->narcs = 0;
  graph->xadj = malloc((graph->nvtxs + 1) * sizeof(int));
  assert(graph->xadj);
  graph->adjncy = malloc(2 * maxedges * sizeof(int)); /* max */
  assert(graph->adjncy);
  graph->vwgts = NULL; /* not used */
  graph->ewgts = NULL; /* not used */

  int adjncyIndex = 0;

  cell_idx = 0;  // I have to add one for each cell encountered in the hm

  for (int l = 0; l < hm->nb_components; l++) {
    cm = LibGraph_getHybridMeshComponent(hm, l);
    assert(cm);
    elm_size = LIBGRAPH_ELEMENT_SIZE[cm->elm_type];
    /* ****** LINE (node connectivity) ****** */
    if (cm->elm_type == LIBGRAPH_LINE) { /* just two nodes (n1,n2)*/
      for (int i = 0; i < cm->nb_cells; i++) {
        graph->xadj[cell_idx] = adjncyIndex;
        int n1 = cm->cells[i * 2];
        int n2 = cm->cells[i * 2 + 1];
        /* test if another line j element is connected to the i-th line element */
        for (int k1 = 0; k1 < dual[n1].size; k1++) {
          int j = dual[n1].array[k1];
          if (j != cell_idx) {
            graph->adjncy[adjncyIndex++] = j;
            assert(adjncyIndex <= 2 * maxedges);
          }
        }
        for (int k2 = 0; k2 < dual[n2].size; k2++) {
          int j = dual[n2].array[k2];
          // if(j != i) {
          if (j != cell_idx) {
            graph->adjncy[adjncyIndex++] = j;
            assert(adjncyIndex <= 2 * maxedges);
          }
        }

        /* check memory */
        if ((adjncyIndex + 2) >= 2 * maxedges) {
          maxedges *= 2;
          graph->adjncy = realloc(graph->adjncy, 2 * maxedges * sizeof(int));
          assert(graph->adjncy);
        }
        cell_idx++;
      }
    }
    /* ****** TRIANGLE & QUANDRANGLE (edge connectivity) ****** */
    else if (cm->elm_type == LIBGRAPH_TRIANGLE || cm->elm_type == LIBGRAPH_QUADRANGLE) {
      /* for all cells */
      for (int i = 0; i < cm->nb_cells; i++) {
        graph->xadj[cell_idx] = adjncyIndex;
        /* for all edges of the i-th cell */
        for (int j = 0; j < elm_size; j++) {
          int n1 = cm->cells[i * elm_size + (j % elm_size)];
          int n2 = cm->cells[i * elm_size + ((j + 1) % elm_size)];
          /* test if another cell is connected to the i-th cell */
          for (int k = 0; k < dual[n1].size; k++) {
            int kk = dual[n1].array[k];
            if (kk != cell_idx) {
              if (LibGraph_findSortedList(&dual[n2], kk) != -1) {
                graph->adjncy[adjncyIndex++] = kk;
                assert(adjncyIndex <= 2 * maxedges);
              }
            }
          }
        }

        /* check memory */
        if ((adjncyIndex + elm_size) >= 2 * maxedges) {
          maxedges *= 2;
          graph->adjncy = realloc(graph->adjncy, 2 * maxedges * sizeof(int));
          assert(graph->adjncy);
        }
        cell_idx++;
      }
    }
    /* ****** TETRAHEDRON (face/edge connectivity) ****** */
    else if (cm->elm_type == LIBGRAPH_TETRAHEDRON) {
#if (FACE_CONNECTIVITY == 1)
      const int nb_faces = 4;
      int faces[nb_faces][3];
      faces[0][0] = 0;
      faces[0][1] = 1;
      faces[0][2] = 2;
      faces[1][0] = 0;
      faces[1][1] = 1;
      faces[1][2] = 3;
      faces[2][0] = 1;
      faces[2][1] = 3;
      faces[2][2] = 2;
      faces[3][0] = 0;
      faces[3][1] = 2;
      faces[3][2] = 3;
#endif

#if (EDGE_CONNECTIVITY == 1)
      const int nb_edges = 6;
      int edges[nb_edges][2];
      edges[0][0] = 0;
      edges[0][1] = 1;
      edges[1][0] = 1;
      edges[1][1] = 2;
      edges[2][0] = 2;
      edges[2][1] = 0;
      edges[3][0] = 0;
      edges[3][1] = 3;
      edges[4][0] = 1;
      edges[4][1] = 3;
      edges[5][0] = 2;
      edges[5][1] = 3;
#endif
      /* for all cells */
      for (int i = 0; i < cm->nb_cells; i++) {
        graph->xadj[cell_idx] = adjncyIndex;
        found.size = 0;  // reset
#if (FACE_CONNECTIVITY == 1)
        /* for all faces of the i-th cell */
        for (int f = 0; f < nb_faces; f++) {
          /* consider face f with 3 nodes {n1,n2,n3} */
          int v1 = faces[f][0];
          int v2 = faces[f][1];
          int v3 = faces[f][2];
          /* consider triangle face with nodes {n1,n2,n3} */
          int n1 = cm->cells[i * elm_size + v1];
          int n2 = cm->cells[i * elm_size + v2];
          int n3 = cm->cells[i * elm_size + v3];
          /* search a cell connected to i by face {n1,n2,n3} */
          for (int k = 0; k < dual[n1].size; k++) {
            /* check if cell kk is connected to cell i */
            int kk = dual[n1].array[k];
            if (kk != cell_idx) {
              if (LibGraph_findSortedList(&dual[n2], kk) != -1 && LibGraph_findSortedList(&dual[n3], kk) != -1)
                LibGraph_insertList(&found, kk);
            }
          }
        }
#endif

#if (EDGE_CONNECTIVITY == 1)
        /* for all edges of the i-th cell */
        for (int e = 0; e < nb_edges; e++) {
          /* consider edge e with 2 nodes {n1,n2} */
          int v1 = edges[e][0];
          int v2 = edges[e][1];
          int n1 = cm->cells[i * elm_size + v1];
          int n2 = cm->cells[i * elm_size + v2];
          /* search a cell connected to i by edge {n1,n2} */
          for (int k = 0; k < dual[n1].size; k++) {
            /* check if cell kk is connected to cell i */
            int kk = dual[n1].array[k];
            if (kk != cell_idx) {
              if (findSortedList(&dual[n2], kk) != -1) insertList(&found, kk);
            }
          }
        }
#endif

        /* insert graph edges */
        LibGraph_sortList(&found);
        for (int x = 0; x < found.size; x++) {
          if (x > 0 && found.array[x] == found.array[x - 1]) continue;  // skip duplicated edges
          graph->adjncy[adjncyIndex++] = found.array[x];
          if (adjncyIndex >= 2 * maxedges) {
            maxedges *= 2;
            graph->adjncy = realloc(graph->adjncy, 2 * maxedges * sizeof(int));
            assert(graph->adjncy);
          }
        }
        cell_idx++;
      }
    }
    /* ****** HEXAHEDRON (face/edge connectivity) ****** */
    else if (cm->elm_type == LIBGRAPH_HEXAHEDRON) {
#if (FACE_CONNECTIVITY == 1)
      const int nb_faces = 6;
      int faces[nb_faces][4];
      faces[0][0] = 0;
      faces[0][1] = 1;
      faces[0][2] = 2;
      faces[0][3] = 3;
      faces[1][0] = 4;
      faces[1][1] = 5;
      faces[1][2] = 6;
      faces[1][3] = 7;
      faces[2][0] = 0;
      faces[2][1] = 1;
      faces[2][2] = 5;
      faces[2][3] = 4;
      faces[3][0] = 2;
      faces[3][1] = 6;
      faces[3][2] = 7;
      faces[3][3] = 3;
      faces[4][0] = 1;
      faces[4][1] = 2;
      faces[4][2] = 6;
      faces[4][3] = 5;
      faces[5][0] = 0;
      faces[5][1] = 3;
      faces[5][2] = 7;
      faces[5][3] = 4;
#endif

#if (EDGE_CONNECTIVITY == 1)
      const int nb_edges = 12;
      int edges[nb_edges][2];
      edges[0][0] = 0;
      edges[0][1] = 1;
      edges[1][0] = 1;
      edges[1][1] = 2;
      edges[2][0] = 2;
      edges[2][1] = 3;
      edges[3][0] = 3;
      edges[3][1] = 0;
      edges[4][0] = 4;
      edges[4][1] = 5;
      edges[5][0] = 5;
      edges[5][1] = 6;
      edges[6][0] = 6;
      edges[6][1] = 7;
      edges[7][0] = 7;
      edges[7][1] = 4;
      edges[8][0] = 0;
      edges[8][1] = 4;
      edges[9][0] = 1;
      edges[9][1] = 5;
      edges[10][0] = 2;
      edges[10][1] = 6;
      edges[11][0] = 3;
      edges[11][1] = 7;
#endif

      /* for all cells */
      for (int i = 0; i < cm->nb_cells; i++) {
        graph->xadj[cell_idx] = adjncyIndex;
        found.size = 0;  // reset

#if (FACE_CONNECTIVITY == 1)

        /* for all faces of the i-th cell */
        for (int f = 0; f < nb_faces; f++) {
          /* consider face f with 4 nodes {n1,n2,n3,n4} */
          int v1 = faces[f][0];
          int v2 = faces[f][1];
          int v3 = faces[f][2];
          int v4 = faces[f][3];
          int n1 = cm->cells[i * elm_size + v1];
          int n2 = cm->cells[i * elm_size + v2];
          int n3 = cm->cells[i * elm_size + v3];
          int n4 = cm->cells[i * elm_size + v4];

          /* search a cell connected to i by face {n1,n2,n3,n4} */
          for (int k = 0; k < dual[n1].size; k++) {
            /* check if cell kk is connected to cell i */
            int kk = dual[n1].array[k];
            if (kk != cell_idx) {
              if (LibGraph_findSortedList(&dual[n2], kk) != -1 && LibGraph_findSortedList(&dual[n3], kk) != -1 &&
                  LibGraph_findSortedList(&dual[n4], kk) != -1)
                LibGraph_insertList(&found, kk);
            }
          }
        }
#endif
#if (EDGE_CONNECTIVITY == 1)
        /* for all edges of the i-th cell */
        for (int e = 0; e < nb_edges; e++) {
          /* consider edge e with 2 nodes {n1,n2} */
          int v1 = edges[e][0];
          int v2 = edges[e][1];
          int n1 = cm->cells[i * elm_size + v1];
          int n2 = cm->cells[i * elm_size + v2];
          /* search a cell connected to i by edge {n1,n2} */
          for (int k = 0; k < dual[n1].size; k++) {
            /* check if cell kk is connected to cell i */
            int kk = dual[n1].array[k];
            if (kk != cell_idx) {
              if (LibGraph_findSortedList(&dual[n2], kk) != -1) LibGraph_insertList(&found, kk);
            }
          }
        }
#endif
        /* insert graph edges */
        LibGraph_sortList(&found);
        for (int x = 0; x < found.size; x++) {
          if (x > 0 && found.array[x] == found.array[x - 1]) continue;  // skip duplicated edges
          graph->adjncy[adjncyIndex++] = found.array[x];
          if (adjncyIndex >= 2 * maxedges) {
            maxedges *= 2;
            graph->adjncy = realloc(graph->adjncy, 2 * maxedges * sizeof(int));
            assert(graph->adjncy);
          }
        }
        cell_idx++;
      }
    }

    /* ****** PRISM (face/edge connectivity) ****** */
    else if (cm->elm_type == LIBGRAPH_PRISM) {
#if (FACE_CONNECTIVITY == 1)
      const int nb_faces = 5;
      int faces[nb_faces][4];
      faces[0][0] = 0;
      faces[0][1] = 1;
      faces[0][2] = 2;
      faces[0][3] = -1; /* triangle */
      faces[1][0] = 3;
      faces[1][1] = 4;
      faces[1][2] = 5;
      faces[1][3] = -1; /* triangle */
      faces[2][0] = 0;
      faces[2][1] = 2;
      faces[2][2] = 5;
      faces[2][3] = 3;
      faces[3][0] = 2;
      faces[3][1] = 1;
      faces[3][2] = 4;
      faces[3][3] = 5;
      faces[4][0] = 0;
      faces[4][1] = 1;
      faces[4][2] = 4;
      faces[4][3] = 3;
#endif

#if (EDGE_CONNECTIVITY == 1)
      const int nb_edges = 9;
      int edges[nb_edges][2];
      edges[0][0] = 0;
      edges[0][1] = 1;
      edges[1][0] = 1;
      edges[1][1] = 2;
      edges[2][0] = 2;
      edges[2][1] = 0;
      edges[3][0] = 3;
      edges[3][1] = 4;
      edges[4][0] = 4;
      edges[4][1] = 5;
      edges[5][0] = 5;
      edges[5][1] = 3;
      edges[6][0] = 0;
      edges[6][1] = 3;
      edges[7][0] = 1;
      edges[7][1] = 4;
      edges[8][0] = 2;
      edges[8][1] = 5;
#endif

      /* for all cells */
      for (int i = 0; i < cm->nb_cells; i++) {
        graph->xadj[cell_idx] = adjncyIndex;
        found.size = 0;  // reset
#if (FACE_CONNECTIVITY == 1)

        /* for all faces of the i-th cell */
        for (int f = 0; f < nb_faces; f++) {
          /* consider face f with 3 or 4 nodes {n1,n2,n3,n4} */
          int v1 = faces[f][0];
          int v2 = faces[f][1];
          int v3 = faces[f][2];
          int v4 = faces[f][3];
          int n1 = cm->cells[i * elm_size + v1];
          int n2 = cm->cells[i * elm_size + v2];
          int n3 = cm->cells[i * elm_size + v3];
          int n4 = (v4 != -1) ? cm->cells[i * elm_size + v4] : -1; /* not always used */
          /* search a cell connected to i by face {n1,n2,n3} or {n1,n2,n3,n4} */
          for (int k = 0; k < dual[n1].size; k++) {
            /* check if cell kk is connected to cell i */
            int kk = dual[n1].array[k];
            if (kk != cell_idx) {
              if (LibGraph_findSortedList(&dual[n2], kk) != -1 && LibGraph_findSortedList(&dual[n3], kk) != -1) {
                if (n4 != -1 && LibGraph_findSortedList(&dual[n4], kk) != -1) /* quad face */
                  LibGraph_insertList(&found, kk);
                else if (n4 == -1) /* triangular face */
                  LibGraph_insertList(&found, kk);
              }
            }
          }
        }

#endif

#if (EDGE_CONNECTIVITY == 1)
        /* for all edges of the i-th cell */
        for (int e = 0; e < nb_edges; e++) {
          /* consider edge e with 2 nodes {n1,n2} */
          int v1 = edges[e][0];
          int v2 = edges[e][1];
          int n1 = cm->cells[i * elm_size + v1];
          int n2 = cm->cells[i * elm_size + v2];
          /* search a cell connected to i by edge {n1,n2} */
          for (int k = 0; k < dual[n1].size; k++) {
            /* check if cell kk is connected to cell i */
            int kk = dual[n1].array[k];
            if (kk != cell_idx) {
              if (LibGraph_findSortedList(&dual[n2], kk) != -1) LibGraph_insertList(&found, kk);
            }
          }
        }

#endif

        /* insert graph edges */
        LibGraph_sortList(&found);
        for (int x = 0; x < found.size; x++) {
          if (x > 0 && found.array[x] == found.array[x - 1]) continue;  // skip duplicated edges
          graph->adjncy[adjncyIndex++] = found.array[x];
          if (adjncyIndex >= 2 * maxedges) {
            maxedges *= 2;
            graph->adjncy = realloc(graph->adjncy, 2 * maxedges * sizeof(int));
            assert(graph->adjncy);
          }
        }
        cell_idx++;
      }
    }  // prism

  }  // hm->nb_components
  /* end */

  graph->xadj[graph->nvtxs] = adjncyIndex;
  graph->narcs = adjncyIndex;

  /* free unused memory */
  graph->adjncy = realloc(graph->adjncy, graph->narcs * sizeof(int));
  free(found.array);

  /* free dual */
  for (int i = 0; i < nb_nodes; i++) free(dual[i].array);
  free(dual);
}

/* *********************************************************** */

int LibGraph_addMigrationEdges(LibGraph_Graph* oldg,                                      /**< [in] Old hypergraph */
                               int* oldpart,                                              /**< [in] Old partition */
                               LibGraph_Hypergraph* modelhg, int repartmult, int migcost, /**< [in] new edges weight */
                               LibGraph_Graph* newg)                                      /**< [out] New hypergraph */
{
  int M = modelhg->nhedges;
  int N = modelhg->nvtxs;

  int nbaddededges = 0;
  for (int v = 0; v < oldg->nvtxs; v++) { nbaddededges += modelhg->eptr[oldpart[v] + 1] - modelhg->eptr[oldpart[v]]; }
  newg->nvtxs = oldg->nvtxs + N;
  newg->narcs = oldg->narcs + nbaddededges * 2;
  newg->xadj = malloc((newg->nvtxs + 1) * sizeof(int));
  assert(newg->xadj);
  newg->adjncy = malloc(newg->narcs * sizeof(int));
  assert(newg->adjncy);
  newg->vwgts = malloc(newg->nvtxs * sizeof(int));
  assert(newg->vwgts);
  newg->ewgts = malloc(newg->narcs * sizeof(int));
  assert(newg->ewgts);

  /* Add edges from regular vertices */
  int k = 0;
  for (int v = 0; v < oldg->nvtxs; v++) {
    newg->xadj[v] = k;
    /* Copy adjncy */
    for (int i = oldg->xadj[v]; i < oldg->xadj[v + 1]; i++) {
      newg->adjncy[k] = oldg->adjncy[i];
      newg->ewgts[k++] = repartmult * (oldg->ewgts ? oldg->ewgts[i] : 1);
    }

    /* Add new edges with new vertices */
    for (int i = modelhg->eptr[oldpart[v]]; i < modelhg->eptr[oldpart[v] + 1]; i++) {
      newg->adjncy[k] = oldg->nvtxs + modelhg->eind[i];
      newg->ewgts[k++] = migcost;
    }
  }

  /* Add new edges from new vertices */
  int* pvtxs = calloc(M, sizeof(int));
  for (int v = 0; v < oldg->nvtxs; v++) pvtxs[oldpart[v]]++;
  newg->xadj[oldg->nvtxs] = k;
  for (int i = 0; i < N; i++) {
    newg->xadj[oldg->nvtxs + i + 1] = newg->xadj[oldg->nvtxs + i];
    for (int j = 0; j < M; j++)
      for (int l = modelhg->eptr[j]; l < modelhg->eptr[j + 1]; l++)
        if (modelhg->eind[l] == i) {
          newg->xadj[oldg->nvtxs + i + 1] += pvtxs[j];
          break;
        }
  }
  int* ind = calloc(N, sizeof(int));
  for (int v = 0; v < oldg->nvtxs; v++) {
    for (int i = modelhg->eptr[oldpart[v]]; i < modelhg->eptr[oldpart[v] + 1]; i++) {
      newg->adjncy[newg->xadj[oldg->nvtxs + modelhg->eind[i]] + ind[modelhg->eind[i]]] = v;
      newg->ewgts[newg->xadj[oldg->nvtxs + modelhg->eind[i]] + ind[modelhg->eind[i]]++] = migcost;
    }
  }
  free(pvtxs);
  free(ind);

  /* Weights */
  for (int v = 0; v < oldg->nvtxs; v++) newg->vwgts[v] = (oldg->vwgts ? oldg->vwgts[v] : 1);
  for (int i = 0; i < N; i++) newg->vwgts[oldg->nvtxs + i] = 1;

  return N;
}

/* *********************************************************** */
