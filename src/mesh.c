#include <assert.h>
#include <limits.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "debug.h"
#include "mesh.h"

#define COMPARE_EPSILON 0.000001

/* *********************************************************** */

LibGraph_Mesh* LibGraph_newMesh(void)
{
  LibGraph_Mesh* m = malloc(sizeof(LibGraph_Mesh));
  assert(m);
  m->elm_type = 0;
  m->nb_nodes = 0;
  m->nb_cells = 0;
  m->nodes = NULL;
  m->cells = NULL;
  // m->nb_node_vars = 0;
  // m->nb_cell_vars = 0;
  // m->node_vars = NULL;
  // m->cell_vars = NULL;
  return m;
}

/* *********************************************************** */

void LibGraph_shiftMesh(LibGraph_Mesh* mesh, double vx, double vy, double vz)
{
  assert(mesh);
  assert(mesh->nodes);
  for (int j = 0; j < mesh->nb_nodes; j++) {
    mesh->nodes[j * 3 + 0] += vx;
    mesh->nodes[j * 3 + 1] += vy;
    mesh->nodes[j * 3 + 2] += vz;
  }
}

/* *********************************************************** */

// deg -> rad
// 180 -> PI

#define PI 3.14159
#define RADIAN(deg) (deg * PI / 180.0)

void LibGraph_rotateMesh(LibGraph_Mesh* mesh, double rx, double ry, double rz)
{
  assert(mesh);
  assert(mesh->nodes);
  double* coords = malloc(3 * mesh->nb_nodes * sizeof(double));

  for (int j = 0; j < mesh->nb_nodes; j++) {
    double* c = mesh->nodes + j * 3;

    // rotation x
    if (rx != 0.0) {
      coords[j * 3 + 0] = c[0];  // invariant
      coords[j * 3 + 1] = c[1] * cos(RADIAN(rx)) - c[2] * sin(RADIAN(rx));
      coords[j * 3 + 2] = c[1] * sin(RADIAN(rx)) + c[2] * cos(RADIAN(rx));
      c[0] = coords[j * 3 + 0];
      c[1] = coords[j * 3 + 1];
      c[2] = coords[j * 3 + 2];
    }

    // rotation y
    if (ry != 0.0) {
      coords[j * 3 + 0] = c[0] * cos(RADIAN(ry)) + c[2] * sin(RADIAN(ry));
      coords[j * 3 + 1] = c[1];  // invariant
      coords[j * 3 + 2] = -c[0] * sin(RADIAN(ry)) + c[2] * cos(RADIAN(ry));
      c[0] = coords[j * 3 + 0];
      c[1] = coords[j * 3 + 1];
      c[2] = coords[j * 3 + 2];
    }

    // rotation z
    if (rz != 0.0) {
      coords[j * 3 + 0] = c[0] * cos(RADIAN(rz)) - c[1] * sin(RADIAN(rz));
      coords[j * 3 + 1] = c[0] * sin(RADIAN(rz)) - c[1] * cos(RADIAN(rz));
      coords[j * 3 + 2] = c[2];  // invariant
      c[0] = coords[j * 3 + 0];
      c[1] = coords[j * 3 + 1];
      c[2] = coords[j * 3 + 2];
    }
  }

  free(coords);
}

/* *********************************************************** */

void LibGraph_scaleMesh(LibGraph_Mesh* mesh, double sx, double sy, double sz)
{
  assert(mesh);
  assert(mesh->nodes);
  for (int j = 0; j < mesh->nb_nodes; j++) {
    mesh->nodes[j * 3 + 0] *= sx;
    mesh->nodes[j * 3 + 1] *= sy;
    mesh->nodes[j * 3 + 2] *= sz;
  }
}

/* *********************************************************** */

// remove nodes unused
void LibGraph_simplifyMesh(LibGraph_Mesh* meshin, LibGraph_Mesh* meshout)
{
  assert(meshin && meshout);
  assert(meshin->nodes);
  LibGraph_dupMesh(meshin, meshout);

  int* mapping = malloc(meshin->nb_nodes * sizeof(int));
  for (int i = 0; i < meshin->nb_nodes; i++) mapping[i] = -1;

  int elm_size = LIBGRAPH_ELEMENT_SIZE[meshin->elm_type];

  // visit all node used...
  int z = 0;
  for (int i = 0; i < meshin->nb_cells; i++) {
    for (int j = 0; j < elm_size; j++) {
      int k = meshin->cells[i * elm_size + j];  // old node index
      if (mapping[k] == -1) mapping[k] = z++;   // new node index
    }
  }
  meshout->nb_nodes = z;
  assert(meshout->nb_nodes <= meshin->nb_nodes);

  // update connectivity in meshout
  for (int i = 0; i < meshout->nb_cells; i++) {
    for (int j = 0; j < elm_size; j++) {
      int k = meshout->cells[i * elm_size + j];  // old node index
      int z = mapping[k];                        // new node index
      assert(z != -1);
      meshout->cells[i * elm_size + j] = z;
    }
  }

  // remove unused nodes in meshout
  for (int k = 0; k < meshin->nb_nodes; k++) {
    if (mapping[k] != -1) {
      int z = mapping[k];  // k-th node in old mesh is the z-th node in new mesh.
      meshout->nodes[z * 3 + 0] = meshin->nodes[k * 3 + 0];
      meshout->nodes[z * 3 + 1] = meshin->nodes[k * 3 + 1];
      meshout->nodes[z * 3 + 2] = meshin->nodes[k * 3 + 2];
    }
  }

  meshout->nodes = realloc(meshout->nodes, meshout->nb_nodes * 3 * sizeof(double));
  free(mapping);
}

/* *********************************************************** */

void LibGraph_subMeshFromSet(const LibGraph_Mesh* m, int ncells, const int* cells, LibGraph_Mesh* subm)
{
  assert(m && subm);
  subm->elm_type = m->elm_type;

  /* duplicate nodes */
  subm->nb_nodes = m->nb_nodes;
  if (m->nodes) {
    subm->nodes = malloc(m->nb_nodes * 3 * sizeof(double));
    memcpy(subm->nodes, m->nodes, m->nb_nodes * 3 * sizeof(double));
  }
  /* apply restriction to cells */
  int elm_size = LIBGRAPH_ELEMENT_SIZE[m->elm_type];
  assert(ncells <= m->nb_cells);
  subm->nb_cells = ncells;
  subm->cells = malloc(ncells * elm_size * sizeof(int));
  for (int i = 0; i < ncells; i++) {
    int k = cells[i];
    for (int j = 0; j < elm_size; j++) subm->cells[i * elm_size + j] = m->cells[k * elm_size + j];
  }

  /* no variables */
  // subm->nb_node_vars = subm->nb_cell_vars = 0;
  // subm->node_vars = subm->cell_vars = NULL;
}

/* *********************************************************** */

void LibGraph_printMesh(LibGraph_Mesh* mesh)
{
  int elm_size = LIBGRAPH_ELEMENT_SIZE[mesh->elm_type];
  printf("Mesh\n");
  printf("- element type = %d\n", mesh->elm_type);
  printf("- element size = %d\n", elm_size);
  printf("- nb nodes = %d\n", mesh->nb_nodes);
  printf("- nb cells = %d\n", mesh->nb_cells);

  /* print connectivity */
  int max_cells = 100;
  int nb_cells = (mesh->nb_cells <= max_cells) ? mesh->nb_cells : max_cells;

  printf("- connectivity list\n");
  for (int i = 0; i < nb_cells; i++) {
    printf("  %d -> [ ", i);
    for (int j = 0; j < elm_size; j++) { printf("%d ", mesh->cells[i * elm_size + j]); }
    printf("]\n");
  }
  if (mesh->nb_cells > max_cells) printf("  ...\n");

  /* print node coordinates */
  int max_nodes = 100;
  int nb_nodes = (mesh->nb_nodes <= max_nodes) ? mesh->nb_nodes : max_nodes;
  printf("- node coordinates\n");
  if (mesh->nodes) {
    for (int j = 0; j < nb_nodes; j++) {
      printf("  %d -> [ ", j);
      for (int i = 0; i < 3; i++) printf("%f ", mesh->nodes[j * 3 + i]);
      printf("]\n");
    }
  }
  if (mesh->nb_nodes > max_nodes) printf("  ...\n");

  // /* print variables */
  // printf("- nb node variables = %d\n", mesh->nb_node_vars);
  // printf("- nb cell variables = %d\n", mesh->nb_cell_vars);

  // /* cell variables */
  // for (int i = 0; i < mesh->nb_cell_vars; i++) {
  //   printf("- cell variable \"%s\" [", mesh->cell_vars[i].id);
  //   for (int j = 0; j < nb_cells; j++)
  //     if (mesh->cell_vars[i].type == LIBGRAPH_VAR_INT)
  //       printf("%d ", ((int*)mesh->cell_vars[i].data)[j]);
  //     else if (mesh->cell_vars[i].type == LIBGRAPH_VAR_DOUBLE)
  //       printf("%f ", ((double*)mesh->cell_vars[i].data)[j]);
  //   printf("]\n");
  //   if (mesh->nb_cells > max_cells) printf("  ...\n");
  // }

  // /* node variables */
  // for (int i = 0; i < mesh->nb_node_vars; i++) {
  //   printf("- node variable \"%s\" [", mesh->node_vars[i].id);
  //   for (int j = 0; j < nb_nodes; j++)
  //     if (mesh->node_vars[i].type == LIBGRAPH_VAR_INT)
  //       printf("%d ", ((int*)mesh->node_vars[i].data)[j]);
  //     else if (mesh->node_vars[i].type == LIBGRAPH_VAR_DOUBLE)
  //       printf("%f ", ((double*)mesh->node_vars[i].data)[j]);
  //   printf("]\n");
  //   if (mesh->nb_nodes > max_nodes) printf("  ...\n");
  // }
}

/* *********************************************************** */

void LibGraph_freeMesh(LibGraph_Mesh* mesh)
{
  if (mesh != NULL) {
    if (mesh->nodes != NULL) free(mesh->nodes);
    if (mesh->cells != NULL) free(mesh->cells);
    //   if (mesh->node_vars != NULL) {
    //     for (int i = 0; i < mesh->nb_node_vars; i++) free(mesh->node_vars[i].data);
    //     free(mesh->node_vars);
    //   }
    //   if (mesh->cell_vars != NULL) {
    //     for (int i = 0; i < mesh->nb_cell_vars; i++) free(mesh->cell_vars[i].data);
    //     free(mesh->cell_vars);
    //   }
    // }
  }
}

/* *********************************************************** */

/* void quotientMesh(Mesh * mesh, int nparts, int * parts, Mesh * qmesh) */
/* { */
/*   double coords[nparts][3]; */
/*   int size[nparts]; */
/*   for(int i = 0 ; i < nparts ; i++) { */
/*     coords[i][0] = coords[i][1] = coords[i][2] = 0.0; */
/*     size[i] = 0; */
/*   } */

/*   int nvtxs = mesh->nb_cells; */

/*   for(int i = 0 ; i < nvtxs ; i++) { */
/*     int p = parts[i];  */
/*     if(p >= 0) { */
/*       double G[3]; */
/*       cellCenter(mesh,i,G); */
/*       coords[p][0] += G[0]; */
/*       coords[p][1] += G[1]; */
/*       coords[p][2] += G[2]; */
/*       size[p]++; */
/*     } */
/*   } */

/*   for(int i = 0 ; i < nparts ; i++) { */
/*     coords[i][0] /= size[i]; */
/*     coords[i][1] /= size[i]; */
/*     coords[i][2] /= size[i]; */
/*   } */

/*   Graph g; */
/*   mesh2Graph(mesh, &g); */

/*   Graph quotient; */
/*   quotientGraph(&g, nparts, parts, &quotient); */

/*   graph2LineMesh(&quotient, (double*)coords, qmesh);   */

/* } */

/* *********************************************************** */

void LibGraph_dupMesh(LibGraph_Mesh* oldm, LibGraph_Mesh* newm)
{
  assert(oldm && newm);
  newm->elm_type = oldm->elm_type;
  newm->nb_cells = oldm->nb_cells;
  newm->nb_nodes = oldm->nb_nodes;

  newm->cells = malloc(newm->nb_cells * LIBGRAPH_ELEMENT_SIZE[newm->elm_type] * sizeof(int));
  memcpy(newm->cells, oldm->cells, newm->nb_cells * LIBGRAPH_ELEMENT_SIZE[newm->elm_type] * sizeof(int));

  if (oldm->nodes) {
    newm->nodes = malloc(newm->nb_nodes * 3 * sizeof(double));
    memcpy(newm->nodes, oldm->nodes, newm->nb_nodes * 3 * sizeof(double));
  }

  // duplicate node variables
  /*
  newm->nb_node_vars = oldm->nb_node_vars;
  newm->node_vars = malloc(newm->nb_node_vars * sizeof(LibGraph_Variable));
  for (int i = 0; i < newm->nb_node_vars; i++) {
    strcpy(newm->node_vars[i].id, oldm->node_vars[i].id);
    newm->node_vars[i].nb_tuples = oldm->node_vars[i].nb_tuples;
    newm->node_vars[i].nb_components = oldm->node_vars[i].nb_components;
    newm->node_vars[i].type = oldm->node_vars[i].type;
    int datasize = newm->node_vars[i].nb_tuples * newm->node_vars[i].nb_components;
    if (newm->node_vars[i].type == LIBGRAPH_VAR_INT)
      datasize *= sizeof(int);
    else
      datasize *= sizeof(double);
    newm->node_vars[i].data = malloc(datasize);
    assert(newm->node_vars[i].data);
    memcpy(newm->node_vars[i].data, oldm->node_vars[i].data, datasize);
  }

  duplicate cell variables
  newm->nb_cell_vars = oldm->nb_cell_vars;
  newm->cell_vars = malloc(newm->nb_cell_vars * sizeof(LibGraph_Variable));
  for (int i = 0; i < newm->nb_cell_vars; i++) {
    strcpy(newm->cell_vars[i].id, oldm->cell_vars[i].id);
    newm->cell_vars[i].nb_tuples = oldm->cell_vars[i].nb_tuples;
    newm->cell_vars[i].nb_components = oldm->cell_vars[i].nb_components;
    newm->cell_vars[i].type = oldm->cell_vars[i].type;
    int datasize = newm->cell_vars[i].nb_tuples * newm->cell_vars[i].nb_components;
    if (newm->cell_vars[i].type == LIBGRAPH_VAR_INT)
      datasize *= sizeof(int);
    else
      datasize *= sizeof(double);
    newm->cell_vars[i].data = malloc(datasize);
    assert(newm->cell_vars[i].data);
    memcpy(newm->cell_vars[i].data, oldm->cell_vars[i].data, datasize);
  }
  */
}

/* *********************************************************** */

bool LibGraph_compareMesh(LibGraph_Mesh *m1, LibGraph_Mesh *m2)
{
  if (m1->elm_type != m2->elm_type) return 0;
  if (m1->nb_nodes != m2->nb_nodes) return 0;
  if (m1->nb_cells != m2->nb_cells) return 0;

  for (int n = 0; n < 3*m1->nb_nodes; n++) {
    if (m1->nodes[n]+COMPARE_EPSILON < m2->nodes[n] ||
        m1->nodes[n]-COMPARE_EPSILON > m2->nodes[n])
      return 0;
  }

  if (memcmp(m1->cells, m2->cells,
        m1->nb_cells*LIBGRAPH_ELEMENT_SIZE[m1->elm_type]*sizeof(int)))
    return 0;

  return 1;
}

/* *********************************************************** */

void LibGraph_removeMeshConnectivity(LibGraph_Mesh* oldm, LibGraph_Mesh* newm)
{
  assert(oldm && newm);
  newm->elm_type = LIBGRAPH_POINT;
  newm->nb_cells = oldm->nb_nodes;
  newm->nb_nodes = oldm->nb_nodes;

  if (oldm->nodes) {
    newm->nodes = malloc(newm->nb_nodes * 3 * sizeof(double));
    memcpy(newm->nodes, oldm->nodes, newm->nb_nodes * 3 * sizeof(double));
  }
  newm->cells = malloc(newm->nb_cells * LIBGRAPH_ELEMENT_SIZE[newm->elm_type] * sizeof(int));
  for (int i = 0; i < newm->nb_cells; i++) newm->cells[i] = i;

  // set old node variables as new cell variable
  /*
  newm->nb_cell_vars = oldm->nb_node_vars;
  newm->cell_vars = malloc(newm->nb_cell_vars * sizeof(LibGraph_Variable));
  for (int i = 0; i < newm->nb_cell_vars; i++) {
    strcpy(newm->cell_vars[i].id, oldm->node_vars[i].id);
    newm->cell_vars[i].nb_tuples = oldm->node_vars[i].nb_tuples;
    newm->cell_vars[i].nb_components = oldm->node_vars[i].nb_components;
    newm->cell_vars[i].type = oldm->node_vars[i].type;
    int datasize = newm->cell_vars[i].nb_tuples * newm->cell_vars[i].nb_components;
    if (newm->cell_vars[i].type == LIBGRAPH_VAR_INT)
      datasize *= sizeof(int);
    else
      datasize *= sizeof(double);
    newm->cell_vars[i].data = malloc(datasize);
    assert(newm->cell_vars[i].data);
    memcpy(newm->cell_vars[i].data, oldm->node_vars[i].data, datasize);
  }
*/

  // not used...
  /*
  newm->nb_node_vars = 0;
  newm->node_vars = NULL;
  */
}

/* *********************************************************** */
