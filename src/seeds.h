#ifndef LIBGRAPH_SEEDS_H
#define LIBGRAPH_SEEDS_H

#include "graph.h"

//@{

/** Compute seeds (for all parts without fixed vertices already assigned) */
/** \return the nb seeds computed */
int LibGraph_computeGraphSeeds(const LibGraph_Graph* g, /**< [in] graph */
                               int nparts,              /**< [in] nb of parts */
                               int optimize,            /**< [in] try to optimize seeds (0 or 1) */
                               int* part                /**< [in/out] array with input fixed vertices and output seeds
                                                           (array of size g->nvtxs) */
);

/** return the minimum distance between all seeds */
int LibGraph_computeSeedDistance(const LibGraph_Graph* g, /**< [in] graph */
                                 int nparts,              /**< [in] nb of parts */
                                 int* part /**< [in] array with input seeds (array of size g->nvtxs, -1 if free vtx) */
);

/** compute graph bubbles from input seeds */
void LibGraph_computeGraphBubbles(const LibGraph_Graph* g, /**< [in]  input graph */
                                  int size,                /**< [in]  nb parts */
                                  int maxpartsize,         /**< [in]  max part size allowed (-1 if infinite) */
                                  int* part                /**< [in/out]  input/output partition (with seeds) */
);

//@}

#endif
