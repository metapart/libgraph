#define _GNU_SOURCE
#include <assert.h>
#include <limits.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "array.h"
#include "debug.h"

#define COMPARE_EPSILON 0.000001

/* *********************************************************** */
/*                           ARRAY                             */
/* *********************************************************** */

static int sizeofTypecode(int typecode)
{
  switch (typecode) {
    case LIBGRAPH_VOID: return 0;
    case LIBGRAPH_INT: return sizeof(int);
    case LIBGRAPH_FLOAT: return sizeof(float);
    case LIBGRAPH_DOUBLE: return sizeof(double);
    case LIBGRAPH_STR: return sizeof(char*);
    default: return -1;
  }
}

/* *********************************************************** */

LibGraph_Array* LibGraph_newArray(void)
{
  LibGraph_Array* a = malloc(sizeof(LibGraph_Array));
  assert(a);
  a->nb_tuples = 0;
  a->nb_components = 0;
  a->typecode = LIBGRAPH_VOID;
  a->flags = 0;
  a->data = NULL;
  a->owner = false;
  return a;
}

/* *********************************************************** */

static void dumpArray(LibGraph_Array* a, int max)
{
  if (max == 0) max = a->nb_tuples * a->nb_components;
  for (int i = 0; i < (a->nb_tuples * a->nb_components) && (i < max); i++) {
    switch (a->typecode) {
      case LIBGRAPH_VOID: break;
      case LIBGRAPH_INT: printf("%d ", ((int*)a->data)[i]); break;
      case LIBGRAPH_FLOAT: printf("%.2f ", ((float*)a->data)[i]); break; // TODO: printf % char for float?
      case LIBGRAPH_DOUBLE: printf("%.2f ", ((double*)a->data)[i]); break;
      case LIBGRAPH_STR: printf("%s ", ((char**)a->data)[i]); break;
      default: printf("? ");
    }
  }
}

/* *********************************************************** */

void LibGraph_printArray(LibGraph_Array* a, bool dump)
{
  printf("Array\n");
  printf("- nb tuples = %d\n", a->nb_tuples);
  printf("- nb components = %d\n", a->nb_components);
  printf("- typecode = %d (size %d bytes)\n", a->typecode, sizeofTypecode(a->typecode));
  if (dump) {
    printf("- data [");
    dumpArray(a, 0);
    printf("]\n");
  }
  printf("- owner = %d\n", a->owner);
  printf("- flags = %d\n", a->flags);
}

/* *********************************************************** */

void LibGraph_freeArray(LibGraph_Array* a)
{
  if (a != NULL) {
    if (a->owner && a->data) {
      if (a->typecode == LIBGRAPH_STR) {
        int length = a->nb_components * a->nb_tuples;
        for (int i = 0; i < length; i++) free(((char**)a->data)[i]);
      }
      free(a->data);
    }
    /* reset */
    a->nb_tuples = 0;
    a->nb_components = 0;
    a->typecode = LIBGRAPH_VOID;
    a->flags = 0;
    a->data = NULL;
    a->owner = false;
  }
}

/* *********************************************************** */

void LibGraph_dupArray(LibGraph_Array* olda, LibGraph_Array* newa, bool deep)
{
  assert(olda && newa);
  newa->nb_tuples = olda->nb_tuples;
  newa->nb_components = olda->nb_components;
  newa->typecode = olda->typecode;
  newa->flags = olda->flags;
  int length = olda->nb_components * olda->nb_tuples;
  int size = length * sizeofTypecode(olda->typecode);  // in bytes
  assert(size > 0);
  if (length > 0) assert(olda->data);
  newa->data = malloc(size);
  assert(newa->data);
  if (deep) /* deep copy */
  {
    newa->owner = true;
    if (olda->typecode == LIBGRAPH_STR)
      for (int i = 0; i < length; i++) ((char**)newa->data)[i] = strdup(((char**)olda->data)[i]);
    else
      memcpy(newa->data, olda->data, size);
  }
  else { /* shallow copy */
    newa->owner = false;
    newa->data = olda->data;
  }
}

/* *********************************************************** */

void* LibGraph_allocArray(LibGraph_Array* a, int nb_tuples, int nb_components, int typecode)
{
  assert(a);
  assert(nb_tuples > 0 && nb_components > 0);
  assert(typecode >= 0 && typecode < LIBGRAPH_NB_TYPECODES);
  a->nb_tuples = nb_tuples;
  a->nb_components = nb_components;
  a->typecode = typecode;
  a->data = calloc(nb_tuples * nb_components, sizeofTypecode(typecode));  // in bytes
  assert(a->data);
  a->owner = true;
  return a->data;
}

/* *********************************************************** */

bool LibGraph_compareArray(LibGraph_Array* a1, LibGraph_Array* a2)
{
  int type1 = a1->typecode;
  int type2 = a2->typecode;
  void* data1 = a1->data;
  void* data2 = a2->data;
  int length1 = a1->nb_tuples * a2->nb_components;
  int length2 = a2->nb_tuples * a2->nb_components;

  if (type1 != type2) return 0;
  if (length1 != length2) return 0;

  if (LIBGRAPH_INT == type1) {
    if (memcmp(a1->data, a2->data, sizeof(sizeofTypecode(type1)*length1))) return 0;

  } else if (LIBGRAPH_FLOAT == type1) {
    float* values1 = data1;
    float* values2 = data2;
    for (int i = 0; i < length1; i++) {
      if (values1[i]+COMPARE_EPSILON < values2[i] ||
          values1[i]-COMPARE_EPSILON > values2[i])
        return 0;
    }

  } else if (LIBGRAPH_DOUBLE == type1) {
    double* values1 = data1;
    double* values2 = data2;
    for (int i = 0; i < length1; i++) {
      if (values1[i]+COMPARE_EPSILON < values2[i] ||
          values1[i]-COMPARE_EPSILON > values2[i])
        return 0;
    }

  } else if (LIBGRAPH_STR == type1) {
    char** strings1 = data1;
    char** strings2 = data2;
    for (int s = 0; s < length1; s++) {
      if (strlen(strings1[s]) != strlen(strings2[s]) ||
              strcmp(strings1[s], strings2[s]))
              return 0;
    }
  }

  return 1;
}

/* *********************************************************** */

void LibGraph_wrapArray(LibGraph_Array* a, int nb_tuples, int nb_components, int typecode, const void* data, bool owner)
{
  assert(a);
  assert(nb_tuples > 0 && nb_components > 0);
  assert(typecode >= 0 && typecode < LIBGRAPH_NB_TYPECODES);
  a->nb_tuples = nb_tuples;
  a->nb_components = nb_components;
  a->typecode = typecode;
  a->data = (void*)data;
  a->owner = owner;
  a->flags = 0;
}

/* *********************************************************** */

void LibGraph_setArrayFlags(LibGraph_Array* a, LibGraph_ArrayFlags flags)
{
  assert(a);
  a->flags = flags;
}

/* *********************************************************** */

LibGraph_ArrayFlags LibGraph_getArrayFlags(LibGraph_Array* a)
{
  assert(a);
  return a->flags;
}

/* *********************************************************** */
/*                            VARIABLES                        */
/* *********************************************************** */

LibGraph_Variables* LibGraph_newVariables(int max)
{
  assert(max > 0);
  LibGraph_Variables* vars = malloc(sizeof(LibGraph_Variables));
  assert(vars);
  vars->nb_vars = 0;
  vars->max_vars = max;
  vars->vars = malloc(max * sizeof(LibGraph_Variable));
  assert(vars->vars);
  for (int i = 0; i < max; i++) {
    vars->vars[i].id = NULL;
    vars->vars[i].array = NULL;
  }
  return vars;
}

/* *********************************************************** */

void LibGraph_addVariable(LibGraph_Variables* vars, char* id, LibGraph_Array* array)
{
  assert(vars && array);
  assert(vars->nb_vars <= vars->max_vars);
  if (vars->nb_vars == vars->max_vars) {
    int max = vars->max_vars * 2;
    vars->vars = realloc(vars->vars, max * sizeof(LibGraph_Variable));
    assert(vars->vars);
    for (int i = vars->max_vars; i < max; i++) {
      vars->vars[i].id = NULL;
      vars->vars[i].array = NULL;
    }
    vars->max_vars = max;
  }
  assert(vars->nb_vars < vars->max_vars);
  int pos = vars->nb_vars;
  vars->vars[pos].id = strdup(id);
  vars->vars[pos].array = array;
  vars->nb_vars++;
}

/* *********************************************************** */

void LibGraph_freeVariables(LibGraph_Variables* vars, bool freearrays)
{
  if (!vars) return;
  for (int i = 0; i < vars->nb_vars; i++) {
    if (vars->vars[i].id) free(vars->vars[i].id);
    if (vars->vars[i].array && freearrays) {
      LibGraph_freeArray(vars->vars[i].array);
      free(vars->vars[i].array);
    }
  }
  free(vars->vars);
  vars->max_vars = vars->nb_vars = 0;
}

/* *********************************************************** */

char* LibGraph_getVariableID(LibGraph_Variables* vars, int i)
{
  assert(vars);
  assert(i >= 0 && i < vars->nb_vars);
  return vars->vars[i].id;
}

/* *********************************************************** */

LibGraph_Array* LibGraph_getVariableArray(LibGraph_Variables* vars, int i)
{
  assert(vars);
  assert(i >= 0 && i < vars->nb_vars);
  return vars->vars[i].array;
}

/* *********************************************************** */

LibGraph_Variables* LibGraph_filterVariables(const LibGraph_Variables* vars, LibGraph_ArrayFlags flag)
{
  if (!vars) return NULL;
  LibGraph_Variables* subvars = LibGraph_newVariables(vars->max_vars);
  for (int i = 0; i < vars->nb_vars; i++) {
    assert(vars->vars[i].array);
    if (vars->vars[i].array->flags & flag) LibGraph_addVariable(subvars, vars->vars[i].id, vars->vars[i].array);
  }
  return subvars;
}

/* *********************************************************** */
