#ifndef LIBGRAPH_COARSEN_H
#define LIBGRAPH_COARSEN_H

#include "graph.h"
#include "mesh.h"

//@{

/** edge matching enum */
enum LIBGRAPH_CTYPE
{
  LIBGRAPH_MATCH_RM,
  LIBGRAPH_MATCH_RMVW,
  LIBGRAPH_MATCH_HEM,
  LIBGRAPH_MATCH_HEM2,
  LIBGRAPH_MATCH_MHEM,
  LIBGRAPH_MATCH_SHEM,
  LIBGRAPH_MATCH_AEM
};

/* *********************************************************** */
/*                    COARSENING ROUTINE                       */
/* *********************************************************** */

/** Compute edge-matching. */
/* \return nb of vtxs in coarse graph */
int LibGraph_matchEdges(const LibGraph_Graph* g, /**< [in] graph */
                        int ctype,               /**< [in] coarsening method for edge-matching */
                        int* match,              /**< [out] matching array (array of size g->nvtxs) */
                        const int* partfixed     /**< [in] array of pre-assigned partitions (of size g->nvtxs) or NULL
                                              if useless. */
);

/** Compute ordered mapping (from vertex to coarser vertex) using edge-matching */
/* \return nb of vtxs in coarse graph */
int LibGraph_match2cmap(int nvtxs,  /**< [in] nb vtxs in graph */
                        int* match, /**< [in] matching array (array of size nvtxs) */
                        int* cmap   /**< [out] mapping from vertex to coarser vertex (array of size g->nvtxs) */
);

/** Coarsen a graph. */
void LibGraph_coarsenGraph(const LibGraph_Graph* g, /**< [in] graph */
                           int ctype,               /**< [in] coarsening method for edge-matching */
                           LibGraph_Graph* cg,      /**< [out] coarser graph */
                           int* cmap,               /**< [out] mapping from vertex to coarser vertex (array of size
                                                       g->nvtxs) */
                           const int* partfixed     /**< [in] array of pre-assigned partitions (of size g->nvtxs) or
                                                 NULL if useless. */
);

/** Coarsen a graph. */
void LibGraph_coarsenGraph2Mesh(
    const LibGraph_Mesh* m,   /**< [in] mesh */
    const LibGraph_Graph* cg, /**< [in] coarser graph */
    const int* cmap,          /**< [in] mapping from vertex to coarser vertex (array of size g->nvtxs) */
    LibGraph_Mesh* cm         /**< [out] coarser mesh */
);

/** Coarsen a graph recursively at multiple levels. */
/* \return nb of levels */
int LibGraph_coarsenGraphMultilevel(
    LibGraph_Graph* g,  /**< [in] graph */
    int ctype,          /**< [in] coarsening method for edge-matching */
    int maxcnvtxs,      /**< [in] max nb of vertices allowed in the coarser graph */
    LibGraph_Graph* cg, /**< [out] coarser graph */
    int* cmap           /**< [out] mapping from vertex to coarser vertices (array of size m->nb_cells) */
);

int LibGraph_coarsenMeshMultilevel(LibGraph_Mesh* m, int ctype, int maxcnvtxs, LibGraph_Mesh* cm, LibGraph_Graph* cg,
                                   int* cmap /* array of size m->nb_cells */
);

int LibGraph_coarsenLineMeshMultilevel(LibGraph_Mesh* m, int ctype, int maxcnvtxs, LibGraph_Mesh* cm,
                                       LibGraph_Graph* cg, int* cmap /* array of size m->nb_nodes */
);

//@}

#endif
