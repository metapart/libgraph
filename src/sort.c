#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "sort.h"

/* *********************************************************** */
/*                      SORTING ROUTINES                       */
/* *********************************************************** */

static inline void swapI(int* array, int i, int j)
{
  int temp;
  temp = array[i];
  array[i] = array[j];
  array[j] = temp;
}

/* *********************************************************** */

static inline void swapP(int* array, int i, int j)
{
  int temp[2];
  temp[0] = array[2 * i];
  temp[1] = array[2 * i + 1];
  array[2 * i] = array[2 * j];
  array[2 * i + 1] = array[2 * j + 1];
  array[2 * j] = temp[0];
  array[2 * j + 1] = temp[1];
}

/* *********************************************************** */

static inline void swapD(double* array, int i, int j)
{
  double temp;
  temp = array[i];
  array[i] = array[j];
  array[j] = temp;
}

/* *********************************************************** */

static int partitionI(int* array, int* perm, int start, int end)
{
  int k = start;

  if (perm) {
    int pivot = array[perm[start]];
    for (int i = start + 1; i <= end; i++) {
      if (array[perm[i]] < pivot) {
        k++;
        swapI(perm, k, i);
      }
    }
    swapI(perm, k, start);
  }
  else {
    int pivot = array[start];
    for (int i = start + 1; i <= end; i++) {
      if (array[i] < pivot) {
        k++;
        swapI(array, k, i);
      }
    }
    swapI(array, k, start);
  }

  return k;
}

/* *********************************************************** */

static int partitionP(int* array, int* perm, int start, int end)
{
  int k = start;

  if (perm) {
    int pivot[2] = {array[perm[start] * 2], array[perm[start] * 2 + 1]};
    for (int i = start + 1; i <= end; i++) {
      if ((array[perm[i] * 2] < pivot[0]) || (array[perm[i] * 2] == pivot[0] && array[perm[i] * 2 + 1] < pivot[1])) {
        k++;
        swapI(perm, k, i);
      }
    }
    swapI(perm, k, start);
  }
  else {
    int pivot[2] = {array[start * 2], array[start * 2 + 1]};
    for (int i = start + 1; i <= end; i++) {
      if ((array[i * 2] < pivot[0]) || (array[i * 2] == pivot[0] && array[i * 2 + 1] < pivot[1])) {
        k++;
        swapP(array, k, i);
      }
    }
    swapP(array, k, start);
  }

  return k;
}

/* *********************************************************** */

static int partitionD(double* array, int* perm, int start, int end)
{
  int k = start;
  if (perm) {
    double pivot = array[perm[start]];
    for (int i = start + 1; i <= end; i++) {
      if (array[perm[i]] < pivot) {
        k++;
        swapI(perm, k, i);
      }
    }
    swapI(perm, k, start);
  }
  else {
    double pivot = array[start];
    for (int i = start + 1; i <= end; i++) {
      if (array[i] < pivot) {
        k++;
        swapD(array, k, i);
      }
    }
    swapD(array, k, start);
  }

  return k;
}

/* *********************************************************** */

static void quicksortI(int* array, int* perm, int start, int end)
{
  if (start < end) {
    int pivot = partitionI(array, perm, start, end);
    quicksortI(array, perm, start, pivot - 1);
    quicksortI(array, perm, pivot + 1, end);
  }
}

/* *********************************************************** */

static void quicksortP(int* array, int* perm, int start, int end)
{
  if (start < end) {
    int pivot = partitionP(array, perm, start, end);
    quicksortP(array, perm, start, pivot - 1);
    quicksortP(array, perm, pivot + 1, end);
  }
}

/* *********************************************************** */

static void quicksortD(double* array, int* perm, int start, int end)
{
  if (start < end) {
    int pivot = partitionD(array, perm, start, end);
    quicksortD(array, perm, start, pivot - 1);
    quicksortD(array, perm, pivot + 1, end);
  }
}

/* *********************************************************** */

void LibGraph_sortI(int size,   /* [in] array size */
                    int* array, /* [in] array of integer */
                    int* perm   /* [out] permutation array */
)
{
  if (perm)
    for (int i = 0; i < size; i++) perm[i] = i;
  quicksortI(array, perm, 0, size - 1);
}

/* *********************************************************** */

void LibGraph_sortP(int size, int* array, int* perm)
{
  if (perm)
    for (int i = 0; i < size; i++) perm[i] = i;
  quicksortP(array, perm, 0, size - 1);
}

/* *********************************************************** */

void LibGraph_sortD(int size,      /* [in] array size */
                    double* array, /* [in] array of double */
                    int* perm      /* [out] permutation array */
)
{
  if (perm)
    for (int i = 0; i < size; i++) perm[i] = i;
  quicksortD(array, perm, 0, size - 1);
}

/* *********************************************************** */

void LibGraph_permuteD(int size,      /**< [in] array size */
                       double* array, /**< [inout] array to permute */
                       int* perm      /**< [in] permutation array */
)
{
  double* tmp = malloc(size * sizeof(double));
  memcpy(tmp, array, size * sizeof(double));
  for (int i = 0; i < size; i++) array[i] = tmp[perm[i]];
  free(tmp);
}

/* *********************************************************** */

void LibGraph_permuteI(int size,   /**< [in] array size */
                       int* array, /**< [inout] array to permute */
                       int* perm   /**< [in] permutation array  */
)
{
  int* tmp = malloc(size * sizeof(int));
  memcpy(tmp, array, size * sizeof(int));
  for (int i = 0; i < size; i++) array[i] = tmp[perm[i]];
  free(tmp);
}

/* *********************************************************** */

void LibGraph_permuteP(int size,   /**< [in] array size */
                       int* array, /**< [inout] array to permute */
                       int* perm   /**< [in] permutation array */
)
{
  int* tmp = malloc(2 * size * sizeof(int));
  memcpy(tmp, array, 2 * size * sizeof(int));
  for (int i = 0; i < size; i++) {
    array[2 * i] = tmp[2 * perm[i]];
    array[2 * i + 1] = tmp[2 * perm[i] + 1];
  }
  free(tmp);
}

/* *********************************************************** */
