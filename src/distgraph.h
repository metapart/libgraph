#ifndef LIBGRAPH_DISTGRAPH_H
#define LIBGRAPH_DISTGRAPH_H

//@{

/** Distributed Graph structure (based on CSR). */
typedef struct _LibGraph_DistGraph
{
  int prank;    /**< processor rank */
  int psize;    /**< nb processors */
  int* vtxdist; /**< array of vertex distribution (of size psize+1) */
  int nvtxs;    /**< nb local vertices */
  int narcs;    /**< nb local arcs */
  int* xadj;    /**< local array of indirection on adjncy (of size nvtxs+1) */
  int* adjncy;  /**< local adjacency array (of size narcs) */
  int* vwgts;   /**< local array vertex weights (of size nvtxs) */
  int* ewgts;   /**< local array vertex edges (of size narcs) */
} LibGraph_DistGraph;


/** Allocate new distributed graph. */
LibGraph_DistGraph* LibGraph_newDistGraph(void);

/** Duplicate a graph. */
void LibGraph_dupDistGraph(LibGraph_DistGraph* oldg, LibGraph_DistGraph* newg);

void LibGraph_freeDistGraph(LibGraph_DistGraph* dg);

void LibGraph_printDistGraph(LibGraph_DistGraph* dg);

//@}

#endif
