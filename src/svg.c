/* SVG routines */

#include <assert.h>
#include <limits.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
// #define _GNU_SOURCE
// #include <fenv.h>

#include "svg.h"
#include "tools.h"

/* *********************************************************** */

#define HE_EXPAND
#define HE_EXPAND_BEZIER
#define HYPEREDGE_ALPHA 0.2
#define BOXSIZE 512
#define NODESIZE 15
#define LINESIZE 2
// #define EDGEWGTFONTSIZE 24
// #define VERTEXWGTFONTSIZE 24
// #define VERTEXFONTSIZE 24
#define EDGEWGTFONTSIZE 10
#define VERTEXWGTFONTSIZE 10
#define VERTEXTAGFONTSIZE 16
#define EDGETAGPOS 0.5  // use 0.5 to center edge tag

/* *********************************************************** */

static void getColor(int p, int n, char* color)
{
  double h = p * 4.0 / (n - 1);
  if (n == 1) h = 0.0;
  double x = 1.0 - fabs(fmod(h, 2.0) - 1.0);
  double c = 0.9;
  double r, g, b;
  if (h < 1.0) {
    r = c;
    g = c * x;
    b = 0.0;
  }
  else if (h < 2.0) {
    r = c * x;
    g = c;
    b = 0.0;
  }
  else if (h < 3.0) {
    r = 0.0;
    g = c;
    b = c * x;
  }
  else {
    r = 0.0;
    g = c * x;
    b = c;
  }
  sprintf(color, "rgb(%d, %d, %d)", (int)(255 * r + 0.5), (int)(255 * g + 0.5), (int)(255 * b + 0.5));
}

/* *********************************************************** */

void LibGraph_svgBegin(FILE* file)
{
  fprintf(file, "<?xml version=\"1.0\" encoding=\"utf-8\" ?> \n");
  fprintf(file, "<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"100%%\" height=\"100%%\" > \n");
}

/* *********************************************************** */

static void defineArrowMarker(FILE* file, const char* color)
{
  /* define marker to draw oriented edges as arrow (triangle)*/
  fprintf(file, "<defs> \n");
  fprintf(file,
          "<marker id=\"arrow\" refX=\"%d\" refY=\"4\" markerUnits=\"strokeWidth\" "
          "markerWidth=\"10\" markerHeight=\"10\" orient=\"auto\"> \n",
          2 * NODESIZE);
  // fprintf (file,"  <path d=\"M 0 0 L 10 5 L 0 10 z\"/> \n");  // full triangle
  fprintf(file, "  <path d=\"M 0 0 4 4 0 8\" style=\"fill: none; stroke: %s;\"/> \n", color);  // simple arrow
  fprintf(file, "</marker>\n");
  fprintf(file, "</defs> \n");
}

/* *********************************************************** */

void LibGraph_svgEnd(FILE* file)
{
  fprintf(file, "</svg>\n");
}

/* *********************************************************** */

static double minimumDistance(int n, double coords[][2])
{
  double min = +INFINITY;
  for (int i = 0; i < n; i++)
    for (int j = i + 1; j < n; j++) {
      double x = coords[i][0] - coords[j][0];
      double y = coords[i][1] - coords[j][1];
      double d = sqrt(x * x + y * y);
      if (d < min) min = d;
    }
  return min;
}

/* *********************************************************** */
/*                 Hyperedge drawing                           */
/* *********************************************************** */

#define PI 3.14159265358979323846
#define EPSILON 1.0e-12

struct vertex
{
  int point;
  struct vertex *prev, *next;
};

struct polygon
{
  struct vertex* first;
};

static void poly_init(struct polygon* p, struct vertex* v);
static void poly_insert_before(struct polygon* p, struct vertex* pos, struct vertex* v);
static void poly_insert_after(struct polygon* p, struct vertex* pos, struct vertex* v);
static void poly_remove(struct polygon* p, struct vertex* v);
static int poly_contains(struct polygon* p, struct vertex* v);
static int poly_inside(struct polygon* p, int point, double coords[][2]);
static struct vertex* nearest_edge(struct polygon* p, int point, double coords[][2], double* d);
static void fix_poly(struct polygon* p, struct vertex* vtx, int n, int in_he, int he[], double coords[][2]);
static double angle(double x, double y);
static void normal(const double a[2], const double b[2], double n[2]);
static void exchange(int* a, int* b);
static void draw_poly(FILE* file, struct polygon* p, int he[], double coords[][2], double r_in, double r_out,
                      const char* color);

/* *********************************************************** */

void LibGraph_svgDrawHyperedge(FILE* file, int n, int he[], double coords[][2], double r_in, double r_out,
                               const char* color)
{
  int tab[n];
  int nhe = 0;
  int pivot = 0;
  while (pivot < n && !he[pivot]) pivot++;

  if (pivot == n) { /* No vertex in hyperedge */
    return;
  }

  tab[nhe++] = pivot;
  int ind_pivot = 0;
  for (int i = pivot + 1; i < n; i++) {
    if (he[i]) {
      if (coords[i][0] < coords[pivot][0] || (coords[i][0] == coords[pivot][0] && coords[i][1] < coords[pivot][1])) {
        pivot = i;
        ind_pivot = nhe;
      }
      tab[nhe++] = i;
    }
  }

  if (nhe == 1) { /* Only one vertex in hyperedge, draw a circle */
    fprintf(file,
            "<circle cx=\"%lg\" cy=\"%lg\" r=\"%lg\" stroke=\"%s\" fill=\"%s\" stroke-width=\"2\" "
            "fill-opacity=\"%lg\" />\n",
            coords[tab[0]][0], coords[tab[0]][1], r_in, color, color, HYPEREDGE_ALPHA);
    return;
  }

  // Sort
  struct vertex vtx[n];
  for (int i = 0; i < n; i++) {
    vtx[i].point = i;
    vtx[i].prev = NULL;
    vtx[i].next = NULL;
  }
  struct polygon poly;
  poly_init(&poly, &vtx[pivot]);
  exchange(&tab[0], &tab[ind_pivot]);
  for (int i = 1; i < nhe; i++) {
    int min = i;
    double min_angle = angle(coords[tab[i]][0] - coords[pivot][0], coords[tab[i]][1] - coords[pivot][1]);
    for (int j = i + 1; j < nhe; j++) {
      double a = angle(coords[tab[j]][0] - coords[pivot][0], coords[tab[j]][1] - coords[pivot][1]);
      if (a < min_angle) {
        min_angle = a;
        min = j;
      }
    }
    poly_insert_before(&poly, poly.first, &vtx[tab[min]]);
    exchange(&tab[i], &tab[min]);
  }

  if (nhe > 2) {
    // Convex Hull
    struct vertex* v = poly.first;
    do {
      double a1 =
          angle(coords[v->point][0] - coords[v->prev->point][0], coords[v->point][1] - coords[v->prev->point][1]);
      double a2 =
          angle(coords[v->next->point][0] - coords[v->point][0], coords[v->next->point][1] - coords[v->point][1]);
      if (fmod(2 * PI + a2 - a1, 2 * PI) > PI) {
        v = v->prev;
        poly_remove(&poly, v->next);
      }
      else
        v = v->next;
    } while (v != poly.first);
    /*draw_poly (&poly, coords, he);*/

    fix_poly(&poly, vtx, n, 0, he, coords);
  }

  // Output
  draw_poly(file, &poly, he, coords, r_in, r_out, color);
}

/* *********************************************************** */

void LibGraph_svgDrawHyperedges(FILE* file, const LibGraph_Hypergraph* hg, double coords[][2])
{
  int he[hg->nvtxs];

  double d = minimumDistance(hg->nvtxs, coords);
  double r_in_min = d / 8.0;
  double r_in_max = d / 4.0;
  double r_out_min = d / 4.0;
  double r_out_max = d / 2.0;

  char color[256];

  for (int e = 0; e < hg->nhedges; e++) {
    memset(he, 0, hg->nvtxs * sizeof(int));
    for (int i = hg->eptr[e]; i < hg->eptr[e + 1]; i++) he[hg->eind[i]] = 1;
    double r_in = (e * r_in_max + (hg->nhedges - 1 - e) * r_in_min) / (hg->nhedges - 1);
    double r_out = (e * r_out_min + (hg->nhedges - 1 - e) * r_out_max) / (hg->nhedges - 1);
    if (hg->nhedges == 1) {
      r_in = (r_in_max + r_in_min) / 2.0;
      r_out = (r_out_max + r_out_min) / 2.0;
    }
    getColor(e, hg->nhedges, color);
    LibGraph_svgDrawHyperedge(file, hg->nvtxs, he, coords, r_in, r_out, color);
  }
}

/* *********************************************************** */

static void poly_init(struct polygon* p, struct vertex* v)
{
  p->first = v;
  v->next = v->prev = v;
}

/* *********************************************************** */

static void poly_insert_before(struct polygon* p, struct vertex* pos, struct vertex* v)
{
  v->prev = pos->prev;
  v->next = pos;
  v->prev->next = v;
  v->next->prev = v;
}

/* *********************************************************** */

static void poly_insert_after(struct polygon* p, struct vertex* pos, struct vertex* v)
{
  v->prev = pos;
  v->next = pos->next;
  v->prev->next = v;
  v->next->prev = v;
}

/* *********************************************************** */

static void poly_remove(struct polygon* p, struct vertex* v)
{
  if (p->first == v) p->first = v->next;

  v->prev->next = v->next;
  v->next->prev = v->prev;
  v->prev = v->next = NULL;
}

/* *********************************************************** */

static int poly_contains(struct polygon* p, struct vertex* v)
{
  return v->next != NULL;
}

/* *********************************************************** */

static int poly_inside(struct polygon* p, int point, double coords[][2])
{
  struct vertex* v = p->first;
  double a = 0.0;
  do {
    double a1, a2;
    a1 = angle(coords[v->point][0] - coords[point][0], coords[v->point][1] - coords[point][1]);
    a2 = angle(coords[v->next->point][0] - coords[point][0], coords[v->next->point][1] - coords[point][1]);
    a += a2 - a1;
    if (a2 - a1 > PI) a -= 2 * PI;
    if (a2 - a1 < -PI) a += 2 * PI;
    v = v->next;
  } while (v != p->first);
  return fabs(a - 2 * PI) < EPSILON;
}

/* *********************************************************** */

static struct vertex* nearest_edge(struct polygon* p, int point, double coords[][2], double* d)
{
  struct vertex* v = p->first;
  struct vertex* nearest = NULL;
  double min = INFINITY;
  do {
    /* Projection H of point M (point) on edge AB (v->point and v->next->point) */
    double ab = 0.0;
    for (int i = 0; i < 2; i++)
      ab += (coords[v->next->point][i] - coords[v->point][i]) * (coords[v->next->point][i] - coords[v->point][i]);
    ab = sqrt(ab);
    double ah = 0.0;
    for (int i = 0; i < 2; i++)
      ah += (coords[v->next->point][i] - coords[v->point][i]) * (coords[point][i] - coords[v->point][i]);
    ah /= ab;

    if (ah >= 0.0 && ah <= ab) {
      double am2 = 0.0;
      for (int i = 0; i < 2; i++)
        am2 += (coords[point][i] - coords[v->point][i]) * (coords[point][i] - coords[v->point][i]);
      double hm = sqrt(am2 - ah * ah);
      if (hm < min) {
        min = hm;
        nearest = v;
      }
    }
    v = v->next;
  } while (v != p->first);
  if (d != NULL) *d = min;
  return nearest;
}

/* *********************************************************** */

static void fix_poly(struct polygon* p, struct vertex* vtx, int n, int in_he, int he[], double coords[][2])
{
  /*
    If in_he = 1, add vertices in the hyperedge in the polygon
    if in_he = 0, remove vertices outside the hyperedge from the polygon

    Moved vertices are inserted in the nearest edge.
    For each polygon edge, move the furthest vertex first.
  */
  double dist[n];
  int v[n];

  int need_fix = 0;
  int changed;
  do {
    for (int i = 0; i < n; i++) {
      dist[i] = 0.0;
      v[i] = -1;
    }
    changed = 0;
    for (int i = 0; i < n; i++)
      if (!poly_contains(p, &vtx[i]) && he[i] == in_he) {
        int in = poly_inside(p, i, coords);
        if ((in && !in_he) || (!in && in_he)) {
          changed = 1;
          need_fix = 1;
          double d;
          struct vertex* edge = nearest_edge(p, i, coords, &d);
          if (v[edge->point] == -1 || d > dist[edge->point]) {
            v[edge->point] = i;
            dist[edge->point] = d;
          }
        }
      }
    for (int i = 0; i < n; i++)
      if (v[i] != -1) poly_insert_after(p, &vtx[i], &vtx[v[i]]);
  } while (changed);
  if (need_fix) fix_poly(p, vtx, n, 1 - in_he, he, coords);
}

/* *********************************************************** */

static double angle(double x, double y)
{
  double a = atan2(y, x);
  return a;
}

/* *********************************************************** */

static void exchange(int* a, int* b)
{
  int tmp = *a;
  *a = *b;
  *b = tmp;
}

/* *********************************************************** */

static void normal(const double a[2], const double b[2], double n[2])
{
  double x = b[0] - a[0];
  double y = b[1] - a[1];
  double norm = sqrt(x * x + y * y);
  n[0] = y / norm;
  n[1] = -x / norm;
}

/* *********************************************************** */

static void draw_poly(FILE* file, struct polygon* p, int he[], double coords[][2], double r_in, double r_out,
                      const char* color)
{
  int first_point = 1;
  double r;
  double n[2];
  struct vertex* v = p->first;
  fprintf(file, "<path stroke=\"%s\" stroke-width=\"2\" fill=\"%s\" fill-opacity=\"%lg\" d=\"", color, color,
          HYPEREDGE_ALPHA);
  do {
#ifdef HE_EXPAND
    int s;
    double dx, dy, l;
    if (he[v->point]) {
      s = +1;
      r = r_in;
    }
    else {
      s = -1;
      r = r_out;
    }
    normal(coords[v->prev->point], coords[v->point], n);
#ifdef HE_EXPAND_BEZIER
    dx = coords[v->prev->point][0] - coords[v->point][0];
    dy = coords[v->prev->point][1] - coords[v->point][1];
    l = sqrt(dx * dx + dy * dy);
    dx /= l;
    dy /= l;
    if (first_point) {
      fprintf(file, "M %lg %lg ", coords[v->point][0] + (v->next == v->prev ? r * n[0] / 4.0 : 0.0) + dx * l / 2.0,
              coords[v->point][1] + (v->next == v->prev ? r * n[1] / 4.0 : 0.0) + dy * l / 2.0);
      first_point = 0;
    }
    fprintf(file, "C %lg %lg %lg %lg %lg %lg ",
            coords[v->point][0] + (v->next == v->prev ? r * n[0] / 4.0 : 0.0) + dx * l / 2.0 - dx * l / 4.0,
            coords[v->point][1] + (v->next == v->prev ? r * n[1] / 4.0 : 0.0) + dy * l / 2.0 - dy * l / 4.0,
            coords[v->point][0] + s * r * n[0] + dx * l / 4.0, coords[v->point][1] + s * r * n[1] + dy * l / 4.0,
            coords[v->point][0] + s * r * n[0], coords[v->point][1] + s * r * n[1]);
#else
    fprintf(file, "%c %lg %lg ", (first_point ? 'M' : 'L'), coords[v->point][0] + s * r * n[0],
            coords[v->point][1] + s * r * n[1]);
    if (first_point) first_point = 0;
#endif
    normal(coords[v->point], coords[v->next->point], n);
    fprintf(file, "A %lg %lg 0 %d %d %lg %lg ", r, r, 0, (s + 1) / 2, coords[v->point][0] + s * r * n[0],
            coords[v->point][1] + s * r * n[1]);
#ifdef HE_EXPAND_BEZIER
    dx = coords[v->next->point][0] - coords[v->point][0];
    dy = coords[v->next->point][1] - coords[v->point][1];
    l = sqrt(dx * dx + dy * dy);
    dx /= l;
    dy /= l;
    fprintf(file, "C %lg %lg %lg %lg %lg %lg ", coords[v->point][0] + s * r * n[0] + dx * l / 4.0,
            coords[v->point][1] + s * r * n[1] + dy * l / 4.0,
            coords[v->point][0] + (v->next == v->prev ? r * n[0] / 4.0 : 0.0) + dx * l / 2.0 - dx * l / 4.0,
            coords[v->point][1] + (v->next == v->prev ? r * n[1] / 4.0 : 0.0) + dy * l / 2.0 - dy * l / 4.0,
            coords[v->point][0] + (v->next == v->prev ? r * n[0] / 4.0 : 0.0) + dx * l / 2.0,
            coords[v->point][1] + (v->next == v->prev ? r * n[1] / 4.0 : 0.0) + dy * l / 2.0);
#endif
#else
    fprintf(file, "%c %lg %lg ", (first_point ? 'M' : 'L'), coords[v->point][0], coords[v->point][1]);
    if (first_point) first_point = 0;
#endif
    v = v->next;
  } while (v != p->first);
  fprintf(file, "z\" />\n");
}

/* *********************************************************** */
/*                    Graph edges                              */
/* *********************************************************** */

/* oriented: 0 if not oriented edges, +1 if oriented, -1 for the opposite direction */
void LibGraph_svgDrawEdge(FILE* file, const LibGraph_Graph* g, int edge, int a, int b, double coords[][2], const char* color,
                          int tag, int oriented)
{
  char* arrow = "marker-end=\"url(#arrow)\"";

  if (oriented > 0) { /* positive oriented */
    fprintf(file,
            "<line x1=\"%lg\" y1=\"%lg\" x2=\"%lg\" y2=\"%lg\" stroke=\"%s\" stroke-width=\"%d\" "
            "%s />\n",
            coords[a][0], coords[a][1], coords[b][0], coords[b][1], color, LINESIZE, arrow);
  }
  else if (oriented < 0) { /* negative oriented */
    fprintf(file,
            "<line x1=\"%lg\" y1=\"%lg\" x2=\"%lg\" y2=\"%lg\" stroke=\"%s\" stroke-width=\"%d\" "
            "%s />\n",
            coords[b][0], coords[b][1], coords[a][0], coords[a][1], color, LINESIZE, arrow);
  }
  else { /* non-oriented */
    fprintf(file, "<line x1=\"%lg\" y1=\"%lg\" x2=\"%lg\" y2=\"%lg\" stroke=\"%s\" stroke-width=\"%d\" />\n",
            coords[a][0], coords[a][1], coords[b][0], coords[b][1], color, LINESIZE);
  }

  /* add weight tag on edges*/
  if (g->ewgts && tag)
    fprintf(file,
            "<text x=\"%lg\" y=\"%lg\" text-anchor=\"middle\" font-size=\"%d\" "
            "fill=\"red\">%d</text>\n",
            round(EDGETAGPOS * coords[a][0] + (1 - EDGETAGPOS) * coords[b][0]),
            round(EDGETAGPOS * coords[a][1] + (1 - EDGETAGPOS) * coords[b][1]) - 10, EDGEWGTFONTSIZE,
            (oriented != 0) ? abs(g->ewgts[edge]) : g->ewgts[edge]);
}

/* *********************************************************** */

void LibGraph_svgDrawEdges(FILE* file, const LibGraph_Graph* g, double coords[][2], const char* color, int tag, int oriented)
{
  if (oriented != 0) defineArrowMarker(file, color);

  for (int v = 0; v < g->nvtxs; v++)
    for (int i = g->xadj[v]; i < g->xadj[v + 1]; i++)
      if (v < g->adjncy[i]) {       /* Draw each edge only once */
        if (oriented && g->ewgts) { /* oriented */
          if (g->ewgts[i] > 0) LibGraph_svgDrawEdge(file, g, i, v, g->adjncy[i], coords, color, tag, +1);
          if (g->ewgts[i] < 0) LibGraph_svgDrawEdge(file, g, i, v, g->adjncy[i], coords, color, tag, -1);
          // if(g->ewgts[i] == 0) drawEdge (file, g, i, v, g->adjncy[i], coords, color, tag, 0);
        }
        else /* not oriented */
          LibGraph_svgDrawEdge(file, g, i, v, g->adjncy[i], coords, color, tag, 0);
      }
}

/* *********************************************************** */
/*                        Vertices                             */
/* *********************************************************** */

void LibGraph_svgDrawVertex(FILE* file, int v, double coords[][2], double r, const char* color, int* vwgts)
{
  fprintf(file, "<circle cx=\"%lg\" cy=\"%lg\" r=\"%lg\" stroke=\"black\" fill=\"%s\" />\n", coords[v][0], coords[v][1],
          r, color);
  fprintf(file,
          "<text x=\"%lg\" y=\"%lg\" height=\"%lg\" width=\"%lg\" stroke=\"none\" fill=\"black\" "
          "style=\"font-size: %dpx; text-anchor: middle\">%d</text>\n",
          coords[v][0],                          // x
          coords[v][1] + VERTEXTAGFONTSIZE / 3,  // y
          2 * r, 2 * r,                          // height, width
          VERTEXTAGFONTSIZE,                     // font size
          v);

  if (vwgts)
    fprintf(file, "<text x=\"%lg\" y=\"%lg\" style=\"font-size: %dpx\">%d</text>\n", coords[v][0] + r + 1,
            coords[v][1] + r + 1, VERTEXWGTFONTSIZE, vwgts[v]);
}

/* *********************************************************** */

void LibGraph_svgDrawVertices(FILE* file, int n, double coords[][2], int* vwgts)
{
  // double d = minimumDistance (n, coords);
  // double r = d/12.0;
  double r = NODESIZE;
  char color[256];

  for (int i = 0; i < n; i++) {
    getColor(i, n, color);
    LibGraph_svgDrawVertex(file, i, coords, r, color, vwgts);
  }
}

/* *********************************************************** */

void LibGraph_resizeCoords(int n, double coords[][2], int boxsize, int invertyaxis)
{
  double xmin = INFINITY, xmax = -INFINITY, ymin = INFINITY, ymax = -INFINITY;

  for (int i = 0; i < n; i++) {
    if (coords[i][0] < xmin) xmin = coords[i][0];
    if (coords[i][0] > xmax) xmax = coords[i][0];
    if (coords[i][1] < ymin) ymin = coords[i][1];
    if (coords[i][1] > ymax) ymax = coords[i][1];
  }

  double d = minimumDistance(n, coords);
  double margin = d / 3.5;

  xmin -= margin;
  xmax += margin;
  ymin -= margin;
  ymax += margin;

  // update coords
  for (int i = 0; i < n; i++) {
    coords[i][0] = (coords[i][0] - xmin) / (xmax - xmin) * boxsize;
    if (invertyaxis)
      coords[i][1] = (ymax - coords[i][1]) / (ymax - ymin) * boxsize;
    else
      coords[i][1] = (coords[i][1] - ymin) / (ymax - ymin) * boxsize;
  }
}

/* *********************************************************** */

void LibGraph_initCoords(int n, double coords[][2])
{
  for (int i = 0; i < n; i++) {
    double theta = i * 2 * 3.14 / n;
    coords[i][0] = cos(theta);
    coords[i][1] = sin(2 * 3.14 - theta);
  }

  // resize in box
  LibGraph_resizeCoords(n, coords, BOXSIZE, 0);
}

/* *********************************************************** */

void LibGraph_initCoordsFromMesh(int n, LibGraph_Mesh* m, int use_cells, int* part, double coords[][2])
{
  int nvtxs = (use_cells ? m->nb_cells : m->nb_nodes);
  int partvtxs[n];

  for (int i = 0; i < n; i++) {
    coords[i][0] = 0.0;
    coords[i][1] = 0.0;
    partvtxs[i] = 0;
  }
  for (int i = 0; i < nvtxs; i++) {
    double c[3];
    if (use_cells)
      LibGraph_cellCenter(m, i, c);
    else /* use nodes */
      for (int j = 0; j < 3; j++) c[j] = m->nodes[3 * i + j];
    coords[part[i]][0] += c[0];
    coords[part[i]][1] += c[1];
    partvtxs[part[i]]++;
  }

  for (int i = 0; i < n; i++) {
    coords[i][0] /= partvtxs[i];
    coords[i][1] /= partvtxs[i];
  }

  // resize in box
  LibGraph_resizeCoords(n, coords, BOXSIZE, 1);
}

/* *********************************************************** */

void LibGraph_svgDrawQuotientAndHypergraph(FILE* file, LibGraph_Graph* quotient, LibGraph_Hypergraph* hg,
                                           LibGraph_Mesh* m, int use_cells, int* part)
{
  int n = quotient->nvtxs;
  double coords[n][2];
  LibGraph_initCoordsFromMesh(n, m, use_cells, part, coords);
  LibGraph_svgDrawEdges(file, quotient, coords, "black", 1, 0);  // tag=1 & oriented=0
  LibGraph_svgDrawHyperedges(file, hg, coords);
  LibGraph_svgDrawVertices(file, n, coords, hg->vwgts);
}

/* *********************************************************** */

void LibGraph_svgDrawGraph(FILE* file, const LibGraph_Graph* graph, double coords[][2], int vtag, int etag, int oriented)
{
  int n = graph->nvtxs;
  LibGraph_resizeCoords(n, coords, BOXSIZE, 0);
  LibGraph_svgDrawEdges(file, graph, coords, "black", etag, oriented);
  if (vtag)
    LibGraph_svgDrawVertices(file, n, coords, graph->vwgts);
  else
    LibGraph_svgDrawVertices(file, n, coords, NULL);
}

/* *********************************************************** */

void LibGraph_svgDrawGraphPart(FILE* file, const LibGraph_Graph* graph, double coords[][2], int vtag, int etag, int nparts,
                               const int* part)
{
  int n = graph->nvtxs;
  char color[256];
  LibGraph_resizeCoords(n, coords, BOXSIZE, 0);
  LibGraph_svgDrawEdges(file, graph, coords, "black", etag, 0);
  for (int i = 0; i < n; i++) {
    if (part[i] >= 0 && part[i] < nparts)
      getColor(part[i], nparts, color);
    else
      sprintf(color, "white");
    // printf("vertex %d: color %s (part %d)\n", i, color, part[i]);
    LibGraph_svgDrawVertex(file, i, coords, NODESIZE, color, vtag ? graph->vwgts : NULL);
  }
}

/* *********************************************************** */
