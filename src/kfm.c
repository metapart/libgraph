#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "debug.h"
#include "diag.h"
#include "graph.h"
#include "kfm.h"
#include "misc.h"
#include "refine.h"

/* *********************************************************** */

/** Chained list */
struct KFM_list
{
  struct KFM_list_element *first, *last;
};

/** Chained list element */
struct KFM_list_element
{
  struct KFM_list_element *prev, *next;
  int vtx;
};

/** Buffers used in K-way FM pass */
struct KFM_buffers
{
  int nparts;
  int degree;
  int** gain;
  struct KFM_list** buckets;
  struct KFM_list_element* vtxs;
  int* partwgts;
  int* bestpart;
  int* locked;
};

/* *********************************************************** */

/** Pointer to the bucket of gain \p gain from part \p1 to part \p p2 */
#define BUCKET(b, p1, p2, gain) (&(b)->buckets[(p1) * (b)->nparts + (p2)][(b)->degree + (gain)])

/** Pointer to the buket list element for vertex \p v and destination part \p p */
#define BUCKET_ELEMENT(b, v, p) (&(b)->vtxs[(v) * (b)->nparts + (p)])

/** Remove \p el from \p list */
static void KFM_list_remove(struct KFM_list* list, struct KFM_list_element* el);
/** Insert \p el at the beginning of \p list */
static void KFM_list_prepend(struct KFM_list* list, struct KFM_list_element* el);
/** Insert \p el at the end of \p list */
// static void KFM_list_append(struct KFM_list* list, struct KFM_list_element* el);
/** Test if \p el is in the bucket \p list */
static int KFM_list_contains(const struct KFM_list* list, const struct KFM_list_element* el,
                             const struct KFM_buffers* b);

/** Allocate buffers for K-way FM pass */
static void KFM_alloc(struct KFM_buffers* buffers, /**< Buffer structure containing the buffers */
                      const LibGraph_Graph* g,     /**< Graph */
                      int nparts,                  /**< Number of parts in the partition to be refined */
                      int* locked                  /**< Boolean array indicating locked vertices */
);

/** Free buffers allocated with KFM_alloc */
static void KFM_free(struct KFM_buffers* buffers);

/** Compute gain for KFM_pass
 * Should be called once before multiple passes */
static void KFM_computeGain(const LibGraph_Graph* g, int* part, struct KFM_buffers* buffers);

/** Compute part weights for KFM_pass
 * Should be called once before multiple passes */
static void KFM_computeWeight(const LibGraph_Graph* g, int* part, struct KFM_buffers* buffers);

/** Perform one K-way FM pass */
static int KFM_pass(const LibGraph_Graph* g, int* part, int ubfactor, int maxnegmove, struct KFM_buffers* buffers);

/** Sort unlocked vertices in the buckets.
 * \return The maximum gain */
static int KFM_sortVertices(const LibGraph_Graph* g, const int* part, struct KFM_buffers* b);

/** Update gain of \p v and its neighbors.
 * \return The new maximum gain */
static int KFM_updateGain(const LibGraph_Graph* g, /**< Graph being partitioned. */
                          const int* part,         /**< Partition */
                          int v,                   /**< Moved vertex */
                          int p1,                  /**< Old part of \p v */
                          int p2,                  /**< New part of \p v */
                          int maxgain,             /**< Maximum gain of the old partition */
                          struct KFM_buffers* b);

/* *********************************************************** */
/*                          PRIVATE                            */
/* *********************************************************** */

static void KFM_list_remove(struct KFM_list* list, struct KFM_list_element* el)
{
  if (el->next) el->next->prev = el->prev;
  if (el->prev) el->prev->next = el->next;
  if (list->first == el) list->first = el->next;
  if (list->last == el) list->last = el->prev;
  el->prev = el->next = NULL;
}

/* *********************************************************** */

static void KFM_list_prepend(struct KFM_list* list, struct KFM_list_element* el)
{
  el->prev = NULL;
  el->next = list->first;
  if (el->next) el->next->prev = el;
  list->first = el;
  if (list->last == NULL) list->last = el;
}

/* *********************************************************** */

// static void KFM_list_append(struct KFM_list* list, struct KFM_list_element* el)
// {
//   el->prev = list->last;
//   el->next = NULL;
//   if (el->prev) el->prev->next = el;
//   if (list->first == NULL) list->first = el;
//   list->last = el;
// }

/* *********************************************************** */

static int KFM_list_contains(const struct KFM_list* list, const struct KFM_list_element* el,
                             const struct KFM_buffers* b)
{
  return !b->locked[el->vtx] && (el->prev != NULL || list->first == el);
}

/* *********************************************************** */

static void KFM_alloc(struct KFM_buffers* b, const LibGraph_Graph* g, int nparts, int* locked)
{
  /* Compute max degree */
  b->degree = 0;
  for (int v = 0; v < g->nvtxs; v++) {
    if (!locked[v]) {
      int d;
      if (!g->ewgts)
        d = g->xadj[v + 1] - g->xadj[v];
      else {
        d = 0;
        for (int i = g->xadj[v]; i < g->xadj[v + 1]; i++) { d += g->ewgts[i]; }
      }
      if (d > b->degree) b->degree = d;
    }
  }
  /*fprintf (stderr, "degree: %d\n", b->degree);*/

  /* Allocate buffers */
  b->nparts = nparts;
  b->gain = malloc(nparts * sizeof(int*));
  b->buckets = malloc(nparts * nparts * sizeof(struct KFM_list*));
  for (int i = 0; i < nparts; i++) {
    b->gain[i] = malloc(g->nvtxs * sizeof(int));
    for (int j = 0; j < nparts; j++) b->buckets[i * nparts + j] = malloc((2 * b->degree + 1) * sizeof(struct KFM_list));
  }
  b->vtxs = malloc(g->nvtxs * nparts * sizeof(struct KFM_list_element));
  b->partwgts = malloc(nparts * sizeof(int));
  b->bestpart = malloc(g->nvtxs * sizeof(int));
  b->locked = malloc(g->nvtxs * sizeof(int));
  memcpy(b->locked, locked, g->nvtxs * sizeof(int));

  /* Init list elements */
  for (int i = 0; i < g->nvtxs; i++)
    for (int j = 0; j < nparts; j++) {
      b->vtxs[i * nparts + j].prev = NULL;
      b->vtxs[i * nparts + j].next = NULL;
      b->vtxs[i * nparts + j].vtx = i;
    }
}

/* *********************************************************** */

static void KFM_free(struct KFM_buffers* b)
{
  free(b->locked);
  free(b->bestpart);
  free(b->partwgts);
  free(b->vtxs);
  for (int i = 0; i < b->nparts; i++) {
    free(b->gain[i]);
    for (int j = 0; j < b->nparts; j++) free(b->buckets[i * b->nparts + j]);
  }
  free(b->gain);
  free(b->buckets);
}

/* *********************************************************** */

static void KFM_computeGain(const LibGraph_Graph* g, int* part, struct KFM_buffers* b)
{
  /* Compute gain */
  for (int i = 0; i < b->nparts; i++) memset(b->gain[i], 0, g->nvtxs * sizeof(int));

  int* n = malloc(b->nparts * sizeof(int));

  for (int v = 0; v < g->nvtxs; v++) {
    memset(n, 0, b->nparts * sizeof(int));
    for (int i = g->xadj[v]; i < g->xadj[v + 1]; i++) n[part[g->adjncy[i]]] += (g->ewgts ? g->ewgts[i] : 1);

    for (int i = 0; i < b->nparts; i++) b->gain[i][v] -= n[part[v]];
    for (int i = 0; i < b->nparts; i++) b->gain[i][v] += n[i];
  }

  free(n);
}

/* *********************************************************** */

static void KFM_computeWeight(const LibGraph_Graph* g, int* part, struct KFM_buffers* b)
{
  memset(b->partwgts, 0, b->nparts * sizeof(int));
  for (int v = 0; v < g->nvtxs; v++) b->partwgts[part[v]] += (g->vwgts ? g->vwgts[v] : 1);
}

/* *********************************************************** */

static int KFM_pass(const LibGraph_Graph* g, int* part, int ub, int maxmove, struct KFM_buffers* b)
{
  KFM_computeGain(g, part, b);
  KFM_computeWeight(g, part, b);
  int totalwgt = 0;
  for (int i = 0; i < b->nparts; i++) { totalwgt += b->partwgts[i]; }
  // fprintf (stderr, "========== new pass==========\n");

  int bestgain = 0, gain = 0;
  memcpy(b->bestpart, part, g->nvtxs * sizeof(int));

  int maxgain = KFM_sortVertices(g, part, b);
  /*fprintf (stderr, "Maxgain : %d\n", maxgain);*/

  int* order1 = malloc(b->nparts * sizeof(int));
  int* order2 = malloc(b->nparts * sizeof(int));

  int unbalanced = 0;
  int negmove = 0;
  double ubfactor = 1.0 + ub / 100.0;

  while (maxmove == -1 || negmove < maxmove) {
    /* Test load balancing */
    /* If the partition is unbalanced, negative gain is accepted until it is balanced */
    if (unbalanced) {
      unbalanced = 0;
      for (int i = 0; i < b->nparts; i++)
        if (b->partwgts[i] > ubfactor * totalwgt / b->nparts) unbalanced = 1;
    }

    /* Find largest part */
    // int pmax = -1;
    int w = 0;
    for (int i = 0; i < b->nparts; i++) {
      if (b->partwgts[i] > w) {
        w = b->partwgts[i];
        // pmax = i;
      }
    }

    /* Find best move */
    /*int v = -1;
      int p2 = -1;
      for (int g = b->degree; g >= -b->degree; g--) {
      for (int i = 0; i < b->nparts; i++)
      if (i != p)
      if (b->buckets[p*b->nparts + i][b->degree+g] != NULL) {
      v = b->buckets[p*b->nparts + i][b->degree+g]->vtx;
      p2 = i;
      assert (b->gain[p2][v] == g);
      goto found;
      }
      }*/

    LibGraph_generateRandomOrder(order1, b->nparts);
    LibGraph_generateRandomOrder(order2, b->nparts);

    /* Find best move which improves balance or keeps it */
    int p, p2, v;
    for (int i = maxgain; i >= -b->degree; i--) {
      for (p = 0; p < b->nparts; p++)
        for (p2 = 0; p2 < b->nparts; p2++) {
          struct KFM_list* bucket = BUCKET(b, order1[p], order2[p2], i);
          /*if (b->partwgts[p] > b->partwgts[p2])*/
          if (bucket->first != NULL && order1[p] != order2[p2] &&
              (b->partwgts[order2[p2]] + (g->vwgts ? g->vwgts[bucket->first->vtx] : 1) <
                   ubfactor * totalwgt / b->nparts ||
               b->partwgts[order2[p2]] + (g->vwgts ? g->vwgts[bucket->first->vtx] : 1) <
                   b->partwgts
                       [order1[p]] /*b->partwgts[p2]+(g->vwgts?g->vwgts[bucket->first->vtx]:1) < b->partwgts[pmax]*/)) {
            p = order1[p];
            p2 = order2[p2];
            v = bucket->first->vtx;
            assert(b->gain[p2][v] == i);
            goto found;
          }
        }
    }

    break; /* No more move */

  found:

    /* Move vertex */
    gain += b->gain[p2][v];
    // fprintf (stderr, "currentgain %d\n", gain);
    assert(part[v] == p);
    part[v] = p2;
    b->partwgts[p] -= (g->vwgts ? g->vwgts[v] : 1);
    b->partwgts[p2] += (g->vwgts ? g->vwgts[v] : 1);

    if (gain > bestgain || unbalanced) {
      bestgain = gain;
      memcpy(b->bestpart, part, g->nvtxs * sizeof(int));
      negmove = 0;
    }
    else
      negmove++;

    /* Remove from buckets */
    for (int i = 0; i < b->nparts; i++)
      if (i != p) {
        struct KFM_list_element* el = BUCKET_ELEMENT(b, v, i);
        struct KFM_list* bucket = BUCKET(b, p, i, b->gain[i][v]);
        KFM_list_remove(bucket, el);
      }

    maxgain = KFM_updateGain(g, part, v, p, p2, maxgain, b);
  }

  memcpy(part, b->bestpart, g->nvtxs * sizeof(int));
  /*fprintf (stderr, "Pass gain : %d\n", bestgain);*/

  free(order1);
  free(order2);

  return bestgain;
}

/* *********************************************************** */

static int KFM_updateGain(const LibGraph_Graph* g, const int* part, int v, int p1, int p2, int maxgain,
                          struct KFM_buffers* b)
{
  /* Update own gain */
  for (int i = 0; i < b->nparts; i++)
    if (i != p2) b->gain[i][v] -= b->gain[p2][v];
  b->gain[p2][v] = 0;

  /* Update neighbors gain */
  for (int i = g->xadj[v]; i < g->xadj[v + 1]; i++) {
    int v2 = g->adjncy[i];
    assert(v != v2);

    /* New gain for each part */
    for (int j = 0; j < b->nparts; j++) {
      if (j != part[v2]) {
        int oldgain = b->gain[j][v2];

        /* New gain */
        if (part[v2] == p1) {
          if (j == p2)
            b->gain[j][v2] += 2 * (g->ewgts ? g->ewgts[i] : 1);
          else if (j != p1)
            b->gain[j][v2] += (g->ewgts ? g->ewgts[i] : 1);
        }
        else if (part[v2] == p2) {
          if (j == p1)
            b->gain[j][v2] -= 2 * (g->ewgts ? g->ewgts[i] : 1);
          else if (j != p2)
            b->gain[j][v2] -= (g->ewgts ? g->ewgts[i] : 1);
        }
        else {
          if (j == p1)
            b->gain[j][v2] -= (g->ewgts ? g->ewgts[i] : 1);
          else if (j == p2)
            b->gain[j][v2] += (g->ewgts ? g->ewgts[i] : 1);
        }
        // fprintf (stderr, "%d %d->%d: %d/%d\n", v2, part[v2], j, b->gain[j][v2], b->degree);
        int newgain = b->gain[j][v2];
        assert(b->locked[v2] || (newgain >= -b->degree && newgain <= b->degree));

        if (!b->locked[v2] && newgain > maxgain) maxgain = newgain;

        struct KFM_list_element* el = BUCKET_ELEMENT(b, v2, j);
        struct KFM_list* bucket = BUCKET(b, part[v2], j, oldgain);
        if (KFM_list_contains(bucket, el, b) && newgain != oldgain) {
          /* Remove from bucket */
          KFM_list_remove(bucket, el);

          /* Add in new bucket */
          bucket = BUCKET(b, part[v2], j, newgain);
          KFM_list_prepend(bucket, el);
        }
      }
    }
  }
  return maxgain;
}

/* *********************************************************** */

static int KFM_sortVertices(const LibGraph_Graph* g, const int* part, struct KFM_buffers* b)
{
  /* Generate random order */
  int* order = malloc(g->nvtxs * sizeof(int));
  LibGraph_generateRandomOrder(order, g->nvtxs);

  /* Sort vertices */
  for (int i = 0; i < b->nparts; i++)
    for (int j = 0; j < b->nparts; j++)
      memset(b->buckets[i * b->nparts + j], 0, (2 * b->degree + 1) * sizeof(struct KFM_list));

  int maxgain = -b->degree;
  for (int u = 0; u < g->nvtxs; u++) {
    int v = order[u];
    if (!b->locked[v])
      for (int i = 0; i < b->nparts; i++) {
        if (b->gain[i][v] > maxgain) maxgain = b->gain[i][v];
        struct KFM_list_element* vtx = BUCKET_ELEMENT(b, v, i);
        struct KFM_list* bucket = BUCKET(b, part[v], i, b->gain[i][v]);
        KFM_list_prepend(bucket, vtx);
      }
  }
  free(order);
  return maxgain;
}

/* *********************************************************** */
/*                          PUBLIC                             */
/* *********************************************************** */

int LibGraph_refineKFM(const LibGraph_Graph* g, int nparts, int ubfactor, int* part, int* locked, int npass,
                       int maxnegmove)
{
  int edgecut_initial = LibGraph_computeGraphEdgeCut(g, nparts, part);
  int kfmgain = 0;
  int pass = 0;

  int* _locked = locked;
  if (!locked) _locked = calloc(g->nvtxs, sizeof(int));

  struct KFM_buffers buffers;
  KFM_alloc(&buffers, g, nparts, _locked);

  if (npass == -1) { /* infinite nb passes */
    int oldkfmgain = -1;
    int newkfmgain = -1;
    do {
      oldkfmgain = newkfmgain;
      newkfmgain = KFM_pass(g, part, ubfactor, maxnegmove, &buffers);
      // PRINT("pass %d => gain %d\n", pass, newkfmgain);
      pass++;
      kfmgain += newkfmgain;
    } while (oldkfmgain > 0 || newkfmgain > 0);
  }
  else {
    for (int i = 0; i < npass; i++) {
      kfmgain += KFM_pass(g, part, ubfactor, maxnegmove, &buffers);
      pass++;
    }
  }

  KFM_free(&buffers);
  if (!locked) free(_locked);

  int edgecut_final = LibGraph_computeGraphEdgeCut(g, nparts, part);

  assert(kfmgain == edgecut_initial - edgecut_final);  // check

  PRINT("refinement performs %d passes for %d gain\n", pass, kfmgain);

  return kfmgain;
}

/* *********************************************************** */

int LibGraph_refineBandKFM(const LibGraph_Graph* g, int nparts, int ubfactor, int* part, int* locked, int npass,
                           int maxnegmove, int width)
{
  assert(g);
  assert(width > 0);

  int* _locked = locked;
  if (!locked) _locked = calloc(g->nvtxs, sizeof(int));

  int edgecut_initial = LibGraph_computeGraphEdgeCut(g, nparts, part);

  /* 1) compute band graph, band part, ... */
  LibGraph_Graph band;
  int* frontier = malloc(g->nvtxs * sizeof(int));
  int band_nvtxs = LibGraph_createBandGraph(g, nparts, part, _locked, &band, width, frontier);
  PRINT("%.2f%% vertices in band graph (width %d)\n", band_nvtxs * 100.0 / g->nvtxs, width);

  int* band_vtxs = calloc(band_nvtxs, sizeof(int));
  for (int i = 0, k = 0; i < g->nvtxs; i++)
    if (frontier[i] > 0) band_vtxs[k++] = i;

  free(frontier);

  int* band_part = calloc(band.nvtxs, sizeof(int));
  int* band_locked = calloc(band.nvtxs, sizeof(int));

  /* regular vertices */
  for (int i = 0; i < band_nvtxs; i++) {
    band_part[i] = part[band_vtxs[i]];
    band_locked[i] = _locked[band_vtxs[i]];
  }
  /* super nodes */
  for (int i = 0; i < nparts; i++) {
    band_part[band_nvtxs + i] = i;
    band_locked[band_nvtxs + i] = 1;
  }

  // debug
  // printGraph(&band);

  /* 2) KFM refinement on band graph */
  int gain = LibGraph_refineKFM(&band, nparts, ubfactor, band_part, band_locked, npass, maxnegmove);

  /* 3) projection of band graph partition on initial graph */
  for (int i = 0; i < band_nvtxs; i++) part[band_vtxs[i]] = band_part[i];

  /* free memory */
  LibGraph_freeGraph(&band);
  free(band_vtxs);
  free(band_part);
  free(band_locked);
  if (!locked) free(_locked);

  int edgecut_final = LibGraph_computeGraphEdgeCut(g, nparts, part);
  assert(gain == edgecut_initial - edgecut_final);  // check

  return gain;
}

/* *********************************************************** */
