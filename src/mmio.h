/*
 *   Matrix Market I/O library for ANSI C
 *
 *   See http://math.nist.gov/MatrixMarket for details.
 *
 *
 */

#ifndef LIBGRAPH_MMIO_H
#define LIBGRAPH_MMIO_H

#include "graph.h"

/** Load graph (Matrix market sparse format). */
int LibGraph_loadMMGraph(const char* input, /**< [in] input filename */
                         LibGraph_Graph* g  /**< [out] Graph struct */
);

/** Load graph coordinates (Matrix market dense format). */
int LibGraph_loadMMCoordinates(const char* infile, int n, double* coords);

#endif
