#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
// #include <getopt.h>

#include "mesh.h"

#ifdef LIBGRAPH_USE_HDF5

#include <hdf5.h>
#include "hdfreader.h"

#define NB_COOR 3
#define MAX_DATANAME_LENGTH 100
#define FALSE 0
#define NB_GROUPS_TO_OPEN 2

/* *********************************************************** */

herr_t LibGraph_hdfOpenFile(const char* filename, hid_t* fileid)
{
  *fileid = H5Fopen(filename, H5F_ACC_RDWR, H5P_DEFAULT);
  if (*fileid >= 0)
    return 1;
  else {
    printf("HDF: Unable to open file %s\n", filename);
    return 0;
  }
}

/* *********************************************************** */

static herr_t hdf_open_group(hid_t fileid, const char* groupname, hid_t* groupid)
{
  *groupid = H5Gopen1(fileid, groupname);
  if (*groupid >= 0)
    return 1;
  else {
    printf("HDF: Unable to open group %s\n", groupname);
    return 0;
  }
}

/* *********************************************************** */

static herr_t hdf_open_data(hid_t groupid, const char* dataname, hid_t* dataid)
{
  *dataid = H5Dopen1(groupid, dataname);
  if (*dataid >= 0)
    return 1;
  else {
    printf("HDF: Unable to open data %s\n", dataname);
    return 0;
  }
}

/* *********************************************************** */

static herr_t file_info(hid_t loc_id, const char* name, void* opdata)
{
  H5G_stat_t statbuf;
  /* Get type and name of the object in root level (keys) of h5file.*/
  H5Gget_objinfo(loc_id, name, FALSE, &statbuf);
  switch (statbuf.type) {
    case H5G_GROUP:
      printf(" Object with name %s is a group \n", name);
      if (!strcmp(opdata, name)) return 1;
      break;
    case H5G_DATASET:
      if (!strcmp(opdata, name)) return 1;
      printf(" Object with name %s is a dataset \n", name);
      break;
    case H5G_TYPE:
      if (!strcmp(opdata, name)) return 1;
      printf(" Object with name %s is a named datatype \n", name);
      break;
    default: printf(" Unable to identify an object "); return 1;
  }
  return 0;
}

/* *********************************************************** */

herr_t LibGraph_hdfCheckGroups(hid_t file_id, char** grouplist, int groupsize)
{
  int success;
  printf("List of objects in the root of h5 file:\n");
  for (int i = 0; i < groupsize; i++) {
    success = H5Giterate(file_id, ".", NULL, file_info, grouplist[i]);
    if (!success) {
      printf("Object %s is not found!\n", grouplist[i]);
      return 1;
    }
  }
  return 0;
}

/* *********************************************************** */

herr_t LibGraph_hdfPrintGroups(hid_t file_id, char** grouplist, int groupsize, int hybrid, char* outfile)
{
  char outfilepart[MAX_DATANAME_LENGTH];
  double* buf[NB_COOR];
  hid_t group_id = 0;
  hid_t data_id = 0;
  hid_t data_type = 0;
  hid_t data_space = 0;
  int i, rank, j, k, l;
  char* coord = malloc(MAX_DATANAME_LENGTH * sizeof(char));
  hsize_t dims_out[NB_COOR];
  FILE** ofp = NULL;
  hsize_t data_length = 0;
  int* connectbuf;
  ElementType cell_type = ELEMENT_TYPE_NBR;
  int cellsize;
  int nbfiles = 1;
  FILE* cfp = NULL;
  hsize_t NbdataIngroup;
  int cell_nb;

  /* Loop for the 2 groups in the root of h5 we want to visit */
  for (k = 0; k < groupsize; k++) {
    /* Opengroup*/
    if (!hdf_open_group(file_id, grouplist[k], &group_id)) {
      printf("Job aborted!\n");
      abort();
    }
    else
      printf("Group %s is opened\n", grouplist[k]);
    if (!strcmp(grouplist[k], "Coordinates")) {
      /* -------------------*/
      /*  Open Coordinates */
      /* -------------------*/
      H5Gget_num_objs(group_id, &NbdataIngroup);
      /* Manage all different data in each group */
      for (i = 0; i < NbdataIngroup; i++) {
        /* extract the name of data and store it in coord */
        H5Lget_name_by_idx(group_id, ".", H5_INDEX_NAME, H5_ITER_INC, i, coord, MAX_DATANAME_LENGTH, H5P_DEFAULT);
        /* open the data */
        if (!hdf_open_data(group_id, coord, &data_id)) {
          printf("Job aborted!\n");
          abort();
        }
        else
          printf("Data %s is opened...", coord);
        /* check if data is multidimensional. We dont handle this case. */
        data_type = H5Dget_type(data_id);   /* datatype handle */
        data_space = H5Dget_space(data_id); /* dataspace handle */
        rank = H5Sget_simple_extent_ndims(data_space);
        if (rank > 1) {
          printf("Dataset is multidimensional. Program will exit.");
          exit(0);
        }
        /* extract the size of data*/
        H5Sget_simple_extent_dims(data_space, &dims_out[i], NULL);
        buf[i] = malloc(dims_out[i] * sizeof(double));
        /* read the data */
        if (H5Dread(data_id, data_type, H5S_ALL, data_space, H5P_DEFAULT, buf[i]) < 0) {
          printf("Job aborted!\n");
          abort();
        }
        else
          printf(" and read!\n");
        if (i) assert(dims_out[i] == dims_out[i - 1]);
      }  // for NbdataIngroup closes
      /* We have to read all 3 coordinates first in order to store them in triples (x,y,z) */
    }  // if coordinates closes
    else if (!strcmp(grouplist[k], "Connectivity")) {
      /* -------------------*/
      /*  Open Connectivity */
      /* -------------------*/
      H5Gget_num_objs(group_id, &NbdataIngroup);
      ofp = malloc(NbdataIngroup * sizeof(FILE*));
      if (ofp == NULL) {
        fprintf(stderr, "Can't allocate pointer for file descriptors!\n");
        exit(1);
      }
      if (!hybrid) {
        nbfiles = 1;
        cfp = fopen(outfile, "w");
        if (cfp == NULL) {
          fprintf(stderr, "Can't open output file %s!\n", outfile);
          exit(1);
        }
      }
      for (i = 0; i < NbdataIngroup; i++) {
        H5Lget_name_by_idx(group_id, ".", H5_INDEX_NAME, H5_ITER_INC, i, coord, MAX_DATANAME_LENGTH, H5P_DEFAULT);
        if (!hdf_open_data(group_id, coord, &data_id)) {
          printf("Job aborted!\n");
          abort();
        }
        else
          printf("Data %s is opened...", coord);
        /*--------------------------------------------------------------*/
        /* check if data is multidimensional. We dont handle this case. */
        data_type = H5Dget_type(data_id);   /* datatype handle */
        data_space = H5Dget_space(data_id); /* dataspace handle */
        rank = H5Sget_simple_extent_ndims(data_space);
        if (rank > 1) {
          printf("Dataset is multidimensional. Program will exit.");
          exit(0);
        }
        /*--------------------------------------------------------------*/
        H5Sget_simple_extent_dims(data_space, &data_length, NULL);
        connectbuf = malloc(data_length * sizeof(int));
        /* Read the data*/
        if (H5Dread(data_id, data_type, H5S_ALL, data_space, H5P_DEFAULT, connectbuf) < 0) {
          printf("Job aborted!\n");
          abort();
        }
        else
          printf(" and read\n");
        for (j = 0; j < data_length; j++) connectbuf[j]--;
        /* If flag -h is enabled, store different cell typess in different files (part1,part2, etc)
         */
        if (hybrid == 1) {
          nbfiles = NbdataIngroup;
          if (NbdataIngroup > 1) {
            sprintf(outfilepart, "%s.part%d", outfile, i + 1);
            printf("New file %s\n", outfilepart);
          }
          else {
            printf("File is not hybrid. Try without the flag\n");
            exit(1);
          }
          ofp[i] = fopen(outfilepart, "w");
          if (ofp[i] == NULL) {
            fprintf(stderr, "Can't open output file %s!\n", outfilepart);
            exit(1);
          }
          printf("Storing connectivity to file...\n");
          /* We use the convention that the first 2 characters of each data name corresponds to the
           * cell type */
          j = 0;
          if (coord[j] == 'p' && coord[j + 1] == 'r')
            cell_type = PRISM;
          else if (coord[j] == 'p' && coord[j + 1] == 'o')
            cell_type = POINT;
          else if (coord[j] == 'l')
            cell_type = LINE;
          else if (coord[j] == 't' && coord[j + 1] == 'r')
            cell_type = TRIANGLE;
          else if (coord[j] == 'q')
            cell_type = QUADRANGLE;
          else if (coord[j] == 't' && coord[j + 1] == 'e')
            cell_type = TETRAHEDRON;
          else if (coord[j] == 'h')
            cell_type = HEXAHEDRON;
          cellsize = ELEMENT_SIZE[cell_type];
          cell_nb = data_length / cellsize;

          fprintf(ofp[i], "# Mesh Header (elm_type, nb_nodes, nb_cells, nb_node_vars, nb_cells_vars)\n");
          fprintf(ofp[i], "%d %d %d 0 0\n\n", cell_type, (int)dims_out[0], cell_nb); /* nb_nodes dims_out[0] */
          if (cell_type == PRISM) {
            fprintf(ofp[i], "# prism cells\n");
            for (j = 0; j < cell_nb; j++) {
              fprintf(ofp[i], "%d ", connectbuf[(j * cellsize) + 0]);
              fprintf(ofp[i], "%d ", connectbuf[(j * cellsize) + 3]);
              fprintf(ofp[i], "%d ", connectbuf[(j * cellsize) + 5]);
              fprintf(ofp[i], "%d ", connectbuf[(j * cellsize) + 1]);
              fprintf(ofp[i], "%d ", connectbuf[(j * cellsize) + 2]);
              fprintf(ofp[i], "%d", connectbuf[(j * cellsize) + 4]);
              fprintf(ofp[i], "\n");
            }
          }
          else {
            fprintf(ofp[i], "# %s cells\n", coord);
            for (j = 0; j < cell_nb; j++) {
              for (l = 0; l < cellsize; l++) fprintf(ofp[i], "%d ", connectbuf[(j * cellsize) + l]);
              fprintf(ofp[i], "\n");
            }
          }
        }
        else if (!hybrid) {
          printf("Storing connectivity to file...\n");
          j = 0;
          if (coord[j] == 'p' && coord[j + 1] == 'r')
            cell_type = PRISM;
          else if (coord[j] == 'p' && coord[j + 1] == 'o')
            cell_type = POINT;
          else if (coord[j] == 'l')
            cell_type = LINE;
          else if (coord[j] == 't' && coord[j + 1] == 'r')
            cell_type = TRIANGLE;
          else if (coord[j] == 'q')
            cell_type = QUADRANGLE;
          else if (coord[j] == 't' && coord[j + 1] == 'e')
            cell_type = TETRAHEDRON;
          else if (coord[j] == 'h')
            cell_type = HEXAHEDRON;
          cellsize = ELEMENT_SIZE[cell_type];
          cell_nb = data_length / cellsize;
          fprintf(cfp, "# Mesh Header (elm_type, nb_nodes, nb_cells, nb_node_vars, nb_cells_vars)\n");
          fprintf(cfp, "%d %d %d 0 0\n\n", cell_type, (int)dims_out[0], cell_nb); /* nb_nodes dims_out[0] */
          if (cell_type == PRISM) {
            fprintf(cfp, "# prism cells\n");
            for (j = 0; j < cell_nb; j++) {
              fprintf(cfp, "%d ", connectbuf[(j * cellsize) + 0]);
              fprintf(cfp, "%d ", connectbuf[(j * cellsize) + 3]);
              fprintf(cfp, "%d ", connectbuf[(j * cellsize) + 5]);
              fprintf(cfp, "%d ", connectbuf[(j * cellsize) + 1]);
              fprintf(cfp, "%d ", connectbuf[(j * cellsize) + 2]);
              fprintf(cfp, "%d", connectbuf[(j * cellsize) + 4]);
              fprintf(cfp, "\n");
            }
          }
          else {
            fprintf(cfp, "# %s cells\n", coord);
            for (j = 0; j < cell_nb; j++) {
              for (l = 0; l < cellsize; l++) fprintf(cfp, "%d ", connectbuf[(j * cellsize) + l]);
              fprintf(cfp, "\n");
            }
          }
        }
        free(connectbuf);
      }  // for NbdataIngroup close
    }    // if connectivity close
  }      // groups

  if (hybrid == 1) {
    /* Write to all files the coordinates*/
    for (l = 0; l < nbfiles; l++) {
      fprintf(ofp[l], "# all nodes (3d coordinates)\n");
      printf("Storing coordinates to file...\n");
      for (j = 0; j < (int)dims_out[0]; j++) {
        for (int m = 0; m < NB_COOR; m++) fprintf(ofp[l], "%.8f ", buf[m][j]);
        fprintf(ofp[l], "\n");
      }
    }
  }
  else {
    fprintf(cfp, "# all nodes (3d coordinates)\n");
    printf("Storing coordinates to file...\n");
    for (j = 0; j < (int)dims_out[0]; j++) {
      for (int m = 0; m < NB_COOR; m++) fprintf(cfp, "%.8f ", buf[m][j]);
      fprintf(cfp, "\n");
    }
  }

  if (hybrid == 1) {
    for (l = 0; l < nbfiles; l++) fclose(ofp[l]);
  }
  else
    fclose(cfp);
  free(coord);
  H5Tclose(data_type);
  H5Dclose(data_id);
  H5Sclose(data_space);
  H5Gclose(group_id);
  H5Fclose(file_id);

  return 0;
}

/* *********************************************************** */

#endif
