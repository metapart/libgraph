#include <assert.h>
#include <limits.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "debug.h"
#include "hybridmesh.h"

/* *********************************************************** */

LibGraph_HybridMesh* LibGraph_newHybridMesh(int nb_components)
{
  LibGraph_HybridMesh* hm = malloc(sizeof(LibGraph_HybridMesh));
  assert(hm);
  hm->nb_components = nb_components;
  hm->components = malloc(hm->nb_components * sizeof(LibGraph_Mesh));
  hm->nb_nodes = 0;
  hm->nodes = NULL;
  // hm->nb_node_vars = 0;
  // hm->node_vars = NULL;
  // hm->nb_cell_vars = 0;
  // hm->cell_vars = NULL;
  return hm;
}

/* *********************************************************** */

void LibGraph_dupHybridMesh(LibGraph_HybridMesh* oldg, LibGraph_HybridMesh* newg)
{
  assert(oldg && newg);
  newg->nb_components = oldg->nb_components;
  newg->shared_nodes = oldg->shared_nodes;
  if (!newg->components) newg->components = malloc(newg->nb_components * sizeof(LibGraph_Mesh));
  newg->nb_nodes = oldg->nb_nodes;
  LibGraph_Mesh *cm_old, *cm_newg;
  for (int j = 0; j < oldg->nb_components; j++) {
    cm_old = LibGraph_getHybridMeshComponent(oldg, j);
    cm_newg = LibGraph_getHybridMeshComponent(newg, j);
    assert(cm_newg);
    assert(cm_old);
    LibGraph_dupMesh(cm_old, cm_newg);
  }
  if (oldg->shared_nodes && oldg->nodes) {
    newg->nodes = malloc(newg->nb_nodes * 3 * sizeof(double));
    memcpy(newg->nodes, oldg->nodes, newg->nb_nodes * 3 * sizeof(double));
  }
  
  // duplicate node variables
  // newg->nb_node_vars = oldg->nb_node_vars;
  // newg->node_vars = malloc(newg->nb_node_vars * sizeof(LibGraph_Variable));
  // for (int i = 0; i < newg->nb_node_vars; i++) {
  //   strcpy(newg->node_vars[i].id, oldg->node_vars[i].id);
  //   newg->node_vars[i].nb_tuples = oldg->node_vars[i].nb_tuples;
  //   newg->node_vars[i].nb_components = oldg->node_vars[i].nb_components;
  //   newg->node_vars[i].type = oldg->node_vars[i].type;
  //   int datasize = newg->node_vars[i].nb_tuples * newg->node_vars[i].nb_components;
  //   if (newg->node_vars[i].type == LIBGRAPH_VAR_INT)
  //     datasize *= sizeof(int);
  //   else
  //     datasize *= sizeof(double);
  //   newg->node_vars[i].data = malloc(datasize);
  //   assert(newg->node_vars[i].data);
  //   memcpy(newg->node_vars[i].data, oldg->node_vars[i].data, datasize);
  // }
  
  // duplicate cell variables
  // newg->nb_cell_vars = oldg->nb_cell_vars;
  // newg->cell_vars = malloc(newg->nb_cell_vars * sizeof(LibGraph_Variable));
  // for (int i = 0; i < newg->nb_cell_vars; i++) {
  //   strcpy(newg->cell_vars[i].id, oldg->cell_vars[i].id);
  //   newg->cell_vars[i].nb_tuples = oldg->cell_vars[i].nb_tuples;
  //   newg->cell_vars[i].nb_components = oldg->cell_vars[i].nb_components;
  //   newg->cell_vars[i].type = oldg->cell_vars[i].type;
  //   int datasize = newg->cell_vars[i].nb_tuples * newg->cell_vars[i].nb_components;
  //   if (newg->cell_vars[i].type == LIBGRAPH_VAR_INT)
  //     datasize *= sizeof(int);
  //   else
  //     datasize *= sizeof(double);
  //   newg->cell_vars[i].data = malloc(datasize);
  //   assert(newg->cell_vars[i].data);
  //   memcpy(newg->cell_vars[i].data, oldg->cell_vars[i].data, datasize);
  // }
}

/* *********************************************************** */

LibGraph_Mesh* LibGraph_getHybridMeshComponent(const LibGraph_HybridMesh* hm, int i)
{
  assert(hm);
  assert(i < hm->nb_components);
  return &(hm->components[i]);
}

/* *********************************************************** */

int LibGraph_getHybridMeshNbNodes(LibGraph_HybridMesh* hm)
{
  assert(hm);
  if (hm->shared_nodes) { return hm->nb_nodes; }
  else {
    LibGraph_Mesh* m = LibGraph_getHybridMeshComponent(hm, 1);
    return m->nb_nodes;
  }
}

/* *********************************************************** */

int LibGraph_getHybridMeshNbCells(LibGraph_HybridMesh* hm)
{
  assert(hm);
  LibGraph_Mesh* m = NULL;
  int nb_cells = 0;
  for (int j = 0; j < hm->nb_components; j++) {
    m = LibGraph_getHybridMeshComponent(hm, j);
    assert(m);
    nb_cells = nb_cells + m->nb_cells;
  }
  return nb_cells;
}

/* *********************************************************** */

int LibGraph_locateMeshComponent(LibGraph_HybridMesh* hm, int cell_idx)
{
  LibGraph_Mesh* cm = NULL;
  int* cmesh_adj = calloc((hm->nb_components + 1), sizeof(int));
  for (int i = 0; i < hm->nb_components; i++) {
    cm = LibGraph_getHybridMeshComponent(hm, i);
    cmesh_adj[i + 1] = cm->nb_cells + cmesh_adj[i];
  }

  for (int i = 0; i < hm->nb_components; i++) {
    if (cmesh_adj[i] <= cell_idx && cell_idx < cmesh_adj[i + 1]) { return i; }
  }
  return 0;
}

/* *********************************************************** */

void LibGraph_freeHybridMesh(LibGraph_HybridMesh* hm)
{
  if (hm != NULL) {
    if (hm->shared_nodes && hm->nodes != NULL) { free(hm->nodes); }
    // if (hm->shared_nodes && hm->node_vars != NULL) {
    //   for (int i = 0; i < hm->nb_node_vars; i++) free(hm->node_vars[i].data);
    //   free(hm->node_vars);
    // }
    // if (hm->cell_vars != NULL) {
    //   for (int i = 0; i < hm->nb_cell_vars; i++) free(hm->cell_vars[i].data);
    //   free(hm->cell_vars);
    // }
    if (hm->components != NULL) { free(hm->components); }
    // not needed
    hm->nb_components = 0;
    // hm->nb_node_vars = 0;
    // hm->nb_cell_vars = 0;
    hm->nb_nodes = 0;
    hm->shared_nodes = 0;
  }
}

/* *********************************************************** */

void LibGraph_shiftHybridMesh(LibGraph_HybridMesh* hm, double vx, double vy, double vz)
{
  if (hm->shared_nodes && hm->nodes) {
    for (int j = 0; j < hm->nb_nodes; j++) {
      hm->nodes[j * 3 + 0] += vx;
      hm->nodes[j * 3 + 1] += vy;
      hm->nodes[j * 3 + 2] += vz;
    }
  }
  else
    for (int i = 0; i < hm->nb_components; i++) LibGraph_shiftMesh(&(hm->components[i]), vx, vy, vz);
}

/* *********************************************************** */

void LibGraph_scaleHybridMesh(LibGraph_HybridMesh* hm, double sx, double sy, double sz)
{
  if (hm->shared_nodes && hm->nodes) {
    for (int j = 0; j < hm->nb_nodes; j++) {
      hm->nodes[j * 3 + 0] *= sx;
      hm->nodes[j * 3 + 1] *= sy;
      hm->nodes[j * 3 + 2] *= sz;
    }
  }
  else
    for (int i = 0; i < hm->nb_components; i++) LibGraph_scaleMesh(&(hm->components[i]), sx, sy, sz);
}

/* *********************************************************** */

/** Print hybridMesh */
void LibGraph_printHybridMesh(LibGraph_HybridMesh* hm)
{
  printf("Hybrid Mesh\n");
  printf("- number of componenets = %d\n", hm->nb_components);
  printf("- shared nodes = %d \n", hm->shared_nodes);

  for (int i = 0; i < hm->nb_components; i++) {
    printf("- Printing Component #%d \n", i);
    LibGraph_printMesh(&(hm->components[i]));
    printf("- End of Component #%d \n", i);
  }
  if (hm->nodes) {
    int max_nodes = 100;
    int nb_nodes = (hm->nb_nodes <= max_nodes) ? hm->nb_nodes : max_nodes;
    printf("- nb nodes = %d\n", hm->nb_nodes);
    printf("- node coordinates\n");
    for (int j = 0; j < nb_nodes; j++) {
      printf("  %d -> [ ", j);
      for (int i = 0; i < 3; i++) printf("%f ", hm->nodes[j * 3 + i]);
      printf("]\n");
    }
    if (hm->nb_nodes > max_nodes) printf("  ...\n");
  }
  // if (hm->node_vars) {
  //   printf("- nb node variables = %d\n", hm->nb_node_vars);
  //   for (int i = 0; i < hm->nb_node_vars; i++) {
  //     printf("- node variable \"%s\" [", hm->node_vars[i].id);
  //     for (int j = 0; j < hm->nb_nodes; j++)
  //       if (hm->node_vars[i].type == LIBGRAPH_VAR_INT)
  //         printf("%d ", ((int*)hm->node_vars[i].data)[j]);
  //       else if (hm->node_vars[i].type == LIBGRAPH_VAR_DOUBLE)
  //         printf("%f ", ((double*)hm->node_vars[i].data)[j]);
  //     printf("]\n");
  //   }
  // }
  // if (hm->cell_vars) {
  //   printf("- nb cell variables = %d\n", hm->nb_cell_vars);
  //   for (int i = 0; i < hm->nb_cell_vars; i++) {
  //     printf("- cell variable \"%s\" [", hm->cell_vars[i].id);
  //     for (int j = 0; j < LibGraph_getHybridMeshNbCells(hm); j++)
  //       if (hm->cell_vars[i].type == LIBGRAPH_VAR_INT)
  //         printf("%d ", ((int*)hm->cell_vars[i].data)[j]);
  //       else if (hm->cell_vars[i].type == LIBGRAPH_VAR_DOUBLE)
  //         printf("%f ", ((double*)hm->cell_vars[i].data)[j]);
  //     printf("]\n");
  //   }
  // }
}

/* *********************************************************** */
