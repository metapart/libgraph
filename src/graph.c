#include <assert.h>
#include <limits.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "debug.h"
#include "graph.h"
#include "hypergraph.h"
#include "sort.h"

/* *********************************************************** */

#define MIN(a, b) (((a) <= (b)) ? (a) : (b))
#define MAX(a, b) (((a) >= (b)) ? (a) : (b))

/* *********************************************************** */

LibGraph_Graph* LibGraph_newGraph(void)
{
  LibGraph_Graph* g = malloc(sizeof(LibGraph_Graph));
  assert(g);
  g->nvtxs = 0;
  g->narcs = 0;
  g->xadj = NULL;
  g->adjncy = NULL;
  g->vwgts = NULL;
  g->ewgts = NULL;
  return g;
}

/* *********************************************************** */

void LibGraph_wrapGraph(LibGraph_Graph* g, int nvtxs, int narcs, int* xadj, int* adjncy, int* vwgts, int* ewgts)
{
  assert(g);
  g->nvtxs = nvtxs;
  g->narcs = narcs;
  g->xadj = xadj;
  g->adjncy = adjncy;
  g->vwgts = vwgts;
  g->ewgts = ewgts;
}

/* *********************************************************** */

void LibGraph_printGraph(LibGraph_Graph* g)
{
  printf("Graph\n");
  printf("- nb vertices = %d\n", g->nvtxs);
  printf("- nb arcs = %d\n", g->narcs);
  printf("- adjacency list\n");
  for (int i = 0; i < g->nvtxs; i++) {
    printf("  %d -> [ ", i);
    for (int j = g->xadj[i]; j < g->xadj[i + 1]; j++) {
      printf("%d ", g->adjncy[j]);
      if (g->ewgts) printf("(%d) ", g->ewgts[j]);
    }
    printf("]\n");
  }

  if (g->vwgts) {
    printf("- vertex weight = [ ");
    for (int i = 0; i < g->nvtxs; i++) printf("%d ", g->vwgts[i]);
    printf("]\n");
  }
}

/* *********************************************************** */

void LibGraph_freeGraph(LibGraph_Graph* g)
{
  if (g != NULL) {
    if (g->xadj) free(g->xadj);
    if (g->adjncy) free(g->adjncy);
    if (g->vwgts) free(g->vwgts);
    if (g->ewgts) free(g->ewgts);
    g->xadj = g->adjncy = g->vwgts = g->ewgts = NULL;
  }
}

/* *********************************************************** */

void LibGraph_dupGraph(LibGraph_Graph* oldg, LibGraph_Graph* newg)
{
  assert(oldg && newg);
  newg->nvtxs = oldg->nvtxs;
  newg->narcs = oldg->narcs;
  newg->xadj = malloc((newg->nvtxs + 1) * sizeof(int));
  memcpy(newg->xadj, oldg->xadj, (newg->nvtxs + 1) * sizeof(int));
  newg->adjncy = malloc(newg->narcs * sizeof(int));
  memcpy(newg->adjncy, oldg->adjncy, newg->narcs * sizeof(int));
  newg->vwgts = newg->ewgts = NULL;
  if (oldg->vwgts != NULL) {
    newg->vwgts = malloc(newg->nvtxs * sizeof(int));
    memcpy(newg->vwgts, oldg->vwgts, newg->nvtxs * sizeof(int));
  }
  if (oldg->ewgts != NULL) {
    newg->ewgts = malloc(newg->narcs * sizeof(int));
    memcpy(newg->ewgts, oldg->ewgts, newg->narcs * sizeof(int));
  }
}

/* *********************************************************** */

void LibGraph_quotientGraph(const LibGraph_Graph* input, int nparts, const int* parts, LibGraph_Graph* output)
{
  /* compute communication table */
  /* comm[snd][rcv] = edge cut between part snd and rcv */

  int* comm = calloc(nparts * nparts, sizeof(int));
  assert(comm);

  for (int i = 0; i < input->nvtxs; i++) {
    int proc_snd = parts[i];
    for (int j = input->xadj[i]; j < input->xadj[i + 1]; j++) {
      int k = input->adjncy[j]; /* edge (i,k) */
      int proc_rcv = parts[k];
      if (proc_snd != proc_rcv) comm[proc_snd * nparts + proc_rcv] += ((input->ewgts == NULL) ? 1 : input->ewgts[k]);
    }
  }

  /* compute quotient graph by using comm. table*/

  output->nvtxs = nparts;
  output->narcs = 0;
  int maxarcs = nparts * (nparts - 1);
  output->xadj = malloc((output->nvtxs + 1) * sizeof(int));
  output->adjncy = malloc(maxarcs * sizeof(int));
  output->vwgts = malloc(output->nvtxs * sizeof(int));
  output->ewgts = malloc(maxarcs * sizeof(int));

  int k = 0;
  for (int i = 0; i < nparts; i++) {
    output->xadj[i] = k;
    for (int j = 0; j < nparts; j++) {
      if (comm[i * nparts + j] > 0) {
        output->adjncy[k] = j;                   /* edge (i,j) */
        output->ewgts[k] = comm[i * nparts + j]; /* comm cost */
        k++;                                     /* one edge more */
      }
    }
  }
  output->xadj[nparts] = k; /* end */
  output->narcs = k;

  /* vertex weight */

  memset(output->vwgts, 0, output->nvtxs * sizeof(int));

  if (input->vwgts == NULL)
    for (int i = 0; i < input->nvtxs; i++) output->vwgts[parts[i]]++; /* uniform weight */
  else
    for (int i = 0; i < input->nvtxs; i++) output->vwgts[parts[i]] += input->vwgts[i];

  /* free unused memory */

  output->adjncy = realloc(output->adjncy, output->narcs * sizeof(int));
  output->ewgts = realloc(output->ewgts, output->narcs * sizeof(int));

  free(comm);
}

/* *********************************************************** */

#define RETURN(val)                                                                                                    \
  do {                                                                                                                 \
    if (exitonerror)                                                                                                   \
      assert(val);                                                                                                     \
    else                                                                                                               \
      return (val);                                                                                                    \
  } while (0)

int LibGraph_checkGraph(LibGraph_Graph* g, int exitonerror)
{
  if (!g) RETURN(0);
  if (g->nvtxs < 0) RETURN(0);
  if (g->narcs < 0) RETURN(0);
  if (!g->xadj) RETURN(0);
  if (!g->adjncy) RETURN(0);

  int narcs = 0;
  for (int i = 0; i < g->nvtxs; i++) {
    // if(g->xadj[i] >= g->xadj[i+1]) RETURN(0); // no single vertex!
    for (int j = g->xadj[i]; j < g->xadj[i + 1]; j++) {
      int ii = g->adjncy[j];                    /* edge (i, ii) */
      if (ii < 0 || ii >= g->nvtxs) RETURN(0);  // vtx index out of range
      if (i == ii) RETURN(0);                   // no loop
      narcs++;

      // check duplicated edges (if not sorted)
      for (int jj = j + 1; jj < g->xadj[i + 1]; jj++) {
        int iii = g->adjncy[jj]; /* edge (i, iii) */
        if (ii == iii) RETURN(0);
      }

      // check symmetry
      int symmetry = 0;
      for (int jj = g->xadj[ii]; jj < g->xadj[ii + 1]; jj++) {
        int iii = g->adjncy[jj]; /* edge (ii, iii) */
        if (iii == i) {
          symmetry = 1;
          break;
        }
      }
      if (!symmetry) RETURN(0);
    }
  }

  if (g->narcs != narcs) RETURN(0);           // bad checksum for edges
  if (g->xadj[g->nvtxs] != narcs) RETURN(0);  // bad xadj end or bad nb of egdes

  return 1;
}

/* *********************************************************** */

bool LibGraph_compareGraph(LibGraph_Graph *g1, LibGraph_Graph *g2)
{
  if (g1->nvtxs != g2->nvtxs) return 0;
  if (g1->narcs != g2->narcs) return 0;
  if (memcmp(g1->xadj, g2->xadj, sizeof(int[g1->nvtxs+1]))) return 0;
  if (memcmp(g1->adjncy, g2->adjncy, sizeof(int[g1->narcs]))) return 0;

  if (!g1->ewgts != !g2->ewgts) return 0;
  if (g1->ewgts && memcmp(g1->ewgts, g2->ewgts, sizeof(int[g1->narcs]))) return 0;

  if (!g1->vwgts != !g2->vwgts) return 0;
  if (g1->vwgts && memcmp(g1->vwgts, g2->vwgts, sizeof(int[g1->nvtxs]))) return 0;

  return 1;
}

/* *********************************************************** */

void LibGraph_subGraphFromSet(const LibGraph_Graph* g, int nvtxs, const int* vtxs, LibGraph_Graph* subg)
{
  int* mapping = malloc(g->nvtxs * sizeof(int));
  for (int i = 0; i < g->nvtxs; i++) mapping[i] = -1;
  for (int i = 0; i < nvtxs; i++) mapping[vtxs[i]] = i;

  subg->nvtxs = nvtxs;
  assert(subg->nvtxs >= 0 && subg->nvtxs <= g->nvtxs);
  subg->narcs = 0;
  int maxarcs = g->narcs;
  subg->xadj = malloc((subg->nvtxs + 1) * sizeof(int));
  subg->adjncy = malloc(maxarcs * sizeof(int));
  subg->vwgts = (g->vwgts) ? malloc(nvtxs * sizeof(int)) : NULL;
  subg->ewgts = (g->ewgts) ? malloc(maxarcs * sizeof(int)) : NULL;

  int vtx = 0; /* vertex index in subgraph */
  int k = 0;

  /* for all edges */
  // for(int i = 0 ; i < g->nvtxs ; i++) {
  for (int z = 0; z < nvtxs; z++) {
    int i = vtxs[z];
    subg->xadj[vtx] = k;
    assert(mapping[i] != -1);
    // if(mapping[i] == -1) continue; /* skip, next vertex in graph */
    /* for each edge (i,ii), test if the vertex ii is in subgraph */
    for (int j = g->xadj[i]; j < g->xadj[i + 1]; j++) {
      int ii = g->adjncy[j];           /* edge (i, ii) */
      if (mapping[ii] == -1) continue; /* skip, next edge in graph */
      /* else add this edge in subgraph */
      subg->adjncy[k] = mapping[ii];
      if (subg->ewgts) subg->ewgts[k] = g->ewgts[j]; /* edge weight */
      /*       printf("found edge %d: (%d,%d) in graph, (%d,%d) in subgraph\n",  */
      /* 	     k, i, ii, mapping[i], mapping[ii]);  */
      k++; /* next edge in subLibGraph_Graph*/
    }

    vtx++; /* next vertex in subgraph */
  }

  assert(subg->nvtxs == vtx);  // check

  subg->xadj[vtx] = k; /* end */
  subg->narcs = k;

  /* vertex weight in subgraph */
  if (subg->vwgts) {
    for (int i = 0; i < nvtxs; i++) subg->vwgts[i] = g->vwgts[vtxs[i]];
  }

  /* free memory unused with realloc */
  subg->adjncy = realloc(subg->adjncy, subg->narcs * sizeof(int));
  if (g->ewgts) subg->ewgts = realloc(subg->ewgts, subg->narcs * sizeof(int));

  free(mapping);
}

/* *********************************************************** */

#ifdef POUET
/* TO REMOVE */
void subGraphFromSet2(LibGraph_Graph* g, int nvtxs, int* vtxs, LibGraph_Graph* subg)
{
  int* mapping = malloc(g->nvtxs * sizeof(int));
  for (int i = 0; i < g->nvtxs; i++) mapping[i] = -1;
  for (int i = 0; i < nvtxs; i++) mapping[vtxs[i]] = i;

  subg->nvtxs = nvtxs;
  assert(subg->nvtxs >= 0 && subg->nvtxs <= g->nvtxs);
  subg->nedges = 0;
  int maxedges = g->nedges;
  subg->xadj = malloc((subg->nvtxs + 1) * sizeof(int));
  subg->adjncy = malloc(2 * maxedges * sizeof(int));
  subg->vwgts = (g->vwgts) ? malloc(nvtxs * sizeof(int)) : NULL;
  subg->ewgts = (g->ewgts) ? malloc(2 * maxedges * sizeof(int)) : NULL;

  int vtx = 0; /* vertex index in subgraph */
  int k = 0;

  /* for all edges */
  for (int i = 0; i < g->nvtxs; i++) {
    subg->xadj[vtx] = k;
    if (mapping[i] == -1) continue; /* skip, next vertex in graph */
    /* for each edge (i,ii), test if the vertex ii is in subgraph */
    for (int j = g->xadj[i]; j < g->xadj[i + 1]; j++) {
      int ii = g->adjncy[j];           /* edge (i, ii) */
      if (mapping[ii] == -1) continue; /* skip, next edge in graph */
      /* else add this edge in subgraph */
      subg->adjncy[k] = mapping[ii];
      if (subg->ewgts) subg->ewgts[k] = g->ewgts[j]; /* edge weight */
      /*       printf("found edge %d: (%d,%d) in graph, (%d,%d) in subgraph\n",  */
      /* 	     k, i, ii, mapping[i], mapping[ii]);  */
      k++; /* next edge in subLibGraph_Graph*/
    }

    vtx++; /* next vertex in subgraph */
  }

  assert(subg->nvtxs == vtx);  // check

  subg->xadj[vtx] = k; /* end */
  subg->nedges = k / 2;

  /* vertex weight in subgraph */
  if (subg->vwgts) {
    for (int i = 0; i < nvtxs; i++) subg->vwgts[i] = g->vwgts[vtxs[i]];
  }

  /* free memory unused with realloc */
  subg->adjncy = realloc(subg->adjncy, 2 * subg->nedges * sizeof(int));
  if (g->ewgts) subg->ewgts = realloc(subg->ewgts, 2 * subg->nedges * sizeof(int));

  free(mapping);
}
#endif

/* *********************************************************** */

void LibGraph_subGraphFromArray(const LibGraph_Graph* g, const int* array, LibGraph_Graph* subg)
{
  int* vtxs = malloc(g->nvtxs * sizeof(int));
  int nvtxs = 0;
  assert(vtxs);
  for (int i = 0; i < g->nvtxs; i++)
    if (array[i] > 0) vtxs[nvtxs++] = i;
  LibGraph_subGraphFromSet(g, nvtxs, vtxs, subg);
  free(vtxs);
}

/* *********************************************************** */

void LibGraph_subGraphFromRange(LibGraph_Graph* g, int vtxstart, int vtxend, LibGraph_Graph* subg)
{
  subg->nvtxs = vtxend - vtxstart + 1;
  assert(subg->nvtxs >= 0 && subg->nvtxs <= g->nvtxs);
  subg->narcs = 0;
  int maxedges = g->narcs;
  subg->xadj = malloc((subg->nvtxs + 1) * sizeof(int));
  subg->adjncy = malloc(2 * maxedges * sizeof(int));
  subg->vwgts = NULL; /* todo */
  subg->ewgts = NULL; /* todo */

  int vtx = 0; /* vertex index in subgraph */
  int k = 0;

  /* for all edges */
  for (int i = 0; i < g->nvtxs; i++) {
    subg->xadj[vtx] = k;
    if (i < vtxstart || i > vtxend) continue; /* skip, next vertex in graph */
    /* for each edge (i,ii), test if the vertex ii is in range [vtxstart,vtxend] */
    for (int j = g->xadj[i]; j < g->xadj[i + 1]; j++) {
      int ii = g->adjncy[j];                      /* edge (i, ii) */
      if (ii < vtxstart || ii > vtxend) continue; /* skip, next edge in graph */
      /* else add this edge in subgraph */
      subg->adjncy[k] = ii - vtxstart;
      /*       printf("found edge %d: (%d,%d) in graph, (%d,%d) in subgraph\n",  */
      /*       	     k, i, ii, i - vtxstart, ii - vtxstart);  */
      k++; /* next edge in subLibGraph_Graph*/
    }

    vtx++; /* next vertex in subgraph */
  }

  assert(subg->nvtxs == vtx);

  subg->xadj[vtx] = k; /* end */
  subg->narcs = k;

  /* free memory unused with realloc */
  subg->adjncy = realloc(subg->adjncy, subg->narcs * sizeof(int));
}

/* *********************************************************** */

void LibGraph_splitGraph(const LibGraph_Graph* g, const int* part, LibGraph_Graph* gLeft, LibGraph_Graph* gRight, int* map)  // global to local mapping
{
  int nvtxsLeft = 0;
  int nvtxsRight = 0;
  int* vtxsLeft = malloc(g->nvtxs * sizeof(int));   // max
  int* vtxsRight = malloc(g->nvtxs * sizeof(int));  // max
  assert(vtxsLeft && vtxsRight);

  for (int i = 0; i < g->nvtxs; i++) {
    assert(part[i] == 0 || part[i] == 1);  // check bipartition
    if (part[i] == 0) {
      vtxsLeft[nvtxsLeft] = i;
      map[i] = nvtxsLeft;
      nvtxsLeft++;
    }
    if (part[i] == 1) {
      vtxsRight[nvtxsRight] = i;
      map[i] = nvtxsRight;
      nvtxsRight++;
    }
  }

  LibGraph_subGraphFromSet(g, nvtxsLeft, vtxsLeft, gLeft);
  LibGraph_subGraphFromSet(g, nvtxsRight, vtxsRight, gRight);

  assert(gLeft->nvtxs + gRight->nvtxs == g->nvtxs);

  free(vtxsLeft);
  free(vtxsRight);
}

/* *********************************************************** */

void LibGraph_createBigraph(int nvtxsA, /**< [in] nb vertex in part A */
                            int nvtxsB, /**< [in] nb vertex in part B */
                            int* edges, /**< [in] matrix of edges of dimensions nvtxsA x nvtxsB */
                            // int edges[nvtxsA][nvtxsB],	/**< [in] matrix of edges */
                            LibGraph_Graph* g /**< [out] bipartite graph */
)
{
  g->nvtxs = nvtxsA + nvtxsB;
  g->narcs = 0;
  for (int i = 0; i < nvtxsA; i++)
    for (int j = 0; j < nvtxsB; j++)
      if (edges[i * nvtxsB + j] > 0) g->narcs++;
  g->xadj = malloc((g->nvtxs + 1) * sizeof(int));
  g->adjncy = malloc(g->narcs * sizeof(int));
  g->vwgts = g->ewgts = NULL;
  int k = 0;
  for (int i = 0; i < nvtxsA; i++) {
    g->xadj[i] = k;
    // printf("xadj[%d] = %d\n",i,k);
    for (int j = 0; j < nvtxsB; j++)
      if (edges[i * nvtxsB + j] > 0) {
        /* add k-th edge (i, nvtxsA+j) */
        g->adjncy[k] = nvtxsA + j;
        k++;
      }
  }
  for (int j = 0; j < nvtxsB; j++) {
    g->xadj[nvtxsA + j] = k;
    // printf("xadj[%d] = %d\n",nvtxsA+j,k);
    for (int i = 0; i < nvtxsA; i++)
      if (edges[i * nvtxsB + j] > 0) {
        /* add k-th edge (nvtxsA+j, i) */
        g->adjncy[k] = i;
        k++;
      }
  }
  g->xadj[nvtxsA + nvtxsB] = k; /* end */
  // printf("xadj[%d] = %d\n",nvtxsA+nvtxsB,k);
}

/* *********************************************************** */

void LibGraph_sortGraph(LibGraph_Graph* g)
{
  int maxdegree = -1;
  for (int i = 0; i < g->nvtxs; i++) {
    int degree = g->xadj[i + 1] - g->xadj[i];
    if (degree > maxdegree) maxdegree = degree;
  }

  assert(maxdegree != -1);
  int* perm = malloc(maxdegree * sizeof(int));
  int* tmp = malloc(maxdegree * sizeof(int));
  assert(perm && tmp);

  for (int i = 0; i < g->nvtxs; i++) {
    int degree = g->xadj[i + 1] - g->xadj[i];
    int offset = g->xadj[i];
    /* sort edges */
    LibGraph_sortI(degree, g->adjncy + offset, perm);

    /* apply permutation */
    for (int j = 0; j < degree; j++) tmp[j] = g->adjncy[offset + j];
    for (int j = 0; j < degree; j++) g->adjncy[offset + j] = tmp[perm[j]];

    if (g->ewgts)
      for (int j = 0; j < degree; j++) tmp[j] = g->ewgts[offset + j];
    if (g->ewgts)
      for (int j = 0; j < degree; j++) g->ewgts[offset + j] = tmp[perm[j]];
  }

  free(perm);
  free(tmp);
}

/* *********************************************************** */

#define NEXTEDGE(g, i, j)                                                                                              \
  do {                                                                                                                 \
    j++;                                                                                                               \
    if (j == g->xadj[i + 1]) i++;                                                                                      \
  } while (0)

/* *********************************************************** */

void LibGraph_mergeGraphs(LibGraph_Graph* gA,  /**< [in] input graph A */
                          LibGraph_Graph* gB,  /**< [in] input graph B */
                          int vwfA,   /**< [in] vertex weight factor for A */
                          int ewfA,   /**< [in] edge weight factor for A */
                          int vwfB,   /**< [in] vertex weight factor for B */
                          int ewfB,   /**< [in] edge weight factor for B */
                          LibGraph_Graph* newg /**< [out] output graph */
)
{
  assert(gA && gB);
  assert(newg);

  /* sort graphs */

  LibGraph_sortGraph(gA);
  LibGraph_sortGraph(gB);

  // printf("A:" ); printGraph(gA);
  // printf("B:" ); printGraph(gB);

  // merge graphs
  int nvtxs = MAX(gA->nvtxs, gB->nvtxs);
  int narcs = 0;
  int maxnarcs = gA->narcs + gB->narcs;  // max

  newg->nvtxs = nvtxs;
  newg->xadj = malloc((nvtxs + 1) * sizeof(int));
  assert(newg->xadj);
  newg->adjncy = malloc(maxnarcs * sizeof(int));
  assert(newg->adjncy);
  // newg->vwgts = newg->ewgts = NULL;
  newg->vwgts = malloc(nvtxs * sizeof(int));
  assert(newg->vwgts);
  newg->ewgts = malloc(maxnarcs * sizeof(int));
  assert(newg->ewgts);

  int iA = 0, jA = 0, iB = 0, jB = 0;
  int oldvAB = -1, jAB = 0;

  while (iA < gA->nvtxs || iB < gB->nvtxs) {
    int vA = -1;
    int vvA = -1;
    // int vwA = 0;
    int ewA = 0;
    int vB = -1;
    int vvB = -1;
    // int vwB = 0;
    int ewB = 0;
    int vAB = -1;
    int vvAB = -1;
    // int vwAB = 0;
    int ewAB = 0;

    if (iA < gA->nvtxs) {
      /* skip vertex without edges */
      int dA = gA->xadj[iA + 1] - gA->xadj[iA];
      if (dA == 0) {
        iA++;
        jA = gA->xadj[iA];
        continue;
      }
      /* edge (vA,vvA) */
      vA = iA;
      vvA = gA->adjncy[jA];
      // vwA = gA->vwgts?gA->vwgts[iA]:1;
      // vwA *= vwfA;
      ewA = gA->ewgts ? gA->ewgts[jA] : 1;
      ewA *= ewfA;
    }

    if (iB < gB->nvtxs) {
      /* skip vertex without edges */
      int dB = gB->xadj[iB + 1] - gB->xadj[iB];
      if (dB == 0) {
        iB++;
        jB = gB->xadj[iB];
        continue;
      }
      /* edge (vB,vvB) */
      vB = iB;
      vvB = gB->adjncy[jB];
      // vwB = gB->vwgts?gB->vwgts[iB]:1;
      // vwB *= vwfB;
      ewB = gB->ewgts ? gB->ewgts[jB] : 1;
      ewB *= ewfB;
    }

    // check
    // assert(iA >= 0 && iA < gA->nvtxs);
    // assert(iB >= 0 && iB < gB->nvtxs);
    // assert(jA >= gA->xadj[iA] && jA < gA->xadj[iA+1]);
    // assert(jB >= gB->xadj[iB] && jB < gB->xadj[iB+1]);

    /* next edge */
    if (vA == -1 && vB != -1) {
      NEXTEDGE(gB, iB, jB);
      vAB = vB;
      vvAB = vvB;
      ewAB = ewB;
    }
    else if (vB == -1 && vA != -1) {
      NEXTEDGE(gA, iA, jA);
      vAB = vA;
      vvAB = vvA;
      ewAB = ewA;
    }
    else if (vA < vB) {
      NEXTEDGE(gA, iA, jA);
      vAB = vA;
      vvAB = vvA;
      ewAB = ewA;
    }
    else if (vB < vA) {
      NEXTEDGE(gB, iB, jB);
      vAB = vB;
      vvAB = vvB;
      ewAB = ewB;
    }
    else if (vA == vB && vvA < vvB) {
      NEXTEDGE(gA, iA, jA);
      vAB = vA;
      vvAB = vvA;
      ewAB = ewA;
    }
    else if (vA == vB && vvA > vvB) {
      NEXTEDGE(gB, iB, jB);
      vAB = vB;
      vvAB = vvB;
      ewAB = ewB;
    }
    else if (vA == vB && vvA == vvB) {
      NEXTEDGE(gA, iA, jA);
      NEXTEDGE(gB, iB, jB);
      vAB = vA;
      vvAB = vvA;
      ewAB = ewA + ewB;
    }  // merge 2 edges
    else
      assert(0);

    // add newg edge (vAB,vvAB)
    assert(oldvAB <= vAB); /* increasing order */
    if (oldvAB != vAB) {
      newg->xadj[vAB] = jAB;
      oldvAB = vAB;
    } /* newg vtx vAB */
    newg->adjncy[jAB] = vvAB;
    newg->ewgts[jAB] = ewAB;
    jAB++;
    // printf("(%d,%d) ", vAB, vvAB);
    narcs++;
  }
  // printf("\n");

  assert(iA == gA->nvtxs && iB == gB->nvtxs);
  assert(jA == gA->narcs && jB == gB->narcs);

  newg->xadj[nvtxs] = narcs; /* end */
  newg->narcs = narcs;
  newg->adjncy = realloc(newg->adjncy, narcs * sizeof(int));  // free mem
  newg->ewgts = realloc(newg->ewgts, narcs * sizeof(int));    // free mem

  for (int i = 0; i < nvtxs; i++) {
    newg->vwgts[i] = 0;
    if (i < gA->nvtxs) newg->vwgts[i] += (gA->vwgts ? gA->vwgts[i] : 1) * vwfA;
    if (i < gB->nvtxs) newg->vwgts[i] += (gB->vwgts ? gB->vwgts[i] : 1) * vwfB;
  }

  // printf("nvtxs: %d, nedges: %d\n", nvtxs, nedges);
}

/* *********************************************************** */

void LibGraph_addGraphVertices(LibGraph_Graph* g, int nnewvtxs, int w, int endpos)
{
  assert(g);
  assert(nnewvtxs > 0);

  /* graph */

  g->xadj = realloc(g->xadj, (g->nvtxs + nnewvtxs + 1) * sizeof(int));
  assert(g->xadj);

  if (endpos) { /* end position */
    for (int i = g->nvtxs + 1; i < g->nvtxs + nnewvtxs + 1; i++) g->xadj[i] = g->xadj[g->nvtxs];
  }
  else { /* begin position */
    for (int i = g->nvtxs; i >= 0; i--) g->xadj[i + nnewvtxs] = g->xadj[i];
    for (int i = 0; i < nnewvtxs; i++) g->xadj[i] = 0;
    for (int i = 0; i < g->narcs; i++) g->adjncy[i] += nnewvtxs;
  }

  /* weight */

  if (g->vwgts) {
    g->vwgts = realloc(g->vwgts, (g->nvtxs + nnewvtxs) * sizeof(int));
    assert(g->vwgts);
    if (endpos) {
      for (int i = g->nvtxs; i < g->nvtxs + nnewvtxs; i++) g->vwgts[i] = w;
    }
    else {
      for (int i = g->nvtxs - 1; i >= 0; i--) g->vwgts[i + nnewvtxs] = g->vwgts[i];
      for (int i = 0; i < nnewvtxs; i++) g->vwgts[i] = w;
    }
  }
  else {
    g->vwgts = malloc((g->nvtxs + nnewvtxs) * sizeof(int));
    if (endpos) {
      for (int i = 0; i < g->nvtxs; i++) g->vwgts[i] = 1;
      for (int i = g->nvtxs; i < g->nvtxs + nnewvtxs; i++) g->vwgts[i] = w;
    }
    else {
      for (int i = nnewvtxs; i < g->nvtxs + nnewvtxs; i++) g->vwgts[i] = 1;
      for (int i = 0; i < nnewvtxs; i++) g->vwgts[i] = w;
    }
  }

  g->nvtxs += nnewvtxs;
}

/* *********************************************************** */

void LibGraph_permGraph(LibGraph_Graph* oldg, int* perm, LibGraph_Graph* newg)
{
  assert(oldg && newg && perm);

  // compute inverse permutation
  int* invperm = malloc(oldg->nvtxs * sizeof(int));
  for (int i = 0; i < oldg->nvtxs; i++) invperm[perm[i]] = i;

  // alloc new graph
  newg->nvtxs = oldg->nvtxs;
  newg->narcs = oldg->narcs;
  newg->xadj = malloc((newg->nvtxs + 1) * sizeof(int));
  newg->adjncy = malloc(newg->narcs * sizeof(int));
  newg->vwgts = newg->ewgts = NULL;
  assert(oldg->vwgts == NULL);
  assert(oldg->ewgts == NULL);
  // if(oldg->vwgts != NULL) newg->vwgts = malloc(newg->nvtxs*sizeof(int));
  // if(oldg->ewgts != NULL) newg->ewgts = malloc(2*newg->nedges*sizeof(int));

  int kk = 0;
  for (int ii = 0; ii < oldg->nvtxs; ii++) {
    int i = invperm[ii];
    newg->xadj[ii] = kk;
    for (int k = oldg->xadj[i]; k < oldg->xadj[i + 1]; k++) {
      int j = oldg->adjncy[k];
      int jj = perm[j]; /* perm edge (ii,jj) */
      newg->adjncy[kk] = jj;
      kk++;
    }
  }
  newg->xadj[newg->nvtxs] = kk;

  // free mem
  free(invperm);
}

/* *********************************************************** */

/** make graph out of all edges (iarray[k],jarray[k]) */
void LibGraph_makeGraph(int nvtxs, int nedges, int* iarray, int* jarray, int* ewgts, int* vwgts, LibGraph_Graph* g)
{
  assert(g && iarray && jarray);

  /* check inputs and increasing order of iarray */
  for (int k = 0; k < nedges; k++) {
    assert(iarray[k] >= 0 && iarray[k] < nvtxs);
    assert(jarray[k] >= 0 && jarray[k] < nvtxs);
  }

  for (int k = 1; k < nedges; k++) assert(iarray[k - 1] <= iarray[k]);

  /* create new graph */
  g->nvtxs = nvtxs;
  g->narcs = nedges * 2;
  g->xadj = malloc((g->nvtxs + 1) * sizeof(int));
  assert(g->xadj);
  g->adjncy = malloc(g->narcs * sizeof(int));
  assert(g->adjncy);
  g->vwgts = NULL;
  if (vwgts) {
    g->vwgts = calloc(g->nvtxs, sizeof(int));
    assert(g->vwgts);
  }
  g->ewgts = NULL;
  if (ewgts) {
    g->ewgts = calloc(g->narcs, sizeof(int));
    assert(g->ewgts);
  }

  int k = 0;  /* k-th coord (i,j) */
  int i = -1; /* i-th vtx */
  int j = 0;  /* j-th edge */
  int v = -1, vv = -1, oldv = -1, oldvv = -1;

  while (k < 2 * nedges) {
    v = iarray[k];
    vv = jarray[k];

    assert(v >= 0 && v < nvtxs);
    assert(vv >= 0 && vv < nvtxs);
    assert(oldv < v || (oldv == v && oldvv <= vv));
    assert(!(v == oldv && vv == oldvv)); /* duplicated edges */

    /* start edge with new vtx endpoint */
    if (oldv == -1) { g->xadj[++i] = 0; }
    else if (oldv != v) {
      g->xadj[++i] = j;
    }

    /* skip single vertices */
    if (i < v) continue;

    /* add j-th edge (v,vv) */
    g->adjncy[j] = vv;
    oldv = v;
    oldvv = vv;

    j++;
    k++;  // next edge & coord (i,j)
  }

  /* end */
  for (int k = i + 1; k <= nvtxs; k++) g->xadj[k] = j;
  assert(j == 2 * nedges);

  /* weighted graph */
  if (vwgts) memcpy(g->vwgts, vwgts, g->nvtxs * sizeof(int));
  if (ewgts) memcpy(g->ewgts, ewgts, g->narcs * sizeof(int));
}

/* *********************************************************** */
