#ifndef LIBGRAPH_RB_H
#define LIBGRAPH_RB_H

#include "graph.h"

//@{

typedef void (*LibGraph_BisectionMethod)(LibGraph_Graph* g, float rLeft, float rRight, int ubfactor, int* part,
                                         void* arg);

/** Partition graph with recusive bisection paradigm. */
void LibGraph_partitionGraphRB(LibGraph_Graph* g, /**< [in] graph struct */
                               int nparts,        /**< [in] nb of desired partitions */
                               int ubfactor,      /**< [in] unbalanced factor */
                               int* part,         /**< [out] array of computed partitions (of size graph->nvtxs) */
                               LibGraph_BisectionMethod bisect, void* arg);

//@}

#endif
