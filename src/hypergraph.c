#include <assert.h>
#include <limits.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "debug.h"
#include "hypergraph.h"

/* *********************************************************** */

LibGraph_Hypergraph* LibGraph_newHypergraph(void)
{
  LibGraph_Hypergraph* hg = malloc(sizeof(LibGraph_Hypergraph));
  assert(hg);
  hg->nvtxs = 0;
  hg->nhedges = 0;
  hg->eptr = NULL;
  hg->eind = NULL;
  hg->vwgts = NULL;
  hg->hewgts = NULL;
  return hg;
}

/* *********************************************************** */

void LibGraph_dupHypergraph(LibGraph_Hypergraph* oldhg, LibGraph_Hypergraph* newhg)
{
  assert(oldhg && newhg);
  newhg->nvtxs = oldhg->nvtxs;
  newhg->nhedges = oldhg->nhedges;

  newhg->eptr = malloc((newhg->nhedges + 1) * sizeof(int));
  memcpy(newhg->eptr, oldhg->eptr, (newhg->nhedges + 1) * sizeof(int));
  int eindsize = oldhg->eptr[oldhg->nhedges];
  newhg->eind = malloc(eindsize * sizeof(int));
  memcpy(newhg->eind, oldhg->eind, eindsize * sizeof(int));

  newhg->vwgts = newhg->hewgts = NULL;

  if (oldhg->vwgts != NULL) {
    newhg->vwgts = malloc(newhg->nvtxs * sizeof(int));
    memcpy(newhg->vwgts, oldhg->vwgts, newhg->nvtxs * sizeof(int));
  }
  if (oldhg->hewgts != NULL) {
    newhg->hewgts = malloc(newhg->nhedges * sizeof(int));
    memcpy(newhg->hewgts, oldhg->hewgts, newhg->nhedges * sizeof(int));
  }
}

/* *********************************************************** */

void LibGraph_printHypergraph(LibGraph_Hypergraph* hg)
{
  printf("Hypergraph\n");
  printf("- nb vertices = %d\n", hg->nvtxs);
  if (hg->vwgts) {
    printf("- vertices weight =");
    for (int i = 0; i < hg->nvtxs; i++) printf(" %d", hg->vwgts[i]);
    printf("\n");
  }
  printf("- nb hyperedges = %d\n", hg->nhedges);
  printf("- hyperedge list\n");
  for (int i = 0; i < hg->nhedges; i++) {
    printf("  %d -> [ ", i);
    for (int j = hg->eptr[i]; j < hg->eptr[i + 1]; j++) {
      printf("%d ", hg->eind[j]);
    }
    printf("]");
    if (hg->hewgts) printf(" (%d)", hg->hewgts[i]);
    printf("\n");
  }
}

/* *********************************************************** */

void LibGraph_subHypergraphFromRange(
    LibGraph_Hypergraph* hg, int vtxstart, int vtxend, LibGraph_Hypergraph* subhg,
    int** vtxmap,  /* vertex local-to-global mapping (allocated array of size subhg->nvtxs) */
    int** hedgemap /* hyperedge local-to-global mapping (allocated array of size subhg->nhedges) */
    )
{
  subhg->nvtxs = vtxend - vtxstart + 1;
  assert(subhg->nvtxs >= 0 && subhg->nvtxs <= hg->nvtxs);
  subhg->nhedges = 0;
  int maxhedges = hg->nhedges;
  int maxeind = hg->eptr[hg->nhedges] + 1;  // bug? why +1 ?
  subhg->eptr = malloc((maxhedges + 1) * sizeof(int));
  subhg->eind = malloc(maxeind * sizeof(int));
  subhg->vwgts = (hg->vwgts == NULL) ? NULL : malloc(subhg->nvtxs * sizeof(int));
  subhg->hewgts = (hg->hewgts == NULL) ? NULL : malloc(maxhedges * sizeof(int));

  *vtxmap = (vtxmap == NULL) ? NULL : malloc(subhg->nvtxs * sizeof(int));
  *hedgemap = (hedgemap == NULL) ? NULL : malloc(maxhedges * sizeof(int));

  int ptr = 0; /* local hyperedge index */
  int k = 0;

  /* for all hyperedge */
  for (int i = 0; i < hg->nhedges; i++) {
    subhg->eptr[ptr] = k;
    // ok = 1;
    // /* test if all vertices hg->eind[j] are in range [vtxstart,vtxend] */
    // for (int j = hg->eptr[i]; j < hg->eptr[i + 1]; j++) {
    //   if (hg->eind[j] < vtxstart || hg->eind[j] > vtxend) {
    //     ok = 0;
    //     break;
    //   }
    // }
    /* if it is ok, , add the ptr-th hyperedge in subhg */
    // if (ok) {
    int ok = 0;
    for (int j = hg->eptr[i]; j < hg->eptr[i + 1]; j++) {
      if (hg->eind[j] >= vtxstart && hg->eind[j] <= vtxend) {
        ok = 1;
        subhg->eind[k] = hg->eind[j] - vtxstart;  // local vertex index
        k++;
      }
    }
    if (ok && hg->hewgts) subhg->hewgts[ptr] = hg->hewgts[i]; /* hyperedge weight */
    if (ok && hedgemap) (*hedgemap)[ptr] = i;                 /* local-to-global hyperedge index mapping */
    if (ok) ptr++;                                            /* next hyperedge */
  }
  // }

  subhg->eptr[ptr] = k; /*end */
  subhg->nhedges = ptr;

  /* vertex weight */
  if (hg->vwgts) {
    for (int i = vtxstart, k = 0; i <= vtxend; i++, k++) subhg->vwgts[k] = hg->vwgts[i];
  }

  /* local-to-global vertex mapping */
  if (vtxmap) {
    for (int i = 0; i < subhg->nvtxs; i++) (*vtxmap)[i] = vtxstart + i;
  }

  /* free memory unused with realloc */
  subhg->eptr = realloc(subhg->eptr, (subhg->nhedges + 1) * sizeof(int));
  subhg->eind = realloc(subhg->eind, k * sizeof(int));
  if (subhg->hewgts) subhg->hewgts = realloc(subhg->hewgts, subhg->nhedges * sizeof(int));
  if (hedgemap) *hedgemap = realloc(*hedgemap, subhg->nhedges * sizeof(int));
}

/* *********************************************************** */

void LibGraph_subHypergraphFromPart(LibGraph_Hypergraph* oldhg, int* part, int nparts, int p, LibGraph_Hypergraph* newhg, int** vtxs)
{
  /* Vertices in this part */
  newhg->nvtxs = 0;
  for (int i = 0; i < oldhg->nvtxs; i++)
    if (part[i] == p) newhg->nvtxs++;

  /* Edges in this part */
  newhg->nhedges = 0;
  int eindsize = 0;
  for (int i = 0; i < oldhg->nhedges; i++) {
    int hedge_size = 0;
    for (int j = oldhg->eptr[i]; j < oldhg->eptr[i + 1]; j++)
      if (part[oldhg->eind[j]] == p) hedge_size++;
    if (hedge_size > 0) {
      newhg->nhedges++;
      eindsize += hedge_size;
    }
  }

  /* Compute vertices translations vtxs: new -> old, translate: old -> new */
  *vtxs = malloc(newhg->nvtxs * sizeof(int));
  int* translate = malloc(oldhg->nvtxs * sizeof(int));
  for (int i = 0, j = 0; i < oldhg->nvtxs; i++)
    if (part[i] == p) {
      translate[i] = j;
      (*vtxs)[j++] = i;
    }
  /* Vertices weight */
  newhg->vwgts = malloc(newhg->nvtxs * sizeof(int));
  if (oldhg->vwgts == NULL)
    for (int i = 0; i < newhg->nvtxs; i++) newhg->vwgts[i] = 1;
  else
    for (int i = 0; i < newhg->nvtxs; i++) newhg->vwgts[i] = oldhg->vwgts[(*vtxs)[i]];

  /* Copy edges */
  newhg->eptr = malloc((newhg->nhedges + 1) * sizeof(int));
  newhg->eind = malloc(eindsize * sizeof(int));
  newhg->hewgts = malloc(newhg->nhedges * sizeof(int));
  newhg->eptr[0] = 0;
  for (int i = 0, j = 0; i < oldhg->nhedges; i++) {
    int hedge_size = 0;
    int l = newhg->eptr[j];
    for (int k = oldhg->eptr[i]; k < oldhg->eptr[i + 1]; k++)
      if (part[oldhg->eind[k]] == p) {
        newhg->eind[l++] = translate[oldhg->eind[k]];
        hedge_size++;
      }
    if (hedge_size > 0) {
      if (oldhg->hewgts == NULL)
        newhg->hewgts[j] = 1;
      else
        newhg->hewgts[j] = oldhg->hewgts[i];
      newhg->eptr[++j] = l;
    }
  }

  free(translate);
}

/* *********************************************************** */

void LibGraph_freeHypergraph(LibGraph_Hypergraph* hg)
{
  if (hg != NULL) {
    free(hg->eptr);
    free(hg->eind);
    if (hg->vwgts != NULL) free(hg->vwgts);
    if (hg->hewgts != NULL) free(hg->hewgts);
  }
}

/* *********************************************************** */

#define SET_BIT(bf, b) (bf[b / (8 * sizeof(unsigned int))] |= (unsigned int)1 << b % (8 * sizeof(unsigned int)))
#define RESET_BIT(bf, b) (bf[b / (8 * sizeof(unsigned int))] &= ~((unsigned int)1 << b % (8 * sizeof(unsigned int))))
#define TEST_BIT(bf, b) (bf[b / (8 * sizeof(unsigned int))] & ((unsigned int)1 << b % (8 * sizeof(unsigned int))))

/* *********************************************************** */

void LibGraph_quotientHypergraph(const LibGraph_Hypergraph* input, int nparts, const int* parts, LibGraph_Hypergraph* output)
{
  int i, j, k;

  assert(nparts <= 8 * sizeof(int));  // check ???

  output->nvtxs = nparts;
  output->vwgts = calloc(nparts, sizeof(int));
  for (i = 0; i < input->nvtxs; i++) {
    output->vwgts[parts[i]] += (input->vwgts == NULL ? 1 : input->vwgts[i]);
  }

  output->nhedges = 0;
  output->eptr = malloc((input->nhedges + 1) * sizeof(int));
  output->eptr[0] = 0;
  output->eind = malloc(input->eptr[input->nhedges] * sizeof(int));
  output->hewgts = malloc(input->nhedges * sizeof(int));

  unsigned int s = (nparts - 1) / (8 * sizeof(unsigned int)) + 1;
  unsigned int* hedges = malloc(input->nhedges * s * sizeof(unsigned int));
  unsigned int* hedge = malloc(s * sizeof(unsigned int));
  for (i = 0; i < input->nhedges; i++) {
    /* Compute hyperedge signature */
    memset(hedge, 0, s * sizeof(unsigned int));
    for (k = input->eptr[i]; k < input->eptr[i + 1]; k++)
      SET_BIT(hedge, parts[input->eind[k]]);  // hedge |= (unsigned long long)1 << parts[input->eind[k]];
    /* If the hyperedge is in only one part, skip it */
    RESET_BIT(hedge, parts[input->eind[input->eptr[i]]]);
    for (k = 0; k < s; k++)
      if (hedge[k] != 0) break;
    if (k == s)  // if ((hedge & ~((unsigned long long)1 << parts[input->eind[input->eptr[i]]])) == 0)
      continue;
    SET_BIT(hedge, parts[input->eind[input->eptr[i]]]);
    /* Search the hyperedge */
    for (j = 0; j < output->nhedges; j++) {
      for (k = 0; k < s; k++)
        if (hedges[j * s + k] != hedge[k]) break;
      if (k == s)  // if (hedges[j] == hedge)
        break;
    }
    if (j == output->nhedges) {  // The edge does not exist yet, create it
      output->nhedges++;
      output->eptr[j + 1] = output->eptr[j];
      output->hewgts[j] = 0;
      memcpy(&hedges[j * s], hedge, s * sizeof(unsigned int));  // hedges[j] = hedge;

      for (k = 0; k < nparts; k++) {
        if (TEST_BIT(hedge, k)) {  // if (hedge & ((unsigned long long)1 << k)) {
          output->eind[output->eptr[j + 1]] = k;
          output->eptr[j + 1]++;
        }
      }
    }
    output->hewgts[j] += (input->hewgts == NULL ? 1 : input->hewgts[i]);
  }

  free(hedges);
  free(hedge);

  // printHypergraph (output);

  /* Shrink arrays */
  output->eptr = realloc(output->eptr, (output->nhedges + 1) * sizeof(int));
  output->eind = realloc(output->eind, output->eptr[output->nhedges] * sizeof(int));
  output->hewgts = realloc(output->hewgts, output->nhedges * sizeof(int));
}

/* *********************************************************** */

void LibGraph_dualHypergraph(LibGraph_Hypergraph* hg, LibGraph_Hypergraph* dual)
{
  dual->nhedges = hg->nvtxs;
  dual->nvtxs = hg->nhedges;
  dual->eptr = malloc((dual->nhedges + 1) * sizeof(int));
  dual->vwgts = NULL;
  dual->hewgts = NULL;

  /* The degree of a vertex is the number of hyperedges that contain it. */
  int* degree = malloc(hg->nvtxs * sizeof(int));
  for (int i = 0; i < hg->nvtxs; i++) degree[i] = 0;

  for (int i = 0; i < hg->nhedges; i++) {
    for (int j = hg->eptr[i]; j < hg->eptr[i + 1]; j++) {
      int k = hg->eind[j]; /* vertex k is in hyperedge i */
      degree[k]++;
    }
  }

  /* create dual eptr structure */
  int k = 0;
  for (int i = 0; i < hg->nvtxs; i++) {
    dual->eptr[i] = k;
    k += degree[i];
  }
  dual->eptr[hg->nvtxs] = k; /* end */
  dual->eind = malloc(k * sizeof(int));

  /* temporary offset array */
  int* offset = malloc(hg->nvtxs * sizeof(int));
  for (int i = 0; i < hg->nvtxs; i++) offset[i] = 0;

  /* fill dual eind array, for all hyperedges in hg */
  for (int i = 0; i < hg->nhedges; i++) {
    for (int j = hg->eptr[i]; j < hg->eptr[i + 1]; j++) {
      int k = hg->eind[j]; /* vertex k is in hyperedge i */
      int jj = dual->eptr[k];
      int z = offset[k];
      dual->eind[jj + z] = i; /* add it! */
      offset[k]++;
    }
  }

  /* dual vertex weights */
  if (hg->hewgts != NULL) {
    int size = dual->nvtxs * sizeof(int);
    dual->vwgts = malloc(size);
    memcpy(dual->vwgts, hg->hewgts, size); /* dst, src, size */
  }
  /* dual hyperedge weights */
  if (hg->vwgts != NULL) {
    int size = dual->nhedges * sizeof(int);
    dual->hewgts = malloc(size);
    memcpy(dual->hewgts, hg->vwgts, size);
  }

  /* free memory */

  free(degree);
  free(offset);
}

/* *********************************************************** */
