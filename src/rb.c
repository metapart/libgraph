#include <assert.h>
#include <stdlib.h>

#include "debug.h"
#include "graph.h"
#include "rb.h"

/* *********************************************************** */

/* void bisectGraph(Graph * g,  */
/* 		 float rLeft,  */
/* 		 float rRight, */
/* 		 int ubfactor,  */
/* 		 int * part,  */
/* 		 int * edgecut) */
/* { */
/*   // partitionGraph(g, 2, ubfactor, part, edgecut, NULL, "KMETIS"); */
/*   float twgts[2] = {rLeft, rRight}; */
/*   partitionGraphKMETIS(g, 2, twgts, ubfactor, part, edgecut); */
/* } */

/* *********************************************************** */

static void _partitionGraphRB(LibGraph_Graph* g, int nparts, int shiftpart, int ubfactor, int* part,
                              LibGraph_BisectionMethod bisect, void* arg)
{
  PRINT("recursive part in %d (shift %d)\n", nparts, shiftpart);

  // trivial partition in 1 part
  if (nparts == 1) {
    for (int i = 0; i < g->nvtxs; i++) part[i] = shiftpart;
    // *edgecut = 0;
    return;
  }

  // compute nparts assigned for each subgraph
  int nLeft = nparts >> 1;
  int nRight = nparts - nLeft;
  assert(nLeft <= nRight && nRight > 0);
  PRINT("\t* left = %d parts and right = %d parts\n", nLeft, nRight);

  // compute bisection
  int* bipart = malloc(g->nvtxs * sizeof(int));
  assert(bipart);
  // int ec;
  bisect(g, nLeft * 1.0 / nparts, nRight * 1.0 / nparts, ubfactor, bipart, arg);
  // PRINT("\t* bisection: ec = %d\n", ec);

  // split graph in two subgraphs
  int* map = malloc(g->nvtxs * sizeof(int));
  assert(map);
  LibGraph_Graph gLeft, gRight;
  LibGraph_splitGraph(g, bipart, &gLeft, &gRight, map);
  PRINT("\t* split graph: left = %d vtxs and right = %d vtxs\n", gLeft.nvtxs, gRight.nvtxs);

  // bisection case
  if (nparts == 2) {
    for (int i = 0; i < g->nvtxs; i++) part[i] = shiftpart + bipart[i];
    // *edgecut = ec;
  }

  // recursive call
  if (nparts > 2) {
    // int ecLeft, ecRight;
    int* partLeft = malloc(gLeft.nvtxs * sizeof(int));
    int* partRight = malloc(gRight.nvtxs * sizeof(int));
    assert(partLeft && partRight);

    _partitionGraphRB(&gLeft, nLeft, shiftpart, ubfactor, partLeft, bisect, arg);  // in [shiftpart, shiftpart+nLeft-1]
    _partitionGraphRB(&gRight, nRight, shiftpart + nLeft, ubfactor, partRight, bisect,
                      arg);  // in [shiftpart+nLeft, shiftpart+nLeft+nRight-1]

    // check
    for (int i = 0; i < gLeft.nvtxs; i++) assert(partLeft[i] >= shiftpart && partLeft[i] < shiftpart + nLeft);
    for (int i = 0; i < gRight.nvtxs; i++) assert(partRight[i] >= shiftpart && partRight[i] < shiftpart + nparts);

    // merge partition left & right
    for (int i = 0; i < g->nvtxs; i++) {
      assert(bipart[i] == 0 || bipart[i] == 1);
      if (bipart[i] == 0) part[i] = partLeft[map[i]];
      if (bipart[i] == 1) part[i] = partRight[map[i]];
      assert(part[i] >= shiftpart && part[i] < (shiftpart + nparts));
    }
    // *edgecut = ecLeft + ecRight;
    free(partLeft);
    free(partRight);
  }

  // free extra mem (3*nvtxs*sizeof(int))
  LibGraph_freeGraph(&gLeft);
  LibGraph_freeGraph(&gRight);
  free(bipart);
  free(map);
}

/* *********************************************************** */

void LibGraph_partitionGraphRB(LibGraph_Graph* g, int nparts, int ubfactor, int* part, LibGraph_BisectionMethod bisect,
                               void* arg)
{
  _partitionGraphRB(g, nparts, 0, ubfactor, part, bisect, arg);
}

/* *********************************************************** */
