#ifndef LIBGRAPH_ARRAY_H
#define LIBGRAPH_ARRAY_H

#include <stdbool.h>

//@{

/* *********************************************************** */
/*                           ARRAY                             */
/* *********************************************************** */

/** basic array typecodes. */
enum LIBGRAPH_TYPECODE
{
  LIBGRAPH_VOID = 0,
  LIBGRAPH_INT,
  LIBGRAPH_FLOAT,
  LIBGRAPH_DOUBLE,
  LIBGRAPH_STR,
  LIBGRAPH_NB_TYPECODES
};

/** special array flags for mesh, graph or hypergraph variables */

typedef enum _LibGraph_ArrayFlags
{
  LIBGRAPH_NOT_VAR = 0,
  LIBGRAPH_MESH_CELL_VAR = 1,
  LIBGRAPH_MESH_NODE_VAR = 2,
  LIBGRAPH_GRAPH_VERTEX_VAR = 4,
  LIBGRAPH_GRAPH_EDGE_VAR = 8,
  LIBGRAPH_HYPERGRAPH_VERTEX_VAR = 16,
  LIBGRAPH_HYPERGRAPH_HYPEREDGE_VAR = 32
  // TODO: add HybridMesh
} LibGraph_ArrayFlags;

/** Generic array structure. */
typedef struct _LibGraph_Array
{
  int nb_tuples;     /**< nb tuples */
  int nb_components; /**< nb components */
  int typecode;      /**< int, float, double, or string*/
  void* data;        /**< raw data pointer */
  bool owner;        /**< memory ownership of raw array */
  int flags;         /**< special array flags for mesh, graph or hypergraph variables */
} LibGraph_Array;

/** Create a new array structure */
LibGraph_Array* LibGraph_newArray(void);

/** Allocate underlying array structure */
void* LibGraph_allocArray(LibGraph_Array* a, int nb_tuples, int nb_components, int typecode);

/** Compare two arrays, return 1 if equal */
bool LibGraph_compareArray(LibGraph_Array* a1, LibGraph_Array* a2);

/** wrap an array structure */
void LibGraph_wrapArray(LibGraph_Array* a, int nb_tuples, int nb_components, int typecode, const void* data, bool owner);

/** Free memory allocated within array structure */
void LibGraph_freeArray(LibGraph_Array* a);

/** Duplicate an array structure */
void LibGraph_dupArray(LibGraph_Array* olda, LibGraph_Array* newa, bool deep);

/** Print array struture */
void LibGraph_printArray(LibGraph_Array* a, bool dump);

/** Dump array struture */
void LibGraph_dumpArray(LibGraph_Array* a);

/** set array flags. */
void LibGraph_setArrayFlags(LibGraph_Array* a, LibGraph_ArrayFlags flags);

/** get array flags */
LibGraph_ArrayFlags LibGraph_getArrayFlags(LibGraph_Array* a);

/* *********************************************************** */
/*                           VARIABLES                         */
/* *********************************************************** */

typedef struct _LibGraph_Variable
{
  char* id;
  LibGraph_Array* array;
} LibGraph_Variable;

/** Collection of variables. */
typedef struct _LibGraph_Variables
{
  int nb_vars;
  int max_vars;
  LibGraph_Variable* vars;
} LibGraph_Variables;

LibGraph_Variables* LibGraph_newVariables(int max);
void LibGraph_addVariable(LibGraph_Variables* vars, char* id, LibGraph_Array* array);
char* LibGraph_getVariableID(LibGraph_Variables* vars, int i);
LibGraph_Array* LibGraph_getVariableArray(LibGraph_Variables* vars, int i);
void LibGraph_freeVariables(LibGraph_Variables* vars, bool freearrays);
LibGraph_Variables* LibGraph_filterVariables(const LibGraph_Variables* vars, LibGraph_ArrayFlags flag);

/* *********************************************************** */

//@}

#endif
