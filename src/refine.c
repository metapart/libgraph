#include <assert.h>
#include <limits.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "debug.h"
#include "diag.h"
#include "graph.h"
#include "refine.h"

/* *********************************************************** */

int LibGraph_createBandGraph(const LibGraph_Graph* g, int nparts, const int* part, const int* locked, LibGraph_Graph* band, int width, int* frontier)
{
  assert(g && locked && band && frontier);

  // debug
  /* printGraph(g);  */
  /* PRINT("part: ["); */
  /* for(int i = 0 ; i < g->nvtxs ; i++) PRINT("%d ",  part[i]); */
  /* PRINT("]\n");   */

  /* look for vertices in frontier and compute distance from frontier */
  memset(frontier, 0, g->nvtxs * sizeof(int));  // reset all frontier with 0
  for (int k = 0; k < width; k++) {
    for (int i = 0; i < g->nvtxs; i++) {
      if (locked[i])
        continue;  // skip fixed vertices because this is not required and for other tricky reasons
                   // ;-)
      for (int j = g->xadj[i]; j < g->xadj[i + 1]; j++) {
        int ii = g->adjncy[j]; /* edge (i, ii) */
        if (k == 0 && part[i] != part[ii])
          frontier[i] = frontier[ii] = 1; /* init frontier */
        else if (k > 0 && frontier[i] == k && frontier[ii] == 0 && part[i] == part[ii])
          frontier[ii] = k + 1; /* increment frontier band */
      }
    }
  }

  // debug
  /* PRINT("frontier: ["); */
  /* for(int i = 0 ; i < g->nvtxs ; i++) PRINT("%d ",  frontier[i]); */
  /* PRINT("]\n");   */

  /* compute nb vertices in band graph */
  int band_nvtxs = 0;
  for (int i = 0; i < g->nvtxs; i++)
    if (frontier[i] > 0) band_nvtxs++;
  assert(band_nvtxs > 0 && band_nvtxs <= g->nvtxs);

  /* compute mapping: vtxs[newvtx] = oldvtx */
  int* vtxs = calloc(band_nvtxs, sizeof(int));
  for (int i = 0, k = 0; i < g->nvtxs; i++)
    if (frontier[i] > 0) vtxs[k++] = i;

  /* compute inverse mapping: invvtxs[oldvtx] = newvtx */
  int* invvtxs = malloc(g->nvtxs * sizeof(int));
  for (int i = 0; i < g->nvtxs; i++) invvtxs[i] = -1;
  for (int i = 0; i < band_nvtxs; i++) invvtxs[vtxs[i]] = i;

  /* compute band graph */
  /* 1) first, we add vertices on frontier */
  /* 2) then, we add one super-node for each part at the end (that merged remaining vertices) */
  band->nvtxs = band_nvtxs + nparts;
  band->narcs = 0;
  int maxarcs = g->narcs;
  band->xadj = malloc((band->nvtxs + 1) * sizeof(int));
  band->adjncy = malloc(maxarcs * sizeof(int));
  band->vwgts = malloc(band->nvtxs * sizeof(int));  // required !
  band->ewgts = malloc(maxarcs * sizeof(int));      // required !

  int newvtx = 0; /* vertex index in bandgraph */
  int k = 0;

  int nbsuperedges = 0;

  /* 1) add regular vertices in band */
  for (int i = 0; i < g->nvtxs; i++) {
    band->xadj[newvtx] = k;
    if (frontier[i] == 0) continue; /* skip, next vertex in graph */

    int superedgeweight = 0;
    int superedge = 0;

    for (int j = g->xadj[i]; j < g->xadj[i + 1]; j++) {
      int ii = g->adjncy[j]; /* edge (i, ii) */

      /* superedge case */
      if (frontier[i] == width && frontier[ii] == 0) { /* if connected outside of the band  */
        assert(invvtxs[ii] == -1);                     // check
        superedge = 1;
        superedgeweight += g->ewgts ? g->ewgts[j] : 1;
        continue; /* skip, next edge in graph */
      }

      /* normal edge in band graph */
      assert(frontier[i] > 0 && frontier[ii] > 0);  // check
      band->adjncy[k] = invvtxs[ii];
      band->ewgts[k] = g->ewgts ? g->ewgts[j] : 1; /* edge weight */
      k++;                                         /* next edge in bandLibGraph_Graph*/
    }

    /* add superedge at the end... */
    if (superedge) {
      band->adjncy[k] = band_nvtxs + part[i]; /* at the end */
      band->ewgts[k] = superedgeweight;
      k++;
      nbsuperedges++;
    }

    assert(k <= maxarcs);
    newvtx++; /* next vertex in bandgraph */
  }
  band->xadj[newvtx] = k; /* end */

  assert(band_nvtxs == newvtx);  // check

  /* 2) add nparts supernodes in band graph with all their edges... */

  int checksuperedges = 0;
  for (int z = 0; z < nparts; z++) { /* for each supernode */
    band->xadj[newvtx] = k;
    for (int bi = 0; bi < band_nvtxs; bi++) { /* only vertices in band graph */
      int gi = vtxs[bi];
      if (part[gi] == z && frontier[gi] == width) { /* in part z and connected to supernode */
        int kk = band->xadj[bi + 1] - 1;
        int bii = band->adjncy[kk]; /* edge (bi, bii) in band graph */
        assert(bii < band->nvtxs);
        if (bii >= band_nvtxs) { /* found superedge (i,ii) with supernode ii */
          // PRINT("found superedge (%d,%d)\n", bi, bii);
          assert(part[gi] == bii - band_nvtxs);  // check
          band->adjncy[k] = bi;
          band->ewgts[k] = band->ewgts[kk];
          checksuperedges++;
          k++;
          assert(k <= maxarcs);
        }
      }
    }
    newvtx++; /* next vertex in bandgraph */
  }

  assert(checksuperedges == nbsuperedges);  // check
  assert(band->nvtxs == newvtx);            // check

  band->xadj[newvtx] = k; /* end */
  band->narcs = k;

  /* 1) vertex weight in bandgraph */
  for (int i = 0; i < band_nvtxs; i++) band->vwgts[i] = g->vwgts ? g->vwgts[vtxs[i]] : 1;

  /* 2) vertex weight for super nodes */
  for (int i = 0; i < nparts; i++) band->vwgts[band_nvtxs + i] = 0;
  for (int i = 0; i < g->nvtxs; i++)
    if (frontier[i] == 0) band->vwgts[band_nvtxs + part[i]] += g->vwgts ? g->vwgts[i] : 1;

  /* free memory unused with realloc */
  band->adjncy = realloc(band->adjncy, band->narcs * sizeof(int));
  band->ewgts = realloc(band->ewgts, band->narcs * sizeof(int));
  free(vtxs);
  free(invvtxs);

  // debug
  // printGraph(band);

  /* check weigth for vertices & edges */
  int gvw = 0, bvw = 0, gew = 0, bew = 0, cbew = 0, ccbew = 0;
  for (int i = 0; i < g->nvtxs; i++) gvw += (g->vwgts ? g->vwgts[i] : 1);
  for (int i = 0; i < band->nvtxs; i++) bvw += band->vwgts[i];
  for (int i = 0; i < g->narcs; i++) gew += (g->ewgts ? g->ewgts[i] : 1);
  for (int i = 0; i < band->narcs; i++) bew += band->ewgts[i];
  for (int i = 0; i < g->nvtxs; i++) {
    for (int j = g->xadj[i]; j < g->xadj[i + 1]; j++) {
      int ii = g->adjncy[j]; /* edge (i, ii) */
      if ((frontier[i] > 0 && frontier[ii] >= 0) || (frontier[i] >= 0 && frontier[ii] > 0))
        cbew += (g->ewgts ? g->ewgts[j] : 1);
      if (frontier[i] == 0 && frontier[ii] == 0) ccbew += (g->ewgts ? g->ewgts[j] : 1);
    }
  }

  assert(gvw == bvw);
  assert(gew >= bew);
  assert(cbew + ccbew == gew);
  assert(cbew == bew);

  return band_nvtxs;  // nb vertices in band (without supernodes)
}

/* *********************************************************** */
/*              GREEDY REFINEMENT ROUTINE                      */
/* *********************************************************** */

enum GreedyRefinementMode
{
  OptimizeBalance,
  OptimizeCut,
  OptimizeBoth
};

/* Refine graph with Greedy strategy ("Multilevel k-way Partitioning Scheme for Iregular Graphs" by
 * Karypis and Kumar, JPDC'98). */
int LibGraph_refineGreedyMode(LibGraph_Graph* g,     /* [in] graph */
                              int nparts,   /* [in] nb of input parts */
                              int ubfactor, /* [in] unbalanced factor */
                              int* part,    /* [in/out] array of input partition (of size g->nvtxs) */
                              int* locked,  /* [in] boolean array of locked vertices in part (of size g->nvtxs) */
                              enum GreedyRefinementMode mode)
{
  assert(mode == OptimizeCut || mode == OptimizeBalance || mode == OptimizeBoth);
  // int edgecut_initial = computeGraphEdgeCut(g, nparts, part);
  int gaincut = 0;
  int gainwgt = 0;

  // TODO: use random order...

  // compute weights
  int totalwgt = 0;
  int* currentwgts = calloc(nparts, sizeof(int));
  for (int i = 0; i < g->nvtxs; i++) {
    int vw = g->vwgts ? g->vwgts[i] : 1;
    currentwgts[part[i]] += vw;
    totalwgt += vw;
  }
  int targetwgt = ceil(totalwgt * (1.0 + ubfactor / 100.0) / nparts);

  // adjacent parts
  int* adjparts = malloc(nparts * sizeof(int));

  // array to store internal/external degrees in cut optimization
  int* ewgts = malloc(nparts * sizeof(int));

  // for all vertices
  for (int i = 0; i < g->nvtxs; i++) {
    if (locked && locked[i]) continue;  // skip fixed vertex
    int from = part[i];

    // look for adjacent parts to vtx i
    int nadjparts = 0;
    for (int p = 0; p < nparts; p++) adjparts[p] = 0;
    for (int j = g->xadj[i]; j < g->xadj[i + 1]; j++) {
      int ii = g->adjncy[j]; /* edge (i, ii) */
      int to = part[ii];
      if (from != to && adjparts[to] == 0) {
        adjparts[to] = 1;
        nadjparts++;
      }
    }

    // skip internal vertex (not at the boundary of partition)
    if (nadjparts == 0) continue;

    // compute internal/external degree of vtx i
    // => TODO: should be precomputed and updated after...
    for (int p = 0; p < nparts; p++) ewgts[p] = 0;
    for (int j = g->xadj[i]; j < g->xadj[i + 1]; j++) {
      int ii = g->adjncy[j]; /* edge (i, ii) */
      int p = part[ii];
      int ew = g->ewgts ? g->ewgts[j] : 1;
      ewgts[p] += ew;
    }

    // select best move of vtx 'i' if possible...

    int vw = g->vwgts ? g->vwgts[i] : 1;
    int intdeg = ewgts[from];  // internal degreex

    int bestgain = 0;
    int to = -1;

    for (int p = 0; p < nparts; p++) {  // for all adjacent parts
      if (adjparts[p] == 0) continue;   // skip if not adjacent...

      // check balance condition
      if (currentwgts[p] + vw > targetwgt) continue;

      int extdeg = ewgts[p];       // external degree
      int gain = extdeg - intdeg;  // edgecut gain

      if (mode == OptimizeCut) { /* stricly optimize cut */
        if (gain > 0 && gain > bestgain) {
          bestgain = gain;
          to = p;
        }
      }
      if (mode == OptimizeBalance) { /* stricly optimize balance */
        // if(gain == 0 && (currentwgts[p] + vw < currentwgts[from]) ) { bestgain = gain; to = p;
        // break;}
        if (gain >= 0 && (currentwgts[p] + vw < currentwgts[from])) {
          bestgain = gain;
          to = p;
          break;
        }
      }
      if (mode == OptimizeBoth) { /* optimize cut or optimize balance if cut optimization is not possible */
        if (gain > 0 && gain > bestgain) {
          bestgain = gain;
          to = p;
        }
        else if (gain == 0 && (currentwgts[p] + vw < currentwgts[from])) {
          bestgain = gain;
          to = p;
        }
      }
    }

    // just move vtx i from 'from' to 'to'
    if (to != -1) {
      if (locked) assert(!locked[i]);
      assert(part[i] == from);
      assert(from != to);
      part[i] = to;
      currentwgts[from] -= vw;
      currentwgts[to] += vw;
      gainwgt += vw;
      gaincut += bestgain;
    }
  }

  free(adjparts);
  free(currentwgts);
  free(ewgts);

  // int edgecut_final = computeGraphEdgeCut(g, nparts, part);
  // int _gaincut = edgecut_initial-edgecut_final;
  // assert(gaincut == _gaincut);
  assert(gaincut >= 0);
  assert(gainwgt >= 0);

  if (mode == OptimizeBalance) return gainwgt;
  if (mode == OptimizeBoth) return gainwgt + gaincut;
  return gaincut;
}

/* *********************************************************** */

#define MAXPASSBALANCE 10

/* *********************************************************** */

/* try to optimize balance and cut alternately */
int LibGraph_refineGreedy(LibGraph_Graph* g, int nparts, int ubfactor, int* part, int* locked, int npass, int maxnegmove)
{
  assert(npass >= -1);

#ifdef DEBUG
  int edgecut_initial = computeGraphEdgeCut(g, nparts, part);
  double ub_initial = computeGraphMaxUnbalance(g, nparts, part);
  PRINT("[greedy refinement] initial state: cut %d, ub %.2f%%\n", edgecut_initial, ub_initial * 100.0);
#endif

  int gainwgt = 0;
  int gaincut = 0;
  int k;
  int gain;

  // balance optimization
  k = 0;
  gain = -1;
  while (k < MAXPASSBALANCE) {
    int _gain = LibGraph_refineGreedyMode(g, nparts, ubfactor, part, locked, OptimizeBalance);
    gainwgt += _gain;
    assert(_gain >= 0);
    if (_gain == 0 && gain == 0) break;
    gain = _gain;
    k++;
  }

#ifdef DEBUG
  double ub_tmp = computeGraphMaxUnbalance(g, nparts, part);
  int edgecut_tmp = computeGraphEdgeCut(g, nparts, part);
  PRINT("[greedy refinement] balance optimization (%d passes): cut %d, ub %.2f%%\n", k, edgecut_tmp, ub_tmp * 100.0);
  assert(edgecut_initial - edgecut_tmp >= 0);
  assert(ub_initial - ub_tmp >= 0);
#endif

  // cut optimization
  k = 0;
  gain = -1;
  while (k < npass || npass == -1) {
    int _gain = LibGraph_refineGreedyMode(g, nparts, ubfactor, part, locked, OptimizeCut);
    gaincut += _gain;
    assert(_gain >= 0);
    if (_gain == 0 && gain == 0) break;
    gain = _gain;
    k++;
  }

#ifdef DEBUG
  int edgecut_final = computeGraphEdgeCut(g, nparts, part);
  double ub_final = computeGraphMaxUnbalance(g, nparts, part);
  PRINT("[greedy refinement] cut optimization (%d passes): cut %d, ub %.2f%%\n", k, edgecut_final, ub_final * 100.0);
  // check
  assert(edgecut_tmp - edgecut_final >= 0);
  assert(ub_final <= ubfactor);
#endif

  return gaincut;
}

/* *********************************************************** */

/* try to optimize both balance and cut simultaneously */
int LibGraph_refineGreedy2(LibGraph_Graph* g, int nparts, int ubfactor, int* part, int* locked, int npass, int maxnegmove)
{
  assert(npass >= -1);

#ifdef DEBUG
  int edgecut_initial = computeGraphEdgeCut(g, nparts, part);
  double ub_initial = computeGraphMaxUnbalance(g, nparts, part);
  PRINT("[greedy refinement] initial state: cut %d, ub %.2f%%\n", edgecut_initial, ub_initial * 100.0);
#endif

  int gainboth = 0;
  int k;
  int gain;

  // both optimization
  k = 0;
  gain = -1;
  while (k < npass || npass == -1) {
    int _gain = LibGraph_refineGreedyMode(g, nparts, ubfactor, part, locked, OptimizeBoth);
    gainboth += _gain;
    assert(_gain >= 0);
    if (_gain == 0 && gain == 0) break;
    gain = _gain;
    k++;
  }

#ifdef DEBUG
  int edgecut_final = computeGraphEdgeCut(g, nparts, part);
  double ub_final = computeGraphMaxUnbalance(g, nparts, part);
  PRINT("[greedy refinement] both optimization (%d passes): cut %d, ub %.2f%%\n", k, edgecut_final, ub_final * 100.0);
  // check
  assert(edgecut_initial - edgecut_final >= 0);
  assert(ub_final <= ubfactor);
#endif

  return gainboth;
}

/* *********************************************************** */
