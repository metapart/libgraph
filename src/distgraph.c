#include <assert.h>
#include <limits.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "distgraph.h"

/* *********************************************************** */

LibGraph_DistGraph* LibGraph_newDistGraph(void)
{
  LibGraph_DistGraph* dg = malloc(sizeof(LibGraph_DistGraph));
  assert(dg);
  dg->prank = -1;
  dg->psize = 0;
  dg->vtxdist = NULL;
  dg->nvtxs = 0;
  dg->narcs = 0;
  dg->xadj = NULL;
  dg->adjncy = NULL;
  dg->vwgts = NULL;
  dg->ewgts = NULL;
  return dg;
}

/* *********************************************************** */

void LibGraph_dupDistGraph(LibGraph_DistGraph* olddg, LibGraph_DistGraph* newdg)
{
  assert(olddg && newdg);
  newdg->prank = olddg->prank;
  newdg->psize = olddg->psize;
  newdg->vtxdist = malloc((newdg->psize + 1) * sizeof(int));
  memcpy(newdg->vtxdist, olddg->vtxdist, (newdg->psize + 1) * sizeof(int));
  newdg->nvtxs = olddg->nvtxs;
  newdg->narcs = olddg->narcs;
  newdg->xadj = malloc((newdg->nvtxs + 1) * sizeof(int));
  memcpy(newdg->xadj, olddg->xadj, (newdg->nvtxs + 1) * sizeof(int));
  newdg->adjncy = malloc(newdg->narcs * sizeof(int));
  memcpy(newdg->adjncy, olddg->adjncy, newdg->narcs * sizeof(int));
  newdg->vwgts = newdg->ewgts = NULL;
  if (olddg->vwgts != NULL) {
    newdg->vwgts = malloc(newdg->nvtxs * sizeof(int));
    memcpy(newdg->vwgts, olddg->vwgts, newdg->nvtxs * sizeof(int));
  }
  if (olddg->ewgts != NULL) {
    newdg->ewgts = malloc(newdg->narcs * sizeof(int));
    memcpy(newdg->ewgts, olddg->ewgts, newdg->narcs * sizeof(int));
  }
}

/* *********************************************************** */

void LibGraph_freeDistGraph(LibGraph_DistGraph* dg)
{
  if (dg != NULL) {
    if (dg->xadj) free(dg->xadj);
    if (dg->adjncy) free(dg->adjncy);
    if (dg->vwgts) free(dg->vwgts);
    if (dg->ewgts) free(dg->ewgts);
    if (dg->vtxdist) free(dg->vtxdist);
    dg->xadj = dg->adjncy = dg->vwgts = dg->ewgts = dg->vtxdist = NULL;
  }
}

/* *********************************************************** */

void LibGraph_printDistGraph(LibGraph_DistGraph* dg)
{
  printf("Distributed Graph\n");
  printf("- prank = %d / psize = %d\n", dg->prank, dg->psize);
  printf("- nb local vertices = %d\n", dg->nvtxs);
  printf("- nb global vertices = %d\n", dg->vtxdist[dg->psize]);  
  printf("- nb local arcs = %d\n", dg->narcs);
  int offset = dg->vtxdist[dg->prank];
  printf("- adjacency list\n");
  for (int i = 0; i < dg->nvtxs; i++) {
    printf("  %d (%d) -> [ ", i, offset+i);
    for (int j = dg->xadj[i]; j < dg->xadj[i + 1]; j++) {
      printf("%d ", dg->adjncy[j]);
      if (dg->ewgts) printf("(%d) ", dg->ewgts[j]);
    }
    printf("]\n");
  }

  if (dg->vwgts) {
    printf("- vertex weight = [ ");
    for (int i = 0; i < dg->nvtxs; i++) printf("%d ", dg->vwgts[i]);
    printf("]\n");
  }
}

/* *********************************************************** */