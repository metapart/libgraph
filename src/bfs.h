#ifndef LIBGRAPH_BFS_H
#define LIBGRAPH_BFS_H

#include "graph.h"

//@{

/* *********************************************************** */
/*                           BFS                               */
/* *********************************************************** */

/** BFS algorithm used to return a new vertex seed for a set of starting vertices */
int LibGraph_bfs(const LibGraph_Graph* g, /* input graph */
                 int* startvtx,           /* set of starting vertices (set -1 to ignore a seed) */
                 int size,                /* size of startvtx */
                 int* d                   /* distance array from startvtx set (array of size g->nvtxs) */
);

/* *********************************************************** */
/*                           DFS                               */
/* *********************************************************** */

/** depth-first search algorithm. */
int LibGraph_depthFirstSearch(const LibGraph_Graph* g, int root);

/* *********************************************************** */
/*          Auxiliary Algorithms based on BFS                  */
/* *********************************************************** */

/** test graph connectivity (based on BFS algorithm)  */
/** \return true if the graph is connected */
int LibGraph_isConnectedGraph(const LibGraph_Graph* g);

/** test subgraph connectivity given a subset of vertices (based on BFS algorithm)  */
int LibGraph_isConnectedSubgraph(const LibGraph_Graph* g, int* used);

/** test if a graph partition is connected (using BFS algorithm)  */
/** return the nb of islands >= nparts */
int LibGraph_computeNumberOfIslands(const LibGraph_Graph* g, int nparts, const int* part);

/** Compute a graph partition (often imbalanced) using levelset algorithm (from input seeds) */
void LibGraph_levelset1(const LibGraph_Graph* g, /**< [in]  input graph */
                        int size,                /**< [in]  nb parts */
                        int maxpartsize,         /**< [in]  max part size allowed (-1 if infinite) */
                        int* part                /**< [in/out]  input/output partition (with seeds) */
);

/** Compute a graph partition (often imbalanced) using levelset algorithm (from input seeds) */
void LibGraph_levelset2(const LibGraph_Graph* g, /**< [in]  input graph */
                        int size,                /**< [in]  nb parts */
                        int* maxpartsize,        /**< [in]  array of max part size allowed (NULL if infinite) */
                        int* part                /**< [in/out]  input/output partition (with seeds) */
);

//@}

#endif
