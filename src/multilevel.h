#ifndef LIBGRAPH_MULTILEVEL_H
#define LIBGRAPH_MULTILEVEL_H

#include "graph.h"

//@{

/* *********************************************************** */
/*                        MULTILEVEL                           */
/* *********************************************************** */

typedef void (*LibGraph_RefineMethod)(const LibGraph_Graph* g, int nparts, int* part, const int* fixed, int ubfactor,
                                      const void* ropt);
typedef void (*LibGraph_PartitionMethod)(const LibGraph_Graph* g, int nparts, int* part, const int* fixed, int ubfactor,
                                         const void* popt);
typedef void (*LibGraph_CoarsenMethod)(const LibGraph_Graph* g, LibGraph_Graph* cg, int* cmap, const int* part,
                                       const void* copt);

/** Multi-level graph partitioning routine. */
void LibGraph_partitionGraphMultiLevel(
    const LibGraph_Graph* g,            /**< [in] graph */
    int nparts,                         /**< [in] nb desired parts */
    int* part,                          /**< [in/out] array of pre-assigned partitions (of size g->nvtxs). */
    int ubfactor,                       /**< [in] unbalance factor */
    int usefixed,                       /**< [in] use fixed vertices */
    int ninitpass,                      /**< [in] nb of initial partitioning passes at coarsest level */
    int nrefpass,                       /**< [in] nb of refinement passes at each level  (-1 for infinite) */
    int cnvtxspart,                     /**< [in] max size of coarsest graph per part */
    LibGraph_CoarsenMethod coarsen,     /**< [in] graph coarsening routine */
    LibGraph_PartitionMethod partition, /**< [in] graph partitioning routine */
    LibGraph_RefineMethod refine,       /**< [in] graph refinement routine */
    const void* copt,                   /**< [in] coarsening options */
    const void* popt,                   /**< [in] initial partitioning options */
    const void* ropt                    /**< [in] refinement options */
);

//@}

#endif
