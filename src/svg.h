#ifndef LIBGRAPH_SVG_H
#define LIBGRAPH_SVG_H

#include <stdio.h>

#include "graph.h"
#include "hypergraph.h"
#include "mesh.h"

void LibGraph_svgBegin(FILE* file);
void LibGraph_svgEnd(FILE* file);

void LibGraph_svgDrawHyperedge(FILE* file, int n, int he[], double coords[][2], double r_in, double r_out,
                               const char* color);
void LibGraph_svgDrawHyperedges(FILE* file, const LibGraph_Hypergraph* hg, double coords[][2]);
void LibGraph_svgDrawEdge(FILE* file, const LibGraph_Graph* g, int edge, int a, int b, double coords[][2], const char* color,
                          int tag, int oriented);
void LibGraph_svgDrawEdges(FILE* file, const LibGraph_Graph* g, double coords[][2], const char* color, int tag, int oriented);
void LibGraph_svgDrawVertex(FILE* file, int v, double coords[][2], double r, const char* color, int* vwgts);
void LibGraph_svgDrawVertices(FILE* file, int n, double coords[][2], int* vwgts);
void LibGraph_svgDrawGraph(FILE* file, const LibGraph_Graph* graph, double coords[][2], int vtag, int etag, int oriented);
void LibGraph_svgDrawGraphPart(FILE* file, const LibGraph_Graph* graph, double coords[][2], int vtag, int etag, int nparts,
                               const int* part);

// misc
void LibGraph_iniCoords(int n, double coords[][2]);  // circle layout
void LibGraph_initCoordsFromMesh(int n, LibGraph_Mesh* m, int use_cells, int* part, double coords[][2]);
void LibGraph_resizeCoords(int n, double coords[][2], int boxsize, int invertyaxis);

#endif
