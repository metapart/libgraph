#ifndef LIBGRAPH_SORT_H
#define LIBGRAPH_SORT_H

//@{

/* *********************************************************** */
/*                      SORTING ROUTINES                       */
/* *********************************************************** */

/** Quicksort routine for double array. */
void LibGraph_sortD(int size,      /**< [in] array size */
           double* array, /**< [in] array of double (sorted only if perm is NULL) */
           int* perm      /**< [out] permutation array (or NULL if not used) */
           );

/** Quicksort routine for interger array. */
void LibGraph_sortI(int size,   /**< [in] array size */
           int* array, /**< [inout] array of integer (sorted only if perm is NULL) */
           int* perm   /**< [out] permutation array (or NULL if not used) */
           );

/** Quicksort routine for interger pair array. */
void LibGraph_sortP(int size,   /**< [in] array size */
           int* array, /**< [in] array of integer pairs (of 2*size*sizeof(int)) */
           int* perm   /**< [out] permutation array */
           );

/** Apply a permutation to a double array */
void LibGraph_permuteD(int size,      /**< [in] array size */
              double* array, /**< [inout] array to permute */
              int* perm      /**< [in] permutation array */
              );

/** Apply a permutation to an integer array */
void LibGraph_permuteI(int size,   /**< [in] array size */
              int* array, /**< [inout] array to permute */
              int* perm   /**< [in] permutation array  */
              );

/** Apply a permutation to an integer pair array */
void LibGraph_permuteP(int size,   /**< [in] array size */
              int* array, /**< [inout] array to permute */
              int* perm   /**< [in] permutation array */
              );

//@}

#endif
