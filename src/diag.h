#ifndef LIBGRAPH_DIAG_H
#define LIBGRAPH_DIAG_H

#include "graph.h"
#include "hypergraph.h"

/* *********************************************************** */
/*                      DIAGNOSTIC ROUTINE                     */
/* *********************************************************** */

//@{

void LibGraph_printGraphDiagnostic(const LibGraph_Graph* g, /* [in] graph */
                                   int nparts,              /* [in] nb of  partitions */
                                   const int* part);        /* [in] array of partition (of size hg->nvtxs) */

void LibGraph_printHypergraphDiagnostic(const LibGraph_Hypergraph* hg, /* [in] hypergraph */
                                        int nparts,                    /* [in] nb of partitions */
                                        const int* part);              /* [in] array of partition (of size hg->nvtxs) */

/** compute max imbalance */
double LibGraph_computeGraphMaxUnbalance(const LibGraph_Graph* g, int nparts,
                                         const int* part /* [in] regular or incomplete partition (of size g->nvtxs) */
);

/** compute min & max imbalance */
void LibGraph_computeGraphMinMaxUnbalance(const LibGraph_Graph* g, int nparts,
                                          const int* part, /* [in] regular partition (of size g->nvtxs) */
                                          double* minub, double* maxub);

int LibGraph_computeHypergraphEdgeCut(const LibGraph_Hypergraph* hg, /* [in] hypergraph */
                                      int nparts,                    /* [in] nb of partitions */
                                      const int* part);              /* [in] array of partition (of size hg->nvtxs) */

int LibGraph_computeGraphEdgeCut(const LibGraph_Graph* g, /* [in] graph */
                                 int nparts,              /* [in] nb of partitions */
                                 const int* part);        /* [in] array of partition (of size g->nvtxs) */

void LibGraph_computeGraphMinMaxDegree(const LibGraph_Graph* g, double* avgdegree, int* mindegree, int* maxdegree);

void LibGraph_computeGraphMinMaxEdgeWeight(const LibGraph_Graph* g, double* avgew, double* minew, double* maxew);

void LibGraph_computeGraphMinMaxVertexWeight(const LibGraph_Graph* g, double* avgvw, double* minvw, double* maxvw);

/** compute direct factorization fill-in */
int LibGraph_computeFillIn(const LibGraph_Graph* g, /**< [in] graph struct */
                           const int* perm,         /**< [in] permutation array (or NULL) */
                           LibGraph_Graph* gfill    /**< [inout] fill-in graph struct (or NULL)*/
);

//@}

#endif
